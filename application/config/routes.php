<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = 'error';


/*********** USER DEFINED ROUTES *******************/

$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';
$route['logout'] = 'user/logout';
$route['userListing'] = 'user/userListing';
$route['userListing/(:num)'] = "user/userListing/$1";
$route['addNew'] = "user/addNew";

$route['addNewUser'] = "user/addNewUser";
$route['editOld'] = "user/editOld";
$route['editOld/(:num)'] = "user/editOld/$1";
$route['editUser'] = "user/editUser";
$route['deleteUser'] = "user/deleteUser";
$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";
$route['checkUsernameExist'] = "user/checkUsernameExist";
$route['checkMobileExist'] = "user/checkMobileExist";
$route['usersrecords'] = "user/usersrecords";
$route['userView'] = "user/userView";

$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";
/*Through mobile API Start*/
$route['frgtpassword'] = "login/resetPasswordConfirmUserMob";
$route['frgtpassword/(:any)'] = "login/resetPasswordConfirmUserMob/$1";
$route['frgtpassword/(:any)/(:any)'] = "login/resetPasswordConfirmUserMob/$1/$2";
$route['createPasswordUserMobile'] = "login/createPasswordUserMobile";
$route['message'] = "login/message";
$route['forgotPasswordLinkexpired'] = "login/forgotlinkexpired";
/*Through mobile API Start*/
$route['verificationemail/(:any)/(:any)'] = "login/verificationemail/$1/$2";
$route['verificationemail'] = "login/verificationemail";
$route['verificationemailbyid/(:any)/(:any)'] = "login/verificationemailbyid/$1/$2";
$route['verificationemailbyid'] = "login/verificationemailbyid";
$route['resetLinkExpired'] = "login/linkexpired";
/*********** CONTENT DEFINED ROUTES *******************/
$route['contentListing'] = 'content/contentListing';
$route['contentListing/(:num)'] = "content/contentListing/$1";
$route['editOldContent'] = "content/editOldContent";
$route['editOldContent/(:num)'] = "content/editOldContent/$1";
$route['editContent'] = "content/editContent";
$route['addNewContentForm'] = "content/addNewContentForm";
$route['addNewContent'] = "content/addNewContent";
$route['deleteContent'] = "content/deleteContent";
$route['mainDeleteContentImage'] = "content/mainDeleteContentImage";

$route['subContentListing'] = 'content/subContentListing';
$route['subContentListing/(:num)'] = "content/subContentListing/$1";
$route['subEditOldContent'] = "content/subEditOldContent";
$route['subEditOldContent/(:num)'] = "content/subEditOldContent/$1";
$route['subEditContent'] = "content/subEditContent";
$route['subAddNewContentForm'] = "content/subAddNewContentForm";
$route['SubAddNewContent'] = "content/subAddNewContent";
$route['SubAeleteContent'] = "content/subDeleteContent";

/*********** BUSINESS DEFINED ROUTES *******************/
$route['businessListing'] = 'business/businessListing';
$route['businessListing/(:num)'] = "business/businessListing/$1";
$route['editOldBusiness'] = "business/editOldBusiness";
$route['editOldBusiness/(:num)'] = "business/editOldBusiness/$1";
$route['editBusiness'] = "business/editBusiness";
$route['addNewBusinessForm'] = "business/addNewBusinessForm";
$route['addNewBusiness'] = "business/addNewBusiness";
$route['deleteBusiness'] = "business/deleteBusiness";
$route['mainDeleteBusinessImage'] = "business/mainDeleteBusinessImage";
$route['getAlluserId'] = "business/getAlluserId";
$route['getAllcategories'] = "business/getAllcategories";
$route['getselectedcategories'] = "business/getselectedcategories";
$route['getprimarycategories'] = "business/getprimarycategories";

/*********** Category DEFINED ROUTES *******************/
$route['categoryListing'] = "category/categoryListing";
$route['categoryListing/(:num)'] = "category/categoryListing/$1";
$route['editOldCategory'] = "category/editOldCategory";
$route['editOldCategory/(:num)'] = "category/editOldCategory/$1";
$route['editCategory'] = "category/editCategory";
$route['addNewCategoryForm'] = "category/addNewCategoryForm";
$route['addNewCategory'] = "category/addNewCategory";
$route['deleteCategory'] = "category/deleteCategory";

/*********** NOTIFICATION DEFINED ROUTES *******************/
$route['notificationListing'] = "notification/notificationListing";
$route['notificationListing/(:num)'] = "notification/notificationListing/$1";
$route['notifyemail'] = "notification/notifyemail";
$route['notifysms'] = "notification/notifysms";
$route['sendBulkEmail'] = "notification/sendBulkEmail";

/*********** ANALYTICAL REPORT DEFINED ROUTES *******************/
$route['analyticalReport'] = "analytical/analyticalReport";
$route['generateReport'] = "analytical/generateReport";
$route['selectedChart'] = "analytical/selectedChart";
$route['viewReport'] = "analytical/viewReport";
$route['chartimagegenerate'] = "analytical/chartimagegenerate";
/*********** REVIEW DEFINED ROUTES *******************/
$route['reviewListing'] = "review/reviewListing";
$route['reviewListing/(:num)'] = "review/reviewListing/$1";
$route['businessReviewListing'] = "review/businessReviewListing";
$route['businessReviewListing/(:num)'] = "review/businessReviewListing/$1";
$route['businessView'] = "review/businessView";

/*********** PUSH NOTIFICATION DEFINED ROUTES *******************/
$route['pushNotification'] = "pushnotification/pushNotification";
$route['sendfcmnotification'] = "pushnotification/sendfcmnotification";


/*********** RANK PLAN DEFINED ROUTES *******************/
$route['plans'] = 'rank/rankListing';
$route['plans/(:num)'] = "rank/rankListing/$1";
$route['planrecords'] = "rank/rankplanrecords";
$route['planView'] = "rank/planView";

$route['famerank'] = "rank/fameRank";
$route['famerank/(:num)'] = "rank/fameRank/$1";
$route['hprecords'] = "rank/hprecords";
$route['deleteFameRankPlan'] = "rank/deleteFameRankPlan";
$route['addNewRankPlan'] = "rank/addNewRankPlan";
$route['saverankplan'] = "rank/saveRankPlan";
$route['editrankfameplan'] = "rank/editRankPlan";
$route['editrankfameplan/(:num)'] = "rank/editRankPlan/$1";
$route['updaterankfameplan'] = "rank/updateRankPlan";

$route['fameranklist'] = "rank/fameRankList";
$route['fameranklist/(:num)'] = "rank/fameRankList/$1";
$route['fmrecords'] = "rank/fmrecords";

$route['doublepoint'] = "rank/doublePoints";
$route['famerank/(:num)'] = "rank/fameRank/$1";
$route['dprecords'] = "rank/dprecords";
$route['deletedoublepointsPlan'] = "rank/deletedoublepointsPlan";
$route['addNewPointPlan'] = "rank/addNewPointPlan";
$route['savepointplan'] = "rank/savePointPlan";
$route['editdoublepointplan'] = "rank/editPointPlan";
$route['editdoublepointplan/(:num)'] = "rank/editPointPlan/$1";
$route['updatedoublepointplan'] = "rank/updateDoublePointPlan";

/*********** CRON DEFINED ROUTES *******************/
$route['cron'] = "cron/index";
$route['hiflyer'] = "hiflyer/index";
$route['boost'] = "boost/index";
$route['iospush'] = "senderios/index";
$route['testmailsend'] = "testsendemail/index";
/* End of file routes.php */
/* Location: ./application/config/routes.php */