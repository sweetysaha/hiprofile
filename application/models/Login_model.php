<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */
    function loginMe($email, $password)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.password, BaseTbl.name, BaseTbl.roleId, BaseTbl.super_admin_roleId, Roles.role');
        $this->db->from('hiprofile_users as BaseTbl');
        $this->db->join('hiprofile_roles as Roles','Roles.roleId = BaseTbl.roleId');
        $this->db->where('BaseTbl.email', $email);
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($password, $user[0]->password)){
				
				$this->db->select('login_time');
				$this->db->from('hiprofile_user_login_attempt_history');
				$this->db->where('userId', $user[0]->userId);
				$this->db->order_by('login_time','desc');
				$query = $this->db->get();
				$users = $query->result();
				//$this->session->set_userdata('lastlogintime', $users[0]->login_time);
				$this->session->set_userdata('lastlogintime', date('Y-m-d H:i:s'));
				
				
				$data['userId'] = $user[0]->userId;
                $data['login_time'] = date('Y-m-d H:i:s');
                $data['staus'] = 1;
				$this->db->insert('hiprofile_user_login_attempt_history', $data);
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /**
     * This function used to check email exists or not
     * @param {string} $email : This is users email id
     * @return {boolean} $result : TRUE/FALSE
     */
    function checkEmailExist($email)
    {
        $this->db->select('userId');
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('hiprofile_users');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
	
	/**
     * This function used to check email exists or not
     * @param {string} $email : This is admin email id
     * @return {boolean} $result : TRUE/FALSE
     */
    function checkEmailExistforadmin($email)
    {
        $this->db->select('userId');
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
		$this->db->where('roleId', 1);
        $query = $this->db->get('hiprofile_users');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }


    /**
     * This function used to insert reset password data
     * @param {array} $data : This is reset password data
     * @return {boolean} $result : TRUE/FALSE
     */
    function resetPasswordUser($data)
    {
        //$result = $this->db->insert('hiprofile_reset_password', $data);
		$this->db->select('*');
        $this->db->from('hiprofile_reset_password');
        $this->db->where('email', $data['email']);
        $query = $this->db->get();
		$numRows = $query->num_rows();
		
        if($numRows >= 1){
		    $result = $this->db->update('hiprofile_reset_password', $data);
		}else{
		    $result = $this->db->insert('hiprofile_reset_password', $data);
		}
        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This function is used to get customer information by email-id for forget password email
     * @param string $email : Email id of customer
     * @return object $result : Information of customer
     */
    function getCustomerInfoByEmail($email)
    {
        $this->db->select('userId, email, name');
        $this->db->from('hiprofile_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('email', $email);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * This function used to check correct activation deatails for forget password.
     * @param string $email : Email id of user
     * @param string $activation_id : This is activation string
     */
    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('hiprofile_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    // This function used to create new password by reset link
    function createPasswordUser($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $this->db->update('hiprofile_users', array('password'=>getHashedPassword($password)));
        $this->db->delete('hiprofile_reset_password', array('email'=>$email));
    }

	/*The change the update status*/
	 function updateEmailStatus($email, $activation_id)
    {
		
		$this->db->select('userId');
        $this->db->from('hiprofile_users');
        $this->db->where('email', $email);
		$this->db->where('email_verification_code', $activation_id);
		$this->db->where('email_status', 0);
		$this->db->where('isDeleted', 0);
        $query = $this->db->get();
        $rows = $query->num_rows();
		if($rows >=1 )
		{
			$this->db->set('email_verification_code',"");
			$this->db->set('email_status',1);
			$this->db->where('isDeleted', 0);
			$this->db->where('email', $email);
			$this->db->update('hiprofile_users');
			if ($this->db->affected_rows() >= 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
    }
	
	/*The change the update status*/
	function updateEmailStatusById($userid, $activation_id)
    {
		
		$this->db->select('userId');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $userid);
		$this->db->where('email_verification_code', $activation_id);
		$this->db->where('email_status', 0);
		$this->db->where('isDeleted', 0);
        $query = $this->db->get();
        $rows = $query->num_rows();
		if($rows >=1 )
		{
			$this->db->set('email_verification_code',"");
			$this->db->set('email_status',1);
			$this->db->where('isDeleted', 0);
			$this->db->where('userId', $userid);
			$this->db->update('hiprofile_users');
			if ($this->db->affected_rows() >= 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
    }
}

?>