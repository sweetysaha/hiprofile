<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model
{
    /**
     * This function is used to get the content listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function contentListingCount($searchText = '')
    {
        $this->db->select('contentId, contentTitle');
        $this->db->from('hiprofile_contents');
        if(!empty($searchText)) {
            $likeCriteria = "(contentTitle  LIKE '%".$searchText."%'
                            OR  contentDesc	  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('isDeleted', 0);
		$this->db->order_by('contentId', 'DESC');
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the content listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function contentListing($searchText = '', $page, $segment)
    {
        $this->db->select('contentId, contentTitle, contentDesc');
        $this->db->from('hiprofile_contents');
        if(!empty($searchText)) {
            $likeCriteria = "(contentTitle  LIKE '%".$searchText."%'
                            OR  contentDesc	 LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('isDeleted', 0);
		$this->db->order_by('contentId', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to add new content to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewContent($contentInfo)
    {

        $this->db->trans_start();
        $this->db->insert('hiprofile_contents', $contentInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get content information by id
     * @param number $contentId : This is content id
     * @return array $result : This is content information
     */
    function getContentInfo($contentId)
    {
        $this->db->select('contentId, contentTitle, contentDesc,imageurl');
        $this->db->from('hiprofile_contents');
        $this->db->where('isDeleted', 0);
        $this->db->where('contentId', $contentId);
		$this->db->order_by('contentId', 'DESC');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the content information
     * @param array $contentInfo : This is contents updated information
     * @param number $contentId : This is content id
     */
    function editContent($contentInfo, $contentId)
    {
        $this->db->where('contentId', $contentId);
        $this->db->update('hiprofile_contents', $contentInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the content information
     * @param number $contentId : This is content id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteContent($contentId, $contentInfo)
    {
        $this->db->where('contentId', $contentId);
        $this->db->update('hiprofile_contents', $contentInfo);
        
        return $this->db->affected_rows();
    }

	function checkImageExists($contentId)
	{
		$this->db->select('imageurl');
        $this->db->from('hiprofile_contents');
        $this->db->where('isDeleted', 0);
        $this->db->where('contentId', $contentId);
        $query = $this->db->get();
		$img = $query->result();
		if($img[0]->imageurl != ""):
			$imgval = $img[0]->imageurl;
		else:
			$imgval = 0;
		endif;
		return $imgval;
        
	}
	
	function deleteContentImageMain($contentId, $contentInfo)
    {
		
        $this->db->where('contentId', $contentId);
        $this->db->update('hiprofile_contents', $contentInfo);
        
        return $this->db->affected_rows();	
    }
}

  