<?php if(!defined('BASEPATH')) exit('No direct script access allowed');



class User_model extends CI_Model

{

    /**

     * This function is used to get the user listing count

     * @param string $searchText : This is optional search text

     * @return number $count : This is row count

     */

    function userListingCount($searchText = '')

    {

        /*$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, Role.role');

        $this->db->from('hiprofile_users as BaseTbl');

        $this->db->join('hiprofile_roles as Role', 'Role.roleId = BaseTbl.roleId','left');*/

		

		$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name,BaseTbl.username, BaseTbl.mobile, BaseTbl.status,social.rank,social.social_rank,social.points');

        $this->db->from('hiprofile_users as BaseTbl');

        $this->db->join('hiprofile_user_socialsync as social', 'social.userId = BaseTbl.userId','left');

        if(!empty($searchText)) {

            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'

                            OR  BaseTbl.name  LIKE '%".$searchText."%'

                            OR  BaseTbl.username  LIKE '%".$searchText."%'

                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";

            $this->db->where($likeCriteria);

        }

        $this->db->where('BaseTbl.isDeleted', 0);

        $this->db->where('BaseTbl.roleId !=', 1);

		$this->db->order_by('userId', 'DESC');

        $query = $this->db->get();

        

        return count($query->result());

    }

    

    /**

     * This function is used to get the user listing count

     * @param string $searchText : This is optional search text

     * @param number $page : This is pagination offset

     * @param number $segment : This is pagination limit

     * @return array $result : This is result

     */

    function userListing($searchText = '', $page, $segment)

    {

        /*$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, Role.role');

        $this->db->from('hiprofile_users as BaseTbl');

        $this->db->join('hiprofile_roles as Role', 'Role.roleId = BaseTbl.roleId','left');*/

		$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.username, BaseTbl.mobile, BaseTbl.status,social.rank,social.social_rank,social.points');

        $this->db->from('hiprofile_users as BaseTbl');

        $this->db->join('hiprofile_user_socialsync as social', 'social.userId = BaseTbl.userId','left');

        if(!empty($searchText)) {

            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'

                            OR  BaseTbl.name  LIKE '%".$searchText."%'

                            OR  BaseTbl.username  LIKE '%".$searchText."%'

                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";

            $this->db->where($likeCriteria);

        }

        $this->db->where('BaseTbl.isDeleted', 0);

        $this->db->where('BaseTbl.roleId !=', 1);

		$this->db->order_by('userId', 'DESC');

        $this->db->limit($page, $segment);

        $query = $this->db->get();

        

        $result = $query->result();        

        return $result;

    }

    

	function get_userListing()

    {

		$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name,BaseTbl.username, BaseTbl.mobile, BaseTbl.status,social.rank,social.social_rank,social.points');

        $this->db->from('hiprofile_users as BaseTbl');

        $this->db->join('hiprofile_user_socialsync as social', 'social.userId = BaseTbl.userId','left');

        $this->db->where('BaseTbl.isDeleted', 0);

        $this->db->where('BaseTbl.roleId !=', 1);

        $query = $this->db->get();

        $result = $query->result();        

		return $query;

    }

	

    /**

     * This function is used to get the user roles information

     * @return array $result : This is result of the query

     */

    function getUserRoles()

    {

        $this->db->select('roleId, role');

        $this->db->from('hiprofile_roles');

        $this->db->where('roleId !=', 1);

        $query = $this->db->get();

        

        return $query->result();

    }



    /**

     * This function is used to check whether email id is already exist or not

     * @param {string} $email : This is email id

     * @param {number} $userId : This is user id

     * @return {mixed} $result : This is searched result

     */

    function checkEmailExists($email, $userId = 0)

    {

        $this->db->select("email");

        $this->db->from("hiprofile_users");

        $this->db->where("email", $email);   

        $this->db->where("isDeleted", 0);

        if($userId != 0){

            $this->db->where("userId !=", $userId);

        }

        $query = $this->db->get();



        return $query->result();

    }

	

	function checkUsernameExist($username, $userId = 0)

    {

        $this->db->select("username");

        $this->db->from("hiprofile_users");

        $this->db->where("username", $username);   

        $this->db->where("isDeleted", 0);

        if($userId != 0){

            $this->db->where("userId !=", $userId);

        }

        $query = $this->db->get();



        return $query->result();

    }

	

	function checkMobileExist($phone, $userId = 0)

    {

        $this->db->select("mobile");

        $this->db->from("hiprofile_users");

        $this->db->where("mobile", $phone);   

        $this->db->where("isDeleted", 0);

        if($userId != 0){

            $this->db->where("userId !=", $userId);

        }

        $query = $this->db->get();



        return $query->result();

    }

    

    /**

     * This function is used to add new user to system

     * @return number $insert_id : This is last inserted id

     */

    function addNewUser($userInfo, $socialInfo)

    {

        $this->db->trans_start();

        $this->db->insert('hiprofile_users', $userInfo);

		$insert_id = $this->db->insert_id();

        if ($this->db->affected_rows() > 0)

		{

			$userid = array(

			'userId' => $insert_id

			);

			$this->db->insert('hiprofile_user_socialsync', $userid);

			if ($this->db->affected_rows() > 0)

			{

				$this->db->where('userId', $insert_id);

				$this->db->update('hiprofile_user_socialsync', $socialInfo);

			}

			else

			{

				$insert_id = 0;

			}

        }

		else

		{

			$insert_id = 0;

		}

		$this->db->trans_complete();

        return $insert_id;

    }

    

    /**

     * This function used to get user information by id

     * @param number $userId : This is user id

     * @return array $result : This is user information

     */

    function getUserInfo($userId)

    {		

		$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.status,BaseTbl.username,BaseTbl.gender,BaseTbl.dob,BaseTbl.roleId,social.facebook_profile_url,social.twitter_profile_url,social.instagram_profile_url,social.fb_friendcount,social.inst_friendcount,social.twt_friendcount');

        $this->db->from('hiprofile_users as BaseTbl');

        $this->db->join('hiprofile_user_socialsync as social', 'social.userId = BaseTbl.userId','left');

        $this->db->where('BaseTbl.isDeleted', 0);

		$this->db->where('BaseTbl.roleId !=', 1);

        $this->db->where('BaseTbl.userId', $userId);

		$this->db->order_by('BaseTbl.userId', 'DESC');

        $query = $this->db->get();

        

        return $query->result();

    }

	

	/**

     * This function used to get user information by id

     * @param number $userId : This is user id

     * @return array $result : This is user information

     */

    function getUserFrinedInfo($userId)

    {

        $this->db->select('friend_user_id');

        $this->db->from('hiprofile_friends');

        $this->db->where('userId', $userId);

		$this->db->where('request_status','1');

		$this->db->order_by('userId', 'DESC');

        $query = $this->db->get();

        

        return $query->num_rows();

    }

    

    

    /**

     * This function is used to update the user information

     * @param array $userInfo : This is users updated information

     * @param number $userId : This is user id

     */

    function editUser($userInfo, $socialInfo, $userId)

    {

        $this->db->where('userId', $userId);

        $this->db->update('hiprofile_users', $userInfo);

		if ($this->db->affected_rows() >= 0)

		{

			$this->db->where('userId', $userId);

			$this->db->update('hiprofile_user_socialsync', $socialInfo);

			return TRUE;

		}

		else

		{

			return FALSE;

		}        

    }

    

    

    

    /**

     * This function is used to delete the user information

     * @param number $userId : This is user id

     * @return boolean $result : TRUE / FALSE

     */

    function deleteUser($userId, $userInfo)

    {

        $this->db->where('userId', $userId);

        $this->db->update('hiprofile_users', $userInfo);

        

        return $this->db->affected_rows();

    }





    /**

     * This function is used to match users password for change password

     * @param number $userId : This is user id

     */

    function matchOldPassword($userId, $oldPassword)

    {

        $this->db->select('userId, password');

        $this->db->where('userId', $userId);        

        $this->db->where('isDeleted', 0);

        $query = $this->db->get('hiprofile_users');

        

        $user = $query->result();



        if(!empty($user)){

            if(verifyHashedPassword($oldPassword, $user[0]->password)){

                return $user;

            } else {

                return array();

            }

        } else {

            return array();

        }

    }

    

    /**

     * This function is used to change users password

     * @param number $userId : This is user id

     * @param array $userInfo : This is user updation info

     */

    function changePassword($userId, $userInfo)

    {

        $this->db->where('userId', $userId);

        $this->db->where('isDeleted', 0);

        $this->db->update('hiprofile_users', $userInfo);

        

        return $this->db->affected_rows();

    }

}



  