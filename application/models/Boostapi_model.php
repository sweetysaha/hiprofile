<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Boostapi_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('userapi_model');
    }

    //API call - To fetch all the rank plans
    public function fetchRankPlans($data) {
        unset($data['api_key']);

        //Check user active or not
        $checkUserId = $this->checkUserId($data['userID']);
        if ($checkUserId == 1) {
            $this->db->select('ranks_points_package_Google_playId as google_play_product_id,
			ranks_points_package_rank_up as ranks_added,
			ranks_points_package_hours as valid_hours,
			ranks_points_package_price as price');
            $this->db->from('hiprofile_rank_point_caetogry_packages_list');
            $this->db->where('ranks_points_package_category_code', 'hf');
            $this->db->where('isDeleted', 0);
            $query = $this->db->get();
            $higherfame = $query->result();
            if (!empty($higherfame)) {
                $loop_count = count($higherfame);
                for ($i = 0; $i < $loop_count; $i++) {
                    $higherfame[$i]->price = ($higherfame[$i]->price == "") ? "" : '$' . $higherfame[$i]->price;
                }
                $return_res = array();
                $return_res['responsecode'] = "200";
                $return_res['responsedetails'] = "Success";
                $return_res['data']['packages'] = $higherfame;
                return $return_res;
            } else {
                $return_res = array();
                $return_res['responsecode'] = "500";
                $return_res['status'] = "Something went wrong. Please try again later.";
                return $return_res;
            }
        } else {
            $return_res = array();
            $return_res['responsecode'] = "201";
            $return_res['responsedetails'] = "This user id doesn’t exists";
            return $return_res;
        }
    }

    //API call - To fetch all the DoublePoints plans
    public function fetchDoublePointsPlans($data) {
        unset($data['api_key']);

        //Check user active or not
        $checkUserId = $this->checkUserId($data['userID']);
        if ($checkUserId == 1) {
            $this->db->select('ranks_points_package_Google_playId as google_play_product_id,
			ranks_points_package_hours as valid_hours,
			ranks_points_package_price as price');
            $this->db->from('hiprofile_rank_point_caetogry_packages_list');
            $this->db->where('ranks_points_package_category_code', 'dp');
            $this->db->where('isDeleted', 0);
            $query = $this->db->get();
            $higherfame = $query->result();
            if (!empty($higherfame)) {
                $loop_count = count($higherfame);
                for ($i = 0; $i < $loop_count; $i++) {
                    $higherfame[$i]->price = ($higherfame[$i]->price == "") ? "" : '$' . $higherfame[$i]->price;
                }
                $return_res = array();
                $return_res['responsecode'] = "200";
                $return_res['responsedetails'] = "Success";
                $return_res['data']['packages'] = $higherfame;
                return $return_res;
            } else {
                $return_res = array();
                $return_res['responsecode'] = "500";
                $return_res['status'] = "Something went wrong. Please try again later.";
                return $return_res;
            }
        } else {
            $return_res = array();
            $return_res['responsecode'] = "201";
            $return_res['responsedetails'] = "This user id doesn’t exists";
            return $return_res;
        }
    }

    //API call - To save all the hp-higher fame and dp-double points plans
    public function saveHfDf($data) {
        unset($data['api_key']);

        //Check user active or not
        $checkUserId = $this->checkUserId($data['userID']);
        if ($checkUserId == 1) {
            $insert_data = array(
                'userId' => $data['userID'],
                'product_id' => $data['product_id'],
                'product_description' => $data['product_description'],
                'buying_timestamp' => $data['buying_timestamp'],
                'purchase_json' => $data['purchase_json'],
                'orderId' => $data['orderId'],
                'purchase_token' => $data['purchase_token'],
                'category' => strtolower($data['category']),
                'product_title' => $data['product_title'],
                'store' => $data['store'],
                'localised_price' => $data['localised_price']
            );
            $this->db->insert('hiprofile_rank_points_plan_details', $insert_data);
            if ($this->db->affected_rows() >= 0) {
                $return_res = array();
                $return_res['responsecode'] = "200";
                $return_res['responsedetails'] = "Success";
                return $return_res;
            } else {
                $return_res = array();
                $return_res['responsecode'] = "500";
                $return_res['status'] = "Something went wrong. Please try again later.";
                return $return_res;
            }
        } else {
            $return_res = array();
            $return_res['responsecode'] = "201";
            $return_res['responsedetails'] = "This user id doesn’t exists";
            return $return_res;
        }
    }

    //API call - To save all the hp-higher fame and dp-double points plans
    public function saveFrameRank($data) {
        unset($data['api_key']);

        //Check user active or not
        $checkUserId = $this->checkUserId($data['userID']);
        if ($checkUserId == 1) {
            
             
            $insert_data = array(
                'userId' => $data['userID'],
                'product_id' => $data['product_id'],
                'product_description' => $data['product_description'],
                'buying_timestamp' => $data['buying_timestamp'],
                'purchase_json' => $data['purchase_json'],
                'orderId' => $data['orderId'],
                'purchase_token' => $data['purchase_token'],
                'category' => strtolower($data['category']),
                'product_title' => $data['product_title'],
                'store' => $data['store'],
                'localised_price' => $data['localised_price']
            );
            $this->db->insert('hiprofile_rank_points_plan_details', $insert_data);
            if ($this->db->affected_rows() >= 0) {
                
                $this->db->select('BaseTbl.Id,BaseTbl.rank');
		$this->db->from('hiprofile_user_socialsync as BaseTbl');
		$this->db->where('BaseTbl.userId', $data['userID']);		
		$query = $this->db->get();
		$user_arr = $query->result();                
                if(!empty($user_arr)){                    
                    $id = $user_arr[0]->Id;
                    $rank = $user_arr[0]->rank;                    
                    $this->db->set('rank',$rank + 1);                        					
                    $this->db->where('Id', $id);
                    $this->db->update('hiprofile_user_socialsync');                     
                    
                }
                
                $this->db->select('BaseTbl.FramePurchaseCount');
		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->where('BaseTbl.userId', $data['userID']);
                $this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$query1 = $this->db->get();
		$user_arr1 = $query1->result();                
                if(!empty($user_arr1)){                    
                    
                    $updateCount = $user_arr1[0]->FramePurchaseCount + 1;
                                    
                    $this->db->set('FramePurchaseCount',$updateCount);                        					
                    $this->db->where('userID', $data['userID']);
                    $this->db->update('hiprofile_users'); 
                    
                    
                }
                
                $return_res = array();
                $return_res['responsecode'] = "200";
                $return_res['responsedetails'] = "Success";
                return $return_res;
                
                
                
            } else {
                $return_res = array();
                $return_res['responsecode'] = "500";
                $return_res['status'] = "Something went wrong. Please try again later.";
                return $return_res;
            }
        } else {
            $return_res = array();
            $return_res['responsecode'] = "201";
            $return_res['responsedetails'] = "This user id doesn’t exists";
            return $return_res;
        }
    }

    //API call - To get all the rank plans details of the user
    public function userRankPlans($data) {
        unset($data['api_key']);

        //Check user active or not
        $checkUserId = $this->checkUserId($data['userID']);
        if ($checkUserId == 1) {
            $this->db->select('
				basetbl.product_id as google_play_product_id,
				jointbl.ranks_points_package_rank_up as ranks_added,
				jointbl.ranks_points_package_hours as valid_hours,
				jointbl.ranks_points_package_price as price,			
				basetbl.buying_timestamp,
				basetbl.purchase_json,
				basetbl.product_description,
				basetbl.product_title,
				basetbl.orderId,
				basetbl.purchase_token,
				basetbl.is_activated,
				basetbl.activation_time,
				basetbl.store,
				basetbl.localised_price,				
				basetbl.ad_type
			');
            $this->db->from('hiprofile_rank_points_plan_details as basetbl');
            $this->db->join('hiprofile_rank_point_caetogry_packages_list as jointbl', 'jointbl.ranks_points_package_Google_playId = basetbl.product_id', 'left');
            $this->db->where('basetbl.category', 'hf');
            $this->db->where('basetbl.userId', $data['userID']);
            $this->db->where('basetbl.is_activated !=', 2);
            $query = $this->db->get();
            $rankplan = $query->result();
            if (!empty($rankplan)) {
                $rp_count = count($rankplan);
                for ($i = 0; $i < $rp_count; $i++) {
                    $rankplan[$i]->price = ($rankplan[$i]->price == "") ? "" : '$' . $rankplan[$i]->price;



                    /* if($rankplan[$i]->ad_type == '' ){
                      $rankplan[$i]->product_title	 = ($rankplan[$i]->product_title == "")?"":$rankplan[$i]->product_title;
                      }else{
                      $rankplan[$i]->product_title	 = $rankplan[$i]->ad_type == '0' ? 'Double Points for Your profile' : '5 Fame Rank Boost for Your Profile';
                      }

                      if($rankplan[$i]->ad_type == '' ){
                      $rankplan[$i]->product_description	 = ($rankplan[$i]->product_description == "")?"":$rankplan[$i]->product_description;
                      }else{
                      $rankplan[$i]->product_description	 = $rankplan[$i]->ad_type == '0' ? 'Double your Social Points to your HiProfile account.' : 'Get 5 Fame Ranks added to your HiProfile account.';
                      } */

                    if ($rankplan[$i]->ad_type == '0') {
                        $rankplan[$i]->product_title = 'Double Points for Your Profile';
                        $rankplan[$i]->product_description = 'Double your Social points to your HiProfile account.';
                    } elseif ($rankplan[$i]->ad_type == '1') {
                        $rankplan[$i]->product_title = '5 Fame Rank Boost for Your Profile';
                        $rankplan[$i]->product_description = 'Get 5 Fame Ranks added to your HiProfile account.';
                    } else {
                        $rankplan[$i]->product_title = ($rankplan[$i]->product_title == "") ? "" : $rankplan[$i]->product_title;
                        $rankplan[$i]->product_description = ($rankplan[$i]->product_description == "") ? "" : $rankplan[$i]->product_description;
                    }

                    $rankplan[$i]->ad_type = ($rankplan[$i]->ad_type != NUll) ? $rankplan[$i]->ad_type : "";
                    $rankplan[$i]->valid_hours = ($rankplan[$i]->valid_hours != NUll) ? $rankplan[$i]->valid_hours : "";

                    $rankplan[$i]->is_activated = ($rankplan[$i]->is_activated == 0) ? "false" : "true";
                    //date_default_timezone_set('Asia/Calcutta');
                    $rankplan[$i]->activation_time = ($rankplan[$i]->activation_time == "") ? "" : strval($rankplan[$i]->activation_time);
                }
                $return_res = array();
                $return_res['responsecode'] = "200";
                $return_res['responsedetails'] = "Success";
                $return_res['data']['packages'] = $rankplan;
                return $return_res;
            } else {
                $return_res = array();
                $return_res['responsecode'] = "202";
                $return_res['status'] = "User didn't bought a package yet.";
                return $return_res;
            }
        } else {
            $return_res = array();
            $return_res['responsecode'] = "201";
            $return_res['responsedetails'] = "This user id doesn’t exists";
            return $return_res;
        }
    }

    //API call - To get all the rank plans details of the user
    public function userPointsPlans($data) {
        unset($data['api_key']);

        //Check user active or not
        $checkUserId = $this->checkUserId($data['userID']);
        if ($checkUserId == 1) {
            $this->db->select('
				basetbl.product_id as google_play_product_id,
				jointbl.ranks_points_package_hours as valid_hours,
				jointbl.ranks_points_package_price as price,			
				basetbl.buying_timestamp,
				basetbl.purchase_json,
				basetbl.product_description,
				basetbl.product_title,
				basetbl.orderId,
				basetbl.purchase_token,
				basetbl.is_activated,
				basetbl.activation_time,
				basetbl.store,
				basetbl.localised_price,				
				basetbl.ad_type
			');
            $this->db->from('hiprofile_rank_points_plan_details as basetbl');
            $this->db->join('hiprofile_rank_point_caetogry_packages_list as jointbl', 'jointbl.ranks_points_package_Google_playId = basetbl.product_id', 'left');
            $this->db->where('basetbl.category', 'dp');
            $this->db->where('basetbl.userId', $data['userID']);
            $this->db->where('basetbl.is_activated !=', 2);
            $query = $this->db->get();
            $pointplan = $query->result();
            //echo $this->db->last_query();
            //die;
            if (!empty($pointplan)) {
                $dp_count = count($pointplan);
                for ($i = 0; $i < $dp_count; $i++) {
                    $pointplan[$i]->price = ($pointplan[$i]->price == "") ? "" : '$' . $pointplan[$i]->price;

                    if ($pointplan[$i]->ad_type == '0') {
                        $pointplan[$i]->product_title = 'Double Points for Your Profile';
                        $pointplan[$i]->product_description = 'Double your Social points to your HiProfile account.';
                    } elseif ($pointplan[$i]->ad_type == '1') {
                        $pointplan[$i]->product_title = '5 Fame Rank Boost for Your Profile';
                        $pointplan[$i]->product_description = 'Get 5 Fame Ranks added to your HiProfile account.';
                    } else {
                        $pointplan[$i]->product_title = ($pointplan[$i]->product_title == "") ? "" : $pointplan[$i]->product_title;
                        $pointplan[$i]->product_description = ($pointplan[$i]->product_description == "") ? "" : $pointplan[$i]->product_description;
                    }

                    $pointplan[$i]->ad_type = ($pointplan[$i]->ad_type != NUll) ? $pointplan[$i]->ad_type : "";
                    $pointplan[$i]->valid_hours = ($pointplan[$i]->valid_hours != NUll) ? $pointplan[$i]->valid_hours : "";

                    /* if($pointplan[$i]->ad_type == '' ){
                      $pointplan[$i]->product_title	 = ($pointplan[$i]->product_title == "")?"":$pointplan[$i]->product_title;
                      }else{
                      $pointplan[$i]->product_title	 = $pointplan[$i]->ad_type == '0' ? 'Double Points for Your Profile' : '5 Fame Rank Boost for Your Profile ';
                      }

                      if($pointplan[$i]->ad_type == '' ){
                      $pointplan[$i]->product_description	 = ($pointplan[$i]->product_description == "")?"":$pointplan[$i]->product_description;
                      }else{
                      $pointplan[$i]->product_description	 = $pointplan[$i]->ad_type == '0' ? 'Double your Social points to your HiProfile account.' : 'Get 5 Fame Ranks added to your HiProfile account.';
                      } */
                    //$pointplan[$i]->ad_type = ($pointplan[$i]->ad_type) ? $pointplan[$i]->ad_type : "";


                    $pointplan[$i]->is_activated = ($pointplan[$i]->is_activated == 0) ? "false" : "true";
                    //date_default_timezone_set('Asia/Calcutta');
                    $pointplan[$i]->activation_time = ($pointplan[$i]->activation_time == "") ? "" : strval($pointplan[$i]->activation_time);
                }
                $return_res = array();
                $return_res['responsecode'] = "200";
                $return_res['responsedetails'] = "Success";
                $return_res['data']['packages'] = $pointplan;
                return $return_res;
            } else {
                $return_res = array();
                $return_res['responsecode'] = "202";
                $return_res['status'] = "User didn't bought a package yet.";
                return $return_res;
            }
        } else {
            $return_res = array();
            $return_res['responsecode'] = "201";
            $return_res['responsedetails'] = "This user id doesn’t exists";
            return $return_res;
        }
    }

    //API call - To Activate rank and points plans details for the user
    public function toActivatePlans($data) {
        unset($data['api_key']);

        //Check user active or not
        $checkUserId = $this->checkUserId($data['userID']);
        if ($checkUserId == 1) {
            $packagestatus = $this->toCheckPackageActive($data['userID'], $data['category']);
            if ($packagestatus >= 1) {
                $return_res = array();
                $return_res['responsecode'] = "202";
                $return_res['responsedetails'] = "This user id already has an active Rank Upgrade / Double Point boost";
                return $return_res;
            } else {

                //Catgeory of the highfam boost
                if ($data['category'] == 'hf') {
                    /* if($data['is_free_pcakage'] == '1'){
                      $packagedetail = $obj = (object) array('rank' => '5','hours'=>'1h');
                      }else{
                      $packagedetail = $this->toGetPackageDetails($data['product_id'],$data['category']);
                      }
                     */
                    $packagedetail = $this->toGetPackageDetails($data['product_id'], $data['category']);
                    if (!empty($packagedetail)) {
                        $rank = $packagedetail->rank;
                        $time = strtolower($packagedetail->hours);
                        $currentrank = $this->getuserrank($data['userID']);
                        $totalrank = $rank + $currentrank;
                        //To udpate the rank
                        $update_data = array('rank' => $totalrank);
                        $this->db->where('userId', $data['userID']);
                        $this->db->update('hiprofile_user_socialsync', $update_data);
                        if ($this->db->affected_rows() >= 0) {
                            if (stripos(strtolower($time), 'm') == true) {
                                $time = str_replace("m", "", $time);
                                //to caluclate the activation time
                                $starttime = $this->get_current_datime();
                                $activatetime = date("Y-m-d H:i", strtotime($starttime));
                                //calculate expire date and time
                                $block_date = "+" . $time . "minute";
                                $expiretime = date('Y-m-d H:i', strtotime($block_date, strtotime($starttime)));
                            } elseif (stripos(strtolower($time), 'h') == true) {
                                $time = str_replace("h", "", $time);
                                //to caluclate the activation time
                                $starttime = $this->get_current_datime();
                                $activatetime = date("Y-m-d H:i", strtotime($starttime));
                                //calculate expire date and time
                                $block_date = "+" . $time . "hour";
                                $expiretime = date('Y-m-d H:i', strtotime($block_date, strtotime($starttime)));
                            }
                            //To update the date time is_activated
                            $update_datas = array(
                                'activation_time' => $activatetime,
                                'expiration_time' => $expiretime,
                                'is_activated' => '1'
                            );

                            $this->db->where('userId', $data['userID']);
                            $this->db->where('product_id', $data['product_id']);
                            $this->db->where('category', $data['category']);
                            $this->db->where('orderId', $data['orderId']);


                            $this->db->update('hiprofile_rank_points_plan_details', $update_datas);
                            $milliseconds = $this->millisecsBetween($activatetime, $expiretime);
                            $return_res = array();
                            $return_res['responsecode'] = "200";
                            $return_res['responsedetails'] = "success";
                            $return_res['expiration_time'] = strval($milliseconds);
                            return $return_res;
                        } else {
                            $return_res = array();
                            $return_res['responsecode'] = "500";
                            $return_res['status'] = "Something went wrong. Please try again later.";
                            return $return_res;
                        }
                    } else {
                        $return_res = array();
                        $return_res['responsecode'] = "202";
                        $return_res['status'] = "User didn't bought a package yet.";
                        return $return_res;
                    }
                }
                //category of the doublepoints boost
                elseif ($data['category'] == 'dp') {

                    //echo $data['is_free_pcakage'];die;

                    if ($data['is_free_pcakage'] == '1') {

                        $this->db->select('*');
                        $this->db->from('hiprofile_free_package_list');
                        $this->db->where('userId', $data['userID']);
                        $query = $this->db->get();
                        $result = $query->result_array();
                        if (count($result) > 0) {
                            $return_res = array();
                            $return_res['responsecode'] = "500";
                            $return_res['status'] = "You have already applied free package.";
                            return $return_res;
                        } else {
                            $insert_data = array(
                                'userId' => $data['userID'],
                                'orderId' => $data['orderId']
                            );

                            $this->db->insert('hiprofile_free_package_list', $insert_data);
                            /* echo $this->db->last_query();
                              die; */

                            if ($this->db->affected_rows() >= 0) {
                                $packagedetail = $obj = (object) array('hours' => '30m');
                                $time = strtolower($packagedetail->hours);
                                if (stripos(strtolower($time), 'm') == true) {
                                    $time = str_replace("m", "", $time);
                                    //to caluclate the activation time
                                    $starttime = $this->get_current_datime();
                                    $activatetime = date("Y-m-d H:i", strtotime($starttime));
                                    //calculate expire date and time
                                    $block_date = "+" . $time . "minute";
                                    $expiretime = date('Y-m-d H:i', strtotime($block_date, strtotime($starttime)));
                                }
                                //To update the date time is_activated
                                $update_datas = array(
                                    'activation_time' => $activatetime,
                                    'expiration_time' => $expiretime,
                                    'is_activated' => '1'
                                );

                                $this->db->where('userId', $data['userID']);
                                $this->db->where('category', $data['category']);
                                $this->db->where('orderId', $data['orderId']);

                                $this->db->update('hiprofile_rank_points_plan_details', $update_datas);
                                /* echo $this->db->last_query();
                                  die; */
                                $return_res = array();
                                $return_res['responsecode'] = "200";
                                $return_res['responsedetails'] = "success";
                                $return_res['expiration_time'] = "0.0";
                                return $return_res;
                            } else {
                                $return_res = array();
                                $return_res['responsecode'] = "500";
                                $return_res['status'] = "Something went wrong. Please try again later.";
                                return $return_res;
                            }
                        }
                    } else {

                        //echo "double";die;

                        $packagedetail = $this->toGetPackageDetails($data['product_id'], $data['category']);

                        if (!empty($packagedetail)) {
                            $time = strtolower($packagedetail->hours);
                            $currentpoints = $this->getuserpoint($data['userID']);

                            $this->db->select('Id,rank,points,social_rank');
                            $this->db->from('hiprofile_user_socialsync');
                            $this->db->where('userId', $data['userID']);
                            $query = $this->db->get();
                            $result = $query->result_array();

                            $points = 0;
                            $rank = 0;
                            if (count($result) > 0) {
                                $rank = $result[0]['rank'];
                                $points = $result[0]['points'];
                            }
                            //To udpate the rank
                            $totalpoints = $points * 2;

                            if ($totalpoints >= 1000) {
                                $new_rank = $rank + 1;
                                $new_points = $totalpoints - 1000;
                                $update_data = array('points' => $new_points, 'rank' => $new_rank);
                            } else {
                                $update_data = array('points' => $totalpoints);
                            }


                            $this->db->where('userId', $data['userID']);
                            $this->db->update('hiprofile_user_socialsync', $update_data);
                            if ($this->db->affected_rows() >= 0) {
                                if (stripos(strtolower($time), 'm') == true) {
                                    $time = str_replace("m", "", $time);
                                    //to caluclate the activation time
                                    $starttime = $this->get_current_datime();
                                    $activatetime = date("Y-m-d H:i", strtotime($starttime));
                                    //calculate expire date and time
                                    $block_date = "+" . $time . "minute";
                                    $expiretime = date('Y-m-d H:i', strtotime($block_date, strtotime($starttime)));
                                } elseif (stripos(strtolower($time), 'h') == true) {
                                    $time = str_replace("h", "", $time);
                                    //to caluclate the activation time
                                    $starttime = $this->get_current_datime();
                                    $activatetime = date("Y-m-d H:i", strtotime($starttime));
                                    //calculate expire date and time
                                    $block_date = "+" . $time . "hour";
                                    $expiretime = date('Y-m-d H:i', strtotime($block_date, strtotime($starttime)));
                                }
                                //To update the date time is_activated
                                $update_datas = array(
                                    'activation_time' => $activatetime,
                                    'expiration_time' => $expiretime,
                                    'is_activated' => '1'
                                );

                                $this->db->where('product_id', $data['product_id']);
                                $this->db->where('userId', $data['userID']);
                                $this->db->where('category', $data['category']);
                                $this->db->where('orderId', $data['orderId']);

                                $this->db->update('hiprofile_rank_points_plan_details', $update_datas);

                                $milliseconds = $this->millisecsBetween($activatetime, $expiretime);
                                $return_res = array();
                                $return_res['responsecode'] = "200";
                                $return_res['responsedetails'] = "success";
                                //$return_res['expiration_time'] = strval($milliseconds * 1000);
                                $return_res['expiration_time'] = strval($milliseconds);
                                return $return_res;
                            } else {
                                $return_res = array();
                                $return_res['responsecode'] = "500";
                                $return_res['status'] = "Something went wrong. Please try again later.";
                                return $return_res;
                            }
                        } else {
                            $return_res = array();
                            $return_res['responsecode'] = "500";
                            $return_res['status'] = "Something went wrong. Please try again later.";
                            return $return_res;
                        }
                    }
                }
            }
        } else {
            $return_res = array();
            $return_res['responsecode'] = "201";
            $return_res['responsedetails'] = "This user id doesn’t exists";
            return $return_res;
        }
        exit;
    }

    //Function to check if the userId is valid or not
    function checkUserId($userID) {
        $this->db->select('userId');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $userID);
        $this->db->where('isDeleted', 0);
        $this->db->where('status', 0);
        $this->db->where('roleId', 0);
        $query = $this->db->get();
        $numRows = $query->num_rows();
        return $numRows;
    }

    //Function to check if the user has already activated an boost in the same cateogry
    function toCheckPackageActive($userID, $category) {
        $this->db->select('is_activated');
        $this->db->from('hiprofile_rank_points_plan_details');
        $this->db->where('userId', $userID);
        $this->db->where('category', $category);
        $this->db->where('is_activated', 1);
        $query = $this->db->get();
        $numRows = $query->num_rows();
        return $numRows;
    }

    //Function get the rank,hours or points using the category
    function toGetPackageDetails($product_id, $category) {

        if ($category == 'hf') {
            $this->db->select('
			ranks_points_package_rank_up as rank,
			ranks_points_package_hours as hours
			');
            $this->db->from('hiprofile_rank_point_caetogry_packages_list');
            $this->db->where('ranks_points_package_Google_playId', $product_id);
            $this->db->where('ranks_points_package_category_code', $category);
            $this->db->where('IsDeleted', 0);
            $query = $this->db->get();
            $rankpackageplan = $query->result();
            return $rankpackageplan[0];
        } elseif ($category == 'dp') {
            $this->db->select('
			ranks_points_package_hours as hours
			');
            $this->db->from('hiprofile_rank_point_caetogry_packages_list');
            $this->db->where('ranks_points_package_Google_playId', $product_id);
            $this->db->where('ranks_points_package_category_code', $category);
            $this->db->where('IsDeleted', 0);
            $query = $this->db->get();
            $pointpackageplan = $query->result();
            return $pointpackageplan[0];
        } else {
            $return_res = array();
            $return_res['responsecode'] = "500";
            $return_res['status'] = "Something went wrong. Please try again later.";
            return $return_res;
        }
    }

    //function to get the user current rank
    function getuserrank($userID) {
        $this->db->select('rank');
        $this->db->from('hiprofile_user_socialsync');
        $this->db->where('userId', $userID);
        $query = $this->db->get();
        $currentrank = $query->result();
        return $currentrank[0]->rank;
    }

    //function to get the user current points
    function getuserpoint($userID) {
        $this->db->select('points');
        $this->db->from('hiprofile_user_socialsync');
        $this->db->where('userId', $userID);
        $query = $this->db->get();
        $currentpoint = $query->result();
        return $currentpoint[0]->points;
    }

    /* To fetch the curren time from the database */

    function get_current_datime() {
        $this->db->select('Now() as ctime');
        $query = $this->db->get();
        $time = $query->result();
        return $time[0]->ctime;
    }

    //fucntion to calculate milliseconds from two dates
    function millisecsBetween($dateOne, $dateTwo, $abs = true) {
        $func = $abs ? 'abs' : 'intval';
        return $func(strtotime($dateOne) - strtotime($dateTwo));
    }

}
