<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Business_model extends CI_Model
{
    /**
     * This function is used to get the business listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function businessListingCount($searchText = '')
    {
		$this->db->select('user.name,BaseTbl.business_id,BaseTbl.image_url,BaseTbl.business_title,BaseTbl.address,BaseTbl.country,BaseTbl.state,BaseTbl.city,BaseTbl.pin,BaseTbl.contact,BaseTbl.email ,BaseTbl.day_open_from,BaseTbl.day_open_to,BaseTbl.hours_open_from,BaseTbl.hours_open_to,BaseTbl.latitude,BaseTbl.longitude,BaseTbl.approved_status,cat.business_category_names as primary_category');
        $this->db->from('hiprofile_users as user');
        $this->db->join('hiprofile_business as BaseTbl', 'BaseTbl.userId = user.userId','left');
		$this->db->join('hiprofile_business_categories_lists as cat', 'BaseTbl.primary_category = cat.business_categoryId','left');
				
        //$this->db->select('*');
        //$this->db->from('hiprofile_business');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.business_title  LIKE '%".$searchText."%'
                            OR  BaseTbl.address	  LIKE '%".$searchText."%'
							 OR  BaseTbl.country	  LIKE '%".$searchText."%'
							  OR  BaseTbl.state	  LIKE '%".$searchText."%'
							   OR  BaseTbl.city	  LIKE '%".$searchText."%'
							    OR  BaseTbl.pin	  LIKE '%".$searchText."%'
								 OR  BaseTbl.contact	  LIKE '%".$searchText."%'
								  OR  BaseTbl.email	  LIKE '%".$searchText."%'
								   OR  BaseTbl.day_open_from	  LIKE '%".$searchText."%'
								    OR  BaseTbl.day_open_to	  LIKE '%".$searchText."%'
									 OR  BaseTbl.hours_open_from	  LIKE '%".$searchText."%'
									 OR  cat.business_category_names	  LIKE '%".$searchText."%'
									  OR  BaseTbl.hours_open_to	  LIKE '%".$searchText."%'
									   OR  BaseTbl.latitude	  LIKE '%".$searchText."%'
									    OR  BaseTbl.longitude	  LIKE '%".$searchText."%'
										OR  user.name	  LIKE '%".$searchText."%'
										 OR  BaseTbl.approved_status	  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('cat.cat_status', 0);
		$this->db->where('cat.isDeleted', 0);
		$this->db->order_by('BaseTbl.business_id', 'DESC');
		
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the content listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function businessListing($searchText = '', $page, $segment)
    {
		$this->db->select('user.name,BaseTbl.business_id,BaseTbl.image_url,BaseTbl.business_title,BaseTbl.address,BaseTbl.country,BaseTbl.state,BaseTbl.city,BaseTbl.pin,BaseTbl.contact,BaseTbl.email ,BaseTbl.day_open_from,BaseTbl.day_open_to,BaseTbl.hours_open_from,BaseTbl.hours_open_to,BaseTbl.latitude,BaseTbl.longitude,BaseTbl.approved_status,cat.business_category_names as primary_category');
        $this->db->from('hiprofile_users as user');
        $this->db->join('hiprofile_business as BaseTbl', 'BaseTbl.userId = user.userId','left');
		$this->db->join('hiprofile_business_categories_lists as cat', 'BaseTbl.primary_category = cat.business_categoryId','left');
				
        //$this->db->select('*');
        //$this->db->from('hiprofile_business');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.business_title  LIKE '%".$searchText."%'
                            OR  BaseTbl.address	  LIKE '%".$searchText."%'
							 OR  BaseTbl.country	  LIKE '%".$searchText."%'
							  OR  BaseTbl.state	  LIKE '%".$searchText."%'
							   OR  BaseTbl.city	  LIKE '%".$searchText."%'
							    OR  BaseTbl.pin	  LIKE '%".$searchText."%'
								 OR  BaseTbl.contact	  LIKE '%".$searchText."%'
								  OR  BaseTbl.email	  LIKE '%".$searchText."%'
								   OR  BaseTbl.day_open_from	  LIKE '%".$searchText."%'
								    OR  BaseTbl.day_open_to	  LIKE '%".$searchText."%'
									 OR  BaseTbl.hours_open_from	  LIKE '%".$searchText."%'
									 OR  cat.business_category_names	  LIKE '%".$searchText."%'
									  OR  BaseTbl.hours_open_to	  LIKE '%".$searchText."%'
									   OR  BaseTbl.latitude	  LIKE '%".$searchText."%'
									    OR  BaseTbl.longitude	  LIKE '%".$searchText."%'
										OR  user.name	  LIKE '%".$searchText."%'
										 OR  BaseTbl.approved_status	  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
		$this->db->order_by('BaseTbl.business_id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to add new content to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewBusiness($businessInfo)
    {
        $this->db->trans_start();
        $this->db->insert('hiprofile_business', $businessInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get content information by id
     * @param number $contentId : This is content id
     * @return array $result : This is content information
     */
    function getBusinessInfo($businessId)
    {
        $this->db->select('*');
        $this->db->from('hiprofile_business');
        $this->db->where('isDeleted', 0);
        $this->db->where('business_id', $businessId);
		$this->db->order_by('business_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    /**
     * This function is used to update the content information
     * @param array $contentInfo : This is contents updated information
     * @param number $contentId : This is content id
     */
    function editBusiness($businessInfo, $businessId, $status)
    {
		if($status == 1):
			$this->db->set('is_paid', 0);
			$this->db->set('approved_status',0);
		elseif($status == 2):
			$this->db->set('approved_status', 0);
			$this->db->set('is_paid', 1);
		elseif($status == 3):
			$this->db->set('approved_status',1);
		endif;
        $this->db->where('business_id', $businessId);
        $this->db->update('hiprofile_business', $businessInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the content information
     * @param number $contentId : This is content id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteBusiness($businessId, $businessInfo)
    {
        $this->db->where('business_id', $businessId);
        $this->db->update('hiprofile_business', $businessInfo);
        
        return $this->db->affected_rows();
    }

	function checkImageExists($businessId)
	{
		$this->db->select('image_url');
        $this->db->from('hiprofile_business');
        $this->db->where('isDeleted', 0);
        $this->db->where('business_id', $businessId);
        $query = $this->db->get();
		$img = $query->result();
		if($img[0]->image_url != ""):
			$imgval = $img[0]->image_url;
		else:
			$imgval = "";
		endif;
		return $imgval;
	}
	
	function deleteBusinessImageMain($businessId, $businessInfo)
    {
		
        $this->db->where('business_id', $businessId);
        $this->db->update('hiprofile_business', $businessInfo);
        
        return $this->db->affected_rows();	
    }
	
	function getAllUserIds()
	{
		$this->db->select('userId,email,name');
        $this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('RoleId', 0);
		$this->db->order_by("userId","desc");
		$this->db->from(' hiprofile_users');
        $query = $this->db->get();
		$result = $query->result_array(); 
		$option = '';
		echo '<select name="userid" id="userid" class="form-control" required>';
		echo '<option value="">Select an user</option>';
		foreach($result as $row)
		{
		  echo '<option value="'.$row['userId'].'">'.$row['email'].' - '.$row['name'].'</option>';
		}	
		echo '</select>';
		exit;
	}
	
	function getAllcategories()
	{
		$this->db->select('business_categoryId as category_id,business_category_names as category');
		$this->db->where('cat_status', 0);
		$this->db->where('isDeleted', 0);
		$this->db->order_by("business_categoryId","desc");
		$this->db->from('hiprofile_business_categories_lists');
        $query = $this->db->get();
		$result = $query->result_array(); 
		$option = '';
		echo '<select name="businesscategoryicon[]" id="businesscategoryicon" class="form-control" required multiple onBlur="getMultipleSelectedValue()">';
		foreach($result as $row)
		{
		  echo '<option value="'.$row['category_id'].'">'.$row['category'].'</option>';
		}	
		echo '</select>';
	}
	
	function getselectedcategories($businessId)
	{
		$this->db->select('categories');
		$this->db->from('hiprofile_business');
        $this->db->where('isDeleted', 0);
        $this->db->where('business_id', $businessId);
		$query = $this->db->get();
		$result = $query->result_array(); 		
		echo $result[0]['categories'];
		exit;
	}
	
	function getprimarycategories($businessId)
	{
		$this->db->select('primary_category');
		$this->db->from('hiprofile_business');
        $this->db->where('isDeleted', 0);
        $this->db->where('business_id', $businessId);
		$query = $this->db->get();
		$result = $query->result(); 		
		$this->db->select('business_categoryId as category_id,business_category_names as category');
		$this->db->where('business_categoryId', $result[0]->primary_category);
		$this->db->where('cat_status', 0);
		$this->db->where('isDeleted', 0);
		$this->db->order_by("business_categoryId","desc");
		$this->db->from('hiprofile_business_categories_lists');
        $query = $this->db->get();
		$result = $query->result_array(); 
		$option = '';
		echo '<select name="businesscategoryname" id="businesscategoryname" class="form-control" required>';
		foreach($result as $row)
		{
		  echo '<option value="'.$row['category_id'].'">'.$row['category'].'</option>';
		}	
		echo '</select>';
		exit;
	}
}

  