<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Review_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function reviewListingCount($searchText = '')
    {		
		$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.feedback, BaseTbl.rating');
        $this->db->from('hiprofile_users as BaseTbl');
		//$this->db->join('hiprofile_business as business', 'business.userId = BaseTbl.userId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
							OR  BaseTbl.rating  LIKE '%".$searchText."%'
                            OR  BaseTbl.feedback  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
		$this->db->order_by('BaseTbl.rating', 'DESC');
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function reviewListing($searchText = '', $page, $segment)
    {
              
		$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.feedback, BaseTbl.rating');
        $this->db->from('hiprofile_users as BaseTbl');
		//$this->db->join('hiprofile_business as business', 'business.userId = BaseTbl.userId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.rating  LIKE '%".$searchText."%'
                            OR  BaseTbl.feedback  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
		$this->db->order_by('BaseTbl.rating', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
	
	function getBusinessReviewListingCount($searchText = '')
    {		
		$this->db->select('business_id,business_title,email,business_rating,contact');
        $this->db->from('hiprofile_business'); 
		//$this->db->join('hiprofile_business as business', 'business.userId = BaseTbl.userId','left');
        if(!empty($searchText)) {
         $likeCriteria = "(business_title  LIKE '%".$searchText."%'
                            OR  email  LIKE '%".$searchText."%'
							OR  contact  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('isDeleted', 0);	
		$this->db->order_by('business_rating', 'DESC');
        $query = $this->db->get();
        
        return count($query->result());
    }
	
	function getBusinessReviewListing($searchText = '', $page, $segment)
    {
              
		$this->db->select('business_id,business_title,email,business_rating,contact');
        $this->db->from('hiprofile_business');  
        if(!empty($searchText)) {
            $likeCriteria = "(business_title  LIKE '%".$searchText."%'
                            OR  email  LIKE '%".$searchText."%'
							OR  contact  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('isDeleted', 0);
		$this->db->order_by('business_rating', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
	
	
	
	
    
}