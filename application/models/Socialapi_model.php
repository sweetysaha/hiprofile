<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Socialapi_model extends CI_Model
{
	//API call - To send friend request
	/*
		User A(user id 20) sends Social Media request to User B (user id 22)
		User A will only receive the API response and no push
		User B will Receive the following in push
		{
		  "friend_user_id":"20",
		  "notification_type":"social_media_request",
		  "friend_pic_url":"https://hjbchjvb/skjnjkfs.png",
		  "friend_name":"User A",
		  "friend_gender":"undisclosed",
		  "social_media":"facebook"
		}
	*/
    public function sendFriendRequest($data)
	{
		unset($data['api_key']);
		//To check friend user has social media link or not
		$socialmedia = strtolower($data['social_media']);
		$this->db->select('userId');
        $this->db->from('hiprofile_user_socialsync');
		/* if($socialmedia == 'facebook'):
			$this->db->where('auth_provider_fb', $socialmedia);
		endif;
		if($socialmedia == 'twitter'):
			$this->db->where('auth_provider_twt', $socialmedia);
		endif;
		if($socialmedia == 'instagram'):
			$this->db->where('auth_provider_inst', $socialmedia);
		endif; */
		$this->db->where('userId', $data['friend_user_id']);
        $query = $this->db->get();
		$numRows = $query->num_rows();
		if($numRows >= 1)
		{
			// To check if the user has already made a request to this or not
			$this->db->select('request_status');
			$this->db->from('hiprofile_social_friends');
			$this->db->where('userId', $data['userID']);
			$this->db->where('friend_user_id', $data['friend_user_id']);
			if($socialmedia == 'facebook'):
			$this->db->where('facebook_status', 1);
			endif;
			if($socialmedia == 'twitter'):
				$this->db->where('twitter_status', 1);
			endif;
			if($socialmedia == 'instagram'):
				$this->db->where('instagram_status', 1);
			endif;
			if($socialmedia == 'contact'):
				$this->db->where('contact_status', 1);
			endif;
			$query = $this->db->get();
			$result = $query->row_array();
			if(isset($result['request_status']) && $result['request_status'] != '' && $result['request_status'] == '4')
			{
				$return_res = array();
				$return_res['responsecode'] 	= "201";
				$return_res['responsedetails'] 	= "Social Media Request Already Sent";
				return $return_res;
			}
			elseif(isset($result['request_status']) && $result['request_status'] != '' && $result['request_status'] == '1')
			{
				$return_res = array();
				$return_res['responsecode'] 	= "201";
				$return_res['responsedetails'] 	= "Social Media Request Already Accepted";
				return $return_res;
			}
			else
			{
				$this->db->select('request_status');
				$this->db->from('hiprofile_social_friends');
				if($socialmedia == 'facebook'):
				$this->db->where('facebook_status', 0);
				endif;
				if($socialmedia == 'twitter'):
					$this->db->where('twitter_status', 0);
				endif;
				if($socialmedia == 'instagram'):
					$this->db->where('instagram_status', 0);
				endif;
				if($socialmedia == 'contact'):
				$this->db->where('contact_status', 0);
				endif;
				$this->db->where('userId', $data['userID']);
				$this->db->where('friend_user_id', $data['friend_user_id']);
				$query = $this->db->get();
				$result = $query->row_array();
				if(isset($result['request_status']) && $result['request_status'] != '' && $result['request_status'] == '3')
				{
					$this->db->set('userId',$data['userID']);
					$this->db->set('friend_user_id',$data['friend_user_id']);
					$this->db->set('request_status', '4');
					if($socialmedia == 'facebook'):
						$this->db->set('facebook', 1);
						$this->db->set('facebook_status', 1);
					endif;
					if($socialmedia == 'twitter'):
						$this->db->set('twitter', 1);
						$this->db->set('twitter_status', 1);
					endif;
					if($socialmedia == 'instagram'):
						$this->db->set('instagram', 1);
						$this->db->set('instagram_status', 1);
					endif;
					if($socialmedia == 'contact'):
						$this->db->set('contact', 1);
						$this->db->set('contact_status', 1);
					endif;
					$this->db->update('hiprofile_social_friends');
				}
				else
				{
					$this->db->set('userId',$data['userID']);
					$this->db->set('friend_user_id',$data['friend_user_id']);
					$this->db->set('request_status', '4');
					if($socialmedia == 'facebook'):
						$this->db->set('facebook', 1);
						$this->db->set('facebook_status', 1);
					endif;
					if($socialmedia == 'twitter'):
						$this->db->set('twitter', 1);
						$this->db->set('twitter_status', 1);
					endif;
					if($socialmedia == 'instagram'):
						$this->db->set('instagram', 1);
						$this->db->set('instagram_status', 1);
					endif;
					if($socialmedia == 'contact'):
						$this->db->set('contact', 1);
						$this->db->set('contact_status', 1);
					endif;
					$this->db->insert('hiprofile_social_friends');
				}
				if ($this->db->affected_rows() >= 0)
				{
					//To select the profileimage,name,gender of the friend to insert in notification table
					$this->db->select('social.profileimage as friend_pic_url,BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.gender_disclose as friend_gender_disclose');
					$this->db->from('hiprofile_users as BaseTbl');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
					$this->db->where('BaseTbl.userId', $data['userID']);
					$query = $this->db->get();
					$result = $query->row_array();
					$this->db->set('userId',$data['userID']);
					$this->db->set('friend_pic_url',$result['friend_pic_url']);
					$this->db->set('friend_name',$result['friend_name']);
					$this->db->set('friend_user_id',$data['friend_user_id']);
					// To check which social media request is been made
					if($socialmedia == 'facebook'):
						$this->db->set('social_media', $socialmedia);
					endif;
					if($socialmedia == 'twitter'):
						$this->db->set('social_media', $socialmedia);
					endif;
					if($socialmedia == 'instagram'):
						$this->db->set('social_media', $socialmedia);
					endif;
					if($socialmedia == 'contact'):
						$this->db->set('social_media', $socialmedia);
					endif;
					// To check gender disclosed or not
					if($result['friend_gender_disclose'] == 0):
						$this->db->set('friend_gender', 'undisclosed');
					else:
						$this->db->set('friend_gender', strtolower($result['friend_gender']));
					endif;
					$this->db->set('notification_type', 'social_request_received');
					$this->db->insert('hiprofile_notification_list');
					if ($this->db->affected_rows() >= 0)
					{
						$this->db->select('device_token as FCMtoken');
						$this->db->from('hiprofile_users');
						$this->db->where('userId', $data['friend_user_id']);
						$query = $this->db->get();
						$results = $query->row_array();
						$this->db->set('FCMtoken',$results['FCMtoken']);
						$this->db->update('hiprofile_notification_list');
						if ($this->db->affected_rows() >= 0)
						{
							sendSocialFriendRequest($data);
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['status'] = "Social Media Request Sent";
							return $return_res;
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "500";
							$return_res['status'] = "Something went wrong. Please try again later.";
							return $return_res;
						}
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "202";
			$return_res['responsedetails'] = "User Doesn’t have that Social Media associated";
			return $return_res;
		}
		exit;
	}
	
	//API call - To accept the social media friend request or cancel
	function socialResponse($data)
	{	
		unset($data['api_key']);
		$status = strtolower($data['status']);
		$socialmedia = strtolower($data['social_media']);
		/*Now if User B accepts the request, User B will only receive the API response, User A will receive the following
		{
		  "friend_user_id":"22",
		  "notification_type":"social_media_respond",
		  "friend_pic_url":"https://hjbchjvb/skjnjkfs.png",
		  "friend_name":"User B",
		  "friend_gender":"undisclosed",
		  "social_media":"facebook",
		  "status":"cancel"
		}*/
		if($status == 'false' && $status != "")
		{
			$this->db->select('request_status');
			$this->db->from('hiprofile_social_friends');
			$this->db->where('userId', $data['friend_user_id']);
			$this->db->where('friend_user_id', $data['userID']);
			if($socialmedia == 'facebook'):
				$this->db->where('facebook_status', 1);
			endif;
			if($socialmedia == 'twitter'):
				$this->db->where('twitter_status', 1);
			endif;
			if($socialmedia == 'instagram'):
				$this->db->where('instagram_status', 1);
			endif;
			if($socialmedia == 'contact'):
				$this->db->where('contact_status', 1);
			endif;
			$query = $this->db->get();
			$numRows = $query->num_rows();
			if($numRows >= 1)
			{
				$this->db->set('request_status', '3');
				if($socialmedia == 'facebook'):
					$this->db->set('facebook', 0);
					$this->db->set('facebook_status', 0);
				endif;
				if($socialmedia == 'twitter'):
					$this->db->set('twitter', 0);
					$this->db->set('twitter_status', 0);
				endif;
				if($socialmedia == 'instagram'):
					$this->db->set('instagram', 0);
					$this->db->set('instagram_status', 0);
				endif;
				if($socialmedia == 'contact'):
					$this->db->set('contact', 0);
					$this->db->set('contact_status', 0);
				endif;
				//To check where condition
				if($socialmedia == 'facebook'):
					$this->db->where('facebook', 1);
					$this->db->where('facebook_status', 1);
				endif;
				if($socialmedia == 'twitter'):
					$this->db->where('twitter', 1);
					$this->db->where('twitter_status', 1);
				endif;
				if($socialmedia == 'instagram'):
					$this->db->where('instagram', 1);
					$this->db->where('instagram_status', 1);
				endif;
				if($socialmedia == 'contact'):
					$this->db->where('contact', 1);
					$this->db->where('contact_status', 1);
				endif;
				
				$this->db->where('userId', $data['friend_user_id']);
				$this->db->where('friend_user_id', $data['userID']);
				$this->db->update('hiprofile_social_friends');
				if ($this->db->affected_rows() >=1)
				{
					//To select the profileimage,name,gender of the friend to insert in notification table
					$this->db->select('social.profileimage as friend_pic_url,BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.gender_disclose as friend_gender_disclose');
					$this->db->from('hiprofile_users as BaseTbl');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
					$this->db->where('BaseTbl.userId', $data['userID']);
					$query = $this->db->get();
					$result = $query->row_array();
					$this->db->set('userId',$data['userID']);
					$this->db->set('friend_pic_url',$result['friend_pic_url']);
					$this->db->set('friend_name',$result['friend_name']);
					$this->db->set('friend_user_id',$data['friend_user_id']);
					// To check which social media request is been made
					if($socialmedia == 'facebook'):
						$this->db->set('social_media', $socialmedia);
					endif;
					if($socialmedia == 'twitter'):
						$this->db->set('social_media', $socialmedia);
					endif;
					if($socialmedia == 'instagram'):
						$this->db->set('social_media', $socialmedia);
					endif;
					if($socialmedia == 'contact'):
						$this->db->set('social_media', $socialmedia);
					endif;
					// To check gender disclosed or not
					if($result['friend_gender_disclose'] == 0):
						$this->db->set('friend_gender', 'undisclosed');
					else:
						$this->db->set('friend_gender', strtolower($result['friend_gender']));
					endif;
					$this->db->set('notification_type', 'social_media_request_rejected');
					$this->db->set('status', '2');
					$this->db->insert('hiprofile_notification_list');
					if ($this->db->affected_rows() >= 0)
					{
						
						$this->db->select('device_token as FCMtoken');
						$this->db->from('hiprofile_users');
						$this->db->where('userId', $data['friend_user_id']);
						$query = $this->db->get();
						$results = $query->row_array();
						$this->db->set('FCMtoken',$results['FCMtoken']);
						$this->db->where('friend_user_id', $data['friend_user_id']);
						$this->db->where('userId', $data['userID']);
						$this->db->update('hiprofile_notification_list');
						if ($this->db->affected_rows() >= 0)
						{
							$this->socialapi_model->deleteSocialRequest($data['userID'],$data['friend_user_id'],$socialmedia);
							sendSocialFriendRequestCancel($data);
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['status'] = "Social Media Request Cancelled";
							return $return_res;
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "500";
							$return_res['status'] = "Something went wrong. Please try again later.";
							return $return_res;
						}					
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "201";
					$return_res['status'] = "Social Media Request Already Cancelled";
					return $return_res;
				}
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "Never sent request to this user for this Social Media";
				return $return_res;
			}
		}
		elseif($status == 'true' && $status != "")
		{
			/*Now if User B accepts the request, User B will only receive the API response, User A will receive the following
			{
			  "friend_user_id":"22",
			  "notification_type":"social_media_respond",
			  "friend_pic_url":"https://hjbchjvb/skjnjkfs.png",
			  "friend_name":"User B",
			  "friend_gender":"undisclosed",
			  "social_media":"facebook",
			  "status":"accept"
			}*/	
			$this->db->select('request_status');
			$this->db->from('hiprofile_social_friends');
			$this->db->where('userId', $data['friend_user_id']);
			$this->db->where('friend_user_id', $data['userID']);
			if($socialmedia == 'facebook'):
				$this->db->where('facebook_status', 1);
			endif;
			if($socialmedia == 'twitter'):
				$this->db->where('twitter_status', 1);
			endif;
			if($socialmedia == 'instagram'):
				$this->db->where('instagram_status', 1);
			endif;
			if($socialmedia == 'contact'):
				$this->db->where('contact_status', 1);
			endif;
			$query = $this->db->get();
			$numRows = $query->num_rows();
			if($numRows >= 1)
			{
				$this->db->set('request_status', '1');
				//To check where condition
				if($socialmedia == 'facebook'):
					$this->db->where('facebook', 1);
					$this->db->where('facebook_status', 1);
				endif;
				if($socialmedia == 'twitter'):
					$this->db->where('twitter', 1);
					$this->db->where('twitter_status', 1);
				endif;
				if($socialmedia == 'instagram'):
					$this->db->where('instagram', 1);
					$this->db->where('instagram_status', 1);
				endif;
				if($socialmedia == 'contact'):
					$this->db->where('contact', 1);
					$this->db->where('contact_status', 1);
				endif;

				$this->db->where('userId', $data['friend_user_id']);
				$this->db->where('friend_user_id', $data['userID']);
				$this->db->update('hiprofile_social_friends');
				if ($this->db->affected_rows() >=1)
				{
					//To select the profileimage,name,gender of the friend to insert in notification table
					$this->db->select('social.profileimage as friend_pic_url,BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.gender_disclose as friend_gender_disclose');
					$this->db->from('hiprofile_users as BaseTbl');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
					$this->db->where('BaseTbl.userId', $data['userID']);
					$query = $this->db->get();
					$result = $query->row_array();
					$this->db->set('userId',$data['userID']);
					$this->db->set('friend_pic_url',$result['friend_pic_url']);
					$this->db->set('friend_name',$result['friend_name']);
					$this->db->set('friend_user_id',$data['friend_user_id']);
					// To check which social media request is been made
					if($socialmedia == 'facebook'):
						$this->db->set('social_media', $socialmedia);
					endif;
					if($socialmedia == 'twitter'):
						$this->db->set('social_media', $socialmedia);
					endif;
					if($socialmedia == 'instagram'):
						$this->db->set('social_media', $socialmedia);
					endif;
					if($socialmedia == 'contact'):
						$this->db->set('social_media', $socialmedia);
					endif;
					// To check gender disclosed or not
					if($result['friend_gender_disclose'] == 0):
						$this->db->set('friend_gender', 'undisclosed');
					else:
						$this->db->set('friend_gender', strtolower($result['friend_gender']));
					endif;
					$this->db->set('notification_type', 'social_media_request_approved');
					$this->db->set('status', '1');
					$this->db->insert('hiprofile_notification_list');
					if ($this->db->affected_rows() >= 0)
					{
						$this->db->select('device_token as FCMtoken');
						$this->db->from('hiprofile_users');
						$this->db->where('userId', $data['friend_user_id']);
						$query = $this->db->get();
						$results = $query->row_array();
						$this->db->set('FCMtoken',$results['FCMtoken']);
						$this->db->where('friend_user_id', $data['friend_user_id']);
						$this->db->where('userId', $data['userID']);
						$this->db->update('hiprofile_notification_list');
						if ($this->db->affected_rows() >= 0)
						{
							//update users rank if required
							/*$requester_user_id = $data['userID'];
							$friend_user_id = $data['friend_user_id'];
							$this->socialapi_model->updateUsersRank($data['userID'],$data['friend_user_id'],$socialmedia);*/
							
							$this->socialapi_model->deleteSocialRequest($data['userID'],$data['friend_user_id'],$socialmedia);
							sendSocialFriendRequestAccept($data);
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['status'] = "Social Media Request Accepted";
							return $return_res;
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "500";
							$return_res['status'] = "Something went wrong. Please try again later.";
							return $return_res;
						}					
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}
					
					
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "201";
					$return_res['status'] = "Social Media Request Already Accepted";
					return $return_res;
				}
				
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "Never sent request to this user for this Social Media";
				return $return_res;
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['responsedetails'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
	}
	
	
	
	//API call - To send friend request cancel
	/*Now if User B accepts the request, User B will only receive the API response, User A will receive the following
	{
	  "friend_user_id":"22",
	  "notification_type":"social_media_respond",
	  "friend_pic_url":"https://hjbchjvb/skjnjkfs.png",
	  "friend_name":"User B",
	  "friend_gender":"undisclosed",
	  "social_media":"facebook",
	  "status":"cancel"
	}*/
	public function socialRequestCancel($data)
	{

		unset($data['api_key']);
		//To check friend user has social media link or not
		$socialmedia = strtolower($data['social_media']);
		$this->db->select('request_status');
		$this->db->from('hiprofile_social_friends');
		$this->db->where('userId', $data['userID']);
		$this->db->where('friend_user_id', $data['friend_user_id']);
		if($socialmedia == 'facebook'):
			$this->db->where('facebook_status', 1);
		endif;
		if($socialmedia == 'twitter'):
			$this->db->where('twitter_status', 1);
		endif;
		if($socialmedia == 'instagram'):
			$this->db->where('instagram_status', 1);
		endif;
		if($socialmedia == 'contact'):
			$this->db->where('contact_status', 1);
		endif;
		$query = $this->db->get();
		$numRows = $query->num_rows();
		if($numRows >= 1)
		{
			$this->db->where('userId',$data['userID']);
			$this->db->where('friend_user_id',$data['friend_user_id']);
			if($socialmedia == 'facebook'):
			$this->db->where('facebook_status', 1);
			endif;
			if($socialmedia == 'twitter'):
				$this->db->where('twitter_status', 1);
			endif;
			if($socialmedia == 'instagram'):
				$this->db->where('instagram_status', 1);
			endif;
			if($socialmedia == 'contact'):
				$this->db->where('contact_status', 1);
			endif;
			$this->db->delete('hiprofile_social_friends');
			if ($this->db->affected_rows() >= 0)
			{
				$this->db->where('userId',$data['userID']);
				$this->db->where('friend_user_id',$data['friend_user_id']);
				if($socialmedia == 'facebook'):
					$this->db->where('social_media', $socialmedia);
				endif;
				if($socialmedia == 'twitter'):
					$this->db->where('social_media', $socialmedia);
				endif;
				if($socialmedia == 'instagram'):
					$this->db->where('social_media', $socialmedia);
				endif;
				if($socialmedia == 'contact'):
					$this->db->where('social_media', $socialmedia);
				endif;
				$this->db->delete('hiprofile_notification_list');	
				if ($this->db->affected_rows() >= 0)
				{
					$return_res = array();
					$return_res['responsecode'] = "200";
					$return_res['status'] = "Social Media Request Cancelled";
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}					
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['status'] = "Social Media Request Already Cancelled";
				return $return_res;
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "Never sent request to this user for this Social Media";
			return $return_res;
		}
		exit;
	}
	
	
	function notificationList($data)
	{
		unset($data['api_key']);
		$this->db->select('friend_user_id');
		$this->db->from('hiprofile_notification_list');
		$this->db->where('friend_user_id',$data['userID']);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		if($numRows >= 1)
		{
			$this->db->select('list.userId as friend_user_id,social.profileimage as friend_pic_url,list.friend_name,list.friend_gender,list.notification_type,list.social_media,list.status');
			$this->db->from('hiprofile_notification_list as list');
			$this->db->join('hiprofile_user_socialsync as social','social.userId = list.userId');		
			$this->db->where('list.friend_user_id',$data['userID']);
			$this->db->order_by('created_date', 'desc');
			$query = $this->db->get();
			$notificationval = $query->result();
			foreach($notificationval as $userval)
			{
				$checkMyStatus = $this->checkMyBlockStatus($data['userID'],$userval->friend_user_id);
				$checkMyFriendStatus = $this->checkMyFriendBlockStatus($data['userID'],$userval->friend_user_id);
				if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
				{
					$notification[] = $userval;
				}
					
			}
			if(!empty($notification))
			{
				$loopcount = count($notification);
				for($i=0;$i<$loopcount;$i++)
				{
					$notification[$i]->friend_pic_url    = ($notification[$i]->friend_pic_url == "") ? "" : base_url().'assets'.$notification[$i]->friend_pic_url;				
					$notification[$i]->notification_type = $notification[$i]->notification_type;
					if($notification[$i]->social_media == ""):
					
						$notification[$i]->social_media = "";
						
					elseif($notification[$i]->social_media != ""):
					
					$notification[$i]->social_media = $notification[$i]->social_media;
					
					else:
						unset($notification[$i]->social_media);
					endif;
					if($notification[$i]->status == "1"):
						$notification[$i]->status = "accepted";
					elseif($notification[$i]->status == "2"):
						$notification[$i]->status = "rejected";
					else:
						$notification[$i]->status = "";
					endif;
				}
				$return_res = array();
				$return_res['responsecode'] 	= "200";
				$return_res['responsedetails'] 	= "Success";
				$return_res['notifications'] 	= $notification;
				return $return_res;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "Never sent request to this user";
				return $return_res;
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "Never sent request to this user";
			return $return_res;
		}
		exit;
	}
	
	function friendRequestResponseOld($data)
	{	
		unset($data['api_key']);
		$status = strtolower($data['status']);
		$this->db->select('request_status');
		$this->db->from('hiprofile_friends');
		//$this->db->where('request_status', "0");
		$this->db->where('userId', $data['friend_user_id']);
		$this->db->where('friend_user_id', $data['userID']);
		$query = $this->db->get();
		$result = $query->row_array();
		$numRows = $query->num_rows();
		if(isset($result['request_status']) && $result['request_status'] != "" && $result['request_status'] == "1" ){
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "Friend Request Already Approved";
			return $return_res;
			exit;
		}
		elseif(isset($result['request_status']) && $result['request_status'] != "" && $result['request_status'] == "2" ){
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "Friend Request Already Denied";
			return $return_res;
			exit;
		}
		elseif($numRows >= 1)
		{
			if($status == 'false' && $status != "")
			{
			
				$this->db->set('request_status', '0');
				$this->db->set('add_friend', $status);
				$this->db->where('userId', $data['friend_user_id']);
				$this->db->where('friend_user_id', $data['userID']);
				$this->db->delete('hiprofile_friends');

				$this->db->set('userId_choice', $data['userID']);
				$this->db->set('userId_requesting', $data['friend_user_id']);
				$this->db->set('declined', '1');	
				$this->db->insert('hiprofile_declined_friend_requests');
				
				if ($this->db->affected_rows() >= 0)
				{
					//To select the profileimage,name,gender of the friend to insert in notification table									
					$this->db->select('social.profileimage as friend_pic_url,BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.gender_disclose as friend_gender_disclose');
					$this->db->from('hiprofile_users as BaseTbl');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
					$this->db->where('BaseTbl.userId', $data['userID']);
					$query = $this->db->get();
					$result = $query->row_array();
					$this->db->set('userId',$data['userID']);
					$this->db->set('friend_pic_url',$result['friend_pic_url']);
					$this->db->set('friend_name',$result['friend_name']);
					$this->db->set('friend_user_id',$data['friend_user_id']);
					$this->db->set('social_media', "");				
					// To check gender disclosed or not
					if($result['friend_gender_disclose'] == 0):
						$this->db->set('friend_gender', 'undisclosed');
					else:
						$this->db->set('friend_gender', strtolower($result['friend_gender']));
					endif;
					$this->db->set('notification_type', 'friend_request_rejected');
					$this->db->set('status', '2');
					$this->db->insert('hiprofile_notification_list');
					if ($this->db->affected_rows() >= 0)
					{
						$this->db->select('device_token as FCMtoken');
						$this->db->from('hiprofile_users');
						$this->db->where('userId', $data['friend_user_id']);
						$query = $this->db->get();
						$results = $query->row_array();
						$this->db->set('FCMtoken',$results['FCMtoken']);
						$this->db->where('friend_user_id', $data['friend_user_id']);
						$this->db->where('userId', $data['userID']);
						$this->db->update('hiprofile_notification_list');
						if ($this->db->affected_rows() >= 0)
						{
							$this->socialapi_model->deleteRequest($data['userID'],$data['friend_user_id']);
							
							//To Send the push notification
							friendRequestCancel($data);
							
							//$this->socialapi_model->deleteMyRequest($data['userID'],$data['friend_user_id']);
							
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['status'] = "Friend Request Denied";
							return $return_res;
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "500";
							$return_res['status'] = "Something went wrong. Please try again later.";
							return $return_res;
						}		
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					} 
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				} 
			}
			elseif($status == 'true' && $status != "")
			{
				
				$this->db->set('request_status', '1');
				$this->db->set('add_friend', $status);
				$this->db->where('userId', $data['friend_user_id']);
				$this->db->where('friend_user_id', $data['userID']);
				$this->db->update('hiprofile_friends');
				
				$this->db->set('request_status', '1');
				$this->db->set('add_friend', $status);
				$this->db->set('userId', $data['userID']);
				$this->db->set('friend_user_id', $data['friend_user_id']);
				$this->db->insert('hiprofile_friends');
				
				$this->socialapi_model->deleteFriendRequest($data['userID'],$data['friend_user_id']);
				$this->socialapi_model->deleteFriendRequest($data['friend_user_id'],$data['userID']);
				
				if ($this->db->affected_rows() >= 0)
				{
					//Check if the user are added before and been unblocked if not do the calcualtion or else just send friend notification
					$checkUnBlockedMyStatus = $this->checkMyUnBlockStatus($data['userID'],$data['friend_user_id']);
					
					$checkMyFriendUnBlockedStatus = $this->checkMyFriendUnBlockStatus($data['userID'],$data['friend_user_id']);
					
					//Check if the user are added before and been unfriended if not do the calcualtion or else just send friend notification
					$checkUnFriendedMyStatus = $this->checkMyUnFriendStatus($data['userID'],$data['friend_user_id']);
					$checkMyFriendUnFriendedStatus = $this->checkMyFriendUnFriendedStatus($data['userID'],$data['friend_user_id']);
					
					//check if the friend request is been declined or not 
					$checkDeclinedMyReqStatus = $this->checkMyReqDeclinedStatus($data['userID'],$data['friend_user_id']);
					
					if(($checkUnBlockedMyStatus == 0 && $checkMyFriendUnBlockedStatus ==0) && ($checkUnFriendedMyStatus == 0 && $checkMyFriendUnFriendedStatus == 0))
					{
						if($checkDeclinedMyReqStatus == 0)
						{
							//$rankrange = $this->userFriendRankRangeCalculation($data);
							$rankrange = $this->userFriendRankRange($data);
						}
					}	
					//$rankrange = $this->socialapi_model->userFriendRankRange($data);
					//To select the profileimage,name,gender of the friend to insert in notification table
					$this->db->select('social.profileimage as friend_pic_url,BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.gender_disclose as friend_gender_disclose');
					$this->db->from('hiprofile_users as BaseTbl');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
					$this->db->where('BaseTbl.userId', $data['userID']);
					$query = $this->db->get();
					$result = $query->row_array();
					$this->db->set('userId',$data['userID']);
					$this->db->set('friend_pic_url',$result['friend_pic_url']);
					$this->db->set('friend_name',$result['friend_name']);
					$this->db->set('friend_user_id',$data['friend_user_id']);
					$this->db->set('social_media', "");
					// To check gender disclosed or not
					if($result['friend_gender_disclose'] == 0):
						$this->db->set('friend_gender', 'undisclosed');
					else:
						$this->db->set('friend_gender', strtolower($result['friend_gender']));
					endif;
					$this->db->set('notification_type', 'friend_request_approved');
					$this->db->set('status', '1');
					$this->db->insert('hiprofile_notification_list');
					if ($this->db->affected_rows() >= 0)
					{
						$this->db->select('device_token as FCMtoken');
						$this->db->from('hiprofile_users');
						$this->db->where('userId', $data['friend_user_id']);
						$query = $this->db->get();
						$results = $query->row_array();
						
						$this->db->set('FCMtoken',$results['FCMtoken']);
						$this->db->where('friend_user_id', $data['friend_user_id']);
						$this->db->where('userId', $data['userID']);
						$this->db->update('hiprofile_notification_list');
				
						if ($this->db->affected_rows() >= 0)
						{
							$this->socialapi_model->deleteRequest($data['userID'],$data['friend_user_id']);
							
							//To send the push notification
							friendRequestAccept($data);
							iosfriendRequestAccept($data);
							/*if(strtolower($data['device_type']) == 'android'){
								friendRequestAccept($data);
							}
							if(strtolower($data['device_type']) == 'ios'){
								iosfriendRequestAccept($data);
							}*/
							
							//$this->socialapi_model->deleteMyRequest($data['userID'],$data['friend_user_id']);
							
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['status'] = "Friend Request Approved";
							return $return_res;
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "500";
							$return_res['status'] = "Something went wrong. Please try again later.";
							return $return_res;
						}			
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				} 
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "Never received request from this user";
			return $return_res;
		}
		exit;
		
	}
		
	/*Fucntion to caluclate the user and friend rank range*/
	function userFriendRankRange($data = null)
	{
		$result = array_merge(array($data['userID']),array($data['friend_user_id']));
		for($i=0;$i<count($result);$i++)
		{

			$this->db->select('social.rank_f2,social.points,social.social_rank,social.rank,social.social_points');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
			$this->db->where('BaseTbl.userId', $result[$i]);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.status', 0);
			$query = $this->db->get();
			$user[] = $query->result();
		}
		$this->load->model('userapi_model');
		$userrank = $this->userapi_model->getuserrank($data['userID']);
		
		//FRIEND USER WHO ACCEPTS THE MAIN FRIEND USER WHO SENT AN REQUEST
		//$RankA 		= $user[0][0]->rank_f2 + $user[0][0]->social_rank + $user[0][0]->rank;
		
		$RankA 		= $user[0][0]->social_rank + $user[0][0]->rank;
		$Rankoftheuser 			= $user[0][0]->rank;	
		
		$RankAPoints 	= $user[0][0]->points;
		//USER MAIN WHEN HE SENDS A NEW FRIEND REQUEST SENT
		//$RankB 		= $user[1][0]->rank_f2 + $user[1][0]->social_rank + $user[1][0]->rank;
		
		$RankB 		= $user[1][0]->social_rank + $user[1][0]->rank;
		
		//$RankB 			= $user[1][0]->rank;
		$RankBPoints 	= $user[1][0]->points;
		// CHECK THE RANK RANGE BETWEEN A AND B
		//$RankRangeA 	=  round(($RankA -1)/10)+1 ;
		//$RankRangeB 	=  round(($RankB -1)/10)+1 ;
		if(round($RankA) == 0):	
			$RankRangeA 	=  round(($RankA -1)/10)+1 ;
		else:
			$RankRangeA 	=  round($RankA);
		endif;
		
		if(round($RankB) == 0):	
			$RankRangeB 	=  round(($RankB -1)/10)+1 ;
		else:
			$RankRangeB 	=  round($RankB);
		endif;
		
		/*echo"RankA: ".$RankRangeA;
		echo"</br>";
		echo"RankB: ".$RankRangeB;
		echo"</br>";
		echo"RankAPoints: ".$RankAPoints;
		echo"</br>";
		echo"RankBPoints: ".$RankBPoints;
		exit;  */

		$EarnedRank = 0;
		$EarnedRankPoints = 0;
		
		// For user Range Range 1-10 calculation
		if (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 4/10;
			$EarnedRankPoints = 400;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 5/10;
			$EarnedRankPoints = 500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 9/10+4;
			$EarnedRankPoints = 900+4000;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 10/10+4.5;
			$EarnedRankPoints = 1000+4500;
		}
		
		// For user Range Range 11-20 calculation
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 4/10;
			$EarnedRankPoints = 400;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 9/10+4;
			$EarnedRankPoints = 900+4000;
		}
		
		// For user Range Range 21-30 calculation
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		
		// For user Range Range 31-40 calculation
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		
		// For user Range Range 41-50 calculation
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		
		// For user Range Range 51-60 calculation
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		
		// For user Range Range 61-70 calculation
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		
		// For user Range Range 71-80 calculation
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		
		// For user Range Range 81-90 calculation
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.2/10;
			$EarnedRankPoints = 20;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		
			// For user Range Range 91-100 calculation
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.1/10;
			$EarnedRankPoints = 10;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.2/10;
			$EarnedRankPoints = 20;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		/*
 		echo "AFTER CALCULATION";
		echo"RankA: ".$RankA;
		echo"</br>";
		echo"EarnedRank: ".$EarnedRank;
		echo"</br>";
		echo"RankAPoints: ".$RankAPoints;
		echo"</br>";
		echo"EarnedRankPoints: ".$EarnedRankPoints;
		exit; 
		*/	


		/*echo $data['friend_user_id'];

		echo "--";

		echo $data['userID'];

		die;*/
		
		$finalrank = $Rankoftheuser;
		$finalpoints = $RankAPoints+$EarnedRankPoints;
		if($finalpoints >= 1000)
		{
			$rank = $finalrank+1;
			$finalpoints = $finalpoints - 1000;
			
			 $this->db->set('userId', $data['userID']);
					$this->db->set('previous_rank', $finalrank);
					$this->db->set('new_rank', $rank);
					$this->db->insert('hiprofile_user_ranks_activity');
		}
		else
		{
			$rank = $finalrank;
		}
		$this->db->set('rank',$rank);
		$this->db->set('rank_f2',$EarnedRank);
		$this->db->set('points', $finalpoints);
		$this->db->where('userId', $data['userID']);
		$this->db->update('hiprofile_user_socialsync');
		
		
		$this->load->model('userapi_model');
		$this->userapi_model->updateEarnedPoints($data['userID'],$data['friend_user_id'],$EarnedRankPoints);
		
		/* $this->db->set('earned_points', $EarnedRankPoints);
		$this->db->set('status', 0);
		$this->db->set('userId', $data['userID']);
		$this->db->set('friend_id', $data['friend_user_id']);
		$this->db->insert('hiprofile_earned_user_points'); */
		
		//To send the rank push notification when a increase in rank
		$userfinalrank = $this->userapi_model->getuserrank($data['userID']);
		if($userfinalrank > $userrank){
			sendrankpush($data);	
			iossendrankpush($data);
			/*if(strtolower($data['device_type']) == 'android'){
				sendrankpush($data);
			}
			if(strtolower($data['device_type']) == 'ios'){
				iossendrankpush($data);
			}*/	
		}			
	}
	#################################################################
	function friendRequestResponse($data)
	{	
		unset($data['api_key']);
		$status = strtolower($data['status']);
		$this->db->select('request_status,friend_req_type');
		$this->db->from('hiprofile_friends');
		//$this->db->where('request_status', "0");
		$this->db->where('userId', $data['friend_user_id']);
		$this->db->where('friend_user_id', $data['userID']);
		$query = $this->db->get();
		$result = $query->row_array();

		//print_r($result);die;

		$numRows = $query->num_rows();
		$friend_req_type = isset($result['friend_req_type']) ? $result['friend_req_type'] : 'QR';
		if(isset($result['request_status']) && $result['request_status'] != "" && $result['request_status'] == "1" ){
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "Friend Request Already Approved";
			return $return_res;
			exit;
		}
		elseif(isset($result['request_status']) && $result['request_status'] != "" && $result['request_status'] == "2" ){
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "Friend Request Already Denied";
			return $return_res;
			exit;
		}
		elseif($numRows >= 1)
		{
			if($status == 'false' && $status != "")
			{
			
				$this->db->set('request_status', '0');
				$this->db->set('add_friend', $status);
				$this->db->where('userId', $data['friend_user_id']);
				$this->db->where('friend_user_id', $data['userID']);
				$this->db->delete('hiprofile_friends');

				$this->db->set('userId_choice', $data['userID']);
				$this->db->set('userId_requesting', $data['friend_user_id']);
				$this->db->set('declined', '1');	
				$this->db->insert('hiprofile_declined_friend_requests');
				
				if ($this->db->affected_rows() >= 0)
				{
					//To select the profileimage,name,gender of the friend to insert in notification table									
					$this->db->select('social.profileimage as friend_pic_url,BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.gender_disclose as friend_gender_disclose');
					$this->db->from('hiprofile_users as BaseTbl');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
					$this->db->where('BaseTbl.userId', $data['userID']);
					$query = $this->db->get();
					$result = $query->row_array();
					$this->db->set('userId',$data['userID']);
					$this->db->set('friend_pic_url',$result['friend_pic_url']);
					$this->db->set('friend_name',$result['friend_name']);
					$this->db->set('friend_user_id',$data['friend_user_id']);
					$this->db->set('social_media', "");				
					// To check gender disclosed or not
					if($result['friend_gender_disclose'] == 0):
						$this->db->set('friend_gender', 'undisclosed');
					else:
						$this->db->set('friend_gender', strtolower($result['friend_gender']));
					endif;
					$this->db->set('notification_type', 'friend_request_rejected');
					$this->db->set('status', '2');
					$this->db->insert('hiprofile_notification_list');
					if ($this->db->affected_rows() >= 0)
					{
						$this->db->select('device_token as FCMtoken');
						$this->db->from('hiprofile_users');
						$this->db->where('userId', $data['friend_user_id']);
						$query = $this->db->get();
						$results = $query->row_array();
						$this->db->set('FCMtoken',$results['FCMtoken']);
						$this->db->where('friend_user_id', $data['friend_user_id']);
						$this->db->where('userId', $data['userID']);
						$this->db->update('hiprofile_notification_list');
						if ($this->db->affected_rows() >= 0)
						{
							$this->socialapi_model->deleteRequest($data['userID'],$data['friend_user_id']);
							
							//To Send the push notification
							friendRequestCancel($data);
							
							//$this->socialapi_model->deleteMyRequest($data['userID'],$data['friend_user_id']);
							
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['status'] = "Friend Request Denied";
							return $return_res;
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "500";
							$return_res['status'] = "Something went wrong. Please try again later.";
							return $return_res;
						}		
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					} 
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				} 
			}
			elseif($status == 'true' && $status != "")
			{
				
				$this->db->set('request_status', '1');
				$this->db->set('add_friend', $status);
				$this->db->where('userId', $data['friend_user_id']);
				$this->db->where('friend_user_id', $data['userID']);
				$this->db->update('hiprofile_friends');
				
				$this->db->set('request_status', '1');
				$this->db->set('add_friend', $status);
				$this->db->set('userId', $data['userID']);
				$this->db->set('friend_req_type', $friend_req_type);
				$this->db->set('friend_user_id', $data['friend_user_id']);
				$this->db->insert('hiprofile_friends');
				
				$this->socialapi_model->deleteFriendRequest($data['userID'],$data['friend_user_id']);
				$this->socialapi_model->deleteFriendRequest($data['friend_user_id'],$data['userID']);
				
				if ($this->db->affected_rows() >= 0)
				{
					//Check if the user are added before and been unblocked if not do the calcualtion or else just send friend notification
					$checkUnBlockedMyStatus = $this->checkMyUnBlockStatus($data['userID'],$data['friend_user_id']);
					
					$checkMyFriendUnBlockedStatus = $this->checkMyFriendUnBlockStatus($data['userID'],$data['friend_user_id']);
					
					//Check if the user are added before and been unfriended if not do the calcualtion or else just send friend notification
					$checkUnFriendedMyStatus = $this->checkMyUnFriendStatus($data['userID'],$data['friend_user_id']);
					$checkMyFriendUnFriendedStatus = $this->checkMyFriendUnFriendedStatus($data['userID'],$data['friend_user_id']);
					
					//check if the friend request is been declined or not 
					$checkDeclinedMyReqStatus = $this->checkMyReqDeclinedStatus($data['userID'],$data['friend_user_id']);
					
					if(($checkUnBlockedMyStatus == 0 && $checkMyFriendUnBlockedStatus ==0) && ($checkUnFriendedMyStatus == 0 && $checkMyFriendUnFriendedStatus == 0))
					{
						if($checkDeclinedMyReqStatus == 0)
						{
							//echo $friend_req_type;die;
							//$rankrange = $this->userFriendRankRangeCalculation($data);
							if($friend_req_type=="QR")
							{
								$rankrange = $this->userFriendRankRange($data);
							}elseif($friend_req_type=="NONQR")
							{

								$rankrange = $this->userFriendRankRangeNonQr($data);
							}
							
						}
					}

					//echo $friend_req_type;die;
					//$rankrange = $this->socialapi_model->userFriendRankRange($data);
					//To select the profileimage,name,gender of the friend to insert in notification table
					$this->db->select('social.profileimage as friend_pic_url,BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.gender_disclose as friend_gender_disclose');
					$this->db->from('hiprofile_users as BaseTbl');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
					$this->db->where('BaseTbl.userId', $data['userID']);
					$query = $this->db->get();
					$result = $query->row_array();
					$this->db->set('userId',$data['userID']);
					$this->db->set('friend_pic_url',$result['friend_pic_url']);
					$this->db->set('friend_name',$result['friend_name']);
					$this->db->set('friend_user_id',$data['friend_user_id']);
					$this->db->set('social_media', "");
					// To check gender disclosed or not
					if($result['friend_gender_disclose'] == 0):
						$this->db->set('friend_gender', 'undisclosed');
					else:
						$this->db->set('friend_gender', strtolower($result['friend_gender']));
					endif;
					$this->db->set('notification_type', 'friend_request_approved');
					$this->db->set('status', '1');
					$this->db->insert('hiprofile_notification_list');
					if ($this->db->affected_rows() >= 0)
					{
						$this->db->select('device_token as FCMtoken');
						$this->db->from('hiprofile_users');
						$this->db->where('userId', $data['friend_user_id']);
						$query = $this->db->get();
						$results = $query->row_array();
						
						$this->db->set('FCMtoken',$results['FCMtoken']);
						$this->db->where('friend_user_id', $data['friend_user_id']);
						$this->db->where('userId', $data['userID']);
						$this->db->update('hiprofile_notification_list');
				
						if ($this->db->affected_rows() >= 0)
						{
							$this->socialapi_model->deleteRequest($data['userID'],$data['friend_user_id']);
							
							//To send the push notification
							friendRequestAccept($data);
							iosfriendRequestAccept($data);
							/*if(strtolower($data['device_type']) == 'android'){
								friendRequestAccept($data);
							}
							if(strtolower($data['device_type']) == 'ios'){
								iosfriendRequestAccept($data);
							}*/
							
							//$this->socialapi_model->deleteMyRequest($data['userID'],$data['friend_user_id']);
							
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['status'] = "Friend Request Approved";
							return $return_res;
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "500";
							$return_res['status'] = "Something went wrong. Please try again later.";
							return $return_res;
						}			
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				} 
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "Never received request from this user";
			return $return_res;
		}
		exit;
		
	}

	function userFriendRankRangeCalculation($data = null)
	{
		$result = array_merge(array($data['userID']),array($data['friend_user_id']));
		for($i=0;$i<count($result);$i++)
		{

			$this->db->select('social.rank_f2,social.points,social.social_rank,social.rank,social.social_points');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
			$this->db->where('BaseTbl.userId', $result[$i]);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.status', 0);
			$query = $this->db->get();
			$user[] = $query->result();
		}
		$this->load->model('userapi_model');
		$userrank = $this->userapi_model->getuserrank($data['userID']);

		$userrankB = $this->userapi_model->getuserrank($data['friend_user_id']);
		
		//FRIEND USER WHO ACCEPTS THE MAIN FRIEND USER WHO SENT AN REQUEST
		//$RankA 		= $user[0][0]->rank_f2 + $user[0][0]->social_rank + $user[0][0]->rank;
		
		$RankA 		= $user[0][0]->social_rank + $user[0][0]->rank;
		$Rankoftheuser 			= $user[0][0]->rank;	
		
		$RankAPoints 	= $user[0][0]->points;
		//USER MAIN WHEN HE SENDS A NEW FRIEND REQUEST SENT
		//$RankB 		= $user[1][0]->rank_f2 + $user[1][0]->social_rank + $user[1][0]->rank;
		
		$RankB 		= $user[1][0]->social_rank + $user[1][0]->rank;
		$RankoftheuserB			= $user[1][0]->rank;	
		
		//$RankB 			= $user[1][0]->rank;
		$RankBPoints 	= $user[1][0]->points;
		// CHECK THE RANK RANGE BETWEEN A AND B
		//$RankRangeA 	=  round(($RankA -1)/10)+1 ;
		//$RankRangeB 	=  round(($RankB -1)/10)+1 ;
		if(round($RankA) == 0):	
			$RankRangeA 	=  round(($RankA -1)/10)+1 ;
		else:
			$RankRangeA 	=  round($RankA);
		endif;
		
		if(round($RankB) == 0):	
			$RankRangeB 	=  round(($RankB -1)/10)+1 ;
		else:
			$RankRangeB 	=  round($RankB);
		endif;
		
		/*echo"RankA: ".$RankRangeA;
		echo"</br>";
		echo"RankB: ".$RankRangeB;
		echo"</br>";
		echo"RankAPoints: ".$RankAPoints;
		echo"</br>";
		echo"RankBPoints: ".$RankBPoints;
		exit;  */

		$userRankA = $this->calculateRange($RankRangeA,$RankRangeB);
		$EarnedRank = $userRankA['EarnedRank'];
		$EarnedRankPoints = $userRankA['EarnedRankPoints'];

		//exit; 	
		/*echo $data['friend_user_id'];
		echo "--";
		echo $data['userID'];
		die;*/
		
		$finalrank = $Rankoftheuser;
		$finalpoints = $RankAPoints+$EarnedRankPoints;
		if($finalpoints >= 1000)
		{
			$rank = $finalrank+1;
			$finalpoints = $finalpoints - 1000;
			
			 $this->db->set('userId', $data['userID']);
					$this->db->set('previous_rank', $finalrank);
					$this->db->set('new_rank', $rank);
					$this->db->insert('hiprofile_user_ranks_activity');
		}
		else
		{
			$rank = $finalrank;
		}

		/*echo "AFTER CALCULATION";
		echo"RankA: ".$RankA;
		echo"</br>";
		echo"EarnedRank: ".$EarnedRank;
		echo"</br>";
		echo"RankBPoints: ".$RankBPoints;
		echo"</br>";
		echo"EarnedRankPoints: ".$EarnedRankPoints;
		echo"</br>";
		echo"finalpoints: ".$finalpoints;*/

		$this->db->set('rank',$rank);
		$this->db->set('rank_f2',$EarnedRank);
		$this->db->set('points', $finalpoints);
		$this->db->where('userId', $data['userID']);
		$this->db->update('hiprofile_user_socialsync');
		$this->load->model('userapi_model');
		$this->userapi_model->updateEarnedPoints($data['userID'],$data['friend_user_id'],$EarnedRankPoints);
		
		/* $this->db->set('earned_points', $EarnedRankPoints);
		$this->db->set('status', 0);
		$this->db->set('userId', $data['userID']);
		$this->db->set('friend_id', $data['friend_user_id']);
		$this->db->insert('hiprofile_earned_user_points'); */
		
		//To send the rank push notification when a increase in rank
		$userfinalrank = $this->userapi_model->getuserrank($data['userID']);
		if($userfinalrank > $userrank){
			sendrankpush($data);	
			iossendrankpush($data);
			/*if(strtolower($data['device_type']) == 'android'){
				sendrankpush($data);
			}
			if(strtolower($data['device_type']) == 'ios'){
				iossendrankpush($data);
			}*/	
		}

		//CALCULATE RANK FOR THE USER WHO HAS SENT THE FRIEND REQUEST
		$userRankB = $this->calculateRange($RankRangeB,$RankRangeA);
		$EarnedRank = $userRankB['EarnedRank'];
		$EarnedRankPoints = $userRankB['EarnedRankPoints'];
		$finalrank = $RankoftheuserB;
		$finalpoints = $RankBPoints+$EarnedRankPoints;

		//echo $finalpoints;die;
		if($finalpoints >= 1000)
		{
			//echo $finalpoints;
			$rank = $finalrank+1;
			$finalpoints = $finalpoints - 1000;
			$this->db->set('userId', $data['friend_user_id']);
			$this->db->set('previous_rank', $finalrank);
			$this->db->set('new_rank', $rank);
			$this->db->insert('hiprofile_user_ranks_activity');
		}
		else
		{
			$rank = $finalrank;
		}

		//echo $rank;


		
 		/*echo "AFTER CALCULATION";
		echo"RankB: ".$RankB;
		echo"</br>";
		echo"EarnedRank: ".$EarnedRank;
		echo"</br>";
		echo"RankAPoints: ".$RankAPoints;
		echo"</br>";
		echo"EarnedRankPoints: ".$EarnedRankPoints;
		echo"</br>";
		echo"finalpoints: ".$finalpoints;*/
		
		

		$this->db->set('rank',$rank);
		$this->db->set('rank_f2',$EarnedRank);
		$this->db->set('points', $finalpoints);
		$this->db->where('userId', $data['friend_user_id']);
		$this->db->update('hiprofile_user_socialsync');
		//To send the rank push notification when a increase in rank
		$userfinalrank = $this->userapi_model->getuserrank($data['friend_user_id']);
		if($userfinalrank > $userrankB){
			$data['userID'] = $data['friend_user_id'];
			sendrankpush($data);	
			iossendrankpush($data);
			/*if(strtolower($data['device_type']) == 'android'){
				sendrankpush($data);
			}
			if(strtolower($data['device_type']) == 'ios'){
				iossendrankpush($data);
			}*/	
		}

		exit; 

	}
	//range calculation for NONQR code type of friendrequest
	function userFriendRankRangeNonQr($data = null)
	{
		$result = array_merge(array($data['userID']),array($data['friend_user_id']));
		for($i=0;$i<count($result);$i++)
		{
			$this->db->select('social.rank_f2,social.points,social.social_rank,social.rank,social.social_points');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
			$this->db->where('BaseTbl.userId', $result[$i]);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.status', 0);
			$query = $this->db->get();
			$user[] = $query->result();
		}
		$this->load->model('userapi_model');
		$userrank = $this->userapi_model->getuserrank($data['userID']);
		//FRIEND USER WHO ACCEPTS THE MAIN FRIEND USER WHO SENT AN REQUEST
		$Rankoftheuser 			= $user[0][0]->rank;	
		$RankAPoints 	= $user[0][0]->points;
		
		$EarnedRankPoints = 10;
		$finalrank = $Rankoftheuser;
		$finalpoints = $RankAPoints+$EarnedRankPoints;

		if($finalpoints >= 1000)
		{
			$rank = $Rankoftheuser+1;
			$finalpoints = $finalpoints - 1000;
			$this->db->set('userId', $data['userID']);
			$this->db->set('previous_rank', $finalrank);
			$this->db->set('new_rank', $rank);
			$this->db->insert('hiprofile_user_ranks_activity');
		}
		else
		{
			$rank = $finalrank;
		}

		/*echo "userID";
		echo"userID: ".$data['userID'];
		echo "AFTER CALCULATION";
		echo"RankA: ".$rank;
		echo"</br>";
		echo"RankAPoints: ".$RankAPoints;
		echo"</br>";
		echo"finalpoints: ".$finalpoints;
		die;*/
		$this->db->set('rank',$rank);
		$this->db->set('points', $finalpoints);
		$this->db->where('userId', $data['userID']);
		$this->db->update('hiprofile_user_socialsync');
		$this->load->model('userapi_model');
		$this->userapi_model->updateEarnedPoints($data['userID'],$data['friend_user_id'],$EarnedRankPoints);
		
		//To send the rank push notification when a increase in rank
		$userfinalrank = $this->userapi_model->getuserrank($data['userID']);
		if($userfinalrank > $userrank){
			sendrankpush($data);	
			iossendrankpush($data);
		}
		######################### RANK FOR USER B #########################
		//USER MAIN WHO SEND THE FRIEND REQUEST
		$userrankB = $this->userapi_model->getuserrank($data['friend_user_id']);
		$RankoftheuserB			= $user[1][0]->rank;	
		$RankBPoints 	= $user[1][0]->points;
		//CALCULATE RANK FOR THE USER WHO HAS SENT THE FRIEND REQUEST
		$EarnedRankPoints = 10;
		$finalrank = $RankoftheuserB;
		$finalpoints = $RankBPoints+$EarnedRankPoints;
		//echo $finalpoints;die;
		if($finalpoints >= 1000)
		{
			//echo $finalpoints;
			$rank = $finalrank+1;
			$finalpoints = $finalpoints - 1000;
			$this->db->set('userId', $data['friend_user_id']);
			$this->db->set('previous_rank', $finalrank);
			$this->db->set('new_rank', $rank);
			$this->db->insert('hiprofile_user_ranks_activity');
		}
		else
		{
			$rank = $finalrank;
		}
		/*echo "userID";
		echo"userID: ".$data['friend_user_id'];
		echo "AFTER CALCULATION";
		echo"RankB: ".$rank;
		echo"</br>";
		echo"RankBPoints: ".$RankBPoints;
		echo"</br>";
		echo"finalpoints: ".$finalpoints;
		die;*/
		
		$this->db->set('rank',$rank);
		$this->db->set('points', $finalpoints);
		$this->db->where('userId', $data['friend_user_id']);
		$this->db->update('hiprofile_user_socialsync');
		$this->userapi_model->updateEarnedPoints($data['friend_user_id'],$data['userID'],$EarnedRankPoints);
		//To send the rank push notification when a increase in rank
		$userfinalrank = $this->userapi_model->getuserrank($data['friend_user_id']);
		if($userfinalrank > $userrankB){
			$data['userID'] = $data['friend_user_id'];
			sendrankpush($data);	
			iossendrankpush($data);
		}
		

	}

	//To delete the details of the users from notification list - user2 and user1
	function calculateRange($RankRangeA,$RankRangeB)
	{
		$EarnedRank = 0;
		$EarnedRankPoints = 0;
		// For user Range Range 1-10 calculation
		if (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 4/10;
			$EarnedRankPoints = 400;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 5/10;
			$EarnedRankPoints = 500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 9/10+4;
			$EarnedRankPoints = 900+4000;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 10/10+4.5;
			$EarnedRankPoints = 1000+4500;
		}
		
		// For user Range Range 11-20 calculation
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 4/10;
			$EarnedRankPoints = 400;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 9/10+4;
			$EarnedRankPoints = 900+4000;
		}
		
		// For user Range Range 21-30 calculation
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		
		// For user Range Range 31-40 calculation
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		
		// For user Range Range 41-50 calculation
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		
		// For user Range Range 51-60 calculation
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		
		// For user Range Range 61-70 calculation
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		
		// For user Range Range 71-80 calculation
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		
		// For user Range Range 81-90 calculation
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.2/10;
			$EarnedRankPoints = 20;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		
			// For user Range Range 91-100 calculation
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.1/10;
			$EarnedRankPoints = 10;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.2/10;
			$EarnedRankPoints = 20;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		$userrank['EarnedRank'] = $EarnedRank;
		$userrank['EarnedRankPoints'] = $EarnedRankPoints;
		return $userrank;

		
	}
	
	//To delete the details of the users from notification list - user2 and user1
	function deleteRequest($userId="", $friend_id="")
	{
		$this->db->where('userId',$friend_id);
		$this->db->where('friend_user_id',$userId);
        $this->db->delete('hiprofile_notification_list');
	}
	
	//To delete the details of the users from notification list - user1 and user2
	function deleteMyRequest($userId="", $friend_id="")
	{
		$this->db->where('userId',$userId);
		$this->db->where('friend_user_id',$friend_id);
        $this->db->delete('hiprofile_notification_list');
	}
	
	//To delete the details users from frineds list
	function deleteFriendRequest($userId, $friend_id)
	{
		$this->db->where('userId',$friend_id);
		$this->db->where('friend_user_id',$userId);
		$this->db->where('request_status','0');
        $this->db->delete('hiprofile_friends');
	}
	
	//To delete the social record of the user from the notofication list
	function deleteSocialRequest($userId="", $friend_id="",$socialmedia="")
	{
		$this->db->where('userId',$friend_id);
		$this->db->where('friend_user_id',$userId);
		$this->db->where('social_media',$socialmedia);
        $this->db->delete('hiprofile_notification_list');
	}
	
	/*Fuction to delete the social friends records of the two users - User1 and his friend*/
	function toDeleteMySocailFriends($userId, $friend_id)
	{
		$this->db->where('userId',$userId);
		$this->db->where('friend_user_id',$friend_id);
        $this->db->delete('hiprofile_social_friends');
	}
	
	/*Fuction to delete the social friends records of the two users - friend and user1*/
	function toDeleteSocailFriendsofMy($userId, $friend_id)
	{
		$this->db->where('friend_user_id',$userId);
		$this->db->where('userId',$friend_id);
        $this->db->delete('hiprofile_social_friends');
	}
	
	
	/*API Fucntion to get the users friend list*/
	function get_Friend_List($data){
		unset($data['api_key']);
		
		//To check if the userId is Exists or not
		$this->db->select('userId');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
        $query = $this->db->get();
		$numRows = $query->num_rows();
        if($numRows == 1)
		{
			// To check if the user has any friends
			$this->db->select('friend_user_id');
			$this->db->from('hiprofile_friends');
			$this->db->where('userId', $data['userID']);
			$this->db->where('add_friend', 'true');
			$this->db->where('request_status','1');
			$this->db->order_by('friend_user_id', 'desc');
			$query = $this->db->get();
			$user = $query->result();
			$numRows = $query->num_rows();
			if($numRows >= 1)
			{
				
				$user = $query->result();
				
				foreach($user as $key=>$value)
				{
					
					$checkFriendStatusv = $this->get_Friend_Status($value->friend_user_id);
					if($checkFriendStatusv == 1)
					{
						$checkMyStatus = $this->checkMyBlockStatus($data['userID'],$value->friend_user_id);
						$checkMyFriendStatus = $this->checkMyFriendBlockStatus($data['userID'],$value->friend_user_id);
						if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
						{
							//Function call to check the social and friend status with the user id and friend id.
							$getuserfriend = $this->get_user_status_friend_social($data['userID'],$value->friend_user_id);
						
							//Function to fetch the earned points
							$getearnedpoints = $this->get_user_earned_points($data['userID'],$value->friend_user_id);
										
							$friendsList[] = $this->get_friend_requested_user_details($value->friend_user_id,$getuserfriend,$getearnedpoints,$data['userID']);
							//pre($this->get_friend_requested_user_details($value->friend_user_id,$getuserfriend));exit;
						}
					}
				}
				if(count($friendsList) > 0)
				{
					$return_res = array();
					$return_res['responsecode'] 	= "200";
					$return_res['responsedetails'] 	= "success";
					$return_res['data']['friends'] 	= $friendsList;		
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "201";
					$return_res['responsedetails'] = "These user doesn't have any friends";
					return $return_res;
				}				
			}
			else{
				
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "These user doesn't have any friends";
				return $return_res;
			}
		}
		else{
			
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "This user id doesn’t exists";
			return $return_res;
		}
		exit;
	}
	
	/*API to check friend is active or not*/
	function get_Friend_Status($friendid)
	{
		$this->db->select('userId');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $friendid);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
        $query = $this->db->get();
		$numRows = $query->num_rows();
		if($numRows == 1){
			return 1;
		} else {
			return 0;
		}
	}
	
	/*API Fucntion to get the users block list*/
	function get_Block_List($data){
		
		unset($data['api_key']);
		//To check if the userId is Exists or not
		$this->db->select('userId');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
        $query = $this->db->get();
		$numRows = $query->num_rows();
        if($numRows == 1){
			// To check if the user has any friends
			$this->db->select('friend_user_id');
			$this->db->from('hiprofile_blocked_users');
			$this->db->where('userId', $data['userID']);
			$this->db->where('blocked_status','1');
			$this->db->order_by('friend_user_id', 'desc');
			$query = $this->db->get();
			$user = $query->result();
			$numRows = $query->num_rows();
			if($numRows >= 1){
				
				$user = $query->result();
				
				foreach($user as $key=>$value)
				{
					//Function call to check the social and friend status with the user id and friend id.
					$getuserblockedfriend = $this->get_user_status_friend_social($data['userID'],$value->friend_user_id);
					
					//Function to find the earned points of the user and friend user
					$getearnedpoints = $this->get_user_earned_points($data['userID'],$value->friend_user_id);
					
					//Function call to get the user details.					
					$blockedfriendsList[] = $this->get_friend_requested_user_details($value->friend_user_id,$getuserblockedfriend,$getearnedpoints,$data['userID']);
				}	
				$return_res['responsecode'] 	= "200";
				$return_res['responsedetails'] 	= "success";
				$return_res['data']['friends'] 	= $blockedfriendsList;		
				return $return_res;
			}
			else{
				
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "These user doesn't have any blocked friends";
				return $return_res;
			}
		}
		else{
			
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "This user id doesn’t exists";
			return $return_res;
		}
		exit;
	}
	
	/*API Fucntion to Block as user if status is true /  unblock an user if status is false*/
	function to_Block_Unblock_User($data){
		unset($data['api_key']);
		//To check block status true or false
		$blockstatus = strtolower($data['status']);
		$isfriend = $this->isFriednAccepted($data['userID'],$data['friend_user_id']);
		$isfriendNumRows = count($isfriend);
		
		if($isfriendNumRows == 1)
		{
			$cntResult=0;
			$myId = $data['userID'];
			$friendId = $data['friend_user_id'];
			$cntResult = $this->checkMyBlockStatus($myId,$friendId);
			
			if($cntResult>0)
			{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "This user id doesn’t exists";
			return $return_res;	
			}else
			{ 
		
				if($blockstatus == 'true'){
					
						$checkuserblocked = $this->checUserBlockedStatus($data['userID'],$data['friend_user_id']);
						if($checkuserblocked == 1){
							
							// fucntion to fetch the name of the blocked user
							$blocked_user_name = $this->getFriendName($data['friend_user_id']);
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['responsedetails'] = "Unfriended {".$blocked_user_name."}";
							return $return_res;
						}
						else{
							
							$insertdetail = array(
							'friends_id' => $isfriend[0]['id'],
							'userId'  => $isfriend[0]['userId'],
							'friend_user_id'  => $isfriend[0]['friend_user_id'],
							'blocked_status'  => "1"
							);
							$this->db->insert('hiprofile_blocked_users', $insertdetail);
							// fucntion to fetch the name of the unblocked user
							$blocked_user_name = $this->getFriendName($data['friend_user_id']);
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['responsedetails'] = "Unfriended {".$blocked_user_name."}";
							return $return_res;
						}
					}else if($blockstatus == 'false')
					{
						$checkuserblocked = $this->checUserBlockedStatus($data['userID'],$data['friend_user_id']);
						if($checkuserblocked == 1)
						{	
							
							$insertdetail = array(
							'userId' => $data['userID'],
							'friend_id'  => $data['friend_user_id'],
							);
							$this->db->insert('hiprofile_deleted_blocked_users', $insertdetail);
							if ($this->db->affected_rows() >= 0)
							{
								$this->db->where('userId',$data['userID']);
								$this->db->where('friend_user_id',$data['friend_user_id']);
								$this->db->delete('hiprofile_blocked_users');
								if ($this->db->affected_rows() >= 0)
								{
									$this->db->where('userId',$data['userID']);
									$this->db->where('friend_user_id',$data['friend_user_id']);
									$this->db->delete('hiprofile_friends');
									if ($this->db->affected_rows() >= 0)
									{
										$this->db->where('userId',$data['friend_user_id']);
										$this->db->where('friend_user_id',$data['userID']);
										$this->db->delete('hiprofile_friends');
										
										//to delete the details from the notificationList
										$this->socialapi_model->deleteRequest($data['userID'],$data['friend_user_id']);
										$this->socialapi_model->deleteMyRequest($data['userID'],$data['friend_user_id']);
										
										// fucntion to fetch the name of the unblocked user
										$unblocked_user_name = $this->getFriendName($data['friend_user_id']);
										$return_res['responsecode'] = "200";
										$return_res['responsedetails'] = "Unfriended {".$unblocked_user_name."}";
										return $return_res;
									}	
									else
									{
										$return_res = array();
										$return_res['responsecode'] = "500";
										$return_res['status'] = "Something went wrong. Please try again later.";
										return $return_res;
									}
								}
								else
								{
									$return_res = array();
									$return_res['responsecode'] = "500";
									$return_res['status'] = "Something went wrong. Please try again later.";
									return $return_res;
								}
							}
							else
							{
								$return_res = array();
								$return_res['responsecode'] = "500";
								$return_res['status'] = "Something went wrong. Please try again later.";
								return $return_res;
							}
						}
						else
						{
							$checkUserUnblockStatus = $this->checUserUnBlockedStatus($data['userID'],$data['friend_user_id']);
							if($checkUserUnblockStatus == 1)
							{	
								$blocked_user_name = $this->getFriendName($data['friend_user_id']);
								$return_res = array();
								$return_res['responsecode'] = "200";
								$return_res['responsedetails'] = "Unfriended {".$blocked_user_name."}";
								return $return_res;
								
								/*$return_res['responsecode'] = "201";
								$return_res['responsedetails'] = "User Already Unblocked";
								return $return_res;*/
							}
							else
							{
								$return_res = array();
								$return_res['responsecode'] = "201";
								$return_res['responsedetails'] = "These user doesn't have any blocked friends";
								return $return_res;
							}
							
						}
					}
		
			}
		}
		else{
			
			$cntResult=0;
			$myId = $data['userID'];
			$friendId = $data['friend_user_id'];
			$cntResult = $this->checkMyBlockStatus($myId,$friendId);
			
			if($cntResult>0)
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "This user id doesn’t exists";
				return $return_res;	
			}
			else
			{ 
				if($blockstatus == 'true')
				{
				
					$checkuserblocked = $this->checUserBlockedStatus($data['userID'],$data['friend_user_id']);
					if($checkuserblocked == 1)
					{
						
						// fucntion to fetch the name of the blocked user
						$blocked_user_name = $this->getFriendName($data['friend_user_id']);
						$return_res = array();
						$return_res['responsecode'] = "200";
						$return_res['responsedetails'] = "Unfriended {".$blocked_user_name."}";
						return $return_res;
					}
					else
					{
						
						$insertdetail = array(
						'friends_id' => null,
						'userId'  => $data['userID'],
						'friend_user_id'  => $data['friend_user_id'],
						'blocked_status'  => "1",
						'is_friend' => "1"
						);
						$this->db->insert('hiprofile_blocked_users', $insertdetail);
						// fucntion to fetch the name of the unblocked user
						$blocked_user_name = $this->getFriendName($data['friend_user_id']);
						$return_res = array();
						$return_res['responsecode'] = "200";
						$return_res['responsedetails'] = "Unfriended {".$blocked_user_name."}";
						return $return_res;
					}
				}
				else if($blockstatus == 'false')
				{
					$checkuserblocked = $this->checUserBlockedStatus($data['userID'],$data['friend_user_id']);
					if($checkuserblocked == 1)
					{	
						$this->db->where('userId',$data['userID']);
						$this->db->where('friend_user_id',$data['friend_user_id']);
						$this->db->delete('hiprofile_blocked_users');
						if ($this->db->affected_rows() >= 0)
						{
							$this->db->where('userId',$data['userID']);
							$this->db->where('friend_user_id',$data['friend_user_id']);
							$this->db->delete('hiprofile_friends');
							if ($this->db->affected_rows() >= 0)
							{
								$this->db->where('userId',$data['friend_user_id']);
								$this->db->where('friend_user_id',$data['userID']);
								$this->db->delete('hiprofile_friends');
								
								//to delete the details from the notificationList
								$this->socialapi_model->deleteRequest($data['userID'],$data['friend_user_id']);
								$this->socialapi_model->deleteMyRequest($data['userID'],$data['friend_user_id']);
								
								// fucntion to fetch the name of the unblocked user
								$unblocked_user_name = $this->getFriendName($data['friend_user_id']);
								$return_res['responsecode'] = "200";
								$return_res['responsedetails'] = "Unfriended {".$unblocked_user_name."}";
								return $return_res;
							}	
							else
							{
								$return_res = array();
								$return_res['responsecode'] = "500";
								$return_res['status'] = "Something went wrong. Please try again later.";
								return $return_res;
							}
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "500";
							$return_res['status'] = "Something went wrong. Please try again later.";
							return $return_res;
						}
					}
					else
					{
						$checkUserUnblockStatus = $this->checUserUnBlockedStatus($data['userID'],$data['friend_user_id']);
						if($checkUserUnblockStatus == 1)
						{	
							$blocked_user_name = $this->getFriendName($data['friend_user_id']);
							$return_res = array();
							$return_res['responsecode'] = "200";
							$return_res['responsedetails'] = "Unfriended {".$blocked_user_name."}";
							return $return_res;
							
							/*$return_res['responsecode'] = "201";
							$return_res['responsedetails'] = "User Already Unblocked";
							return $return_res;*/
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "201";
							$return_res['responsedetails'] = "These user doesn't have any blocked friends";
							return $return_res;
						}
						
					}
				}
			}
		}
		exit;
	}
	
	
	/*API Fucntion to Block an User for particular timeframe*/
	function to_blocKUserTimeFrame($data){
		unset($data['api_key']);
		//To check block status true or false
		$timeframe = strtolower($data['timeframe']);
		$isfriend = $this->isFriednAccepted($data['userID'],$data['friend_user_id']);
		$isfriendNumRows = count($isfriend);
		
		if($isfriendNumRows == 1)
		{
			$myId = $data['userID'];
			$friendId = $data['friend_user_id'];
			$checkMyStatus = $this->checkMyBlockStatus($data['userID'],$data['friend_user_id']);
			$checkMyFriendStatus = $this->checkMyFriendBlockStatus($data['userID'],$data['friend_user_id']);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{
				// 'd or h' is in the $timeframe
				if (stripos(strtolower($timeframe), 'd') || stripos(strtolower($timeframe), 'h') !== false) 
				{
					$val = "";
					$days = "";
					if(stripos(strtolower($timeframe), 'd') == true)
					{
						$timeframe = str_replace("d","",$timeframe);
						if($timeframe <= 30)
						{
							//To check the time frame is valid and not greater than 30
							$expr = '/^[1-9][0-9]{0,30}$/';
							if (preg_match($expr, $timeframe) && filter_var($timeframe, FILTER_VALIDATE_INT)) 
							{
								$val = 24*$timeframe;
								$days = $timeframe;
							} 
							else 
							{
								$return_res = array();
								$return_res['responsecode'] = "501";
								$return_res['responsedetails'] = "Enter correct timeframe value.";
								return $return_res;	
							}
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "501";
							$return_res['responsedetails'] = "Enter correct timeframe value.";
							return $return_res;
						}
					}
					elseif(stripos(strtolower($timeframe), 'h') == true)
					{
						$timeframe = str_replace("h","",$timeframe);
						if($timeframe <= 24)
						{
							$val = $timeframe;
							$days = $val/24;
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "501";
							$return_res['responsedetails'] = "Enter correct timeframe value.";
							return $return_res;
						}
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "501";
						$return_res['responsedetails'] = "Enter correct timeframe value.";
						return $return_res;
					}
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "501";
					$return_res['responsedetails'] = "Enter correct timeframe value.";
					return $return_res;
				}
				
				//To insert the timeframe block data
				$current_date_time  = $this->get_current_datime();
				//add the days with current date time with the user defined value
				$block_date = "+".$val." hour";
				$block_end_date =  date('Y-m-d H:i',strtotime($block_date,strtotime($current_date_time)));
				$insertdetail = array(
					'userId' => $data['userID'],
					'friend_id'  => $data['friend_user_id'],
					'start_block_current_time' => $current_date_time,
					'end_block_current_time' => $block_end_date,
					'hours'=>$val,
					'days'=>$days
				);
				$this->db->insert('hiprofile_blocked_timeframe_friends', $insertdetail);
				if($this->db->affected_rows() >= 0)
				{
					$insertblockdetails = array(
						'friends_id' => $isfriend[0]['id'],
						'userId'  => $isfriend[0]['userId'],
						'friend_user_id'  => $isfriend[0]['friend_user_id'],
						'blocked_status'  => "1"
					);
					$this->db->insert('hiprofile_blocked_users', $insertblockdetails);
					// fucntion to fetch the name of the unblocked user
					$unfriendedName = $this->getFriendName($isfriend[0]['friend_user_id']);
					$return_res['responsecode'] = "200";
					$return_res['responsedetails'] = "Unfriended {".$unfriendedName."}";
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
				
			}
			else
			{ 
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "This user id doesn’t exists";
				return $return_res;	
			}
		}
		else
		{
			
			$myId = $data['userID'];
			$friendId = $data['friend_user_id'];
			$checkMyStatus = $this->checkMyBlockStatus($data['userID'],$data['friend_user_id']);
			$checkMyFriendStatus = $this->checkMyFriendBlockStatus($data['userID'],$data['friend_user_id']);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{
				// 'd or h' is in the $timeframe
				if (stripos(strtolower($timeframe), 'd') || stripos(strtolower($timeframe), 'h') !== false) 
				{
					$val = "";
					$days = "";
					if(stripos(strtolower($timeframe), 'd') == true)
					{
						$timeframe = str_replace("d","",$timeframe);
						if($timeframe <= 30)
						{
							//To check the time frame is valid and not greater than 30
							$expr = '/^[1-9][0-9]{0,30}$/';
							if (preg_match($expr, $timeframe) && filter_var($timeframe, FILTER_VALIDATE_INT)) 
							{
								$val = 24*$timeframe;
								$days = $timeframe;
							} 
							else 
							{
								$return_res = array();
								$return_res['responsecode'] = "501";
								$return_res['responsedetails'] = "Enter correct timeframe value.";
								return $return_res;	
							}
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "501";
							$return_res['responsedetails'] = "Enter correct timeframe value.";
							return $return_res;
						}
					}
					elseif(stripos(strtolower($timeframe), 'h') == true)
					{
						$timeframe = str_replace("h","",$timeframe);
						if($timeframe <= 24)
						{
							$val = $timeframe;
							$days = $val/24;
						}
						else
						{
							$return_res = array();
							$return_res['responsecode'] = "501";
							$return_res['responsedetails'] = "Enter correct timeframe value.";
							return $return_res;
						}
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "501";
						$return_res['responsedetails'] = "Enter correct timeframe value.";
						return $return_res;
					}
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "501";
					$return_res['responsedetails'] = "Enter correct timeframe value.";
					return $return_res;
				}
				
				//To insert the timeframe block data
				$current_date_time  = $this->get_current_datime();
				//add the days with current date time with the user defined value
				$block_date = "+".$val." hour";
				$block_end_date =  date('Y-m-d H:i',strtotime($block_date,strtotime($current_date_time)));
				$insertdetail = array(
					'userId' => $data['userID'],
					'friend_id'  => $data['friend_user_id'],
					'start_block_current_time' => $current_date_time,
					'end_block_current_time' => $block_end_date,
					'hours'=>$val,
					'days'=>$days,
					'is_friend'  => "1"
				);
				$this->db->insert('hiprofile_blocked_timeframe_friends', $insertdetail);
				if($this->db->affected_rows() >= 0)
				{
					$insertblockdetails = array(
						'friends_id' => null,
						'userId'  => $data['userID'],
						'friend_user_id'  => $data['friend_user_id'],
						'blocked_status'  => "1",
						'is_friend'  => "1"
					);
					$this->db->insert('hiprofile_blocked_users', $insertblockdetails);
					// fucntion to fetch the name of the unblocked user
					$unfriendedName = $this->getFriendName($data['friend_user_id']);
					$return_res['responsecode'] = "200";
					$return_res['responsedetails'] = "Unfriended {".$unfriendedName."}";
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
				
			}
			else
			{ 
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "This user id doesn’t exists";
				return $return_res;	
			}
		}
		exit;
	}
	
	/*API Fucntion to unblock an User from a particular timeframe*/
	function to_unBlocKUserTimeFrame($data)
	{
		unset($data['api_key']);
		$userid = "";
		$friendid = "";
		$current_date_time  = $this->get_current_datime();
		$this->db->select('*');
		$this->db->from('hiprofile_blocked_timeframe_friends');
		$this->db->where('date_format(end_block_current_time,"%Y-%m-%d %h:%i:%s") <', $current_date_time);
		$this->db->where('status',0);
		$query = $this->db->get();
		$blocked_user = $query->result_array();
		$numRows = $query->num_rows();
		if($numRows >= 1)
		{
			foreach($blocked_user as $value)
			{
				/*update the friend timeframe*/
				$this->db->set('status',1);
				$this->db->where_in('userId',$value['userId']);
				$this->db->where_in('friend_id',$value['friend_id']);
				$this->db->update('hiprofile_blocked_timeframe_friends');
				
				/*delete the record from the blocked users table*/
				$this->db->where_in('userId',$value['userId']);
				$this->db->where_in('friend_user_id',$value['friend_id']);
				$this->db->delete('hiprofile_blocked_users');
				
				//to delete the details from the notificationList
				$this->socialapi_model->deleteRequest($data['userID'],$data['friend_user_id']);
				$this->socialapi_model->deleteMyRequest($data['userID'],$data['friend_user_id']);
			}
			/*To send the response after query is executed successfully*/
			$return_res = array();
			$return_res['responsecode'] = "200";
			$return_res['responsedetails'] = "success";
			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "No records found.";
			return $return_res;
		}
		exit;
	}
	
	/*API Fucntion to Unfriend an user*/
	function to_Unfirend_User($data){
		unset($data['api_key']);
	
		$isfriend = $this->isFriednAccepted($data['userID'],$data['friend_user_id']);
		$isfriendNumRows = count($isfriend);
		if($isfriendNumRows == 1)
		{
			$checkMyStatus = $this->checkMyBlockStatus($data['userID'],$data['friend_user_id']);
			$checkMyFriendStatus = $this->checkMyFriendBlockStatus($data['userID'],$data['friend_user_id']);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{
					
				$insertdetail = array(
				'userId' => $data['userID'],
				'friend_id'  => $data['friend_user_id'],
				);
				$this->db->insert('hiprofile_deleted_unfriend_lists', $insertdetail);
				if ($this->db->affected_rows() >= 0)
				{
					$this->db->where('userId',$data['userID']);
					$this->db->where('friend_user_id',$data['friend_user_id']);
					$this->db->delete('hiprofile_friends');
					if ($this->db->affected_rows() >= 0)
					{
						$this->db->where('userId',$data['friend_user_id']);
						$this->db->where('friend_user_id',$data['userID']);
						$this->db->delete('hiprofile_friends');
						
						//to delete the details from the notificationList
						$this->socialapi_model->deleteRequest($data['userID'],$data['friend_user_id']);
						
						$this->socialapi_model->deleteMyRequest($data['userID'],$data['friend_user_id']);
						
						//to delete the details from the social friends of the user and friend user
						$this->socialapi_model->toDeleteMySocailFriends($data['userID'],$data['friend_user_id']);
						
						$this->socialapi_model->toDeleteSocailFriendsofMy($data['userID'],$data['friend_user_id']);
										
						// fucntion to fetch the name of the unblocked user
						$unfriendedName = $this->getFriendName($data['friend_user_id']);
						$return_res['responsecode'] = "200";
						$return_res['responsedetails'] = "Unfriended {".$unfriendedName."}";
						return $return_res;
					}	
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
			}
			else
			{ 
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "This user id doesn’t exists";
				return $return_res;	
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "This user is not your friend";
			return $return_res;	
		}
		
	}
	
	
	/*To add the facebook friend list sync*/
	function addFriendList($data)
	{
		unset($data['api_key']);
		
		$this->db->select('userId');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
        $query = $this->db->get();
		$numRows = $query->num_rows();
		//Check user active or not
		if($numRows == 1) 
		{ 
			//convert json array to normal php array
			$arr = json_decode($data['facebook_friends'],true);
			foreach($arr as $values)
			{
				$this->db->trans_start();
				$this->db->set('facebook_friend_name', $values['name']);
				$this->db->set('facebook_fb_id', $values['id']);
				$this->db->set('userId', $data['userID']);
				$this->db->insert('hiprofile_fb_friends');
				$this->db->trans_complete();
			}
			if ($this->db->trans_status() === FALSE)
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "200";
				$return_res['responsedetails'] = "Success";
				return $return_res;
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "This user id doesn’t exists";
			return $return_res;	
		}
		
		
	}
	
	/*To get the facebook friend list sync*/
	function getFriendList($data)
	{
		unset($data['api_key']);
		
		$this->db->select('userId');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
        $query = $this->db->get();
		$numRows = $query->num_rows();
		//Check user active or not
		if($numRows == 1) 
		{ 
			$frinedllsit = $this->getUserFriendsListAll($data['userID']);
			if(empty($frinedllsit))
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "These user doesn't have any friends";
				return $return_res;	
			}
			else
			{
				
				$myId = $data['userID'];	
				$friendid = '';
				foreach($frinedllsit as $friends)
				{
					$friendid .= $this->getusetFromFbId($friends->facebook_fb_id).",";
				}
				// implode  the values and remove comma at last
				$friendIDS_implode = rtrim($friendid,',');
				// explode  the values and store in array
				$frnd_explode = explode(",",$friendIDS_implode);
				//remove the empty elements from the array
				$frnd_remove_empty = array_filter($frnd_explode);
				//reset the array
				$frndids = array_values($frnd_remove_empty);
				if(empty($frndids[0]) || empty($frndids))
				{
					$return_res = array();
					$return_res['responsecode'] = "201";
					$return_res['responsedetails'] = "These user doesn't have any friends";
					return $return_res;	
				}
				else
				{
					foreach($frndids as $myfrndid)
					{
						//To check the friend blocked by any of them
						$checkMyStatus = $this->checkMyBlockStatus($data['userID'],$myfrndid);
						$checkMyFriendStatus = $this->checkMyFriendBlockStatus($data['userID'],$myfrndid);
						if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
						{
							//Function call to check the social and friend status with the user id and friend id.
							$getuserfriend = $this->get_user_status_friend_social($data['userID'],$myfrndid);
						
							//Function to fetch the earned points
							$getearnedpoints = $this->get_user_earned_points($data['userID'],$myfrndid);
										
							$friendsList[] = $this->get_friend_requested_user_details($myfrndid,$getuserfriend,$getearnedpoints,$data['userID']);
						}	
					}
					if(count($friendsList) > 0)
					{
						$return_res = array();
						$return_res['responsecode'] 	= "200";
						$return_res['responsedetails'] 	= "success";
						$return_res['data']['friends'] 	= $friendsList;		
						return $return_res;
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "201";
						$return_res['responsedetails'] = "These user doesn't have any friends";
						return $return_res;
					}
				}
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "This user id doesn’t exists";
			return $return_res;	
		}
		
		
	}
	
	/*Function to get all the user friends */
	function getusetFromFbId($fbid)
	{
		$this->db->select('BaseTbl.userId as userID');
		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where('social.fbuserid', $fbid);
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$query = $this->db->get();
		$user = $query->result();
		return $user[0]->userID;
	}
	
	/*Function to get all the user friends */
	function getUserFriendsListAll($userId)
	{
		$this->db->select('facebook_fb_id');
        $this->db->from('hiprofile_fb_friends');
        $this->db->where('userId', $userId);
        $query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	// fucntion to get the name of the user id passed
	/*
	* Returns name of the user
	*/
	function getFriendName($friendId){
		
		$this->db->select('name');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $friendId);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId !=', 1);
		$query = $this->db->get();
		$user = $query->result();
		return $user[0]->name;
		
	}
	
	
	// fucntion to check if the user is been already blocked or not
	/*
	* Returns numofrows as 1 if already blocked or 0 if not value is been 
	*/
	function changeRequestStausUnblock($userid,$friend_id){
		
		
		$this->db->select('*');
        $this->db->from('hiprofile_blocked_users');
        $this->db->where('userId', $userid);
		$this->db->where('friend_user_id', $friend_id);
		$this->db->where('blocked_status', "1");
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check if the user is been already blocked or not
	/*
	* Returns numofrows as 1 if already blocked or 0 if not value is been 
	*/
	function checUserBlockedStatus($userid,$friend_id){
		
		
		$this->db->select('*');
        $this->db->from('hiprofile_blocked_users');
        $this->db->where('userId', $userid);
		$this->db->where('friend_user_id', $friend_id);
		$this->db->where('blocked_status', "1");
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check if the user is been already unblocked or not
	/*
	* Returns numofrows as 1 if already blocked or 0 if not value is been 
	*/
	function checUserUnBlockedStatus($userid,$friend_id){
		
		
		$this->db->select('*');
        $this->db->from('hiprofile_blocked_users');
        $this->db->where('userId', $userid);
		$this->db->where('friend_user_id', $friend_id);
		$this->db->where('blocked_status', "0");
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	
	// fucntion to check if Friend is blocked me or not
	/*
	* Returns numofrows as 1 if already blocked or 0 if not value is been 
	*/
	function checkMyBlockStatus($myId,$friend_id){
		
		
		$this->db->select('*');
        $this->db->from('hiprofile_blocked_users');
        $this->db->where('userId', $friend_id);
		$this->db->where('friend_user_id', $myId);
		$this->db->where('blocked_status', "1");
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check i am blocked my Friend
	/*
	* Returns numofrows as 1 if already blocked or 0 if not value is been 
	*/
	function checkMyFriendBlockStatus($myId,$friend_id){
		
		
		$this->db->select('*');
        $this->db->from('hiprofile_blocked_users');
        $this->db->where('userId', $myId);
		$this->db->where('friend_user_id', $friend_id);
		$this->db->where('blocked_status', "1");
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	//Function to check the user and friend request status
	/*
	* Return the details of the request status
	*/
	function requestStatus($userid,$frienduserid){
		
		$this->db->select('friend.request_status as friendRequestStatus');
		$this->db->from('hiprofile_friends as friend');
		$this->db->where('friend.userId', $userid);
		$this->db->where_in('friend.friend_user_id', $frienduserid);
		$query = $this->db->get();
		$status = $query->result_array();
		return $status;
	}
	
	//Fucntion to check the users Pending request
	/*
	* Return All data
	*/
	function pendingRequestList($userid){
		
		$this->db->select('*');
		$this->db->from('hiprofile_friends as friend');
		$this->db->where('friend.userId', $userid);
		$this->db->where('friend.add_friend', 'true');
		$this->db->where('friend.request_status', '0');
		$query = $this->db->get();
		$pending_status = $query->result_array();
		return $pending_status;
	}
	
	//Fucntion to check the users Approved request
	/*
	* Return All data
	*/
	function approvedRequestList($userid){
		
		$this->db->select('*');
		$this->db->from('hiprofile_friends as friend');
		$this->db->where('friend.userId', $userid);
		$this->db->where('friend.add_friend', 'true');
		$this->db->where('friend.request_status', '1');
		$query = $this->db->get();
		$approved_status = $query->result_array();
		return $approved_status;
	}
	
	//Function to check the user and friend user id are is friends
	/*
	* Return numofrows 1 or 0
	*/
	function isFriednAccepted($userid,$friendid){
		//To check if the userId is Exists or not
		$this->db->select('*');
		$this->db->from('hiprofile_friends');
		$this->db->where('userId', $userid);
		$this->db->where('friend_user_id', $friendid);
		$this->db->where('add_friend', 'true');
		$this->db->where('request_status','1');
		$this->db->order_by('friend_user_id', 'desc');
		$query = $this->db->get();
		$user = $query->result_array();
		return $user;
	}
	
	// fucntion to check if the friend is been unblocked by me or not
	/*
	* Returns numofrows as 1 if already unblocked or 0 if not value is been 
	*/
	function checkMyUnBlockStatus($myId,$friend_id){
		
		
		$this->db->select('*');
        $this->db->from('hiprofile_deleted_blocked_users');
        $this->db->where('userId', $friend_id);
		$this->db->where('friend_id', $myId);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check friend blocked and unblocked
	/*
	* Returns numofrows as 1 if already ublocked or 0 if not value is been 
	*/
	function checkMyFriendUnBlockStatus($myId,$friend_id){
				
		$this->db->select('*');
        $this->db->from('hiprofile_deleted_blocked_users');
        $this->db->where('userId', $myId);
		$this->db->where('friend_id', $friend_id);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	//Fucntion to load the friends data of the user id
	function get_user_status_friend_social($userId,$friend_user_id){
		
		$user[] = new stdClass();
		//$this->db->select('friend.request_status as friend_req_status');
		$this->db->select('friend.request_status as friend_req_status,friend.friend_user_id,friend.userId');
		$this->db->from('hiprofile_friends as friend');
		//$this->db->where('friend.userId', $userId);
		//$this->db->where('friend.friend_user_id', $friend_user_id);
		
		$andwhere = '(friend.friend_user_id='.$friend_user_id.' and friend.userId = '.$userId.')';
		$orwhere = '(friend.friend_user_id='.$userId.' and friend.userId = '.$friend_user_id.')';
		$this->db->where($andwhere);
		$this->db->or_where($orwhere);
		
		$this->db->order_by('friend.friend_user_id', 'desc');
		$query = $this->db->get();
		$user1 = $query->result();
		$friendsacceptcount = count($user1);
		if($friendsacceptcount>0){
			for($i=0;$i<$friendsacceptcount;$i++)
			{
				if($user1[$i]->friend_user_id == $friend_user_id && $user1[$i]->userId==$userId)
				{	
					if($user1[$i]->friend_req_status == '1'):
						$user[$i]->friend_req_status = 'approved';
						$user[$i]->is_friend = 'true';
					elseif($user1[$i]->friend_req_status == '2'):
						$user[$i]->friend_req_status = 'rejected';
						$user[$i]->is_friend = 'false';
					elseif($user1[$i]->friend_req_status == '0'):
						$user[$i]->friend_req_status = 'pending';
						$user[$i]->is_friend = 'false';
					endif;	
				}else if($user1[$i]->friend_user_id == $userId && $user1[$i]->userId==$friend_user_id)
				{	
				
					if($user1[$i]->friend_req_status == '1'):
						$user[$i]->friend_req_status = 'approved';
						$user[$i]->is_friend = 'true';
					elseif($user1[$i]->friend_req_status == '2'):
						$user[$i]->friend_req_status = 'rejected';
						$user[$i]->is_friend = 'false';
					elseif($user1[$i]->friend_req_status == '0'):
						$user[$i]->friend_req_status = 'received';
						$user[$i]->is_friend = 'false';
					endif;
			
				}
				
			}
		}else
		{
			
					$user[0]->friend_req_status = 'none';
					$user[0]->is_friend = 'false';	
		}
		$this->db->select('
		social.facebook_status as req_fb_status,
		social.twitter_status as req_twitter_status,
		social.instagram_status as req_insta_status,
		social.contact_status as req_contact_status,
		social.request_status
		');
		$this->db->from('hiprofile_social_friends as social');
		$this->db->where('social.userId', $userId);
		$this->db->where('social.friend_user_id', $friend_user_id);
		$this->db->order_by('social.friend_user_id', 'desc');
		$query = $this->db->get();
		$user2 = $query->result();
		$friendsacceptcount = count($user2);
		// echo $friendsacceptcount;
		//pre($user2);
		$constval = 0;
		$status_val = array("0"=>"none","1"=>"approved","2"=>"rejected","3"=>"cancelled","4"=>"pending");
		$status_Arr = array("req_fb_status","req_twitter_status","req_insta_status","req_contact_status");
		
		if($friendsacceptcount!= 0){
			$j = 0;
			for($i=0;$i<$friendsacceptcount;$i++){
				$temp_arr[] = new stdClass();
				
				if($user2[$i]->req_fb_status == 1) {
					$temp_arr[$j]->req_fb_status = $status_val[$user2[$i]->request_status];
				} 
				
				if($user2[$i]->req_twitter_status == 1) {
					$temp_arr[$j]->req_twitter_status = $status_val[$user2[$i]->request_status];
				} 
				
				if($user2[$i]->req_insta_status == 1) {
					$temp_arr[$j]->req_insta_status = $status_val[$user2[$i]->request_status];
				} 
				if($user2[$i]->req_contact_status == 1) {
					$temp_arr[$j]->req_contact_status = $status_val[$user2[$i]->request_status];
				} 
				$temp_arr[$j]->i = $i;
				$j++;
			}
			$i =0;
			$final_val = array();
			
			foreach($temp_arr as $key=>$value) {
				
				if($value->req_insta_status) {
					$final_val['req_insta_status'] = $value->req_insta_status;
				}
				if($value->req_contact_status) {
					$final_val['req_contact_status'] = $value->req_contact_status;
				}
				if($value->req_fb_status) {
					$final_val['req_fb_status'] = $value->req_fb_status;
				}
				if($value->req_twitter_status) {
					$final_val['req_twitter_status'] = $value->req_twitter_status;
				}
				if($value->i) {
					$final_val['i'] = $value->i;
				}
				
			}
			$final_keys = array_keys($final_val);
			
			$final_com = array_diff($status_Arr, $final_keys);
		
			if(count($final_com) >= 1) {
				foreach($final_com as $value){
					//pre($value);exit;
					if($value == "req_fb_status")      { $user[$i]->req_fb_status = "none"; }
					if($value == "req_twitter_status") { $user[$i]->req_twitter_status = "none"; }
					if($value == "req_insta_status")   { $user[$i]->req_insta_status = "none"; }
					if($value == "req_contact_status") { $user[$i]->req_contact_status = "none"; }
					
				}
			} 
			// pre($final_val);exit;
			if(count($final_val) >= 1) {
				foreach($final_val as $key=>$value){
					// echo $value;exit;
					if($key == "req_fb_status")      { $user[$i]->req_fb_status = $value; }
					if($key == "req_twitter_status") { $user[$i]->req_twitter_status = $value; }
					if($key == "req_insta_status")   { $user[$i]->req_insta_status = $value; }
					if($key == "req_contact_status") { $user[$i]->req_contact_status = $value; }
					
				}
			} 
		
		}else{
			$user[0]->req_fb_status	='none';
			$user[0]->req_contact_status='none';	
			$user[0]->req_twitter_status='none';
			$user[0]->req_insta_status='none';
		}
		return $user;
	}
	
	//Fucntion To get user details when sending friend request
	function get_friend_requested_user_details($frnduserid,$getuserfriend,$getearnedpoints,$userid){
		
		$this->load->model('userapi_model');
		$this->db->select('BaseTbl.userId as userID,BaseTbl.name, BaseTbl.location_disclose, BaseTbl.dob, BaseTbl.gender, BaseTbl.gender_disclose,	BaseTbl.mobile as mobileNumber, BaseTbl.email,BaseTbl.email_status, BaseTbl.username as userName,BaseTbl.rating,BaseTbl.feedback,BaseTbl.approachability,BaseTbl.share_mobile,social.social_rank,social.rank,social.rank_f2,social.points,social.worth,social.bio,social.points_required,social.fbuserid as fbUserId,twitteruserid as twitterUserId, instagramuserid as instagramUserId, social.facebook_profile_url as fbURL,social.twitter_profile_url as twitterURL, instagram_profile_url as instagramURL,social.profileimage as image_url');

		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where_in('BaseTbl.userId', $frnduserid);
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$this->db->order_by('BaseTbl.userId', 'desc');
		$query = $this->db->get();
		$user = $query->result();
		$usercount = count($user);
		$users[] = new stdClass();
		$userdetails = array();
		if($usercount != 0)
		{
			for($i=0;$i<$usercount;$i++){

				$users[$i]->userID = $user[$i]->userID;
				$users[$i]->name = $user[$i]->name;
				$users[$i]->dob = $user[$i]->dob;
				$users[$i]->gender = strtolower($user[$i]->gender);
				$users[$i]->gender_disclose = $user[$i]->gender_disclose;
				$users[$i]->mobileNumber = $user[$i]->mobileNumber;
				$users[$i]->email = $user[$i]->email;
				$users[$i]->email_status = $user[$i]->email_status;
				$users[$i]->userName = $user[$i]->userName;
				$users[$i]->points = $user[$i]->points;
				$users[$i]->rating = $user[$i]->rating;
				$users[$i]->feedback = $user[$i]->feedback;
				$users[$i]->approachability = $user[$i]->approachability;
				//$users[$i]->worth = $user[$i]->worth;
				$users[$i]->worth = $this->userapi_model->getCalculateWorth($userid,$frnduserid);
				$users[$i]->bio = $user[$i]->bio;
				$users[$i]->registration_timestamp = json_decode(json_encode(time()), FALSE);
				$users[$i]->fbUserId = ($user[$i]->fbUserId == 0)?"":$user[$i]->fbUserId;
				$users[$i]->twitterUserId = ($user[$i]->twitterUserId == 0)?"":$user[$i]->twitterUserId;
				$users[$i]->instagramUserId = ($user[$i]->instagramUserId == 0)?"":$user[$i]->instagramUserId;
				$users[$i]->fbURL = ($user[$i]->fbURL == "")?"":$user[$i]->fbURL;
				$users[$i]->twitterURL = ($user[$i]->twitterURL == "")?"":$user[$i]->twitterURL;
				$users[$i]->instagramURL	 = ($user[$i]->instagramURL == "")?"":$user[$i]->instagramURL;
				$users[$i]->image_url	 = ($user[$i]->image_url == "")?"":base_url().'assets'.$user[$i]->image_url;
				$users[$i]->points_required	 = ($user[$i]->points_required == "")?"":$user[$i]->points_required;
				$users[$i]->share_mobile	 = ($user[$i]->share_mobile == "0")?"false":"true";
				$users[$i]->location_disclose = $user[$i]->location_disclose;
				$users[$i]->rating = ($user[$i]->rating == "")?"0":$user[$i]->rating;
				$users[$i]->feedback = ($user[$i]->feedback == "")?"":$user[$i]->feedback;
				//$users[$i]->rank = strval(round($user[$i]->social_rank + $user[$i]->rank + $user[$i]->rank_f2));
				$users[$i]->rank = strval($user[$i]->social_rank + $user[$i]->rank);
				$users[$i]->earned_points = (string)$getearnedpoints;
				$users[$i]->last_seen = (string)getLastSeenDataUser($frnduserid);
				unset($user[$i]->social_rank);
				unset($user[$i]->rank_f2);	
			
				$userdetails =(object) array_merge((array)$users[$i],(array)$getuserfriend[$i]);	
				
			}
			return $userdetails;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
		
	}
	
	//API function to get the users earned points by adding friend
	function get_user_earned_points($userId,$frinedId)
    {
        $this->db->select('earned_points');
        $this->db->from('hiprofile_earned_user_points');
        $this->db->where('userId', $userId);
		$this->db->where('friend_id', $frinedId);
		$this->db->order_by("earned_points", "desc");
        $query = $this->db->get();
        $earnedPoints = $query->result();
		if(!empty($earnedPoints))
		{
			return $earnedPoints[0]->earned_points;
		}
		else
		{
			$earnedPoints = 0;
			return $earnedPoints;
		}
    }
	
	// fucntion to check if Friend is unfriend me or not
	/*
	* Returns numofrows as 1 if already unfriended or 0 if not value is been 
	*/
	function checkMyUnFriendStatus($myId,$friend_id){
		
		
		$this->db->select('*');
        $this->db->from('hiprofile_deleted_unfriend_lists');
        $this->db->where('userId', $friend_id);
		$this->db->where('friend_id', $myId);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check i am unfriended my Friend
	/*
	* Returns numofrows as 1 if already unfriended or 0 if not value is been 
	*/
	function checkMyFriendUnFriendedStatus($myId,$friend_id)
	{
		
		
		$this->db->select('*');
        $this->db->from('hiprofile_deleted_unfriend_lists');
        $this->db->where('userId', $myId);
		$this->db->where('friend_id', $friend_id);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check if Friend has declined request or not
	/*
	* Returns numofrows as 1 if already request send or 0 if no request sent
	*/
	function checkMyReqDeclinedStatus($myId,$friend_id){		
		$this->db->select('*');
        $this->db->from('hiprofile_declined_friend_requests');
		$this->db->where('userId_requesting', $myId);
		$this->db->where('userId_choice', $friend_id);
		$this->db->where('declined', 1);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	/*To fetch the curren time from the database*/	
	function get_current_datime()	{		
		$this->db->select('Now() as ctime');	
		$query = $this->db->get();	
		$time = $query->result();		
		return $time[0]->ctime;	
	}

	//API TO set favorite list of six photo
	public function favListSixPhotos($data)
	{
		unset($data['api_key']);
		
		//To check if the userId is Exists or not
		$this->db->select('userId');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
        $query = $this->db->get();
		$numRows = $query->num_rows();
        if($numRows == 1)
		{

			$this->db->select('friend_user_id');
			$this->db->from('hiprofile_user_six_photo_favourite_lists');
			$this->db->where('userID',$data['userID']);			
			$this->db->where('image_id',$data['image_id']);			
			$query = $this->db->get();
			$user = $query->result();
			$numRows = $query->num_rows();
			if($numRows >= 1)
			{
				
				$user = $query->result();
				
				foreach($user as $key=>$value)
				{
					
					$checkFriendStatusv = $this->get_Friend_Status($value->friend_user_id);
					if($checkFriendStatusv == 1)
					{
						$checkMyStatus = $this->checkMyBlockStatus($data['userID'],$value->friend_user_id);
						$checkMyFriendStatus = $this->checkMyFriendBlockStatus($data['userID'],$value->friend_user_id);
						if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
						{
							//Function call to check the social and friend status with the user id and friend id.
							$getuserfriend = $this->get_user_status_friend_social($data['userID'],$value->friend_user_id);
						
							//Function to fetch the earned points
							$getearnedpoints = $this->get_user_earned_points($data['userID'],$value->friend_user_id);
										
							$friendsList[] = $this->get_friend_requested_user_details($value->friend_user_id,$getuserfriend,$getearnedpoints,$data['userID']);
							//pre($this->get_friend_requested_user_details($value->friend_user_id,$getuserfriend));exit;
						}
					}
				}
				if(count($friendsList) > 0)
				{

					$return_res = array();
					$return_res['responsecode'] 	= "200";
					$return_res['responsedetails'] 	= "success";
					$return_res['data']['favourite'] 	= $friendsList;		
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "201";
					$return_res['responsedetails'] = "These user doesn't have any friends";
					return $return_res;
				}				
			}
			else{
				
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "These user doesn't have any friends";
				return $return_res;
			}
		}
		else{
			
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "This user id doesn’t exists";
			return $return_res;
		}
		exit;		
			
		
	}

	//To delete the social record of the user from the notofication list
	function updateUsersRank($requester_user_id="", $friend_user_id="",$socialmedia="")
	{
			//add 10 points to user A
			$this->db->set('points', 'points+10');
			$this->db->where('userId', $requester_user_id);
			$this->db->update('hiprofile_user_socialsync');

			//check rank for user A
			$requester_rank_details = $this->db->select('points','rank')
	                ->where('userId', $requester_user_id)
	                ->get('hiprofile_user_socialsync');

	        if($requester_rank_details['points']>1000)
	        {

	        	$this->db->set('old_earned_points', $requester_rank_details['points']);
	        	$this->db->set('old_rank', $requester_rank_details['rank']);
	        	$this->db->set('points', '0');
	        	$this->db->set('rank', 'rank+1');
				$this->db->where('userId', $requester_user_id);
				$this->db->update('hiprofile_user_socialsync');

	        }
			
			//add 10 points to user B
			$this->db->set('points', 'points+10');
			$this->db->where('userId', $friend_user_id);
			$this->db->update('hiprofile_user_socialsync');

			//check rank for user B
			$requester_rank_details = $this->db->select('points','rank')
	                ->where('userId', $friend_user_id)
	                ->get('hiprofile_user_socialsync');

	        //update rank
	        if($requester_rank_details['points']>1000)
	        {

	        	$this->db->set('old_earned_points', $requester_rank_details['points']);
	        	$this->db->set('old_rank', $requester_rank_details['rank']);
	        	$this->db->set('points', '0');
	        	$this->db->set('rank', 'rank+1');
				$this->db->where('userId', $friend_user_id);
				$this->db->update('hiprofile_user_socialsync');

	        }
	

	}
}