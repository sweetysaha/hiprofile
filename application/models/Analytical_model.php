<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Analytical_model extends CI_Model
{
	/***
	**
	1 No of users signed</option>
	2 No of businesses added</option>
	3 No of users boosted there ranks</option>
	4 No of users upgraded their account</option>
	5 No of friends added</option>
	***/
	function generatePdf($fromdate = '', $todate = '', $selectSection = '')
	{
		if($selectSection == 1):
			$this->db->select('*');
			$this->db->from('hiprofile_users');
			$this->db->where('isDeleted', 0);
			$this->db->where('roleId', 0);
			$this->db->where('createdDtm >=', $fromdate);
			$this->db->where('createdDtm <=', $todate);
			$this->db->order_by('createdDtm', 'DESC');
			$query = $this->db->get();
			$results = $query->result_array(); 
			return $results;
		endif;
		if($selectSection == 2):
			$this->db->select('user.name,BaseTbl.business_id,BaseTbl.image_url,BaseTbl.business_title,BaseTbl.address,BaseTbl.country,BaseTbl.state,BaseTbl.city,BaseTbl.pin,BaseTbl.contact,BaseTbl.email ,BaseTbl.day_open_from,BaseTbl.day_open_to,BaseTbl.hours_open_from,BaseTbl.hours_open_to,BaseTbl.latitude,BaseTbl.longitude,BaseTbl.approved_status,BaseTbl.is_paid,cat.business_category_names as primary_category');
			$this->db->from('hiprofile_users as user');
			$this->db->join('hiprofile_business as BaseTbl', 'BaseTbl.userId = user.userId','left');
			$this->db->join(' hiprofile_business_categories_lists as cat', 'BaseTbl.primary_category = cat.business_categoryId','left');
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('cat.cat_status', 0);
			$this->db->where('cat.isDeleted', 0);
			$this->db->where('created_date >=', $fromdate);
			$this->db->where('created_date <=', $todate);
			$this->db->order_by('BaseTbl.business_id', 'DESC');
			$query = $this->db->get();
			$results = $query->result_array(); 
			return $results;
		endif;
		if($selectSection == 3):
			$this->db->select('BaseTbl.name,plan.userId,plan.product_title,plan.product_description,plan.category,plan.is_activated,plan.product_id,plan.purchase_json,plan.purchase_token,plan.activation_time as activatedtime,plan.created_date as purchasedtime,plan.expiration_time as expiredtime');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_rank_points_plan_details as plan', 'plan.userId = BaseTbl.userId','right');
			$this->db->where('plan.category','hf');
			$this->db->where('plan.is_activated!=',0);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.roleId !=', 1);
			$query = $this->db->get();
			$results = $query->result_array();     
			return $results;
		endif;
		if($selectSection == 4):
			$this->db->select('BaseTbl.name,plan.userId,plan.product_title,plan.product_description,plan.category,plan.is_activated,plan.product_id,plan.purchase_json,plan.purchase_token,plan.activation_time as activatedtime,plan.created_date as purchasedtime,plan.expiration_time as expiredtime');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_rank_points_plan_details as plan', 'plan.userId = BaseTbl.userId','right');
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.roleId !=', 1);
			$query = $this->db->get();
			$results = $query->result_array();     
			return $results;
		endif;
		if($selectSection == 5):
			$this->db->select("friend.userId,user.email as useremail,user.name as username,user.dob as userdob,user.gender as usergender,user.mobile as usermobile");
			$this->db->from("hiprofile_users as user");
			$this->db->join('hiprofile_friends as friend', 'friend.userId = user.userId','left');
			$this->db->where('user.isDeleted', 0);
			$this->db->where('user.roleId', 0);
			$this->db->where("friend.request_status", "1");
			$this->db->where("friend.add_friend", "true");
			$this->db->where("friend.createdDtm >=", $fromdate);
			$this->db->where("friend.createdDtm <=", $todate);
			$query = $this->db->get();
			$array1 = $query->result_array(); 
			if($this->db->affected_rows() >=1)
			{
				$this->db->select("friend.friend_user_id,user.email as frienduseremail,user.name as friendusername,user.dob as frienduserdob,user.gender as friendusergender,user.mobile as friendusermobile");
				$this->db->from("hiprofile_users as user");
				$this->db->join('hiprofile_friends as friend', 'friend.friend_user_id = user.userId','left');
				$this->db->where('user.isDeleted', 0);
				$this->db->where('user.roleId', 0);
				$this->db->where("friend.request_status", "1");
				$this->db->where("friend.add_friend", "true");
				$this->db->where("friend.createdDtm >=", $fromdate);
				$this->db->where("friend.createdDtm <=", $todate);
				$query = $this->db->get();
				$array2 = $query->result_array(); 
			}
			$results = array();
			$array_count= count($array1);
			if(count($array1) > 0 && count($array2) > 0 ) {
				for($i=0;$i<$array_count;$i++){
					$results['users'][$i] = $array1[$i];
					$results['friends'][$i] = $array2[$i];
				}
			}
			return $results;
		endif;
	}

	function generateChart($fromdate = '',$todate = '',$optionvalue = '')
	{
		if($optionvalue == 1):
			$this->db->select('*');
			$this->db->from('hiprofile_users');
			$this->db->where('isDeleted', 0);
			$this->db->where('roleId', 0);
			$this->db->where('createdDtm >=', $fromdate);
			$this->db->where('createdDtm <=', $todate);
			$this->db->order_by('createdDtm', 'DESC');
			$query = $this->db->get();
			return $query->result_array();
			return $results;
		endif;
		if($optionvalue == 2):
			$this->db->select('user.name,BaseTbl.business_id,BaseTbl.image_url,BaseTbl.business_title,BaseTbl.address,BaseTbl.country,BaseTbl.state,BaseTbl.city,BaseTbl.pin,BaseTbl.contact,BaseTbl.email ,BaseTbl.day_open_from,BaseTbl.day_open_to,BaseTbl.hours_open_from,BaseTbl.hours_open_to,BaseTbl.latitude,BaseTbl.longitude,BaseTbl.approved_status,BaseTbl.created_date,BaseTbl.is_paid,cat.business_category_names as primary_category');
			$this->db->from('hiprofile_users as user');
			$this->db->join('hiprofile_business as BaseTbl', 'BaseTbl.userId = user.userId','left');
			$this->db->join(' hiprofile_business_categories_lists as cat', 'BaseTbl.primary_category = cat.business_categoryId','left');
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('cat.cat_status', 0);
			$this->db->where('cat.isDeleted', 0);
			$this->db->where('created_date >=', $fromdate);
			$this->db->where('created_date <=', $todate);
			$this->db->order_by('BaseTbl.business_id', 'DESC');
			$query = $this->db->get();
			$results = $query->result_array(); 
			return $results;
		endif;
		if($optionvalue == 3):
			$this->db->select('BaseTbl.name,plan.userId,plan.product_title,plan.product_description,plan.category,plan.is_activated,plan.product_id,plan.purchase_json,plan.purchase_token,plan.activation_time as activatedtime,plan.created_date as purchasedtime,plan.expiration_time as expiredtime');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_rank_points_plan_details as plan', 'plan.userId = BaseTbl.userId','right');
			$this->db->where('plan.category','hf');
			$this->db->where('plan.is_activated!=',0);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.roleId !=', 1);
			$query = $this->db->get();
			$results = $query->result_array();     
			return $results;
		endif;
		if($optionvalue == 4):
			$this->db->select('BaseTbl.name,plan.userId,plan.product_title,plan.product_description,plan.category,plan.is_activated,plan.product_id,plan.purchase_json,plan.purchase_token,plan.activation_time as activatedtime,plan.created_date as purchasedtime,plan.expiration_time as expiredtime');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_rank_points_plan_details as plan', 'plan.userId = BaseTbl.userId','right');
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.roleId !=', 1);
			$query = $this->db->get();
			$results = $query->result_array();     
			return $results;
		endif;
		if($optionvalue == 5):
			$this->db->select("friend.userId,user.email as useremail,user.name as username,user.dob as userdob,user.gender as usergender,user.mobile as usermobile");
			$this->db->from("hiprofile_users as user");
			$this->db->join('hiprofile_friends as friend', 'friend.userId = user.userId','left');
			$this->db->where('user.isDeleted', 0);
			$this->db->where('user.roleId', 0);
			$this->db->where("friend.request_status", "1");
			$this->db->where("friend.add_friend", "true");
			$this->db->where("friend.createdDtm >=", $fromdate);
			$this->db->where("friend.createdDtm <=", $todate);
			$query = $this->db->get();
			$array1 = $query->result_array(); 
			if($this->db->affected_rows() >=1)
			{
				$this->db->select("friend.friend_user_id,user.email as frienduseremail,user.name as friendusername,user.dob as frienduserdob,user.gender as friendusergender,user.mobile as friendusermobile");
				$this->db->from("hiprofile_users as user");
				$this->db->join('hiprofile_friends as friend', 'friend.friend_user_id = user.userId','left');
				$this->db->where('user.isDeleted', 0);
				$this->db->where('user.roleId', 0);
				$this->db->where("friend.request_status", "1");
				$this->db->where("friend.add_friend", "true");
				$this->db->where("friend.createdDtm >=", $fromdate);
				$this->db->where("friend.createdDtm <=", $todate);
				$query = $this->db->get();
				$array2 = $query->result_array(); 
			}
			$results = array();
			$array_count= count($array1);
			if(count($array1) > 0 && count($array2) > 0 ) {
				for($i=0;$i<$array_count;$i++){
					$results['users'][$i] = $array1[$i];
					$results['friends'][$i] = $array2[$i];
				}
			}
			return $results;
		endif;
	}
	
	function viewReport($fromdate = '', $todate = '', $selectSection = '')
	{
		if($selectSection == 1):
			$this->db->select('*');
			$this->db->from('hiprofile_users');
			$this->db->where('isDeleted', 0);
			$this->db->where('roleId', 0);
			$this->db->where('createdDtm >=', $fromdate);
			$this->db->where('createdDtm <=', $todate);
			$this->db->order_by('createdDtm', 'DESC');
			$query = $this->db->get();
			$results = $query->result_array(); 
			return $results;
		endif;
		if($selectSection == 2):
			$this->db->select('user.name,BaseTbl.business_id,BaseTbl.image_url,BaseTbl.business_title,BaseTbl.address,BaseTbl.country,BaseTbl.state,BaseTbl.city,BaseTbl.pin,BaseTbl.contact,BaseTbl.email ,BaseTbl.day_open_from,BaseTbl.day_open_to,BaseTbl.hours_open_from,BaseTbl.hours_open_to,BaseTbl.latitude,BaseTbl.longitude,BaseTbl.approved_status,BaseTbl.is_paid,cat.business_category_names as primary_category');
			$this->db->from('hiprofile_users as user');
			$this->db->join('hiprofile_business as BaseTbl', 'BaseTbl.userId = user.userId','left');
			$this->db->join(' hiprofile_business_categories_lists as cat', 'BaseTbl.primary_category = cat.business_categoryId','left');
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('cat.cat_status', 0);
			$this->db->where('cat.isDeleted', 0);
			$this->db->where('created_date >=', $fromdate);
			$this->db->where('created_date <=', $todate);
			$this->db->order_by('BaseTbl.business_id', 'DESC');
			$query = $this->db->get();
			$results = $query->result_array(); 
			return $results;
		endif;
		if($selectSection == 3):
			$this->db->select('BaseTbl.name,plan.userId,plan.product_title,plan.product_description,plan.category,plan.is_activated,plan.product_id,plan.purchase_json,plan.purchase_token,plan.activation_time as activatedtime,plan.created_date as purchasedtime,plan.expiration_time as expiredtime');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_rank_points_plan_details as plan', 'plan.userId = BaseTbl.userId','right');
			$this->db->where('plan.category','hf');
			$this->db->where('plan.is_activated!=',0);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.roleId !=', 1);
			$query = $this->db->get();
			$results = $query->result_array();     
			return $results;
		endif;
		if($selectSection == 4):
			$this->db->select('BaseTbl.name,plan.userId,plan.product_title,plan.product_description,plan.category,plan.is_activated,plan.product_id,plan.purchase_json,plan.purchase_token,plan.activation_time as activatedtime,plan.created_date as purchasedtime,plan.expiration_time as expiredtime');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_rank_points_plan_details as plan', 'plan.userId = BaseTbl.userId','right');
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.roleId !=', 1);
			$query = $this->db->get();
			$results = $query->result_array();     
			return $results;
		endif;
		if($selectSection == 5):
			$this->db->select("friend.userId,user.email as useremail,user.name as username,user.dob as userdob,user.gender as usergender,user.mobile as usermobile");
			$this->db->from("hiprofile_users as user");
			$this->db->join('hiprofile_friends as friend', 'friend.userId = user.userId','left');
			$this->db->where('user.isDeleted', 0);
			$this->db->where('user.roleId', 0);
			$this->db->where("friend.request_status", "1");
			$this->db->where("friend.add_friend", "true");
			$this->db->where("friend.createdDtm >=", $fromdate);
			$this->db->where("friend.createdDtm <=", $todate);
			$query = $this->db->get();
			$array1 = $query->result_array(); 
			if($this->db->affected_rows() >=1)
			{
				$this->db->select("friend.friend_user_id,user.email as frienduseremail,user.name as friendusername,user.dob as frienduserdob,user.gender as friendusergender,user.mobile as friendusermobile");
				$this->db->from("hiprofile_users as user");
				$this->db->join('hiprofile_friends as friend', 'friend.friend_user_id = user.userId','left');
				$this->db->where('user.isDeleted', 0);
				$this->db->where('user.roleId', 0);
				$this->db->where("friend.request_status", "1");
				$this->db->where("friend.add_friend", "true");
				$this->db->where("friend.createdDtm >=", $fromdate);
				$this->db->where("friend.createdDtm <=", $todate);
				$query = $this->db->get();
				$array2 = $query->result_array(); 
			}
			$results = array();
			$array_count= count($array1);
			if(count($array1) > 0 && count($array2) > 0 ) {
				for($i=0;$i<$array_count;$i++){
					$results['users'][$i] = $array1[$i];
					$results['friends'][$i] = $array2[$i];
				}
			}
			return $results;
		endif;
	}
}
?>