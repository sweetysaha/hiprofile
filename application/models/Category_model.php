<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model
{
    /**
     * This function is used to get the Category listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function categoryListingCount($searchText = '')
    {
        $this->db->select('business_categoryId as categoryId, business_category_names, business_category_icon,cat_status');
        $this->db->from('hiprofile_business_categories_lists');
        if(!empty($searchText)) {
            $likeCriteria = "(business_category_names  LIKE '%".$searchText."%'
                            OR  business_category_icon	  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('isDeleted', 0);
		$this->db->order_by('business_categoryId', 'DESC');
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the Category listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function categoryListing($searchText = '', $page, $segment)
    {
        $this->db->select('business_categoryId as categoryId, business_category_names, business_category_icon,cat_status');
        $this->db->from('hiprofile_business_categories_lists');
        if(!empty($searchText)) {
            $likeCriteria = "(business_category_names  LIKE '%".$searchText."%'
                            OR  business_category_icon	 LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('isDeleted', 0);
		$this->db->order_by('business_categoryId', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to add new Category to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewCategory($categoryInfo)
    {

        $this->db->trans_start();
        $this->db->insert('hiprofile_business_categories_lists', $categoryInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get Category information by id
     * @param number $categoryId : This is Category id
     * @return array $result : This is Category information
     */
    function getCategoryInfo($categoryId)
    {
        $this->db->select('business_categoryId as categoryId, business_category_names, business_category_icon,cat_status');
        $this->db->from('hiprofile_business_categories_lists');
        $this->db->where('isDeleted', 0);
        $this->db->where('business_categoryId', $categoryId);
		$this->db->order_by('categoryId', 'DESC');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the Category information
     * @param array $categoryInfo : This is contents updated information
     * @param number $categoryId : This is Category id
     */
    function editCategory($categoryInfo, $categoryId)
    {
        $this->db->where('business_categoryId', $categoryId);
        $this->db->update('hiprofile_business_categories_lists', $categoryInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the Category information
     * @param number $contentId : This is Category id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteCategory($categoryId, $categoryInfo)
    {
        $this->db->where('business_categoryId', $categoryId);
        $this->db->update('hiprofile_business_categories_lists', $categoryInfo);
        
        return $this->db->affected_rows();
    }
}