<?php if(!defined('BASEPATH')) exit('No direct script access allowed');



class Rank_model extends CI_Model

{

    /*To list all the rank,points boost purchased by the users*/  

	function get_rankListing()

    {

		$this->db->select('BaseTbl.name,plan.userId,plan.product_title,plan.product_description,plan.category,plan.is_activated,plan.Id');

        $this->db->from('hiprofile_users as BaseTbl');

        $this->db->join('hiprofile_rank_points_plan_details as plan', 'plan.userId = BaseTbl.userId','right');

        $this->db->where('BaseTbl.isDeleted', 0);

        $this->db->where('BaseTbl.roleId !=', 1);

        $query = $this->db->get();

        $result = $query->result();        

		return $query;

		

    }

	

	/*To list all the plan details purchased by an user*/  

	function getPlanInfo($id)

	{

		$this->db->select('BaseTbl.name,plan.userId,plan.product_title,plan.product_description,plan.category,plan.is_activated,plan.product_id,plan.purchase_json,plan.orderId,plan.purchase_token,plan.activation_time,plan.expiration_time,plan.created_date as purchased_date');

        $this->db->from('hiprofile_users as BaseTbl');

        $this->db->join('hiprofile_rank_points_plan_details as plan', 'plan.userId = BaseTbl.userId','right');

        $this->db->where('BaseTbl.isDeleted', 0);

        $this->db->where('BaseTbl.roleId !=', 1);

		$this->db->where('plan.Id',intval($id));

        $query = $this->db->get();

        $result = $query->result(); 

		return $result;

	}

	

	/*To list all the rank plans*/  

	function get_rankplansListing()

    {

		$this->db->select('

			ranks_points_packageId as id,

			ranks_points_package_Google_playId as googleid,

			ranks_points_package_rank_up as ranks,

			ranks_points_package_hours as hours,

			ranks_points_package_price as price,

			ranks_points_package_category_code as category

		');

        $this->db->from('hiprofile_rank_point_caetogry_packages_list');

		$this->db->where('ranks_points_package_category_code', 'hf');

        $this->db->where('IsDeleted', 0);

        $query = $this->db->get();

        $result = $query->result(); 

		return $query;

		

    }


    /*To list all the rank plans*/  

	function get_fameRankListing()

    {

		$this->db->select('

			BaseTbl.Id,

			user.name,

			BaseTbl.product_title,

			BaseTbl.orderId,

			BaseTbl.localised_price,

			BaseTbl.store,

			BaseTbl.created_date

		');

        $this->db->from('hiprofile_rank_points_plan_details as BaseTbl');

        $this->db->join('hiprofile_users as user', 'BaseTbl.userId = user.userId','left');

		$this->db->where('BaseTbl.product_id', 'hf_1_00');       

        $query = $this->db->get();

        $result = $query->result(); 

		return $query;

		

    }

	

	/*To list all the points plans*/  

	function get_pointsplanListing()

    {

		$this->db->select('

			ranks_points_packageId as id,

			ranks_points_package_Google_playId as googleid,

			ranks_points_package_hours as hours,

			ranks_points_package_price as price,

			ranks_points_package_category_code as category

		');

        $this->db->from('hiprofile_rank_point_caetogry_packages_list');

		$this->db->where('ranks_points_package_category_code', 'dp');

        $this->db->where('IsDeleted', 0);

        $query = $this->db->get();

        $result = $query->result(); 

		return $query;

		

    }

	

	/**

     * This function is used to delete the rankplan

     * @param number $id : This is id

     * @return boolean $result : TRUE / FALSE

     */

    function deleteFameRankPlan($id, $rankInfo)

    {

        $this->db->where('ranks_points_packageId', $id);

        $this->db->update('hiprofile_rank_point_caetogry_packages_list', $rankInfo);

        

        return $this->db->affected_rows();

    }



	/**

     * This function is used to delete the points plan

     * @param number $id : This is id

     * @return boolean $result : TRUE / FALSE

     */

    function deleteDoublePointsPlan($id, $pointInfo)

    {

        $this->db->where('ranks_points_packageId', $id);

        $this->db->update('hiprofile_rank_point_caetogry_packages_list', $pointInfo);

        

        return $this->db->affected_rows();

    }

	

	/*To save the new rank plan*/

	function addnewrankplan($rankinfo)

	{

		$this->db->trans_start();

        $this->db->insert('hiprofile_rank_point_caetogry_packages_list', $rankinfo);

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

        return $insert_id;

	}

	

	/*Tp save the new point plan*/

	function addnewpointplan($pointInfo)

	{

		$this->db->trans_start();

        $this->db->insert('hiprofile_rank_point_caetogry_packages_list', $pointInfo);

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

        return $insert_id;

	}

	

	/*To update and save the edited Rank plan details*/

	function updateRankPlan($rankInfo,$id)

	{

		$this->db->where('ranks_points_packageId', $id);

        $this->db->update('hiprofile_rank_point_caetogry_packages_list', $rankInfo);

		if ($this->db->affected_rows() >= 0)

		{

			return TRUE;

		}

		else

		{

			return FALSE;

		}       

	}

	

	/*To update and save the edited Double point plan details*/

	function updatePointPlan($pointInfo,$id)

	{

		$this->db->where('ranks_points_packageId', $id);

        $this->db->update('hiprofile_rank_point_caetogry_packages_list', $pointInfo);

		if ($this->db->affected_rows() >= 0)

		{

			return TRUE;

		}

		else

		{

			return FALSE;

		}       

	}

	

	/*To get the details of the plan to edit*/

	function getplansInfo($id,$category)

    {

        $this->db->select('*');

        $this->db->from('hiprofile_rank_point_caetogry_packages_list');

        $this->db->where('isDeleted', 0);

        $this->db->where('ranks_points_packageId', $id);

		$this->db->where('ranks_points_package_category_code', $category);

		$this->db->order_by('ranks_points_packageId', 'DESC');

        $query = $this->db->get();

        return $query->result();

    }

}



  