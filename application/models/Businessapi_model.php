<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Businessapi_model extends CI_Model
{
	//API call - To add new business record for the user
    public function addbusiness($data)
	{
		unset($data['api_key']);

		$insert_data = array(
			'business_title' => $data['business_title'],
			'address' => $data['address'],
			'country' => $data['country'],
			'state' => $data['state'],
			'city' => $data['city'],
			'pin' => $data['pin'],
			'contact' => $data['contact'],
			'email' => $data['email'],
			'day_open_from' => $data['day_open_from'],
			'day_open_to' => $data['day_open_to'],
			'hours_open_from' => $data['hours_open_from'],
			'hours_open_to' => $data['hours_open_to'],
			'latitude' => $data['latitude'],
			'longitude' => $data['longitude'],
			'userId' => $data['userID'],
			'image_url' => '/business/images/'.$data['image_base64'],
			'categories' => str_replace(' ', '', $data['categories']),
			'primary_category'=> $data['primary_category']
		);
			$this->db->insert('hiprofile_business', $insert_data);
			$insert_id = $this->db->insert_id();
			if ($this->db->affected_rows() > 0 ) 
			{
				$this->db->select('business.approved_status,business.is_paid');
				$this->db->from('hiprofile_business as business');
				$this->db->where('business.business_id', $insert_id);
				$query = $this->db->get();
				$business = $query->result();
				if(!empty($business))
				{
					$loop_count = count($business);
					for($i=0;$i<$loop_count;$i++) {
						/* //$business[$i]->category_ids =  explode(',', $business[$i]->categories);
						//$business[$i]->primary_category_id = $business[$i]->primary_category;
						
						//fucntion to get the name of category from id's
						$catlist = $this->getCategoryNameFromId($business[$i]->categories);
						$business[$i]->categories = $catlist;
						//explode(',', catlist);
						
						$catname = $this->getPrimaryCategoryNameFromId($business[$i]->primary_category);
						$business[$i]->primary_category = $catname; */

						if($business[$i]->approved_status == 0):
							$business[$i]->status = 'payment_pending';
						elseif($business[$i]->is_paid == 0):
							$business[$i]->status = 'pending';
						else:
							$business[$i]->status = 'Approved';
						endif;
						unset($business[$i]->approved_status);
						unset($business[$i]->is_paid);
					}
					$return_res = array();
					$return_res['responsecode'] 	= "200";
					$return_res['responsedetails'] 	= "success";
					$return_res['business'] 	= $business;
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
				$return_res = array();
				$return_res['responsecode'] = "200";
				$return_res['responsedetails'] = "Success";
				return $return_res;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
		exit;
	}
	
	//API call - To return business records for the user owns
    public function getmybusiness($data)
	{
		unset($data['api_key']);
		
		$this->db->select('business_id');
		$this->db->from('hiprofile_business');
		$this->db->where('isDeleted',0);
		$this->db->where('userId',$data['userID']);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			
			$this->db->select('business.business_id,business.business_title,business.address,business.country,			business.state,business.city,business.pin,business.image_url,business.contact,business.email,business.categories,business.primary_category,business.day_open_from,business.day_open_to,business.hours_open_from,business.hours_open_to,business.latitude,business.longitude,business.business_rating as rating,business.approved_status,business.is_paid');
			$this->db->from('hiprofile_business as business');
			$this->db->where('business.userId', $data['userID']);
			$this->db->where('business.isDeleted',0);
			$this->db->order_by('business.business_id', 'DESC');
			$query = $this->db->get();
			$business = $query->result();
			if(!empty($business))
			{
				$loop_count = count($business);
				for($i=0;$i<$loop_count;$i++) {
					$business[$i]->image_url = ($business[$i]->image_url == "") ? "" : base_url().'assets'.$business[$i]->image_url;
					//$business[$i]->category_ids =  explode(',', $business[$i]->categories);
					//$business[$i]->primary_category_id = $business[$i]->primary_category;
					
					//fucntion to get the name of category from id's
					$catlist = $this->getCategoryNameFromId($business[$i]->categories);
					$business[$i]->categories = $catlist;
					//explode(',', catlist);
					
					$catname = $this->getPrimaryCategoryNameFromId($business[$i]->primary_category);
					$business[$i]->primary_category = $catname;

					
					if($business[$i]->approved_status == 0):
						$business[$i]->status = 'payment_pending';
					elseif($business[$i]->is_paid == 0):
						$business[$i]->status = 'pending';
					else:
						$business[$i]->status = 'Approved';
					endif;
					unset($business[$i]->approved_status);
					unset($business[$i]->is_paid);
				}
				$return_res = array();
				$return_res['responsecode'] 	= "200";
				$return_res['responsedetails'] 	= "success";
				$return_res['business'] 	= $business;
				return $return_res;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
        } 
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "User doesn't own the business";
			return $return_res;
        }
		exit;
	}
	
	//API call - To return business records for the user owns
    public function geteditbusiness($data)
	{
		unset($data['api_key']);
		
		
		$this->db->select('business_id');
		$this->db->from('hiprofile_business');
		$this->db->where('isDeleted', 0);
		$this->db->where('business_id',$data['business_id']);
		$this->db->where('userId',$data['userID']);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
		    $updetail = array(
				'business_title' => $data['business_title'],
				'address' => $data['address'],
				'country' => $data['country'],
				'state' => $data['state'],
				'city' => $data['city'],
				'pin' => $data['pin'],
				'contact' => $data['contact'],
				'email' => $data['email'],
				'day_open_from' => $data['day_open_from'],
				'day_open_to' => $data['day_open_to'],
				'hours_open_from' => $data['hours_open_from'],
				'hours_open_to' => $data['hours_open_to'],
				'latitude' => $data['latitude'],
				'longitude' => $data['longitude'],
				'image_url' => '/business/images/'.$data['image_base64'],
				'categories' => str_replace(' ', '', $data['categories']),
				'primary_category'=> $data['primary_category']
			);
			$updatedvalue = array_filter($updetail);
			$this->db->where('business_id',$data['business_id']);
			$this->db->where('userId',$data['userID']);
			$this->db->update('hiprofile_business', $updatedvalue);
			if($this->db->affected_rows() >= 0)
			{
				$this->db->select('business.business_id,business.business_title,business.address,
				business.country,business.state,business.city,business.pin,business.image_url,			business.contact,business.email,business.categories,business.primary_category,
				business.day_open_from,	business.day_open_to,business.hours_open_from,business.hours_open_to,
				business.latitude,business.longitude,business.business_rating as rating,
				business.approved_status,business.is_paid');
				$this->db->from('hiprofile_business as business');
				$this->db->where('business.isDeleted',0);
				$this->db->where('business.userId', $data['userID']);
				$query = $this->db->get();
				$business = $query->result();
				if(!empty($business))
				{
					$loop_count = count($business);
					for($i=0;$i<$loop_count;$i++) {
						$business[$i]->image_url = ($business[$i]->image_url == "") ? "" : base_url().'assets'.$business[$i]->image_url;
						
						//$business[$i]->category_ids =  explode(',', $business[$i]->categories);
						//$business[$i]->primary_category_id = $business[$i]->primary_category;
						
						//fucntion to get the name of category from id's
						$catlist = $this->getCategoryNameFromId($business[$i]->categories);
						$business[$i]->categories = $catlist;
						//explode(',', catlist);
						
						$catname = $this->getPrimaryCategoryNameFromId($business[$i]->primary_category);
						$business[$i]->primary_category = $catname;

						
						if($business[$i]->approved_status == 0):
							$business[$i]->status = 'payment_pending';
						elseif($business[$i]->is_paid == 0):
							$business[$i]->status = 'pending';
						else:
							$business[$i]->status = 'Approved';
						endif;
						
						unset($business[$i]->approved_status);
						unset($business[$i]->is_paid);
					}
					$return_res = array();
					$return_res['responsecode'] 	= "200";
					$return_res['responsedetails'] 	= "success";
					$return_res['business'] 	= $business;
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
				
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
        } 
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "Business Doesn't Exist / User doesn't own the business";
			return $return_res;
        }
		exit;
	}
	
	//API call - To return business records for the user owns
    public function getBusinessCategoryList($data)
	{
		unset($data['api_key']);
		
		
		$this->db->select('business_categoryId as category_id, business_category_icon as icon_name, business_category_names as category');
		$this->db->from('hiprofile_business_categories_lists');
		$this->db->where('cat_status',0);
		$this->db->where('isDeleted',0);
		$query = $this->db->get();
		$categories = $query->result();
		if(!empty($categories))
		{
			$return_res = array();
			$return_res['responsecode'] 	= "200";
			$return_res['responsedetails'] 	= "success";
			$return_res['data']['categories'] 	= $categories;
			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
	}
	
	//API call - To return business List record for the user owns
    public function getBusinessList($data)
	{
		unset($data['api_key']);
		$result = array();
		$id="";
		$start = $end = '';
		$totalpage = $this->getpagecount($data['userID']);
		if($totalpage == $data['page_no'])
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		else
		{
			if(isset($data['page_no']) && $data['page_no'] == ''){
				$pageno = 1;
				$start = 0;
				$end = 9;
			}else{
				$start = $data['page_no'] * 10;
				$end = $start + 9;
				$pageno = $data['page_no'];
			}
			$this->db->select('business_id,business_title,address,country,state,city,
			pin,image_url,contact,email,business_rating as rating,categories,primary_category,			day_open_from,day_open_to,hours_open_from,hours_open_to,latitude,
			longitude,approved_status as status');
			$this->db->from('hiprofile_business');
			$this->db->where('isDeleted',0);
			$this->db->where('approved_status',1);
			$this->db->limit($end, $start);
			$query = $this->db->get();
			$business = $query->result();
			if(!empty($business))
				{
					$loop_count = count($business);
					for($i=0;$i<$loop_count;$i++) {
						$business[$i]->image_url = ($business[$i]->image_url == "") ? "" : base_url().'assets'.$business[$i]->image_url;
						
						//$business[$i]->category_ids =  explode(',', $business[$i]->categories);
						//$business[$i]->primary_category_id = $business[$i]->primary_category;
						
						//fucntion to get the name of category from id's
						$catlist = $this->getCategoryNameFromId($business[$i]->categories);
						$business[$i]->categories = $catlist;
						//explode(',', catlist);
						
						$catname = $this->getPrimaryCategoryNameFromId($business[$i]->primary_category);
						$business[$i]->primary_category = $catname;

						$business[$i]->status = 'Approved';
					}
					$result['responsecode'] = "200";
					$result['responsedetails'] = "success";
					$result['data']['page']['total_pages'] = $totalpage;
					$result['data']['page']['page_no'] = $pageno;
					$result['data']['page']["page_size"] = '10';
					$result['data']["business"] = $business;
					
					return $result;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
		}
	}
	
	// To fetch the page count for pagintion
	public function getpagecount($userid)
	{
		$this->db->select('business_id');
		$this->db->from('hiprofile_business');
		$this->db->where('isDeleted',0);
		$this->db->where('approved_status',1);
		
		$query = $this->db->get();
		$bussness_id = $query->result();
		if(count($bussness_id) > 10 ){
			return ceil(count($bussness_id)/10);
		}else{
			return 1;
		}
	}
	
	//API call - To add new business rating for the business using particular id
    public function addrating($data)
	{
		unset($data['api_key']);
				
		$this->db->select('rating');
		$this->db->from('hiprofile_business_rating');
		$this->db->where('userId',$data['userID']);
		$this->db->where('business_Id',$data['business_id']);
		
		$query = $this->db->get();
		$rating = $query->result();
		if(empty($rating))
		{
			$insert_data = array(
			'rating' => $data['rating'],
			'userId' => $data['userID'],
			'business_Id' => $data['business_id']
			);
			$this->db->insert('hiprofile_business_rating', $insert_data);
		}
		else
		{
			$update_data = array(
			'rating' => $data['rating'],
			'userId' => $data['userID'],
			'business_Id' => $data['business_id']
			);
			$this->db->where('business_id',$data['business_id']);
			$this->db->where('userId',$data['userID']);
			$this->db->update('hiprofile_business_rating', $update_data);
		}
		if ($this->db->affected_rows() >= 0 ) 
		{
			$this->db->select('rating');
			$this->db->from('hiprofile_business_rating');
			$this->db->where('business_Id', $data['business_id']);
			$query = $this->db->get();
			$rating = $query->result_array();
			$loop_count = count($rating);
			/*$max = 0;
			foreach ($rating as $rate => $count) 
			{
				$max = $max+$count['rating'];
			}
			$businessrating = $max / $loop_count;
			$ratings_val = round($businessrating, 2); */
			$max = 0;
			foreach ($rating as $rate => $count) 
			{
				$max = $count['rating']/$loop_count;	
			}
			$businessrating = $max;
			$ratings_val = round(2*$businessrating)/2;
			$this->db->where('business_id',$data['business_id']);
			$this->db->set('business_rating',$ratings_val);
			$this->db->update('hiprofile_business');
			if ($this->db->affected_rows() >= 0 ) 
			{				
					$return_res = array();
					$return_res['responsecode'] 	= "200";
					$return_res['responsedetails'] 	= "success";
					$return_res['status'] 	= "true";
					return $return_res;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] 	= "200";
				$return_res['responsedetails'] 	= "success";
				$return_res['status'] 	= "false";
				return $return_res;
			}	
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
	}
	
	//To fetch all the categories using the id's
	public function getCategoryNameFromId($catIds)
	{
		$id = explode(',',$catIds);
		$this->db->select('business_categoryId as category_id, business_category_icon as icon_name, business_category_names as category');
		$this->db->from('hiprofile_business_categories_lists');
		$this->db->where_in('business_categoryId',$id);
		$this->db->where('cat_status',0);
		$query = $this->db->get();
		$category = $query->result();
		return $category;
	}
	
	//To fetch the primary category using the id
	public function getPrimaryCategoryNameFromId($catIds)
	{
		$this->db->select('business_categoryId as category_id, business_category_icon as icon_name, business_category_names as category');
		$this->db->from('hiprofile_business_categories_lists');
		$this->db->where('business_categoryId',$catIds);
		$this->db->where('cat_status',0);
		$query = $this->db->get();
		$primarycategory = $query->result();
		return $primarycategory[0];
		
	}
}