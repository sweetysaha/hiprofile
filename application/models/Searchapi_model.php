<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Searchapi_model extends CI_Model
{
	//API call - To add new business record for the user
    public function getSearchbusiness($data)
	{
		unset($data['api_key']);
		
		$searchText = $data['search_string'];
		$this->db->select('business.business_id,business.business_title,business.address,business.country,			business.state,business.city,business.pin,business.image_url,business.contact,business.email,business.categories,business.primary_category,business.day_open_from,business.day_open_to,business.hours_open_from,business.hours_open_to,business.latitude,business.longitude,business.business_rating as rating,business.categories as category_ids,business.primary_category as primary_category_id,business.approved_status,business.is_paid');
		$this->db->from('hiprofile_business as business');
		$this->db->join('hiprofile_business_categories_lists as cat', 'business.primary_category = cat.business_categoryId','left');
		$this->db->where('business.isDeleted',0);
		$this->db->where('business.approved_status',1);
		$this->db->order_by('business.business_id', 'DESC');
		if(!empty($searchText)) {
            $likeCriteria = "(business.business_title  LIKE '%$searchText%')";
            $this->db->where($likeCriteria);
        }
		$this->db->where('business.isDeleted', 0);
		$this->db->where('cat.cat_status', 0);
		$this->db->where('cat.isDeleted', 0);
		$this->db->order_by('business.business_id', 'DESC');
        $query = $this->db->get();
		$business = $query->result();
		$loop_count = count($business);
		for($i=0;$i<$loop_count;$i++) 
		{
			$business[$i]->image_url = ($business[$i]->image_url == "") ? "" : base_url().'assets'.$business[$i]->image_url;		
			
			//fucntion to get the name of category from id's
			$catlist = $this->getCategoryNameFromId($business[$i]->category_ids);
			$business[$i]->categories = $catlist;				
			$catname = $this->getPrimaryCategoryNameFromId($business[$i]->primary_category_id);
			$business[$i]->primary_category = $catname;		
			
			// Set status
			if($business[$i]->approved_status == 0){
				$business[$i]->status = 'payment_pending';
			}
			elseif($business[$i]->approved_status == 1){
				$business[$i]->status = 'Approved';
			}else{
				$business[$i]->status = 'pending';
			}
			unset($business[$i]->category_ids);
			unset($business[$i]->primary_category_id);
			unset($business[$i]->approved_status);
			unset($business[$i]->is_paid);
		}

		$this->db->select('BaseTbl.userId as userID,BaseTbl.name, BaseTbl.dob, BaseTbl.gender, BaseTbl.gender_disclose,	BaseTbl.mobile as mobileNumber, BaseTbl.email,BaseTbl.email_status, BaseTbl.username as userName,BaseTbl.rating,BaseTbl.feedback,BaseTbl.latitude,BaseTbl.longitude,BaseTbl.approachability,social.social_rank,social.rank,social.rank_f2,social.points,social.worth,social.bio,social.points_required,social.fbuserid as fbUserId,social.twitteruserid as twitterUserId, social.instagramuserid as instagramUserId, social.facebook_profile_url as fbURL,social.twitter_profile_url as twitterURL, social.instagram_profile_url as instagramURL,social.profileimage as image_url');

		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$this->db->where('BaseTbl.roleId', 0);
		if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		$query = $this->db->get();
		$user = $query->result();
		$loop_count = count($user);
		for($i=0;$i<$loop_count;$i++) 
		{
			$this->load->model('socialapi_model');
			$this->load->model('mapsearchapi_model');
			$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$user[$i]->userID);
			$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$user[$i]->userID);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{
				
				$getuserfriend = $this->mapsearchapi_model->get_user_status_friend_social($data['userID'],$user[$i]->userID);
				
				$getearnedpoints = $this->mapsearchapi_model->get_user_earned_points($data['userID'],$user[$i]->userID);
				
				//Function call to get the user details.					
				$searchFriendsList[] = $this->mapsearchapi_model->get_friend_requested_user_details($user[$i]->userID,$getuserfriend,$getearnedpoints,$data['userID']);				
			}
		}
		
			if(empty($searchFriendsList) && empty($business))
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['status'] = "No Result Found";
				return $return_res;
			}
			else
			{
				$array = array();
				$searchFriendsList = (empty($searchFriendsList))?$array:$searchFriendsList;
				$return_res = array();
				$return_res['responsecode'] = "200";
				$return_res['responsedetails'] = "Success";
				$return_res['data']['people'] = $searchFriendsList;
				$return_res['data']['business'] = $business;
				return $return_res;
				
			}
		exit;
	}
	
	//To fetch all the categories using the id's
	public function getCategoryNameFromId($catIds)
	{
		$id = explode(',',$catIds);
		$this->db->select('business_categoryId as category_id, business_category_icon as icon_name, business_category_names as category');
		$this->db->from('hiprofile_business_categories_lists');
		$this->db->where_in('business_categoryId',$id);
		$this->db->where('cat_status',0);
		$query = $this->db->get();
		$category = $query->result();
		return $category;
	}
	
	//To fetch the primary category using the id
	public function getPrimaryCategoryNameFromId($catIds)
	{
		$this->db->select('business_categoryId as category_id, business_category_icon as icon_name, business_category_names as category');
		$this->db->from('hiprofile_business_categories_lists');
		$this->db->where('business_categoryId',$catIds);
		$this->db->where('cat_status',0);
		$query = $this->db->get();
		$primarycategory = $query->result();
		return $primarycategory[0];
		
	}
}