<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Hiflyer_model extends CI_Model
{
    /**
     * This function is used to get all the user rank and value
     */
    function hiFlyerRankPush()
    {
		$this->db->select('BaseTbl.userId as userID,BaseTbl.latitude, BaseTbl.longitude, BaseTbl.gender,(social.social_rank + social.rank) as totalrank');
		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where('BaseTbl.status',0);
		$this->db->where('BaseTbl.roleId',0);
		$this->db->where('BaseTbl.isDeleted',0);
		$this->db->order_by('totalrank', 'desc');
		$query = $this->db->get();
		$userdetails = $query->result_array();
		if(!empty($userdetails))
		{	
			$rankPush1 = array();
			$rankPush2 = array();
			$rankPush3 = array();
			$rankPush4 = array();
			for($i=0;$i< count($userdetails);$i++)
			{
				if($userdetails[$i]['totalrank'] > 80)
				{
					//echo 'rank greater than 80 userid = '.$userdetails[$i]['userID'];
					$rankrange = 100;
					$meters = 2000;
					$rankPush1[] = $this->getUserAndRank($userdetails[$i]['latitude'],$userdetails[$i]['longitude'],$rankrange,$meters,$userdetails[$i]['userID'],$userdetails[$i]['totalrank']);
					
				}
				if($userdetails[$i]['totalrank'] > 60)
				{
					//echo 'rank greater than 60 userid = '.$userdetails[$i]['userID'];
					$rankrange = 80;
					$meters = 1000;
					$rankPush2[] = $this->getUserAndRank($userdetails[$i]['latitude'],$userdetails[$i]['longitude'],$rankrange,$meters,$userdetails[$i]['userID'],$userdetails[$i]['totalrank']);
				}
				if($userdetails[$i]['totalrank'] > 50)
				{
					//echo 'rank greater than 50 userid = '.$userdetails[$i]['userID'];
					$rankrange = 75;
					$meters = 500;
					$rankPush3[] = $this->getUserAndRank($userdetails[$i]['latitude'],$userdetails[$i]['longitude'],$rankrange,$meters,$userdetails[$i]['userID'],$userdetails[$i]['totalrank']);
				}
				if($userdetails[$i]['totalrank'] > 30)
				{
					//echo 'rank greater than 30 userid = '.$userdetails[$i]['userID'];
					$rankrange = 45;
					$meters = 100;
					$rankPush4[] = $this->getUserAndRank($userdetails[$i]['latitude'],$userdetails[$i]['longitude'],$rankrange,$meters,$userdetails[$i]['userID'],$userdetails[$i]['totalrank']);
				}
			}	
		}
	}	
	
	/*
	** Function to fetch the user details with in the rank and radius range of the users 
	** $lat is the user latitude
	** $long is the user longitude
	** $rankrange is the rank use to query greaterthan
	** $lessrank is the rank use to query lesserthan
	** $meters is the value of the meters to calulate from lat and long
	** $meters is the value of the user rank
	*/
	function getUserAndRank($lat,$long,$rankrange,$meters,$userid,$senderrank)
	{
		//to check the distance meters is been converted to kms
		if($meters == 2000){
			$dist = 2;
			$message = 'A High Flyer of Rank > 80 is in your vacinity.';
			$repeatpushmessage = 60;
			$repeattosameuser = 60;
		}
		elseif($meters == 1000){
			$dist = 1;
			$message = 'A High Flyer of Rank > 60 is in your vacinity.';
			$repeatpushmessage = 80;
			$repeattosameuser = 80;
		}
		elseif($meters == 500){
			$dist = 0.5;
			$message = 'A user of Rank > 50 is in your vicinity.';
			$repeatpushmessage = 120;
			$repeattosameuser = 300;
		}
		elseif($meters == 100){
			$dist = 0.1;
			$message = 'A user of Rank > 30 is in your immediate vicinity.';
			$repeatpushmessage = 120;
			$repeattosameuser = 200;
		}
		
		//To fetch the query using the lat and long of the user distance with other users 
		$this->db->select('
		BaseTbl.userId as friendID,BaseTbl.device_token,BaseTbl.device_type,
		(social.social_rank + social.rank) as totalrank,
		( 6371 * acos( cos( radians("'.$lat.'") ) * cos( radians( BaseTbl.latitude ) ) * cos( radians( BaseTbl.longitude ) - radians("'.$long.'") ) + sin( radians("'.$lat.'") ) * sin( radians( BaseTbl.latitude ) ) ) ) as distance
		');		
		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$this->db->where('BaseTbl.roleId', 0);
		$this->db->where('BaseTbl.userId !=', $userid);
		$this->db->having('totalrank <=', $rankrange);
		$this->db->having('distance <=', $dist);
		$query = $this->db->get();
		$user = $query->result();		
		if(!empty($user))
		{
			for($i=0;$i<count($user);$i++)
			{
				$user[$i]->userID   = $userid;
				$user[$i]->userrank = $senderrank;
				$user[$i]->message = $message;
				$user[$i]->minutes = $repeatpushmessage;
				$user[$i]->sameuserinutes = $repeattosameuser;
				unset($user[$i]->distance);
				
				$this->db->select('id');		
				$this->db->from('hiprofile_hiflyer_push');
				$this->db->where('userId',$userid);
				$this->db->where('userrank',$senderrank);
				$this->db->where('friendId',$user[$i]->friendID);
				$this->db->where('friendrank',$user[$i]->totalrank);
				$this->db->where('device_token',$user[$i]->device_token);
				$this->db->where('message',$user[$i]->message);
				$query = $this->db->get();
			
				if ($query->num_rows() >= 1)
				{
				   $update_data = array(
					'userId' => $userid,
					'userrank' => $senderrank,
					'friendId' => $user[$i]->friendID,
					'friendrank' => $user[$i]->totalrank,
					'device_token' => $user[$i]->device_token,
					'message' => $user[$i]->message,
					'device_type' => $user[$i]->device_type,
					);
					$this->db->where('userId', $val->userID);
					$this->db->where('userrank', $val->userrank);
					$this->db->where('friendId', $val->friendID);
					$this->db->where('friendrank', $val->totalrank);
					$this->db->where('device_token', $val->device_token);
					$this->db->update('hiprofile_hiflyer_push',$update_data);
					$this->checkTimeframe($update_data);
				} 
				else
				{
				    $insert_data = array(
					'userId' => $userid,
					'userrank' => $senderrank,
					'friendId' => $user[$i]->friendID,
					'friendrank' => $user[$i]->totalrank,
					'device_token' => $user[$i]->device_token,
					'message' => $user[$i]->message,
					'minutes' =>$repeatpushmessage,
					'repeatmessage' =>$repeattosameuser,
					'device_type' => $user[$i]->device_type,
					);
					$this->db->insert('hiprofile_hiflyer_push', $insert_data);
					//$this->checkTimeframe($insert_data);
					$this->sendHiflyerPushNotification($insert_data);
				} 
		    }	
		}
	}
	
	/*
	**Function to check the timeframe of the user to send push notification
	*/
	function checkTimeframe($data)
	{
		$this->db->select('*');		
		$this->db->from('hiprofile_hiflyer_push');
		$this->db->where('userId',$data['userId']);
		$this->db->where('userrank',$data['userrank']);
		$this->db->where('friendId',$data['friendId']);
		$this->db->where('friendrank',$data['friendrank']);
		$this->db->where('device_token',$data['device_token']);
		$this->db->where('device_type',$data['device_type']);
		$this->db->where('message',$data['message']);
		// $this->db->where('messagecount',0);
		$query = $this->db->get();
		$timeframe = $query->result();
		$currenttime = $this->get_current_datime();

		foreach($timeframe as $key=>$value) {
			// print($value->friendId);
			$minut 			= trim($value->minutes);
			$repeatmessage 	= trim($value->repeatmessage);
			$cur_time 		= $value->created_datetime;
			$last_notify	= $value->updated_datetime;
			$first_notify	= $value->notify;
			// pre($value);exit;
			$temp_value		= array(
									"Id"=>$value->Id,
									"userId"=>$value->userId,
									"userrank"=>$value->userrank,
									"friendId"=>$value->friendId,
									"friendrank"=>$value->friendrank,
									"device_token"=>$value->device_token,
									"message"=>$value->message,
									"messagecount"=>$value->messagecount,
									"minutes"=>$value->minutes,
									"repeatmessage"=>$value->repeatmessage,
									"device_type"=>$value->device_type
								);
if($minut == 60 || $minut == 80) {
	$newtimestamp3 	= strtotime("+$minut minute", strtotime($last_notify));
	$time_end 		= date('Y-m-d H:i:s', $newtimestamp3);
	
	if($time_end <= $currenttime) {
		// echo "minut==".$minut;
		// pre($value);
		$this->sendHiflyerPushNotification($temp_value);
	}
} 
elseif ($minut == 120) {
	$newtimestamp 	= strtotime("+$minut minute", strtotime($cur_time));
	$newtimestamp1 	= strtotime("+$repeatmessage minute", strtotime($last_notify));
	$end1			= date('Y-m-d H:i:s', $newtimestamp); // +120
	$end2			= date('Y-m-d H:i:s', $newtimestamp1); // +200 or +300
	
	
	if($repeatmessage == 200) {
		if($first_notify == '0') {
			if($end1 >= $currenttime) {
				$update_data = array(
				'notify' => '1'
				);
				$this->db->where('userId', $temp_value['userId']);
				$this->db->where('userrank', $temp_value['userrank']);
				$this->db->where('friendId', $temp_value['friendId']);
				$this->db->where('friendrank', $temp_value['friendrank']);
				$this->db->where('device_token', $temp_value['device_token']);
				$this->db->where('device_type', $temp_value['device_type']);
				$this->db->where('message', $temp_value['message']);
				$this->db->where('Id', $temp_value['Id']);
				$this->db->update('hiprofile_hiflyer_push',$update_data);
				
				$this->sendHiflyerPushNotification($temp_value);
			}
		}
		if($first_notify == '1' && $end2 <= $currenttime) {
			$this->sendHiflyerPushNotification($temp_value);
		}
	}
	if($repeatmessage == 300) {
		if($first_notify == '0') {
			if($end1 >= $currenttime) {
				$update_data = array(
				'notify' => '1'
				);
				$this->db->where('userId', $temp_value['userId']);
				$this->db->where('userrank', $temp_value['userrank']);
				$this->db->where('friendId', $temp_value['friendId']);
				$this->db->where('friendrank', $temp_value['friendrank']);
				$this->db->where('device_token', $temp_value['device_token']);
				$this->db->where('device_type', $temp_value['device_type']);
				$this->db->where('message', $temp_value['message']);
				$this->db->where('Id', $temp_value['Id']);
				$this->db->update('hiprofile_hiflyer_push',$update_data);
				
				$this->sendHiflyerPushNotification($temp_value);
			}
		}
		if($first_notify == '1' && $end2 <= $currenttime) {
			$this->sendHiflyerPushNotification($temp_value);
		}
	}
}


		}
		
	}
	
	/*
	**Function to send the push notification
	*/
	function sendHiflyerPushNotification($data)
	{

		/*if(strtolower($data['device_type']) == 'android')
		{
			//defined a new constant for firebase api key
			define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');
	 
			if(!empty($data))
			{
				$result['friend_user_id'] = '['.$data['userId'].']';
				$result['notification_type'] = "hiflyer";
				$result['friend_pic_url'] = "";
				$result['friend_gender'] = "";
				$result['social_media'] = "";
				$result['status'] = "";
				$result['new_rank'] = "";
				$result['text'] = $data['message'];
				$url = 'https://fcm.googleapis.com/fcm/send';
				$fields = array();
				$fields['to'] = $data['device_token'];
				$fields['data']= $result;
				//echo json_encode($fields); exit;
				//header with content_type api key
				$headers = array(
					'Content-Type:application/json',
					'Authorization: key=' . FIREBASE_API_KEY,
				);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
				
				$result = curl_exec($ch);
				$this->updatetime($data);
				curl_close ($ch);
				
			}
		}
		if(strtolower($data['device_type']) == 'ios')
		{
			//defined a new constant for IOS private key
			$passphrase = 'admin';
			
			// Create the payload body
			$result['friend_user_id'] = '['.$data['userId'].']';
			$result['notification_type'] = "hiflyer";
			
			$message = $data['message'];
			$badge = 1;
			$sound = 'default';
			
			$payload = array();
			$payload['aps'] = array('alert' => $message, 'badge' => intval($badge), 'sound' => $sound,'category'=> 'hiflyer','mutable-content'=> intval(1));
			$payload['payload'] = $result;

			// Encode the payload as JSON
			$payload = json_encode($payload);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', trim($devicetoken)) . pack('n', strlen($payload)) . $payload;
			
			$apnsHost = 'gateway.push.apple.com';
			//$apnsHost = 'gateway.sandbox.push.apple.com';
			$apnsPort = 2195;
			
			// .pem is your certificate file
			$apns_cert = APPPATH.'../assets/IOS/HiProfile_Dis_Push.pem'; 
			//$apns_cert = APPPATH.'../assets/IOS/HiProfile_Dev_Push.pem';
			
			$streamContext  = stream_context_create();
			stream_context_set_option($streamContext , 'ssl', 'local_cert', $apns_cert);
			stream_context_set_option($streamContext , 'ssl', 'passphrase', $passphrase);
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
			//if (!$fp){
				//exit("Failed to connect: $error $errorString" . PHP_EOL);
			//}
			
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			return $result;
			// Close the connection to the server
			socket_close($fp);
			fclose($fp);
		}*/
	}
		
	/*To update the currentime from the database*/
	function updatetime($data)
	{
		$update_data = array(
		'updated_datetime' => $this->get_current_datime(),
		'messagecount' => 1
		);
		$this->db->where('userId', $data['userId']);
		$this->db->where('userrank', $data['userrank']);
		$this->db->where('friendId', $data['friendId']);
		$this->db->where('friendrank', $data['friendrank']);
		$this->db->where('device_token', $data['device_token']);
		$this->db->where('message', $data['message']);
		$this->db->update('hiprofile_hiflyer_push',$update_data);
	}
	
	/*To fetch the curren time from the database*/
	function get_current_datime()
	{
		$this->db->select('Now() as ctime');
		$query = $this->db->get();
		$time = $query->result();
		return $time[0]->ctime;
	}
	
	/*To fetch message count of how many times push is been sent*/
	/*
	**Function to check if the cornjob is running properly or not
	*/
	function testcornjob()
	{
	   // the message
		$msg = "Cron Job For Hyflyer is working";

		// use wordwrap() if lines are longer than 70 characters
		$msg = wordwrap($msg,70);

		// send email
		mail("praveen.kumar@indusnet.co.in","My subject",$msg);
	}
}
?>