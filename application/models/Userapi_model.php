<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Userapi_model extends CI_Model
{

	 //API call - To check login record
	public function checklogin($data){                  
		$this->db->select('BaseTbl.userId as userID,BaseTbl.name, BaseTbl.location_disclose, BaseTbl.dob, BaseTbl.gender, 
			BaseTbl.mobile as mobileNumber, BaseTbl.email, BaseTbl.username as userName, BaseTbl.share_mobile,		BaseTbl.password,BaseTbl.rating,BaseTbl.feedback,BaseTbl.approachability,social.social_rank,social.rank_f2,social.rank,social.points,social.fbuserid as fbUserId,
			twitteruserid as twitterUserId, instagramuserid as instagramUserId,social.facebook_profile_url as fbURL,social.twitter_profile_url as twitterURL,social.instagram_profile_url as instagramURL');

		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		
		if(isset($data['username'])):
			$this->db->where('BaseTbl.username',$data['username']);
		else:
			$this->db->where('BaseTbl.email',$data['email']);
		endif;
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$query = $this->db->get();
		$user = $query->result();

		if(!empty($user)){
			if(verifyHashedPassword($data['password'], $user[0]->password)){ 
                            
                            
                            /** start check device login *****/
                                $this->db->set('is_active', '1');
                                $this->db->where('userId', $user[0]->userID);
                                $this->db->update('hiprofile_users');
                            
                                
                                if(isset($data['force_login']) && $data['force_login'] != 'yes'){
                                    $this->db->select('BaseTbl.*');
                                    $this->db->from('hiprofile_user_device as BaseTbl');
                                    $this->db->where('BaseTbl.userId',$user[0]->userID);
                                    $this->db->where('BaseTbl.status', 'Active');
                                    $query1 = $this->db->get();
                                    $user_device = $query1->result();

                                    if(!empty($user_device)){
                                        $return_res = array();
                                        $return_res['responsecode'] = "301";
                                        $return_res['status'] = "Already logged in with another device.Do you want to proceed?";
                                        return $return_res;                                   
                                    }else{
                                        $data1['userId'] = $user[0]->userID;
                                        $data1['device_token'] = $data['device_token'];                                    
                                        $data1['device_id'] = $data['device_id'];
                                        $data1['device_type'] = $data['device_type'];
                                        $data1['status'] = 'Active';
                                        $this->db->insert('hiprofile_user_device', $data1);
                                        $insert_id = $this->db->insert_id();                                    
                                    }
                                }
                            /** end check device login *****/    
                                
				$user[0]->registration_timestamp = json_decode(json_encode(time()), FALSE);
				$user[0]->fbUserId = ($user[0]->fbUserId == 0)?"":$user[0]->fbUserId;
				$user[0]->twitterUserId = ($user[0]->twitterUserId == 0)?"":$user[0]->twitterUserId;
				$user[0]->instagramUserId = ($user[0]->instagramUserId == 0)?"":$user[0]->instagramUserId;
				$user[0]->rating = ($user[0]->rating == "")?"0":$user[0]->rating;
				$user[0]->feedback = ($user[0]->feedback == "")?"":$user[0]->feedback;
				$users[0]->share_mobile	 = ($user[0]->share_mobile == "0")?"false":"true";
				$users[0]->location_disclose	 = $user[0]->location_disclose;
				$user[0]->rank = strval(round($user[0]->social_rank + $user[0]->rank + $user[0]->rank_f2));
				unset($user[0]->social_rank);
				unset($user[0]->rank_f2);
                                
                                if(isset($data['force_login']) && $data['force_login'] == 'yes'){
                                    $this->db->select('BaseTbl.*');
                                    $this->db->from('hiprofile_user_device as BaseTbl');
                                    $this->db->where('BaseTbl.userId',$user[0]->userID);
                                    $this->db->where('BaseTbl.status','Active');
                                    $query2 = $this->db->get();
                                    $user_device2 = $query2->result();
                                    
                                    if(!empty($user_device2)){
                                        
                                            $this->db->set('status', 'Inactive');
                                            $this->db->where('id', $user_device2[0]->id);
                                            if($this->db->update('hiprofile_user_device')){
                                                $data2['userId'] = $user[0]->userID;
                                                $data2['device_token'] = $data['device_token'];                                    
                                                $data2['device_id'] = $data['device_id'];
                                                $data2['device_type'] = $data['device_type'];
                                                $data2['status'] = 'Active';
                                                $this->db->insert('hiprofile_user_device', $data2);
                                                $insert_id = $this->db->insert_id();                                                
                                            }
                                            
                                       
                                    }
                                    
                                    
                                }
				$return_res = array();
				$return_res['responsecode'] = "200";
				$return_res['status'] 		= "success";
				$return_res['user'] 		= $user[0];
				
				return $return_res;
			}
			else{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['status'] = "Invalid email / username or password";
				return $return_res;
			}
		}
		else{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "Invalid email / username or password";
			return $return_res;
		}
	}
	
	 //API call - To Do The Record Registration
	public function doregister($data){ 
		unset($data['api_key']);
		$data['password'] = getHashedPassword($data['password']);
		$data['createdBy'] = 0;
		$data['createdDtm'] = date('Y-m-d H:i:s');
		$this->db->insert('hiprofile_users', $data);
		$insert_id = $this->db->insert_id();
		if (is_numeric($insert_id)){
			$insert_data = array(
				'userId' => $insert_id,
				'old_rank' => 0,
				'points_required'=>'',
				'worth'=>0,
				'bio'=>'',
				'facebook_profile_url'=>'',
				'twitter_profile_url'=>'',
				'instagram_profile_url'=>'',
				'friend_user_id'=>'',
				'email'=>''
			);
			$this->db->insert('hiprofile_user_socialsync', $insert_data);
			
			/*Email verification*/
			$encoded_email = urlencode(str_rot13($data['email']));
			$this->load->helper('string');
			$data['email'] = $data['email'];
			$data['activation_id'] = uniqid('email_', true);
			$data['createdDtm'] = date('Y-m-d H:i:s');
			$save = $this->userapi_model->sendVerificatinEmail($data['email'],$insert_id,$data['activation_id']);
			if($save)
			{					
				$data1['reset_link'] = base_url() . "verificationemail/" . $data['activation_id'] . "/" . $encoded_email;
				$userInfo = $this->userapi_model->getCustomerInfoByEmail($data['email']);
				if(!empty($userInfo))
				{
					$data1["name"] = $userInfo[0]->name;
					$data1["email"] = $userInfo[0]->email;
					$data1["message"] = "Verify Your Email";
					$sendStatus = verifyEmail($data1);
					if($sendStatus)
					{
						/* $result = array();
						$result = $this->userapi_model->geteditedprofile($data);
						$this->response($result);  */
						$return_res = array();
						$user = new stdClass();
						$user->userID = "$insert_id";
						$user->username = $data['username'];
						$user->registration_timestamp = json_decode(json_encode(time()), FALSE);
						$return_res['responsecode'] = "200";
						$return_res['status'] 		= "success";
						$return_res['user'] 		= $user;
						return $return_res;
					} 
					else 
					{
						$result = array();
						$result['responsecode'] = "500";
						$result['status'] = "Something went wrong. Please try again later.";
						//$this->response($result);
						return $result;
					}
				}
				else 
				{
					$result = array();
					$result['responsecode'] = "500";
					$result['status'] = "Something went wrong. Please try again later.";
					//$this->response($result);
					return $result;
				}
			} 
			else 
			{	
				$result = array();
				$result['responsecode'] = "5001";
				$result['status'] = "Something went wrong. Please try again later.";
				//$this->response($result);
				return $result;
			}			
		}else{
			$return_res = array();
			$return_res['responsecode'] = "5002";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
	}
	
	/******************************************************************************************************************/	
	
	/* Function to check the social login is the customer is already registered or not and do calculation*/
	
	function getsocialLogin($data,$id)
	{
		unset($data['api_key']);
		$userid = $id;
		if($data['auth_provider'] == 'facebook')
		{
			$fbuserid 	    = trim($data['social_id']);
			$fb_friendcount = $data['friendcount'];
			
			//facebook count and userid and auth update
			//$data['profileimage'] = ($data['profileimage'] != 0)?$data['profileimage']:"";
			$data['profileURL'] = ($data['profileURL'] != "")?$data['profileURL']:"";
			$this->db->set('auth_provider_fb', $data['auth_provider']);
			$this->db->set('fbuserid', $fbuserid);
			$this->db->set('fb_friendcount', $fb_friendcount);
			//$this->db->set('profileimage', $data['profileimage']);
			$this->db->set('facebook_profile_url', $data['profileURL']);
			$this->db->where('userId', $userid);
			$this->db->update('hiprofile_user_socialsync');
			
		}
		elseif($data['auth_provider'] == 'instagram')
		{
			$instagramuserid  = trim($data['social_id']);
			$inst_friendcount = $data['friendcount'];
			
			//instagram count and userid and auth update
			//$data['profileimage'] = ($data['profileimage'] != 0)?$data['profileimage']:"";
			$data['profileURL'] = ($data['profileURL'] != "")?$data['profileURL']:"";
			$this->db->set('auth_provider_inst', $data['auth_provider']);
			$this->db->set('instagramuserid', $instagramuserid);
			$this->db->set('inst_friendcount', $inst_friendcount);
			//$this->db->set('profileimage', $data['profileimage']);
			$this->db->set('instagram_profile_url', $data['profileURL']);
			$this->db->where('userId', $userid);
			$this->db->update('hiprofile_user_socialsync');
		}
		elseif($data['auth_provider'] == 'twitter')
		{
			$twitteruserid 	     = trim($data['social_id']);
			$twt_friendcount	 = $data['friendcount'];
			
			//twitter count and userid and auth update
			//$data['profileimage'] = ($data['profileimage'] != 0)?$data['profileimage']:"";
			$data['profileURL'] = ($data['profileURL'] != "")?$data['profileURL']:"";
			$this->db->set('auth_provider_twt', $data['auth_provider']);
			$this->db->set('twitteruserid', $twitteruserid);
			$this->db->set('twt_friendcount', $twt_friendcount);
			//$this->db->set('profileimage', $data['profileimage']);
			$this->db->set('twitter_profile_url', $data['profileURL']);
			$this->db->where('userId', $userid);
			$this->db->update('hiprofile_user_socialsync', $insert_data);
		}

		if($this->db->affected_rows() >= 0)
		{
			$this->db->select('*');
			$this->db->from('hiprofile_user_socialsync');
			$this->db->where('userId', $userid);
			$query = $this->db->get();
			$user = $query->result();
			
			//To get the user social rank for push notification
			$usersocialfinalrank1 = $this->getusersocialrank($userid);
			
			//variable for fb,twitter,instagram,followers count of fb,instagram and twitter
			$fb			  = trim($user[0]->auth_provider_fb);
			$twit		  = trim($user[0]->auth_provider_twt);
			$insta		  = trim($user[0]->auth_provider_inst);
			$fb_follow 	  = trim($user[0]->fb_friendcount) == "" ? 0 : trim($user[0]->fb_friendcount);
			$twit_follow  = trim($user[0]->twt_friendcount) == "" ? 0 : trim($user[0]->twt_friendcount);
			$insta_follow = trim($user[0]->inst_friendcount) == "" ? 0 : trim($user[0]->inst_friendcount);

			
			if($fb == "" && $twit == "" && $insta == "")
			{
				$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*0 + ($twit_follow)*0)+1;
			} 
			elseif($fb == "" && $twit != "" && $insta == "") 
			{
				$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*0 + ($twit_follow)*1)+1;
			} 
			elseif($fb == "" && $twit == "" && $insta != "") 
			{
				$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*1 + ($twit_follow)*0)+1;
			} 
			elseif($fb == "" && $twit != "" && $insta != "") 
			{
				$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*0.5 + ($twit_follow)*0.5)+1;
			} 
			elseif($fb != "" && $twit == "" && $insta == "") 
			{
				$PointsA = 1*(($fb_follow)*1 + ($insta_follow)*0 + ($twit_follow)*0)+1;
			} 
			elseif($fb != "" && $twit != "" && $insta == "") 
			{
				$PointsA = 1*(($fb_follow)*0.55 + ($insta_follow)*0 + ($twit_follow)*0.45)+1;
			} 
			elseif($fb != "" && $twit == "" && $insta != "") 
			{
				$PointsA = 1*(($fb_follow)*0.55 + ($insta_follow)*0.45 + ($twit_follow)*0)+1;
			} 
			elseif($fb != "" && $twit != "" && $insta != "") 
			{
				$PointsA = 1*(($fb_follow)*0.4 + ($insta_follow)*0.3 + ($twit_follow)*0.3)+1;
			}
			
			if($PointsA > 1000) 
			{
				if($PointsA > 100000000) 
				{
					if($PointsA > 200000000) 
					{
						//Fame Rank 100+
						$RankA =101;
					} 
					else 
					{
						//Fame Rank 90-100
						$RankA = Round(log10($PointsA)*33.22 - 175.75);
					}
				} 
				else 
				{
					//Fame Rank 16-90
					$RankA = Round(log10($PointsA)*15 - 30);
				}
			} 
			else
			{
				$RankA = Round(log10($PointsA)*4.67) + 1;
			}
			
			$insert_data = array(
			//'profileimage' => ($data['profileimage'] != 0)?$data['profileimage']:"",
				'social_rank' => ($RankA != 0)?$RankA:0,
				'social_points' => ($PointsA != 0)?$PointsA:0
			);
			$this->db->where('userId', $data['userID']);
			$this->db->update('hiprofile_user_socialsync', $insert_data);		
			if ($this->db->affected_rows() >= 0)
			{
				$this->db->select('points,rank,rank_f2');
				$this->db->from('hiprofile_user_socialsync');
				$this->db->where('userId', $userid);
				$query = $this->db->get();
				$user = $query->result();
				$finalrank = $user[0]->rank+$RankA;
				$return_res = array();
				
				$getuserfriend = $this->get_user_status_friend_social($userid,$userid);

				$getearnedpoints = $this->get_user_earned_points($userid,$userid);
				$sociallogincheck = 1;
				$friendsList = $this->getsocial_friend_requested_user_details($userid,$getuserfriend,$getearnedpoints,$userid,$sociallogincheck);
				//To send the push notification when an increase in the social rank
				$usersocialfinalrank2 = $this->getusersocialrank($userid);
				if($usersocialfinalrank2 > $usersocialfinalrank1)
				{
					$data = array();
					$data['userID'] = $userid;
					sendrankpush($data);
					iossendrankpush($data);
				}
				return $friendsList;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
			/* $return_res['responsecode'] = "200";
			$return_res['responsedetails'] 		= "success";
			$return_res['rank'] 		= $RankA;
			$return_res['points'] 		= $PointsA;
			return $return_res; */
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}

		
	}	
	
	/*Function to chec the user already registere or not if not register and do calculation*/
	
	function getsocialRegister($data)
	{
		unset($data['api_key']);
		$firstinsert_data = array(
			'password' => getHashedPassword($data['password']),
			'createdBy' => 0,
			'createdDtm' => $this->get_current_datime(),
			'device_token' =>$data['device_token'],
			'device_id' =>$data['device_id'],
			'device_token' =>$data['device_type'],
			'name' =>$data['name'],
			'email' =>$data['email'],
			'username' =>$data['username'],
			'dob' =>$data['dob'],
			'gender' =>$data['gender'],
			'mobile' =>$data['mobile'],
			'latitude' =>$data['latitude'],
			'longitude' =>$data['longitude']
		);
		$this->db->insert('hiprofile_users', $firstinsert_data);
		$insert_id = $this->db->insert_id();
		if (is_numeric($insert_id))
		{
			$socialinsert_data = array(
				'userId' => $insert_id
			);
			$this->db->insert('hiprofile_user_socialsync', $socialinsert_data);
			if ($this->db->affected_rows() >= 0)
			{				
				//to calculate social point
				if($data['auth_provider'] == 'facebook')
				{
					$fbuserid 	    = trim($data['social_id']);
					$fb_friendcount = $data['friendcount'];
					
					//facebook count and userid and auth update
					$data['profileimage'] = ($data['profileimage'] != 0)?$data['profileimage']:"";
					$data['profileURL'] = ($data['profileURL'] != "")?$data['profileURL']:"";
					$this->db->set('auth_provider_fb', $data['auth_provider']);
					$this->db->set('fbuserid', $fbuserid);
					$this->db->set('fb_friendcount', $fb_friendcount);
					//$this->db->set('profileimage', $data['profileimage']);
					$this->db->set('facebook_profile_url', $data['profileURL']);
					$this->db->where('userId', $insert_id);
					$this->db->update('hiprofile_user_socialsync');
					
				}
				elseif($data['auth_provider'] == 'instagram')
				{
					$instagramuserid  = trim($data['social_id']);
					$inst_friendcount = $data['friendcount'];
					
					//instagram count and userid and auth update
					$data['profileimage'] = ($data['profileimage'] != 0)?$data['profileimage']:"";
					$data['profileURL'] = ($data['profileURL'] != "")?$data['profileURL']:"";
					$this->db->set('auth_provider_inst', $data['auth_provider']);
					$this->db->set('instagramuserid', $instagramuserid);
					$this->db->set('inst_friendcount', $inst_friendcount);
					//$this->db->set('profileimage', $data['profileimage']);
					$this->db->set('instagram_profile_url', $data['profileURL']);
					$this->db->where('userId', $insert_id);
					$this->db->update('hiprofile_user_socialsync');
				}
				elseif($data['auth_provider'] == 'twitter')
				{
					$twitteruserid 	     = trim($data['social_id']);
					$twt_friendcount	 = $data['friendcount'];
					
					//twitter count and userid and auth update
					$data['profileimage'] = ($data['profileimage'] != 0)?$data['profileimage']:"";
					$data['profileURL'] = ($data['profileURL'] != "")?$data['profileURL']:"";
					$this->db->set('auth_provider_twt', $data['auth_provider']);
					$this->db->set('twitteruserid', $twitteruserid);
					$this->db->set('twt_friendcount', $twt_friendcount);
					//$this->db->set('profileimage', $data['profileimage']);
					$this->db->set('twitter_profile_url', $data['profileURL']);
					$this->db->where('userId', $insert_id);
					$this->db->update('hiprofile_user_socialsync');
					
				}
				if($this->db->affected_rows() >= 0)
				{
					$this->db->select('*');
					$this->db->from('hiprofile_user_socialsync');
					$this->db->where('userId', $insert_id);
					$query = $this->db->get();
					$user = $query->result();
					
					//To get the user social rank for push notification
					$usersocialfinalrank1 = $this->getusersocialrank($insert_id);
					
					//variable for fb,twitter,instagram,followers count of fb,instagram and twitter
					$fb			  = trim($user[0]->auth_provider_fb);
					$twit		  = trim($user[0]->auth_provider_twt);
					$insta		  = trim($user[0]->auth_provider_inst);
					$fb_follow 	  = trim($user[0]->fb_friendcount) == "" ? 0 : trim($user[0]->fb_friendcount);
					$twit_follow  = trim($user[0]->twt_friendcount) == "" ? 0 : trim($user[0]->twt_friendcount);
					$insta_follow = trim($user[0]->inst_friendcount) == "" ? 0 : trim($user[0]->inst_friendcount);

					
					if($fb == "" && $twit == "" && $insta == "")
					{
						$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*0 + ($twit_follow)*0)+1;
					} 
					elseif($fb == "" && $twit != "" && $insta == "") 
					{
						$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*0 + ($twit_follow)*1)+1;
					} 
					elseif($fb == "" && $twit == "" && $insta != "") 
					{
						$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*1 + ($twit_follow)*0)+1;
					} 
					elseif($fb == "" && $twit != "" && $insta != "") 
					{
						$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*0.5 + ($twit_follow)*0.5)+1;
					} 
					elseif($fb != "" && $twit == "" && $insta == "") 
					{
						$PointsA = 1*(($fb_follow)*1 + ($insta_follow)*0 + ($twit_follow)*0)+1;
					} 
					elseif($fb != "" && $twit != "" && $insta == "") 
					{
						$PointsA = 1*(($fb_follow)*0.55 + ($insta_follow)*0 + ($twit_follow)*0.45)+1;
					} 
					elseif($fb != "" && $twit == "" && $insta != "") 
					{
						$PointsA = 1*(($fb_follow)*0.55 + ($insta_follow)*0.45 + ($twit_follow)*0)+1;
					} 
					elseif($fb != "" && $twit != "" && $insta != "") 
					{
						$PointsA = 1*(($fb_follow)*0.4 + ($insta_follow)*0.3 + ($twit_follow)*0.3)+1;
					}
					
					if($PointsA > 1000) 
					{
						if($PointsA > 100000000) 
						{
							if($PointsA > 200000000) 
							{
								//Fame Rank 100+
								$RankA =101;
							} 
							else 
							{
								//Fame Rank 90-100
								$RankA = Round(log10($PointsA)*33.22 - 175.75);
							}
						} 
						else 
						{
							//Fame Rank 16-90
							$RankA = Round(log10($PointsA)*15 - 30);
						}
					} 
					else
					{
						$RankA = Round(log10($PointsA)*4.67) + 1;
					}
					
					$insert_data2 = array(
					//'profileimage' => ($data['profileimage'] != 0)?$data['profileimage']:"",
						'social_rank' => ($RankA != 0)?$RankA:0,
						'social_points' => ($PointsA != 0)?$PointsA:0
					);
					$this->db->where('userId', $insert_id);
					$this->db->update('hiprofile_user_socialsync', $insert_data2);		
					if ($this->db->affected_rows() >= 0)
					{
						/*Email verification*/
						$encoded_email = urlencode(str_rot13($data['email']));
						$this->load->helper('string');
						$data['email'] = $data['email'];
						$data['activation_id'] = uniqid('email_', true);
						$data['createdDtm'] = date('Y-m-d H:i:s');
						$save = $this->userapi_model->sendVerificatinEmail($data['email'],$insert_id,$data['activation_id']);
						if($save)
						{					
							$data1['reset_link'] = base_url() . "verificationemail/" . $data['activation_id'] . "/" . $encoded_email;
							$userInfo = $this->userapi_model->getCustomerInfoByEmail($data['email']);
							if(!empty($userInfo))
							{
								$data1["name"] = $userInfo[0]->name;
								$data1["email"] = $userInfo[0]->email;
								$data1["message"] = "Verify Your Email";
								$sendStatus = verifyEmail($data1);
								if($sendStatus)
								{
									$getuserfriend = $this->get_user_status_friend_social($insert_id,$insert_id);

									$getearnedpoints = $this->get_user_earned_points($insert_id,$insert_id);
									$sociallogincheck = 0;
									$friendsList = $this->getsocial_friend_requested_user_details($insert_id,$getuserfriend,$getearnedpoints,$insert_id,$sociallogincheck);
										//To send the push notification when an increase in the social rank
									$usersocialfinalrank2 = $this->getusersocialrank($insert_id);
									if($usersocialfinalrank2 > $usersocialfinalrank1)
									{
										$data = array();
										$data['userID'] = $insert_id;
										sendrankpush($data);
										iossendrankpush($data);
									}
									return $friendsList;									
								} 
								else 
								{
									$result = array();
									$result['responsecode'] = "500";
									$result['status'] = "Something went wrong. Please try again later.";
									$this->response($result);
								}
							}
							else 
							{
								$result = array();
								$result['responsecode'] = "500";
								$result['status'] = "Something went wrong. Please try again later.";
								$this->response($result);
							}
						} 
						else 
						{	
							$result = array();
							$result['responsecode'] = "500";
							$result['status'] = "Something went wrong. Please try again later.";
							$this->response($result);
						}							
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
			}		
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
	}
	
	//Fucntion To get user details when sending friend request
	function getsocial_friend_requested_user_details($frnduserid,$getuserfriend,$getearnedpoints,$userid,$sociallogincheck){
		$this->db->select('BaseTbl.userId as userID,BaseTbl.name, BaseTbl.dob, BaseTbl.location_disclose, BaseTbl.gender, BaseTbl.gender_disclose,	BaseTbl.mobile as mobileNumber, BaseTbl.email,BaseTbl.email_status, BaseTbl.username as userName,BaseTbl.rating,BaseTbl.feedback,BaseTbl.approachability,BaseTbl.share_mobile,social.social_rank,social.rank,social.rank_f2,social.points,social.worth,social.bio,social.points_required,social.fbuserid as fbUserId,twitteruserid as twitterUserId, instagramuserid as instagramUserId, social.facebook_profile_url as fbURL,social.twitter_profile_url as twitterURL, instagram_profile_url as instagramURL,social.profileimage as image_url');

		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where('BaseTbl.userId', $frnduserid);
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$this->db->order_by('BaseTbl.userId', 'desc');
		$query = $this->db->get();
		$user = $query->result();
		$usercount = count($user);
		$users[] = new stdClass();
		$userdetails = array();
		if($usercount != 0)
		{
			for($i=0;$i<$usercount;$i++){

				$users[$i]->userID = $user[$i]->userID;
				$users[$i]->name = $user[$i]->name;
				$users[$i]->dob = $user[$i]->dob;
				$users[$i]->gender = strtolower($user[$i]->gender);
				$users[$i]->gender_disclose = $user[$i]->gender_disclose;
				$users[$i]->mobileNumber = $user[$i]->mobileNumber;
				$users[$i]->email = $user[$i]->email;
				$users[$i]->email_status = $user[$i]->email_status;
				$users[$i]->userName = $user[$i]->userName;
				$users[$i]->points = $user[$i]->points;
				$users[$i]->rating = $user[$i]->rating;
				$users[$i]->feedback = $user[$i]->feedback;
				$users[$i]->approachability = $user[$i]->approachability;
				//$users[$i]->worth = $user[$i]->worth;
				$users[$i]->worth = $this->getCalculateWorth($userid,$frnduserid);
				$users[$i]->bio = $user[$i]->bio;
				$users[$i]->registration_timestamp = json_decode(json_encode(time()), FALSE);
				$users[$i]->fbUserId = ($user[$i]->fbUserId == 0)?"":$user[$i]->fbUserId;
				$users[$i]->twitterUserId = ($user[$i]->twitterUserId == 0)?"":$user[$i]->twitterUserId;
				$users[$i]->instagramUserId = ($user[$i]->instagramUserId == 0)?"":$user[$i]->instagramUserId;
				$users[$i]->fbURL = ($user[$i]->fbURL == "")?"":$user[$i]->fbURL;
				$users[$i]->twitterURL = ($user[$i]->twitterURL == "")?"":$user[$i]->twitterURL;
				$users[$i]->instagramURL	 = ($user[$i]->instagramURL == "")?"":$user[$i]->instagramURL;
				$users[$i]->image_url	 = ($user[$i]->image_url == "")?"":base_url().'assets'.$user[$i]->image_url;
				$users[$i]->points_required	 = ($user[$i]->points_required == "")?"":$user[$i]->points_required;
				$users[$i]->share_mobile	 = ($user[$i]->share_mobile == "0")?"false":"true";
				$users[$i]->location_disclose	 = $user[$i]->location_disclose ;
				$users[$i]->rating = ($user[$i]->rating == "")?"0":$user[$i]->rating;
				$users[$i]->feedback = ($user[$i]->feedback == "")?"":$user[$i]->feedback;
				//$users[$i]->rank = strval(round($user[$i]->social_rank + $user[$i]->rank + $user[$i]->rank_f2));
				$users[$i]->rank = strval($user[$i]->social_rank + $user[$i]->rank);
				$users[$i]->earned_points = (string)$getearnedpoints;
				$users[$i]->last_seen = (string)getLastSeenDataUser($frnduserid);
				unset($user[$i]->social_rank);
				unset($user[$i]->rank_f2);	
				/*New object to check the user active package*/
				$activerankplan   = $this->getactiveuserrankplans($user[$i]->userID);
				$activepointsplan = $this->getactiveuserpointsplans($user[$i]->userID);
				$users[$i]->activated_double_points = ($activepointsplan == "")?"":$activepointsplan;
				$users[$i]->activated_higher_fame = ($activerankplan == "")?"":$activerankplan;
				$userdetails =(object) array_merge((array)$users[$i],(array)$getuserfriend[$i]);	
			}
			$return_res = array();
			$return_res['responsecode'] 	= "200";
			$return_res['responsedetails'] 	= "success";
			if($sociallogincheck == 1){ $return_res['already_registered'] = "true"; }
			$return_res['user'] 	= $userdetails;
			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
		
	}
	
	/******************************************************************************************************/
	
     //API call to check whether email already exist or not
	public function checkemailexists($data)
	{

		$this->db->select('email');
		$this->db->from('hiprofile_users');
		$this->db->where('email',$data);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return 1;
		} 
		else
		{
			return 0;
		} 
	}
	
	//API to get the users totalrank
	function getuserrank($userId)
	{
		$this->db->select('rank');
		$this->db->from('hiprofile_user_socialsync');
		$this->db->where('userId',$userId);
		$query = $this->db->get();
		$user = $query->result();
		$rank = $user[0]->rank;
		return $rank;
	}
	
	//API to get the users socialtotalrank
	function getusersocialrank($userId)
	{
		$this->db->select('social_rank');
		$this->db->from('hiprofile_user_socialsync');
		$this->db->where('userId',$userId);
		$query = $this->db->get();
		$user = $query->result();
		$socialrank = round($user[0]->social_rank);
		return $socialrank;
	}
	
	//API call to check whether username already exist or not
	public function checkuserexists($data)
	{
		$this->db->select('username');
		$this->db->from('hiprofile_users');
		$this->db->where('username',$data);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return 1;
		} 
		else{
			return 0;
		} 
	}
	
	//API call to check whether username already exist or not
	public function checkuseremailexists($email = '', $userId = '')
	{
		$this->db->select('email');
		$this->db->from('hiprofile_users');
		$this->db->where('email',$email);
		$this->db->where('userId',$userId);
		$this->db->where('isDeleted', 0);
		$this->db->where('roleId', 0);
		$this->db->where('status', 0);
		$query = $this->db->get();
		if ($query->num_rows() == 1){
			return 1;
		} 
		else{
			return 0;
		} 
	}
	
	//API call to check whether mobilenumber already exist or not
	public function checkusermobileexists($data)
	{
		$this->db->select('mobile');
		$this->db->from('hiprofile_users');
		$this->db->where('mobile',$data);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return 1;
		} 
		else{
			return 0;
		} 
	}
	
	//API call to insert or update reset password link.
	public function resetUserPassword($data)
	{
		unset($data['api_key']);
		$this->db->select('*');
		$this->db->from('hiprofile_reset_password');
		$this->db->where('email', $data['email']);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		
		if($numRows >= 1)
		{
			$result = $this->db->update('hiprofile_reset_password', $data);
		}
		else
		{
			$result = $this->db->insert('hiprofile_reset_password', $data);
		}
		if($result) 
		{
			return TRUE;
		} 
		else 
		{
			return FALSE;
		}
	}
	

	//API function is to get customer information by email for forget password email
	function getCustomerInfoByEmail($email)
	{
		$this->db->select('userId, email, name');
		$this->db->from('hiprofile_users');
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('email', $email);
		$query = $this->db->get();

		return $query->result();
	}
	//API function is to update the device token of the user for his id
	function getupdateDeviceToken($data)
	{
		unset($data['api_key']);	
		$this->db->select('device_id','userId');
		$this->db->from('hiprofile_user_device');
		$this->db->where('userId', $data['userID']);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		$users = $query->result();
		$check = "newone";
		if(!empty($users))
		{
			foreach($users as $user)
			{
				if($user->device_id == $data['device_id'])
				{
					$check = "old";
				} 
			}
		}

		if($check == "newone") {
			$this->db->trans_start();
			$this->db->insert('hiprofile_user_device', $data);
			$insert_id = $this->db->insert_id();
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
			else 
			{	
				$this->db->select('registration_timestamp');
				$this->db->from('hiprofile_user_device');
				$this->db->where('id', $insert_id);
				$query = $this->db->get();
				$user = $query->result();
				$return_res = array();
				$return_res['responsecode'] = "200";
				$return_res['responsedetails'] = "Success";
				$return_res['registration_timestamp'] = strtotime($user[0]->registration_timestamp);
				return $return_res;
			}
		} 
		else 
		{
			$updetail = array(
				'device_token' => $data['device_token'],
				'device_type'  => $data['device_type']
			);
			$this->db->where('device_id', $data['device_id']);
			$this->db->update('hiprofile_user_device', $updetail);

			if ($this->db->affected_rows() >= 0)
			{
				$this->db->select('registration_timestamp');
				$this->db->from('hiprofile_user_device');
				$this->db->where('device_id', $data['device_id']);
				$query = $this->db->get();
				$user = $query->result();
				$return_res = array();
				$return_res['responsecode'] = "200";
				$return_res['responsedetails'] = "Success";
				$return_res['registration_timestamp'] = strtotime($user[0]->registration_timestamp);
				return $return_res;
			}
			else 
			{	
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
		}
		exit;
	}
	
	/*Start of load profile API*/

	//API function is to update the device details of the user for his id and load frined profile
	function getupdateDeviceDetails($data){
		unset($data['api_key']);

		$id="";
		$this->db->select('userId');
		$this->db->from('hiprofile_users');
		$this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		//Check user active or not
		
		if($numRows == 1) 
		{ 
			$myId = $data['userID'];		
			$friendId = $data['friend_user_id'];
			$checkMyStatus = $this->checkMyBlockStatus($myId,$friendId);
			$checkMyFriendStatus = $this->checkMyFriendBlockStatus($myId,$friendId);
			
			// to check if the user has already has sent friend request to any of the user
			$checkisStatus = $this->checkIsFriendStatus($myId,$friendId);
			$checkisFriendsStatus = $this->checkIsFriendsFriendStatus($myId,$friendId);
			
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{
				//If active
				//if active update in hiprofile users table
				$this->db->set('device_token', $data['device_token']);
				$this->db->set('device_id', $data['device_id']);
				$this->db->set('device_type', $data['device_type']);	
				$this->db->where('userId', $data['userID']);
				$this->db->where('isDeleted', 0);
				$this->db->where('status', 0);
				$this->db->update('hiprofile_users');

				$this->db->select('*');
				$this->db->from('hiprofile_free_package_list');
				$this->db->where('userId', $data['friend_user_id']);
				$query = $this->db->get();
				$result = $query->result_array();						
				if(!empty($result)){
					 $free_package_time = strtotime($result[0]['created_at']);
					 $current_time = strtotime($this->get_current_datime());
					 if(($current_time - $free_package_time) > 1800){
					 	$this->db->where('id', $result[0]['id']);
						$this->db->delete('hiprofile_free_package_list');
						$this->db->where('orderId', $result[0]['orderId']);
						$this->db->where('userId', $data['userID']);
						$this->db->delete('hiprofile_rank_points_plan_details');

					 }
				}
				


				if ($this->db->affected_rows() >= 0) // if hiprofile updated	
				{	
					if($data['userID'] == $data['friend_user_id']) 
					{
						//userid and requested friend userid same
						
						$getuserfriend = $this->get_user_status_friend_social($data['userID'],$data['friend_user_id']);
						
						$getearnedpoints = $this->get_user_earned_points($data['userID'],$data['friend_user_id']);
						
						$friendsList = $this->get_friend_requested_user_details($data['friend_user_id'],$getuserfriend,$getearnedpoints,$data['userID']);
						//add the friend request type
						$friend_req_type = $this->get_friend_requested_type($data['userID'],$data['friend_user_id']);
						$friendsList['data']['user']->friend_req_type = $friend_req_type;
						return $friendsList;
					}
					elseif($checkisStatus == 1 || $checkisFriendsStatus == 1)
					{
						
						//userid and requested friend userid same
						
						//echo "hello";die;
						$getuserfriend = $this->get_user_status_friend_social($data['userID'],$data['friend_user_id']);
						
						$getearnedpoints = $this->get_user_earned_points($data['userID'],$data['friend_user_id']);
						
						$friendsList = $this->get_friend_requested_user_details($data['friend_user_id'],$getuserfriend,$getearnedpoints,$data['userID']);

						//add the friend request type
						$friend_req_type = $this->get_friend_requested_type($data['userID'],$data['friend_user_id']);
						$friendsList['data']['user']->friend_req_type = $friend_req_type;
						return $friendsList;
					}
					else
					{
						//userid and requested friend userid not same
						//echo "hi";die;
						$addFriend = (isset($data['add_friend']) && !empty($data['add_friend'])) ? $data['add_friend'] : 'false';
						$this->db->select('friend_user_id, add_friend');
						$this->db->from('hiprofile_friends');
						$this->db->where('userId', $data['userID']);
						$this->db->where('friend_user_id', $data['friend_user_id']);
						$query = $this->db->get();
						$friend_request_sent_already = $query->result();

						if((strtolower($addFriend) == 'true' && empty($friend_request_sent_already))) 
						{
							//echo "hi";die;

							//check the request type
							$friend_req_type = isset($data['friend_req_type']) ? $data['friend_req_type'] : 'QR';
							//add friend request true
							$this->db->set('friend_user_id', $data['friend_user_id']);
							$this->db->set('request_status',"0");
							$this->db->set('add_friend ', 'true');
							$this->db->set('friend_req_type ', $friend_req_type);
							$this->db->set('userId', $data['userID']);
							$this->db->insert('hiprofile_friends');


							if ($this->db->affected_rows() >= 0) {

								$send_friend_request_notification = $this->update_friend_request_send_notification($data['userID'], $data['friend_user_id'], $data);

								if($send_friend_request_notification == true){

											/*if(strtolower($data['device_type']) == 'android'){
												friendRequestReceived($data);
											}
											if(strtolower($data['device_type']) == 'ios'){
												iosfriendRequestReceived($data);
											}*/
											
											//print_r($data);
											//die;
											iosfriendRequestReceived($data);
											friendRequestReceived($data);
											
											
											$getuserfriend = $this->get_user_status_friend_social($data['userID'],$data['friend_user_id']);
											
											$getearnedpoints = $this->get_user_earned_points($data['userID'],$data['friend_user_id']);
											
											$friendsList = $this->get_friend_requested_user_details($data['friend_user_id'],$getuserfriend,$getearnedpoints,$data['userID']);

											//add the friend request type
											$friend_req_type = $this->get_friend_requested_type($data['userID'],$data['friend_user_id']);
											$friendsList['data']['user']->friend_req_type = $friend_req_type;
											return $friendsList;
										}else{
											$return_res = array();
											$return_res['responsecode'] = "500";
											$return_res['status'] = "Something went wrong. Please try again later.";
											return $return_res;
										}
									}else {
										$return_res = array();
										$return_res['responsecode'] = "500";
										$return_res['status'] = "Something went wrong. Please try again later.";
										return $return_res;
									}
								}
								else if ((strtolower($addFriend) == 'false') || (strtolower($addFriend) == 'true' && !empty($friend_request_sent_already)) )
								{
									$myId = $data['userID'];
									$friendId = $data['friend_user_id'];
									$checkMyStatus = $this->checkMyBlockStatus($myId,$friendId);
									$checkMyFriendStatus = $this->checkMyFriendBlockStatus($myId,$friendId);

									if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
									{
									//add friend request false insert in friend request 0 add friend false
									//if userid already sent request
										$this->db->select('friend_user_id');
										$this->db->from('hiprofile_friends');
										$this->db->where('userId', $data['userID']);
										$this->db->where('friend_user_id', $data['friend_user_id']);
										$query = $this->db->get();
										$friend_request_sent_already = $query->result();
										$getuserfriend = $this->get_user_status_friend_social($data['userID'],$data['friend_user_id']);

										$getearnedpoints = $this->get_user_earned_points($data['userID'],$data['friend_user_id']);

										$friendsList = $this->get_friend_requested_user_details($data['friend_user_id'],$getuserfriend,$getearnedpoints,$data['userID']);

										//add the friend request type
										$friend_req_type = $this->get_friend_requested_type($data['userID'],$data['friend_user_id']);
										$friendsList['data']['user']->friend_req_type = $friend_req_type;
										return $friendsList;
									}
									else
									{

										$return_res = array();
										$return_res['responsecode'] = "201";
										$return_res['responsedetails'] = "This user id doesn’t exists";
										return $return_res;	

									}
								}
							}
						}
				else // if hiprofile not updated
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "This user id doesn’t exists";
				return $return_res;	
			}
		}
		else
		{ 
			//if not active
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
	}
	
	// fucntion to check if Friend is blocked me or not
	/*
	* Returns numofrows as 1 if already blocked or 0 if not value is been 
	*/
	function checkMyBlockStatus($myId,$friend_id){
		
		
		$this->db->select('*');
		$this->db->from('hiprofile_blocked_users');
		$this->db->where('userId', $friend_id);
		$this->db->where('friend_user_id', $myId);
		$this->db->where('blocked_status', "1");
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check i am blocked my Friend
	/*
	* Returns numofrows as 1 if already blocked or 0 if not value is been 
	*/
	function checkMyFriendBlockStatus($myId,$friend_id){
		
		
		$this->db->select('*');
		$this->db->from('hiprofile_blocked_users');
		$this->db->where('userId', $myId);
		$this->db->where('friend_user_id', $friend_id);
		$this->db->where('blocked_status', "1");
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	
	// fucntion to check if Friend is unfriend me or not
	/*
	* Returns numofrows as 1 if already unfriended or 0 if not value is been 
	*/
	function checkMyUnFriendStatus($myId,$friend_id){
		
		
		$this->db->select('*');
		$this->db->from('hiprofile_deleted_unfriend_lists');
		$this->db->where('userId', $friend_id);
		$this->db->where('friend_id', $myId);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check i am unfriended my Friend
	/*
	* Returns numofrows as 1 if already unfriended or 0 if not value is been 
	*/
	function checkMyFriendUnFriendedStatus($myId,$friend_id)
	{
		
		
		$this->db->select('*');
		$this->db->from('hiprofile_deleted_unfriend_lists');
		$this->db->where('userId', $myId);
		$this->db->where('friend_id', $friend_id);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check if the friend is been unblocked by me or not
	/*
	* Returns numofrows as 1 if already unblocked or 0 if not value is been 
	*/
	function checkMyUnBlockStatus($myId,$friend_id){
		
		
		$this->db->select('*');
		$this->db->from('hiprofile_deleted_blocked_users');
		$this->db->where('userId', $friend_id);
		$this->db->where('friend_id', $myId);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check friend blocked and unblocked
	/*
	* Returns numofrows as 1 if already ublocked or 0 if not value is been 
	*/
	function checkMyFriendUnBlockStatus($myId,$friend_id){

		$this->db->select('*');
		$this->db->from('hiprofile_deleted_blocked_users');
		$this->db->where('userId', $myId);
		$this->db->where('friend_id', $friend_id);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}

	// fucntion to check if Friend has friend request or not
	/*
	* Returns numofrows as 1 if already request send or 0 if no request sent
	*/
	function checkIsFriendStatus($myId,$friend_id){
		
		
		$this->db->select('*');
		$this->db->from('hiprofile_friends');
		$this->db->where('userId', $myId);
		$this->db->where('friend_user_id', $friend_id);
		$this->db->where('request_status', "0");
		$this->db->where('add_friend', "true");
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	// fucntion to check if Friend has declined request or not
	/*
	* Returns numofrows as 1 if already request send or 0 if no request sent
	*/
	function checkMyReqDeclinedStatus($myId,$friend_id){		
		$this->db->select('*');
		$this->db->from('hiprofile_declined_friend_requests');
		$this->db->where('userId_requesting', $myId);
		$this->db->where('userId_choice', $friend_id);
		$this->db->where('declined', 1);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}
	
	
	// fucntion to check if Friend has friend request or not
	/*
	* Returns numofrows as 1 if already request send or 0 if no request sent
	*/
	function checkIsFriendsFriendStatus($myId,$friend_id){
		
		
		$this->db->select('*');
		$this->db->from('hiprofile_friends');
		$this->db->where('userId', $friend_id);
		$this->db->where('friend_user_id', $myId);
		$this->db->where('request_status', "0");
		$this->db->where('add_friend', "true");
		$query = $this->db->get();
		$numRows = $query->num_rows();
		return $numRows;
	}

	
	// To send friend notification and calculate the rank
	function update_friend_request_send_notification($userId, $friendUserId, $data){
		
		//Check if the user are added before and been unblocked if not do the calcualtion or else just send friend notification
		$checkUnBlockedMyStatus = $this->checkMyUnBlockStatus($data['userID'],$data['friend_user_id']);
		$checkMyFriendUnBlockedStatus = $this->checkMyFriendUnBlockStatus($data['userID'],$data['friend_user_id']);
		
		//Check if the user are added before and been unfriended if not do the calcualtion or else just send friend notification
		$checkUnFriendedMyStatus = $this->checkMyUnFriendStatus($data['userID'],$data['friend_user_id']);
		$checkMyFriendUnFriendedStatus = $this->checkMyFriendUnFriendedStatus($data['userID'],$data['friend_user_id']);
		
		//check if the friend request is been declined or not 
		$checkDeclinedMyReqStatus = $this->checkMyReqDeclinedStatus($data['userID'],$data['friend_user_id']);
		
		if(($checkUnBlockedMyStatus == 0 && $checkMyFriendUnBlockedStatus ==0) && ($checkUnFriendedMyStatus == 0 && $checkMyFriendUnFriendedStatus == 0))
		{
			if($checkDeclinedMyReqStatus == 0)
			{
				$friend_req_type = isset($data['friend_req_type']) ? $data['friend_req_type'] : 'QR';
				if($friend_req_type=="QR")
				{
					$rankrange = $this->userFriendRankRange($data);
				}
				
			}
		}		
		//To add in notification and send to friend the request of the user
		//To select the profileimage,name,gender of the friend to insert in notification table
		$this->db->select('social.profileimage as friend_pic_url,BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.gender_disclose as friend_gender_disclose');
		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where('BaseTbl.userId', $userId);
		$query = $this->db->get();
		$result = $query->row_array();
		$this->db->set('userId',$userId);
		$this->db->set('friend_pic_url',$result['friend_pic_url']);
		$this->db->set('friend_name',$result['friend_name']);
		$this->db->set('friend_user_id',$friendUserId);
		$this->db->set('social_media', "");
		// To check gender disclosed or not
		if($result['friend_gender_disclose'] == 0):
			$this->db->set('friend_gender', 'undisclosed');
		else:
			$this->db->set('friend_gender', strtolower($result['friend_gender']));
		endif;
		$this->db->set('notification_type', 'friend_request_received');
		//$this->db->set('status', '2');
		$this->db->insert('hiprofile_notification_list');
		if ($this->db->affected_rows() >= 0)
		{
			$this->db->select('device_token as FCMtoken');
			$this->db->from('hiprofile_users');
			$this->db->where('userId', $friendUserId);
			$query = $this->db->get();
			$results = $query->row_array();
			$this->db->set('FCMtoken',$results['FCMtoken']);
			$this->db->where('friend_user_id', $friendUserId);
			$this->db->where('userId', $userId);
			$this->db->update('hiprofile_notification_list');
			if ($this->db->affected_rows() >= 0){
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}

	}

	
	
	//Fucntion to load the friends data of the user id
	function get_user_status_friend_social($userId,$friend_user_id){
		
		$user[] = new stdClass();
		//$this->db->select('friend.request_status as friend_req_status');
		$this->db->select('friend.request_status as friend_req_status,friend.friend_user_id,friend.userId,friend.friend_req_type');
		$this->db->from('hiprofile_friends as friend');
		//$this->db->where('friend.userId', $userId);
		//$this->db->where('friend.friend_user_id', $friend_user_id);
		
		$andwhere = '(friend.friend_user_id='.$friend_user_id.' and friend.userId = '.$userId.')';
		$orwhere = '(friend.friend_user_id='.$userId.' and friend.userId = '.$friend_user_id.')';
		$this->db->where($andwhere);
		$this->db->or_where($orwhere);
		
		$this->db->order_by('friend.friend_user_id', 'desc');
		$query = $this->db->get();
		$user1 = $query->result();
		$friendsacceptcount = count($user1);
		if($friendsacceptcount>0){
			for($i=0;$i<$friendsacceptcount;$i++)
			{
				if($user1[$i]->friend_user_id == $friend_user_id && $user1[$i]->userId==$userId)
				{	
					if($user1[$i]->friend_req_status == '1'):
						$user[$i]->friend_req_status = 'approved';
						$user[$i]->is_friend = 'true';
					elseif($user1[$i]->friend_req_status == '2'):
						$user[$i]->friend_req_status = 'rejected';
						$user[$i]->is_friend = 'false';
					elseif($user1[$i]->friend_req_status == '0'):
						$user[$i]->friend_req_status = 'pending';
						$user[$i]->is_friend = 'false';
					endif;	
				}else if($user1[$i]->friend_user_id == $userId && $user1[$i]->userId==$friend_user_id)
				{	

					if($user1[$i]->friend_req_status == '1'):
						$user[$i]->friend_req_status = 'approved';
						$user[$i]->is_friend = 'true';
					elseif($user1[$i]->friend_req_status == '2'):
						$user[$i]->friend_req_status = 'rejected';
						$user[$i]->is_friend = 'false';
					elseif($user1[$i]->friend_req_status == '0'):
						$user[$i]->friend_req_status = 'received';
						$user[$i]->is_friend = 'false';
					endif;

				}
				
			}
		}else
		{
			
			$user[0]->friend_req_status = 'none';
			$user[0]->is_friend = 'false';	
		}
		
		$this->db->select('
			social.facebook_status as req_fb_status,
			social.twitter_status as req_twitter_status,
			social.instagram_status as req_insta_status,
			social.contact_status as req_contact_status,
			social.request_status
			');
		$this->db->from('hiprofile_social_friends as social');
		$this->db->where('social.userId', $userId);
		$this->db->where('social.friend_user_id', $friend_user_id);
		$this->db->order_by('social.friend_user_id', 'desc');
		$query = $this->db->get();
		$user2 = $query->result();
		$friendsacceptcount = count($user2);
		// echo $friendsacceptcount;
		$constval = 0;
		$status_val = array("0"=>"none","1"=>"approved","2"=>"rejected","3"=>"cancelled","4"=>"pending");
		$status_Arr = array("req_fb_status","req_twitter_status","req_insta_status","req_contact_status");
		
		if($friendsacceptcount!= 0){
			$j = 0;
			for($i=0;$i<$friendsacceptcount;$i++){
				$temp_arr[] = new stdClass();
				
				if($user2[$i]->req_fb_status == 1) {
					$temp_arr[$j]->req_fb_status = $status_val[$user2[$i]->request_status];
				} 
				
				if($user2[$i]->req_twitter_status == 1) {
					$temp_arr[$j]->req_twitter_status = $status_val[$user2[$i]->request_status];
				} 
				
				if($user2[$i]->req_insta_status == 1) {
					$temp_arr[$j]->req_insta_status = $status_val[$user2[$i]->request_status];
				} 
				if($user2[$i]->req_contact_status == 1) {
					$temp_arr[$j]->req_contact_status = $status_val[$user2[$i]->request_status];
				} 
				$temp_arr[$j]->i = $i;
				$j++;
			}
			$i =0;
			$final_val = array();
			
			foreach($temp_arr as $key=>$value) {
				
				if($value->req_insta_status) {
					$final_val['req_insta_status'] = $value->req_insta_status;
				}
				if($value->req_contact_status) {
					$final_val['req_contact_status'] = $value->req_contact_status;
				}
				if($value->req_fb_status) {
					$final_val['req_fb_status'] = $value->req_fb_status;
				}
				if($value->req_twitter_status) {
					$final_val['req_twitter_status'] = $value->req_twitter_status;
				}
				if($value->i) {
					$final_val['i'] = $value->i;
				}
				
			}
			$final_keys = array_keys($final_val);
			
			$final_com = array_diff($status_Arr, $final_keys);

			if(count($final_com) >= 1) {
				foreach($final_com as $value){
					//pre($value);exit;
					if($value == "req_fb_status")      { $user[$i]->req_fb_status = "none"; }
					if($value == "req_twitter_status") { $user[$i]->req_twitter_status = "none"; }
					if($value == "req_insta_status")   { $user[$i]->req_insta_status = "none"; }
					if($value == "req_contact_status") { $user[$i]->req_contact_status = "none"; }
					
				}
			} 
			// pre($final_val);exit;
			if(count($final_val) >= 1) {
				foreach($final_val as $key=>$value){
					// echo $value;exit;
					if($key == "req_fb_status")      { $user[$i]->req_fb_status = $value; }
					if($key == "req_twitter_status") { $user[$i]->req_twitter_status = $value; }
					if($key == "req_insta_status")   { $user[$i]->req_insta_status = $value; }
					if($key == "req_contact_status") { $user[$i]->req_contact_status = $value; }
					
				}
			} 
			

			
			
			
			
		}else{
			$user[0]->req_fb_status	='none';
			$user[0]->req_contact_status='none';	
			$user[0]->req_twitter_status='none';
			$user[0]->req_insta_status='none';
		}

		return $user;
	}
	
	//Fucntion To get user details when sending friend request
	function get_friend_requested_user_details($frnduserid,$getuserfriend,$getearnedpoints,$userid)
	{
                
            
        $this->db->select('BaseTbl.QBuserName,BaseTbl.username');
		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where_in('BaseTbl.userId', $frnduserid);
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$query = $this->db->get();
		$user_arr = $query->result();                
                if(!empty($user_arr)){
                    if(empty($user_arr[0]->QBuserName) || $user_arr[0]->QBuserName == ''){
                        $this->db->set('QBuserName',$user_arr[0]->username);                        					
                        $this->db->where('userId', $frnduserid);
                        $this->db->update('hiprofile_users');
                    }
                }               
                
		$this->db->select('BaseTbl.userId as userID,BaseTbl.name, BaseTbl.dob, BaseTbl.location_disclose, BaseTbl.gender, BaseTbl.gender_disclose,BaseTbl.mobile as mobileNumber, BaseTbl.email,BaseTbl.email_status, BaseTbl.username as userName,BaseTbl.FramePurchaseCount,BaseTbl.QBuserName,BaseTbl.rating,BaseTbl.feedback,BaseTbl.approachability,BaseTbl.share_mobile,BaseTbl.canWatchDoublePointAd,BaseTbl.canWatchHigherFameAd,BaseTbl.watch_add_time,social.social_rank,social.rank,social.rank_f2,social.points,social.worth,social.bio,social.points_required,social.fbuserid as fbUserId,twitteruserid as twitterUserId, instagramuserid as instagramUserId, social.facebook_profile_url as fbURL,social.twitter_profile_url as twitterURL, instagram_profile_url as instagramURL,social.profileimage as image_url');

		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where_in('BaseTbl.userId', $frnduserid);
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$this->db->order_by('BaseTbl.userId', 'desc');

		$query = $this->db->get();
		$user = $query->result();
		$usercount = count($user);
		$users[] = new stdClass();
		$userdetails = array();
		/*echo "u_".$userid;
		echo "f_".$frnduserid;
		die;*/
		$this->db->select('BaseTbl.image_id');
		$this->db->from('hiprofile_user_six_photo_favourite_lists as BaseTbl');
		$this->db->where('BaseTbl.userId', $frnduserid);
		$this->db->where('BaseTbl.friend_user_id', $userid);
		$qry = $this->db->get();
		$fav_image_id = $qry->result();
		$favImage = "";
		if(count($fav_image_id) !=0){
			$favImage = $fav_image_id[0]->image_id;
		}

		$this->db->select('BaseTbl.id as image_id,BaseTbl.isSeen, BaseTbl.fav_count, BaseTbl.img_url,BaseTbl.photo_position');
		$this->db->from('hiprofile_user_six_photos as BaseTbl');
		if($frnduserid == $userid){
			$this->db->where_in('BaseTbl.userId', $userid);
		}else{
			$this->db->where_in('BaseTbl.userId', $frnduserid);
		}		
		
		$query1 = $this->db->get();
		$six_photo = $query1->result();
		$six_photos[] = new stdClass();

		if(count($six_photo) !=0){
			for($i=0;$i<count($six_photo);$i++){
				$val = '0';				
				if($favImage == $six_photo[$i]->image_id){
					$val = '1';
				}
				$six_photos[$i]->image_id = $six_photo[$i]->image_id;
				$six_photos[$i]->isSeen = $six_photo[$i]->isSeen;
				$six_photos[$i]->fav_count = $six_photo[$i]->fav_count != "" ? number_format_short($six_photo[$i]->fav_count) : 0 ;
				$six_photos[$i]->img_url =($six_photo[$i]->img_url == "")?"":base_url().'assets'.$six_photo[$i]->img_url;
				$six_photos[$i]->photo_position = $six_photo[$i]->photo_position;				
				$six_photos[$i]->is_favourite = $val;				
			}
		}
		if($usercount != 0)
		{
			for($i=0;$i<$usercount;$i++){

				$users[$i]->userID = $user[$i]->userID;
				$users[$i]->name = $user[$i]->name;
				$users[$i]->dob = $user[$i]->dob;
				$users[$i]->gender = strtolower($user[$i]->gender);
				$users[$i]->gender_disclose = $user[$i]->gender_disclose;
				$users[$i]->mobileNumber = $user[$i]->mobileNumber;
				$users[$i]->email = $user[$i]->email;
				$users[$i]->email_status = $user[$i]->email_status;
				$users[$i]->userName = $user[$i]->userName;
                                $users[$i]->QBuserName = !empty($user[$i]->QBuserName) ? $user[$i]->QBuserName : "";
                                $users[$i]->FramePurchaseCount = $user[$i]->FramePurchaseCount;
				$users[$i]->points = $user[$i]->points;
				$users[$i]->rating = $user[$i]->rating;
				$users[$i]->feedback = $user[$i]->feedback;
				$users[$i]->approachability = $user[$i]->approachability;
				//$users[$i]->worth = $user[$i]->worth;
				$users[$i]->worth = $this->getCalculateWorth($userid,$frnduserid);
				$users[$i]->bio = $user[$i]->bio;
				$users[$i]->registration_timestamp = json_decode(json_encode(time()), FALSE);
				$users[$i]->fbUserId = ($user[$i]->fbUserId == 0)?"":$user[$i]->fbUserId;
				$users[$i]->twitterUserId = ($user[$i]->twitterUserId == 0)?"":$user[$i]->twitterUserId;
				$users[$i]->instagramUserId = ($user[$i]->instagramUserId == 0)?"":$user[$i]->instagramUserId;
				$users[$i]->fbURL = ($user[$i]->fbURL == "")?"":$user[$i]->fbURL;
				$users[$i]->twitterURL = ($user[$i]->twitterURL == "")?"":$user[$i]->twitterURL;
				$users[$i]->instagramURL	 = ($user[$i]->instagramURL == "")?"":$user[$i]->instagramURL;
				$users[$i]->image_url	 = ($user[$i]->image_url == "")?"":base_url().'assets'.$user[$i]->image_url;
				$users[$i]->points_required	 = ($user[$i]->points_required == "")?"":$user[$i]->points_required;
				$users[$i]->share_mobile	 = ($user[$i]->share_mobile == "0")?"false":"true";
				$users[$i]->location_disclose = $user[$i]->location_disclose;
				$users[$i]->rating = ($user[$i]->rating == "")?"0":$user[$i]->rating;
				$users[$i]->feedback = ($user[$i]->feedback == "")?"":$user[$i]->feedback;
				//$users[$i]->rank = strval(round($user[$i]->social_rank + $user[$i]->rank + $user[$i]->rank_f2));
				$users[$i]->rank = strval($user[$i]->social_rank + $user[$i]->rank);
				$users[$i]->earned_points = (string)$getearnedpoints;
				$users[$i]->last_seen = (string)getLastSeenDataUser($frnduserid);
				unset($user[$i]->social_rank);
				unset($user[$i]->rank_f2);	
				/*New object to check the user active package*/
				$activerankplan   = $this->getactiveuserrankplans($user[$i]->userID);
				$activepointsplan = $this->getactiveuserpointsplans($user[$i]->userID);
				$users[$i]->activated_double_points = ($activepointsplan == "")?"":$activepointsplan;
				$users[$i]->activated_higher_fame = ($activerankplan == "")?"":$activerankplan;
				$users[$i]->canWatchDoublePointAd = $user[$i]->canWatchDoublePointAd;
				$users[$i]->canWatchHigherFameAd = $user[$i]->canWatchHigherFameAd;
                                $users[$i]->is_buying_eligble = $user[$i]->FramePurchaseCount >= 10 ? "false":"true";
				//$users[$i]->watch_add_time = $user[$i]->watch_add_time;
				$userdetails =(object) array_merge((array)$users[$i],(array)$getuserfriend[$i]);

				/*if($user[$i]->watch_add_time != ''){
					$current_time = strtotime(date('Y-m-d H:i:s'));
					$watch_time = 	strtotime($user[$i]->watch_add_time);



					if(($current_time - $watch_time) >= 120){

						$this->db->set('canWatchDoublePointAd','1');
						$this->db->set('canWatchHigherFameAd','1');						
						$this->db->set('watch_add_time',NULL);						
						$this->db->where('userId', $user[$i]->userID);
						$this->db->update('hiprofile_users');

						$this->db->select('Id,rank,old_rank,points,old_earned_points,social_rank');
						$this->db->from('hiprofile_user_socialsync');
						$this->db->where('userId', $user[$i]->userID);
						$query = $this->db->get();
						$result = $query->result_array();
						
						if(count($result) > 0){
							$id = $result[0]['Id'];
							$rank = $result[0]['old_rank'];
							$points = $result[0]['old_earned_points'];
							$this->db->set('points',$points);				
							$this->db->set('rank',$rank);				
							$this->db->set('old_earned_points',0);				
							$this->db->set('old_rank',0);				
							$this->db->where('id', $id);			
							$this->db->update('hiprofile_user_socialsync');
						}
					}
				}*/

				
			}
			
			//$userDetailsAndPhotos =(object) array_merge((array)$userdetails,(array)$six_photo);
			//$userdetails->{'six_photo'} = $six_photo;
			$return_res = array();
			$return_res['responsecode'] 	= "200";
			$return_res['responsedetails'] 	= "success";
			//$return_res['data']['user'] 	= $userDetailsAndPhotos;
			$return_res['data']['user'] 	= $userdetails;
			$return_res['data']['six_photo'] 	= $six_photos;

			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
		
	}
	/*To get the user rank which are active for the user*/
	function getactiveuserrankplans($userid)
	{
		$rankplan = "";
		$this->db->select('
			basetbl.product_id as google_play_product_id,
			jointbl.ranks_points_package_hours as valid_hours,
			jointbl.ranks_points_package_price as price,			
			basetbl.buying_timestamp,
			basetbl.purchase_json,
			basetbl.product_description,
			basetbl.product_title,
			basetbl.orderId,
			basetbl.purchase_token,
			basetbl.is_activated,
			basetbl.activation_time
			');
		$this->db->from('hiprofile_rank_points_plan_details as basetbl');
		$this->db->join('hiprofile_rank_point_caetogry_packages_list as jointbl','jointbl.ranks_points_package_Google_playId = basetbl.product_id','left');
		$this->db->where('basetbl.category','hf');
		$this->db->where('basetbl.userId',$userid);
		$this->db->where('basetbl.is_activated',1);	
		$query = $this->db->get();
		$rankplan = $query->result();
		$rp_count = count($rankplan);
		if($rp_count > 0)
		{	
			$rp_count = count($rankplan);
			
			for($i=0;$i<$rp_count;$i++)
			{
				$rank_ad = $this->get_product_rank_categorypackage_list($rankplan[$i]->google_play_product_id);
				$rankplan[$i]->price	 = ($rankplan[$i]->price == "")?"":'$'.$rankplan[$i]->price;
				$rankplan[$i]->ranks_added  = ($rank_ad =="")?"":$rank_ad;
				$rankplan[$i]->is_activated	 = ($rankplan[$i]->is_activated == 0)?"false":"true";
				//date_default_timezone_set('Asia/Calcutta');
				$rankplan[$i]->activation_time	 = ($rankplan[$i]->activation_time == "")?"":strval(strtotime($rankplan[$i]->activation_time)*1000);
				$rankplan[$i]->valid_hours  = ($rankplan[$i]->valid_hours =="")?"":$rankplan[$i]->valid_hours;
				
			}	
			
			return $rankplan[0];
		}
		else
		{
			$oVal = new stdClass();
			return $oVal;
		}
	}
	/*To get the user points which are active for the user*/
	function getactiveuserpointsplans($userid)
	{
		$pointplan="";
		$this->db->select('
			basetbl.product_id as google_play_product_id,
			jointbl.ranks_points_package_hours as valid_hours,
			jointbl.ranks_points_package_price as price,			
			basetbl.buying_timestamp,
			basetbl.purchase_json,
			basetbl.product_description,
			basetbl.product_title,
			basetbl.orderId,
			basetbl.purchase_token,
			basetbl.is_activated,
			basetbl.activation_time
			');
		$this->db->from('hiprofile_rank_points_plan_details as basetbl');
		$this->db->join('hiprofile_rank_point_caetogry_packages_list as jointbl','jointbl.ranks_points_package_Google_playId = basetbl.product_id','left');
		$this->db->where('basetbl.category','dp');	
		$this->db->where('basetbl.userId',$userid);	
		$this->db->where('basetbl.is_activated',1);			
		$query = $this->db->get();
		$pointplan = $query->result();

		//echo $this->db->last_query();die;
		$dp_count = count($pointplan);
		if($dp_count > 0)
		{	
			$dp_count = count($pointplan);
			for($i=0;$i<$dp_count;$i++)
			{
				$pointplan[$i]->price	 = ($pointplan[$i]->price == "")?"":'$'.$pointplan[$i]->price;
				$pointplan[$i]->ranks_added  = "";
				$pointplan[$i]->is_activated	 = ($pointplan[$i]->is_activated == 0)?"false":"true";
				//date_default_timezone_set('Asia/Calcutta');
				$pointplan[$i]->activation_time	 = ($pointplan[$i]->activation_time == "")?"":strval(strtotime($pointplan[$i]->activation_time)*1000);
				$pointplan[$i]->valid_hours  = ($pointplan[$i]->valid_hours =="")?"":$pointplan[$i]->valid_hours;
			}
			return $pointplan[0];
		}
		else
		{
			$oVal = new stdClass();
			return $oVal;
		}
	}
	/*To get the rank from the product key to supply */
	function get_product_rank_categorypackage_list($id)
	{
		$this->db->select('ranks_points_package_rank_up as rankiee');

		$this->db->from('hiprofile_rank_point_caetogry_packages_list');
		$this->db->where('IsDeleted', 0);
		$this->db->where('ranks_points_package_Google_playId', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->rankiee;
	}
	/*End of load profile API*/

	/*Fucntion to caluclate the user and friend rank range*/
	function userFriendRankRange($data = null)
	{
		
		$result = array_merge(array($data['userID']),array($data['friend_user_id']));
		for($i=0;$i<count($result);$i++)
		{

			$this->db->select('social.rank_f2,social.points,social.social_rank,social.rank,social.social_points');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
			$this->db->where('BaseTbl.userId', $result[$i]);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.status', 0);
			$query = $this->db->get();
			$user[] = $query->result();
		}
		
		$userrank = $this->getuserrank($data['userID']);

		//FRIEND USER WHO ACCEPTS THE MAIN FRIEND USER WHO SENT AN REQUEST
		//$RankA 		= $user[0][0]->rank_f2 + $user[0][0]->social_rank + $user[0][0]->rank; 
		
		$RankA 		= $user[0][0]->social_rank + $user[0][0]->rank; 
		$Rankoftheuser 			= $user[0][0]->rank;	
		
		//$RankA 			= $user[0][0]->rank;		
		$RankAPoints 	= $user[0][0]->points;
		//USER MAIN WHEN HE SENDS A NEW FRIEND REQUEST SENT
		//$RankB 	    = $user[1][0]->rank_f2 + $user[1][0]->social_rank + $user[1][0]->rank;
		
		$RankB 	    = $user[1][0]->social_rank + $user[1][0]->rank;
		
		
		//$RankB 			= $user[1][0]->rank;
		$RankBPoints 	= $user[1][0]->points;
		// CHECK THE RANK RANGE BETWEEN A AND B
		//$RankRangeA 	=  round(($RankA -1)/10)+1 ;
		//$RankRangeB 	=  round(($RankB -1)/10)+1 ;
		if(round($RankA) == 0):	
			$RankRangeA 	=  round(($RankA -1)/10)+1 ;
		else:
			$RankRangeA 	=  round($RankA);
		endif;
		
		if(round($RankB) == 0):
			$RankRangeB 	=  round(($RankB -1)/10)+1 ;
		else:
			$RankRangeB 	=  round($RankB);
		endif;
		/* echo"RankA: ".$RankRangeA;
		echo"</br>";
		echo"RankB: ".$RankRangeB;
		echo"</br>";
		echo"RankAPoints: ".$RankAPoints;
		echo"</br>";
		echo"RankBPoints: ".$RankBPoints;
		exit;  */

		$EarnedRank = 0;
		$EarnedRankPoints = 0;
		
		// For user Range Range 1-10 calculation
		if (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 4/10;
			$EarnedRankPoints = 400;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 5/10;
			$EarnedRankPoints = 500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 9/10+4;
			$EarnedRankPoints = 900+4000;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 10/10+4.5;
			$EarnedRankPoints = 1000+4500;
		}
		
		// For user Range Range 11-20 calculation
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 4/10;
			$EarnedRankPoints = 400;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 9/10+4;
			$EarnedRankPoints = 900+4000;
		}
		
		// For user Range Range 21-30 calculation
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		
		// For user Range Range 31-40 calculation
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		
		// For user Range Range 41-50 calculation
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		
		// For user Range Range 51-60 calculation
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		
		// For user Range Range 61-70 calculation
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		
		// For user Range Range 71-80 calculation
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		
		// For user Range Range 81-90 calculation
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.2/10;
			$EarnedRankPoints = 20;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		
			// For user Range Range 91-100 calculation
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.1/10;
			$EarnedRankPoints = 10;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.2/10;
			$EarnedRankPoints = 20;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		/*
 		echo "AFTER CALCULATION";
		echo"RankA: ".$RankA;
		echo"</br>";
		echo"EarnedRank: ".$EarnedRank;
		echo"</br>";
		echo"RankAPoints: ".$RankAPoints;
		echo"</br>";
		echo"EarnedRankPoints: ".$EarnedRankPoints;
		exit; 
		*/		
		//$finalrank = round($EarnedRank);


		$this->db->select('*');
		$this->db->from('hiprofile_free_package_list');
		$this->db->where('userId', $data['userID']);
		$query = $this->db->get();
		$result = $query->result_array();
		$num = $query->num_rows();

		if($num > 0){

			 $free_package_time = strtotime($result[0]['created_at']);
			 $current_time = strtotime($this->get_current_datime());

			 if(($current_time - $free_package_time) > 1800){

			 	$this->db->where('id', $result[0]['id']);
				$this->db->delete('hiprofile_free_package_list');

				$this->db->where('orderId', $result[0]['orderId']);
				$this->db->where('userId', $data['userID']);
				$this->db->delete('hiprofile_rank_points_plan_details');

				$finalrank = $Rankoftheuser;
				$finalpoints = $RankAPoints+$EarnedRankPoints;
				if($finalpoints >= 1000)
				{
					$rank = $finalrank+1;
					$finalpoints = $finalpoints - 1000;
					
					 $this->db->set('userId', $data['userID']);
							$this->db->set('previous_rank', $finalrank);
							$this->db->set('new_rank', $rank);
							$this->db->insert('hiprofile_user_ranks_activity');
				}
				else
				{
					$rank = $finalrank;
				}
				$this->db->set('rank',$rank);
				$this->db->set('rank_f2',$EarnedRank);
				$this->db->set('points', $finalpoints);
				$this->db->where('userId', $data['userID']);
				$this->db->update('hiprofile_user_socialsync');

			 }else{

			 	$EarnedRankPoints = $EarnedRankPoints*2;
				$finalrank = $Rankoftheuser;
				$finalpoints = $RankAPoints+$EarnedRankPoints;
				if($finalpoints >= 1000)
				{
					$rank = $finalrank+1;
					$finalpoints = $finalpoints - 1000;
				}
				else
				{
					$rank = $finalrank;
				}
				$this->db->set('rank',$rank);
				$this->db->set('rank_f2',$EarnedRank);
				$this->db->set('points', $finalpoints);
				$this->db->where('userId', $data['userID']);
				$this->db->update('hiprofile_user_socialsync');

			 }
							
			
		}else{
			$finalrank = $Rankoftheuser;
			$finalpoints = $RankAPoints+$EarnedRankPoints;
			if($finalpoints >= 1000)
			{
				$rank = $finalrank+1;
				$finalpoints = $finalpoints - 1000;
				
				 $this->db->set('userId', $data['userID']);
						$this->db->set('previous_rank', $finalrank);
						$this->db->set('new_rank', $rank);
						$this->db->insert('hiprofile_user_ranks_activity');
			}
			else
			{
				$rank = $finalrank;
			}
			$this->db->set('rank',$rank);
			$this->db->set('rank_f2',$EarnedRank);
			$this->db->set('points', $finalpoints);
			$this->db->where('userId', $data['userID']);
			$this->db->update('hiprofile_user_socialsync');
		}

		
		
		$this->updateEarnedPoints($data['userID'],$data['friend_user_id'],$EarnedRankPoints);
		
		//To send the push notification when an user rank is been increased
		$userfinalrank = $this->getuserrank($data['userID']);
		if($userfinalrank > $userrank){
			sendrankpush($data);	
			iossendrankpush($data);
			/*if(strtolower($data['device_type']) == 'android'){
				sendrankpush($data);
			}
			if(strtolower($data['device_type']) == 'ios'){
				iossendrankpush($data);
			}*/
		}

	}
	
	//fucntion to insert the earned points of the user
	function updateEarnedPoints($userid,$friendid,$earnedpoints)
	{
		$this->db->select('earned_points');
		$this->db->from('hiprofile_earned_user_points');
		$this->db->where('userId', $userid);
		$this->db->where('friend_id', $friendid);
		$query = $this->db->get();
		$numRows = $query->num_rows();
		$getResultQuery = $query->result();
		//Check user active or not
		
		if($numRows>0) 
		{ 
			$this->db->set('earned_points', $earnedpoints);
			$this->db->set('status', 0);
			$this->db->where('userId', $userid);
			$this->db->where('friend_id', $friendid);
			$this->db->update('hiprofile_earned_user_points');

		}else
		{
			$this->db->set('earned_points', $earnedpoints);
			$this->db->set('status', 0);
			$this->db->set('userId', $userid);
			$this->db->set('friend_id', $friendid);
			$this->db->insert('hiprofile_earned_user_points');
		}
	}
	
	//API To Update The Social Sync and calculate the rank 
	function getsocialSyncinsert($data)
	{ 
		unset($data['api_key']);

		$this->db->set('device_token', $data['device_token']);
		$this->db->set('device_id', $data['device_id']);
		$this->db->set('device_type', $data['device_type']);	
		$this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->update('hiprofile_users');
		if ($this->db->affected_rows() >= 0)
		{
			if($data['auth_provider'] == 'facebook')
			{
				$fbuserid 	    = $data['social_id'];
				$fb_friendcount = $data['friendcount'];

				//facebook count and userid and auth update
				$data['profileimage'] = ($data['profileimage'] != 0)?$data['profileimage']:"";
				$data['profileURL'] = ($data['profileURL'] != "")?$data['profileURL']:"";
				$this->db->set('auth_provider_fb', $data['auth_provider']);
				$this->db->set('fbuserid', $fbuserid);
				$this->db->set('fb_friendcount', $fb_friendcount);
				//$this->db->set('profileimage', $data['profileimage']);
				$this->db->set('facebook_profile_url', $data['profileURL']);
				/*$insert_data = array(
					'auth_provider_fb' => $data['auth_provider'],
					'fbuserid' => $fbuserid,
					'fb_friendcount' => $fb_friendcount,
					'profileimage' => ($data['profileimage'] != 0)?$data['profileimage']:"",
					'facebook_profile_url' => ($data['profileURL'] != "")?$data['profileURL']:""
				);*/
				$this->db->where('userId', $data['userID']);
				$this->db->update('hiprofile_user_socialsync');
				
			}
			elseif($data['auth_provider'] == 'instagram')
			{
				$instagramuserid  = $data['social_id'];
				$inst_friendcount = $data['friendcount'];
				
				//instagram count and userid and auth update
				
				$data['profileimage'] = ($data['profileimage'] != 0)?$data['profileimage']:"";
				$data['profileURL'] = ($data['profileURL'] != "")?$data['profileURL']:"";
				$this->db->set('auth_provider_inst', $data['auth_provider']);
				$this->db->set('instagramuserid', $instagramuserid);
				$this->db->set('inst_friendcount', $inst_friendcount);
				///$this->db->set('profileimage', $data['profileimage']);
				$this->db->set('instagram_profile_url', $data['profileURL']);
				/*$insert_data = array(
					'auth_provider_inst' => $data['auth_provider'],
					'instagramuserid' => $instagramuserid,
					'inst_friendcount' => $inst_friendcount,
					'profileimage' => ($data['profileimage'] != 0)?$data['profileimage']:"",
					'instagram_profile_url' => ($data['profileURL'] != "")?$data['profileURL']:""
				);*/
				$this->db->where('userId', $data['userID']);
				$this->db->update('hiprofile_user_socialsync');
			}
			elseif($data['auth_provider'] == 'twitter')
			{
				$twitteruserid 	     = $data['social_id'];
				$twt_friendcount	 = $data['friendcount'];
				
				//twitter count and userid and auth update
				$data['profileimage'] = ($data['profileimage'] != 0)?$data['profileimage']:"";
				$data['profileURL'] = ($data['profileURL'] != "")?$data['profileURL']:"";
				$this->db->set('auth_provider_twt', $data['auth_provider']);
				$this->db->set('twitteruserid', $twitteruserid);
				$this->db->set('twt_friendcount', $twt_friendcount);
				//$this->db->set('profileimage', $data['profileimage']);
				$this->db->set('twitter_profile_url', $data['profileURL']);
				/* $insert_data = array(
					'auth_provider_twt' => $data['auth_provider'],
					'twitteruserid' => $twitteruserid,
					'twt_friendcount' => $twt_friendcount,
					'profileimage' => ($data['profileimage'] != 0)?$data['profileimage']:"",
					'twitter_profile_url' => ($data['profileURL'] != "")?$data['profileURL']:""
				); */
				$this->db->where('userId', $data['userID']);
				$this->db->update('hiprofile_user_socialsync');
			}

			if($this->db->affected_rows() >= 0)
			{
				$this->db->select('*');
				$this->db->from('hiprofile_user_socialsync');
				$this->db->where('userId', $data['userID']);
				$query = $this->db->get();
				$user = $query->result();
				
				//To get the user social rank for push notification
				$usersocialfinalrank1 = $this->getusersocialrank($data['userID']);
				
				//variable for fb,twitter,instagram,followers count of fb,instagram and twitter
				$fb			  = trim($user[0]->auth_provider_fb);
				$twit		  = trim($user[0]->auth_provider_twt);
				$insta		  = trim($user[0]->auth_provider_inst);
				$fb_follow 	  = trim($user[0]->fb_friendcount) == "" ? 0 : trim($user[0]->fb_friendcount);
				$twit_follow  = trim($user[0]->twt_friendcount) == "" ? 0 : trim($user[0]->twt_friendcount);
				$insta_follow = trim($user[0]->inst_friendcount) == "" ? 0 : trim($user[0]->inst_friendcount);

				
				if($fb == "" && $twit == "" && $insta == "")
				{
					$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*0 + ($twit_follow)*0)+1;
				} 
				elseif($fb == "" && $twit != "" && $insta == "") 
				{
					$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*0 + ($twit_follow)*1)+1;
				} 
				elseif($fb == "" && $twit == "" && $insta != "") 
				{
					$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*1 + ($twit_follow)*0)+1;
				} 
				elseif($fb == "" && $twit != "" && $insta != "") 
				{
					$PointsA = 1*(($fb_follow)*0 + ($insta_follow)*0.5 + ($twit_follow)*0.5)+1;
				} 
				elseif($fb != "" && $twit == "" && $insta == "") 
				{
					$PointsA = 1*(($fb_follow)*1 + ($insta_follow)*0 + ($twit_follow)*0)+1;
				} 
				elseif($fb != "" && $twit != "" && $insta == "") 
				{
					$PointsA = 1*(($fb_follow)*0.55 + ($insta_follow)*0 + ($twit_follow)*0.45)+1;
				} 
				elseif($fb != "" && $twit == "" && $insta != "") 
				{
					$PointsA = 1*(($fb_follow)*0.55 + ($insta_follow)*0.45 + ($twit_follow)*0)+1;
				} 
				elseif($fb != "" && $twit != "" && $insta != "") 
				{
					$PointsA = 1*(($fb_follow)*0.4 + ($insta_follow)*0.3 + ($twit_follow)*0.3)+1;
				}
				
				if($PointsA > 1000) 
				{
					if($PointsA > 100000000) 
					{
						if($PointsA > 200000000) 
						{
							//Fame Rank 100+
							$RankA =101;
						} 
						else 
						{
							//Fame Rank 90-100
							$RankA = Round(log10($PointsA)*33.22 - 175.75);
						}
					} 
					else 
					{
						//Fame Rank 16-90
						$RankA = Round(log10($PointsA)*15 - 30);
					}
				} 
				else
				{
					$RankA = Round(log10($PointsA)*4.67) + 1;
				}
				
				$insert_data = array(
				//'profileimage' => ($data['profileimage'] != 0)?$data['profileimage']:"",
					'social_rank' => ($RankA != 0)?$RankA:0,
					'social_points' => ($PointsA != 0)?$PointsA:0
				);
				$this->db->where('userId', $data['userID']);
				$this->db->update('hiprofile_user_socialsync', $insert_data);		
				if ($this->db->affected_rows() >= 0)
				{
					$this->db->select('points,rank,rank_f2');
					$this->db->from('hiprofile_user_socialsync');
					$this->db->where('userId', $data['userID']);
					$query = $this->db->get();
					$user = $query->result();
					$finalrank = $user[0]->rank+$RankA;
					$return_res = array();
					$return_res['responsecode'] = "200";
					$return_res['responsedetails'] 		= "success";
					$return_res['rank'] 		= $user[0]->rank+$RankA;
					$return_res['points'] 		= $user[0]->points;
					
					//To send the push notification when an increase in the social rank
					$usersocialfinalrank2 = $this->getusersocialrank($data['userID']);
					if($usersocialfinalrank2 > $usersocialfinalrank1){
						
						sendrankpush($data);
						iossendrankpush($data);
						/*if(strtolower($data['device_type']) == 'android'){
							sendrankpush($data);
						}
						if(strtolower($data['device_type']) == 'ios'){
							iossendrankpush($data);
						}*/
					}
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}

		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
	}	
	//API call - To do the Submit Feedback
	public function getfeedback($data)
	{ 
		unset($data['api_key']);
		$updetail = array(
			'rating' => $data['rating'],
			'feedback'  => $data['feedback']
		);
		$this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->update('hiprofile_users', $updetail);
		if ($this->db->affected_rows() >= 0)
		{
			$return_res['responsecode'] = "200";
			$return_res['responsedetails'] = "Success";
			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
	}
	
	//API TO CHANGE PASSWORD
	public function getchangepassword($userId, $userInfo)
	{
		$this->db->where('userId', $userId);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
		$this->db->update('hiprofile_users', $userInfo);
		return $this->db->affected_rows();
	}

	//API CHECK MATCH OLDPASSWORD
	function matchOldPassword($userId, $oldPassword)
	{

		$this->db->select('userId, password');
		$this->db->where('userId', $userId);        
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
		$query = $this->db->get('hiprofile_users');

		$user = $query->result();

		if(!empty($user))
		{
			if(verifyHashedPassword($oldPassword, $user[0]->password))
			{
				return $user;
			} 
			else 
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	//API to check the userId restiction 
	function restrictID($userId)
	{

		$this->db->select('userId');
		$this->db->where('userId', $userId);        
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
		$query = $this->db->get('hiprofile_users');

		$user = $query->result();

		if(!empty($user))
		{
			return $user;
		}
		else
		{
			return array();
		}
	}
	
	//API to check the Email restiction 
	function restrictEmail($email)
	{

		$this->db->select('email');
		$this->db->where('email', $email);  		
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
		$query = $this->db->get('hiprofile_users');

		$user = $query->result();

		if(!empty($user))
		{
			return $user;
		}
		else
		{
			return array();
		}
	}
	
	public function geteditedprofile($data)
	{
		unset($data['api_key']);
                //echo $data['userName'];die;
		if($data['mobile_no']!=""){
			$this->db->set('mobile', $data['mobile_no']);
		}
		if($data['email']!=""){
			$this->db->set('email', $data['email']);
		}
		if($data['dob']!=""){
			$this->db->set('dob', $data['dob']);
		}
		if($data['gender_disclose']!=""){
			$this->db->set('gender_disclose', $data['gender_disclose']);
		}
		if($data['gender']!=""){
			$this->db->set('gender', strtolower($data['gender']));
		}
		if($data['share_mobile']!=""){
			$this->db->set('share_mobile', strtolower($data['share_mobile']));
		}
		if($data['location_disclose']!=""){
			$this->db->set('location_disclose', strtolower($data['location_disclose']));
		}
        if($data['userName']!=""){
                    $this->db->select('username');
                    $this->db->from('hiprofile_users');
                    $this->db->where('username',$data['userName']);
                    $this->db->where('isDeleted', 0);
                    $this->db->where('status', 0);
                    $query = $this->db->get();
                    $user = $query->result();
                    $username = $user[0]->username;
                    
                    if($username != $data['userName']){
                        $status = $this->checkuserexists($data['userName']);
                        if($status == 1){
                            $return_res = array();
                            $return_res['responsecode'] = "201";
                            $return_res['error'] = "Username already exists!";
                            return $return_res;                        
                        }else{
                            $this->db->set('username', $data['userName']);
                        }
                    }
			
		}
		if($data['mobile_no']!="" || $data['email']!="" || $data['dob']!="" || $data['gender_disclose']!="" || $data['gender']!="" || $data['share_mobile']!="" || $data['location_disclose']!="" || $data['userName']!="")
		{
			$this->db->where('userId', $data['userID']);	
			$this->db->where('isDeleted', 0);
			$this->db->where('status', 0);
			$this->db->update('hiprofile_users');
			if($this->db->affected_rows() >= 0)
			{
				$updata = $this->editupdateprofile($data);
				return $updata;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "501";
				$return_res['error'] = "Something went wrong. Please try again later.";
				return $return_res;
			}	
		}
		elseif($data['mobile_no'] == "" && $data['email'] == "" && $data['dob'] == "" && $data['userName'] == "" && $data['gender_disclose'] && $data['gender'] && $data['share_mobile'])
		{
			$updata = $this->editupdateprofile($data);
			return $updata;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "502";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}		
	}
	public function editupdateprofile($data)
	{
		if($data['facebook_profile_url']!=""){
			$this->db->set('facebook_profile_url', $data['facebook_profile_url']);
		}
		if($data['twitter_profile_url']!=""){
			$this->db->set('twitter_profile_url', $data['twitter_profile_url']);
		}
		if($data['instagram_profile_url']!=""){
			$this->db->set('instagram_profile_url', $data['instagram_profile_url']);
		}
		if($data['bio']!=""){
			$this->db->set('bio', $data['bio']);
		}
		if($data['facebook_profile_url']!="" || $data['twitter_profile_url']!="" || $data['instagram_profile_url']!="" || $data['bio']!="" )
		{
			$this->db->where('userId', $data['userID']);
			$this->db->update('hiprofile_user_socialsync');	
			if($this->db->affected_rows() >= 0)
			{
				if($data['gender_disclose']!="")
				{
					$this->db->set('gender_disclose', $data['gender_disclose']);
					$this->db->where('userId', $data['userID']);
					$this->db->update('hiprofile_users');
					if($this->db->affected_rows() >= 0)
					{					
						$return_res['responsecode'] = "200";
						$return_res['responsedetails'] = "Success";
						return $return_res;
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}
				}
				else
				{
					$return_res['responsecode'] = "200";
					$return_res['responsedetails'] = "Success";
					return $return_res;
				}
				
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
		}
		elseif($data['gender_disclose']!="")
		{
			$this->db->set('gender_disclose', $data['gender_disclose']);
			$this->db->where('userId', $data['userID']);
			$this->db->update('hiprofile_users');
			if($this->db->affected_rows() >= 0)
			{					
				$return_res['responsecode'] = "200";
				$return_res['responsedetails'] = "Success";
				return $return_res;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
		}
		elseif($data['facebook_profile_url'] == "" && $data['twitter_profile_url'] == "" && $data['instagram_profile_url'] == "" && $data['gender_disclose'] == "")
		{
			$return_res['responsecode'] = "200";
			$return_res['responsedetails'] = "Success";
			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
	}
	
	//API TO UPDATE PROFILE IMAGE
	public function editprofileimg($data)
	{
		unset($data['api_key']);		
		$this->db->set('profileimage', '/userprofile/images/'.$data['profile_pic_base64']);
		$this->db->where('userId', $data['userID']);
		$this->db->update('hiprofile_user_socialsync');	
		if($this->db->affected_rows() >= 0)
		{
			$return_res['responsecode'] = "200";
			$return_res['responsedetails'] = "Success";
			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
	}

	//API TO UPLOAD SIX IMAGE
	/*public function uploadsiximg($data)
	{
		unset($data['api_key']);
		if($data['image_id'] !='')
		{

			$this->db->select('id');
			$this->db->where('image_id', $data['image_id']);	        
			$query = $this->db->get('hiprofile_user_six_photo_favourite_lists');	        
			$result = $query->result_array();

			if(count($result) > 0){
				foreach ($result as $key => $value) {
					$this->db->where('id', $value['id']);
					$this->db->delete('hiprofile_user_six_photo_favourite_lists'); 
				}

			}

			$this->db->set('img_url', '/userprofile/six_photos/'.$data['image_base64']);
			$this->db->set('photo_position', $data['image_position']);
			$this->db->set('isSeen', '0');
			$this->db->set('fav_count', '0');
			$this->db->where('id', $data['image_id']);			
			$this->db->update('hiprofile_user_six_photos');			
		}
		else
		{
			$this->db->set('userId',$data['userID']);
			$this->db->set('photo_position',$data['image_position']);			
			$this->db->set('img_url','/userprofile/six_photos/'.$data['image_base64']);			
			$this->db->insert('hiprofile_user_six_photos');
			
		}

		if($this->db->affected_rows() >= 0)
		{
			$return_res['responsecode'] = "200";
			$return_res['responsedetails'] = "Success";
			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}		

		
	}*/


	public function uploadsiximg($data)
	{
		unset($data['api_key']);
		if($data['image_id'] !='')
		{

			$this->db->select('id');
			$this->db->where('image_id', $data['image_id']);	        
			$query = $this->db->get('hiprofile_user_six_photo_favourite_lists');	        
			$result = $query->result_array();

			if(count($result) > 0){
				foreach ($result as $key => $value) {
					$this->db->where('id', $value['id']);
					$this->db->delete('hiprofile_user_six_photo_favourite_lists'); 
				}

			}

			$this->db->set('img_url', '/userprofile/six_photos/'.$data['image_base64']);
			$this->db->set('photo_position', $data['image_position']);
			$this->db->set('isSeen', '0');
			$this->db->set('fav_count', '0');
			$this->db->where('id', $data['image_id']);			
			$this->db->update('hiprofile_user_six_photos');
			
			$this->db->select('name');
			$this->db->where('userId', $data['userID']);	        
			$query = $this->db->get('hiprofile_users');	        
			$res = $query->result_array();
			if(count($res) > 0){
				$name = $res[0]['name'];
				$msg = '<b>'.$name.'</b>'.' has updated '.'<b> SixPhotos </b>';
			}else{
				$msg = "";
			}
			$this->db->set('userId',$data['userID']);
			$this->db->set('msg_text',$msg);
			$this->db->insert('hiprofile_user_six_photos_notification');			
		}
		else
		{
			$this->db->set('userId',$data['userID']);
			$this->db->set('photo_position',$data['image_position']);			
			$this->db->set('img_url','/userprofile/six_photos/'.$data['image_base64']);			
			$this->db->insert('hiprofile_user_six_photos');
			
			$this->db->select('name');
			$this->db->where('userId', $data['userID']);	        
			$query = $this->db->get('hiprofile_users');	        
			$res = $query->result_array();
			if(count($res) > 0){
				$name = $res[0]['name'];
				$msg = '<b>'.$name.'</b>'.' has updated '.'<b> SixPhotos </b>';
			}else{
				$msg = "";
			}
			$this->db->set('userId',$data['userID']);
			$this->db->set('msg_text',$msg);
			$this->db->insert('hiprofile_user_six_photos_notification');
			
		}

		if($this->db->affected_rows() >= 0)
		{
			$return_res['responsecode'] = "200";
			$return_res['responsedetails'] = "Success";
			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}		

		
	}

	// API to update the six photos is seen or not 
	public function updateSixPhotosIsSeen($data)
	{
		unset($data['api_key']);
		$image_id = $data['image_id'];
		$isSeen = $data['isSeen'];		

		if($isSeen !=1){
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}else{
			$this->db->set('isSeen', $isSeen);
			$this->db->where('id', $image_id);			
			$this->db->update('hiprofile_user_six_photos');
		}	

		if($this->db->affected_rows() >= 0){
			$return_res['responsecode'] = "200";
			$return_res['responsedetails'] = "Success";
			return $return_res;
		}else{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}		

		
	}
	
	//API to update approachability of the user
	public function toUpdateApproachability($data)
	{
		unset($data['api_key']);	
		
		$this->db->select('userId');
		$this->db->where('userId', $data['userID']);        
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
		$query = $this->db->get('hiprofile_users');
		if($query->num_rows() == 1)
		{
			$this->db->set('approachability', $data['approachability']);
			$this->db->where('userId', $data['userID']);
			$this->db->update('hiprofile_users');	
			if($this->db->affected_rows() >= 0)
			{
				$getuserfriend = $this->get_user_status_friend_social($data['userID'],$data['userID']);
				$getearnedpoints = $this->get_user_earned_points($data['userID'],$data['userID']);
				$friendsList = $this->get_friend_requested_user_details($data['userID'],$getuserfriend,$getearnedpoints,$data['userID']);

				return $friendsList;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['responsedetails'] = "This user id doesn’t exists";
			return $return_res;
		}
	}
	
	//APi to show the points of the user friends
	public function getShowpoints($data)
	{
		$id = "";
		unset($data['api_key']);
		
		$this->db->select('friend_user_id');
		$this->db->from('hiprofile_friends');
		$this->db->where('userId', $data['userID']);
		$this->db->where('add_friend','true');
		$this->db->where('request_status','1');
		$query = $this->db->get();
		
		
		/* $this->db->select('userId');
		$this->db->from('hiprofile_friends');
		$this->db->where('friend_user_id',$data['userID']);
		$this->db->where('add_friend','true'); */
		
		$returnResult=array();
		$frndid = $query->result();
		
		if($query->num_rows() > 0)
		{
			foreach($frndid as $key=>$frined_id)
			{
				$frinedID = $frined_id->friend_user_id;
				//$getearnedpoints = $this->get_user_earned_points($frinedID,$data['userID']);
				$getearnedpoints = $this->get_user_earned_points($data['userID'],$frinedID);
				$this->load->model('socialapi_model');
				$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$frinedID);
				$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$frinedID);
				if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
				{
					
					
					$this->db->select('BaseTbl.name,BaseTbl.username,BaseTbl.userId as userID,social.profileimage as image_url,social.updatedDtm as date,BaseTbl.gender,social.social_rank,			social.rank,social.rank_f2,social.points,social.worth,BaseTbl.latitude,BaseTbl.longitude,social.points as data');
					$this->db->from('hiprofile_users as BaseTbl');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
					$this->db->where('BaseTbl.userId', $frinedID);
					$this->db->where('BaseTbl.isDeleted', 0);
					$this->db->where('BaseTbl.status', 0);
					$query = $this->db->get();
					$user = $query->result();
					if(!empty($user))
					{	
						$loop_count = count($user);
						for($i=0;$i<$loop_count;$i++) {
							$user[$i]->image_url	 = ($user[$i]->image_url == "")?"":base_url().'assets'.$user[$i]->image_url;
							
							$user[$i]->date = date("d/m/Y",strtotime($user[$i]->date));
							
							$user[$i]->data = ($getearnedpoints == 0) ? "" : '<b>'.$getearnedpoints.'</b> points earned';
							$user[$i]->rank = strval(round($user[$i]->social_rank + $user[$i]->rank + $user[$i]->rank_f2));
							unset($user[$i]->social_rank);
							unset($user[$i]->rank_f2);
							$returnResult[]=$user[$i];
						}
					}
				}
			}
			usort($returnResult, function($a, $b)
			{
				return strcmp(strtotime($a->date),strtotime($b->date));
			});
			if(!empty($returnResult))
			{
				$return_res = array();
				$return_res['responsecode'] 	= "200";
				$return_res['responsedetails'] 	= "success";
				$return_res['data']['points'] 	= $returnResult;
				return $return_res;
			}
			else
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['status'] = " No Records Found.";
				return $return_res;
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = " No Records Found.";
			return $return_res;
		}
		exit;
		
	}
	
	
	// TO generate The Jwt TOken 
	public function getJwtToken($data)
	{
		unset($data['api_key']);
		$insert_data = array(
			'otp' => $data['jwt'],
		);

		$this->db->where('email', $data['email']);
		$this->db->update('hiprofile_users', $insert_data);
		$this->db->where('isDeleted', 0);
		if ($this->db->affected_rows() >= 0)
		{
			$return_res = array();
			$return_res['responsecode'] = "200";
			$return_res['status'] = "success";
			$return_res['jwt_token'] =  $data['jwt'];
			return $return_res;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
	}
	
	//TO validate Mobile Already Registered or not
	public function checkmobileexists($data)
	{

		$this->db->select('mobile');
		$this->db->from('hiprofile_users');
		$this->db->where('mobile',$data);
		$this->db->where('isDeleted', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return 1;
		} 
		else
		{
			return 0;
		} 
	}
	
	//API to call the user acticvity - added-5-18-2018 for block
	//COMMENTED ON 31/8/2018
	/*public function getactivity($data)
	{	
		unset($data['api_key']);
		
		$this->db->select('userId');
		$this->db->from('hiprofile_friends');
		$this->db->where('friend_user_id',$data['userID']);
		$this->db->where('add_friend','true');
		$this->db->where('request_status','1');
		$query = $this->db->get();
		$frndid = $query->result();
		$numRows = $query->num_rows();
		foreach($frndid as $user_arr)
		{	
			$this->load->model('socialapi_model');
			$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$user_arr->userId);
			$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$user_arr->userId);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{	
				$frndids[] = $user_arr->userId;				
			}
		}
		$friend_id =  array_values($frndids);
		if(count($friend_id) >= 1)
		{
			$result = array();
			$id="";
			$start = $end = '';
			$totalpage = $this->getpagecount($data['userID']);
			if($totalpage == $data['page_no'])
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
			else
			{
				if(isset($data['page_no']) && $data['page_no'] == ''){
					$pageno = 1;
					$start = 0;
					$end = 9;
				}else{
					$start = $data['page_no'] * 10;
					$end = $start + 9;
					//$pageno = $data['page_no'];
					$pageno = ($data['page_no'] == 0) ? "1" : $data['page_no'];
				}
				$this->db->select('userId');
				$this->db->from('hiprofile_friends');
				$this->db->where('friend_user_id',$data['userID']);
				$this->db->where('add_friend','true');
				$this->db->where('request_status','1');
				$this->db->limit($end, $start);
				$query = $this->db->get();
				$frndid = $query->result();
				
				
				foreach($frndid as $key=>$value)
				{
					$id .= $value->userId . ",";
			
				}
				$userid =  rtrim($id,',');
				$userid = explode(",",$userid);
				
				// to fetch the records of rank, points, name and updated Dtm
				foreach($userid as $userID)
				{
					$this->load->model('socialapi_model');
					$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$userID);
					$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$userID);
					if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
					{	
						$this->db->select('social.profileimage as imgUrl,users.name as description,social.social_rank,social.rank_f2,social.rank,social.points,social.updatedDtm as times,users.userId as friend_user_id');
						$this->db->from('hiprofile_users as users');
						$this->db->join('hiprofile_user_socialsync as social','social.userId = users.userId','inner');
						$this->db->where('users.userId',$userID);
						$this->db->where('users.status', 0);
						$this->db->where('users.isDeleted', 0);
						$query = $this->db->get();
						$user[] = $query->result();			
					}
				}
			
				
				usort($user, function($a, $b)
				{
					$a = date("d/m/Y",strtotime($a->times));
					$b = date("d/m/Y",strtotime($b->times));
					return strcmp(strtotime($a),strtotime($b));
				});
					
				foreach($user as $key=>$value) {
					foreach($value as $finalvalue) {
						$tempArr['imgUrl'] = ($finalvalue->imgUrl == "")?"":base_url().'assets'.$finalvalue->imgUrl;
						$tempArr['description'] = $finalvalue->description;
						$tempArr['time'] = $this->time_elapsed_string($finalvalue->times);
						$tempArr['friend_user_id'] = $finalvalue->friend_user_id;
						$tempArr['rank'] = round($finalvalue->social_rank + $finalvalue->rank+$finalvalue->rank_f2);
						for($r=1;$r<=2;$r++){
							if($r == 1) { $tempArr['description'] = '<b>'.$finalvalue->description.'</b>'.' reached rank '.'<b>'.$tempArr['rank'].'</b>';}
							if($r == 2) { $tempArr['description'] = '<b>'.$finalvalue->description.'</b>'.' reached '.'<b>'.$finalvalue->points.'</b>'.' points';}
							unset($tempArr['rank']);
							$fullArr[] = $tempArr;
						}
					}
				}
				
				if(!empty($fullArr))
				{
					$return_res = array();
					$return_res['responsecode'] = "200";
					$return_res['responsedetails'] = "success";
					$return_res['data']['page']['total_pages'] = $totalpage;
					$return_res['data']['page']['page_no'] = $pageno;
					$return_res['data']['page']["page_size"] = '10';
					$return_res['data']["recent_activities"] = $fullArr;
					
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "User not added any friends";
			return $return_res;
		}
		exit;
	}*/
	
	// New code for recent activity with friend details3
	/*public function getactivity($data)
	{	
		unset($data['api_key']);
		
		$this->db->select('userId');
		$this->db->from('hiprofile_friends');
		$this->db->where('friend_user_id',$data['userID']);
		$this->db->where('add_friend','true');
		$this->db->where('request_status','1');
		$query = $this->db->get();
		$frndid = $query->result();
		$numRows = $query->num_rows();
		foreach($frndid as $user_arr)
		{	
			$this->load->model('socialapi_model');
			$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$user_arr->userId);
			$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$user_arr->userId);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{	
				$frndids[] = $user_arr->userId;				
			}
		}
		$friend_id =  array_values($frndids);
		if(count($friend_id) >= 1)
		{
			$result = array();
			$id="";
			$start = $end = '';
			$totalpage = $this->getpagecount($data['userID']);
			if($totalpage == $data['page_no'])
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
			else
			{
				if(isset($data['page_no']) && $data['page_no'] == ''){
					$pageno = 1;
					$start = 0;
					$end = 9;
				}else{
					$start = $data['page_no'] * 10;
					$end = $start + 9;
					//$pageno = $data['page_no'];
					$pageno = ($data['page_no'] == 0) ? "1" : $data['page_no'];
				}
				
				$myID = $data['userID'];
				
				$this->db->select('hiprofile_friends.userId');
				$this->db->from('hiprofile_friends');
				$this->db->join('hiprofile_earned_user_points as earnedsocial','earnedsocial.friend_id = '.$myID.' AND earnedsocial.userId = hiprofile_friends.userId','inner');
				$this->db->where('friend_user_id',$data['userID']);
				$this->db->where('add_friend','true');
				$this->db->where('request_status','1');
				$this->db->order_by('hiprofile_friends.updatedDtm', 'desc');
				$this->db->limit($end, $start);
				$query = $this->db->get();
				$frndid = $query->result();
				//pre($frndid);
				foreach($frndid as $key=>$value)
				{
					$id .= $value->userId . ",";

				}
				$userid =  rtrim($id,',');
				$userid = explode(",",$userid);
				
				// to fetch the records of rank, points, name and updated Dtm
				foreach($userid as $userID)
				{
					$this->load->model('socialapi_model');
					$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$userID);
					$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$userID);
					
					if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
					{	
						$this->db->select('social.profileimage as imgUrl,users.username as description,social.social_rank,social.rank_f2,social.rank,social.points,social.updatedDtm as timesold,earnedsocial.updated_date as times,users.userId as friend_user_id');
						$this->db->from('hiprofile_users as users');
						$this->db->join('hiprofile_user_socialsync as social','social.userId = users.userId','inner');
						$this->db->join('hiprofile_earned_user_points as earnedsocial','earnedsocial.friend_id = '.$myID.' AND earnedsocial.userId = '.$userID.'','inner');
						$this->db->where('users.userId',$userID);
						$this->db->where('users.status', 0);
						$this->db->where('users.isDeleted', 0);
						$this->db->order_by('earnedsocial.updated_date', 'desc');
						$query = $this->db->get();
						$user[] = $query->result();	
					}

				}
				usort($user, function($a, $b)
				{
					$a = date("d/m/Y",strtotime($a->times));
					$b = date("d/m/Y",strtotime($b->times));
					return strcmp(strtotime($a),strtotime($b));
				});
				

				foreach($user as $key=>$value) {
					foreach($value as $finalvalue) {
						if($finalvalue->times!='')
						{
							$tempArr['imgUrl'] = ($finalvalue->imgUrl == "")?"":base_url().'assets'.$finalvalue->imgUrl;
							$tempArr['description'] = $finalvalue->description;
							$tempArr['time'] = $this->time_elapsed_string($finalvalue->times);
							$tempArr['times'] = $finalvalue->times;						
							$tempArr['Mtimes'] = $this->time_elapsed_string_minutes($finalvalue->times);
							$tempArr['friend_user_id'] = $finalvalue->friend_user_id;
							$tempArr['rank'] = round($finalvalue->social_rank + $finalvalue->rank+$finalvalue->rank_f2);
							for($r=1;$r<=2;$r++)
							{
								if($r == 1) 
								{ 
									$tempArr['description'] = '<b>'.$finalvalue->description.'</b>'.' reached rank '.'<b>'.$tempArr['rank'].'</b>';
								}
								if($r == 2) 
								{ 
									$tempArr['description'] = '<b>'.$finalvalue->description.'</b>'.' earned '.'<b>'.$this->get_user_earned_points($finalvalue->friend_user_id,$data['userID']).' points</b> from meeting <b>you</b>';
								}

								unset($tempArr['rank']);							
								$fullArr[] = $tempArr;
							}
						}	
					}
					
					$friendarray[] = $this->get_other_user_activities($data['userID'],$value[0]->friend_user_id,$end,$start);
					
				}
				
				$final_value_friendmerge=array();
				
				if(!empty($friendarray) && $friendarray[0]!= "" && $friendarray[0]!= null)
				{
					$values_friend = array_values(array_filter($friendarray));
					
					$final_value_frient = "";
					foreach($values_friend as $kay=>$value) {
						$final_value_frient = $value;
						$final_value_friendmerge = array_merge((array)$final_value_friendmerge,(array)$final_value_frient);
					}
					$final_value_friendmerge = array_merge((array)$fullArr,(array)$final_value_friendmerge);
				}
				else
				{
					
					$final_value_friendmerge = $fullArr;
				}
				
				//pre($final_value_friendmerge);
				if(!empty($final_value_friendmerge))
				{
					usort($final_value_friendmerge, function($a, $b)
					{
						return $a['Mtimes'] - $b['Mtimes'];
					});

					foreach($final_value_friendmerge as $key=>$data)
					{
						unset($final_value_friendmerge[$key]['Mtimes']);
					}
				}
				
				foreach($userid as $userID){
					$this->db->select('social.profileimage as imgUrl,users.username as description,social.social_rank,social.rank_f2,social.rank,social.points,social.updatedDtm as timesold,six_photos.updatedDtm as times,users.userId as friend_user_id');
					$this->db->from('hiprofile_users as users');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = users.userId','inner');
					$this->db->join('hiprofile_user_six_photos as six_photos','users.userId = six_photos.userId','left');				
					$this->db->where('six_photos.userId',$userID);
					$this->db->where('users.status', 0);
					$this->db->where('users.isDeleted', 0);
					$this->db->order_by('six_photos.updatedDtm', 'desc');
					$query2 = $this->db->get();
					$user2[] = $query2->result();
				}
				if(!empty($user2)){
					foreach($user2 as $key=>$value1) {
						foreach($value1 as $finalvalue1) {
							if($finalvalue1->times!='')
							{
								$tempArr1['imgUrl'] = ($finalvalue1->imgUrl == "")?"":base_url().'assets'.$finalvalue1->imgUrl;
								$tempArr1['description'] = '<b>'.$finalvalue1->description.'</b>'.' has just updated '.'<b> SixPhotos </b>';
								$tempArr1['time'] = $this->time_elapsed_string($finalvalue1->times);
								$tempArr1['times'] = $finalvalue1->times;
								$tempArr1['Mtimes'] = $this->time_elapsed_string_minutes($finalvalue1->times);
								$tempArr1['friend_user_id'] = $finalvalue1->friend_user_id;
								$tempArr1['rank'] = round($finalvalue1->social_rank + $finalvalue1->rank+$finalvalue1->rank_f2);

								unset($tempArr1['rank']);							
								$fullArr1[] = $tempArr1;
							}
						}	
					}

				}
				if(!empty($fullArr1)){
					$final_value_friendmerge_sort = array_merge((array)$final_value_friendmerge,(array)$fullArr1);
				}else{
					$final_value_friendmerge_sort = $final_value_friendmerge;
				}	
				
				usort($final_value_friendmerge_sort,function($a,$b){
					$a = new DateTime($a['times']);
					$b = new DateTime($b['times']);
					if ($a == $b) {
						return 0;
					}
					return $a > $b ? -1 : 1;
				});
				
				if(!empty($final_value_friendmerge_sort))
				{
					$return_res = array();
					$return_res['responsecode'] = "200";
					$return_res['responsedetails'] = "success";
					$return_res['data']['page']['total_pages'] = $totalpage;
					$return_res['data']['page']['page_no'] = $pageno;
					$return_res['data']['page']["page_size"] = '10';
					$return_res['data']["recent_activities"] = $final_value_friendmerge_sort;
					
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "User not added any friends";
			return $return_res;
		}
		exit;
	}*/


	public function getactivity($data)
	{	
		unset($data['api_key']);
		
		$this->db->select('userId');
		$this->db->from('hiprofile_friends');
		$this->db->where('friend_user_id',$data['userID']);
		$this->db->where('add_friend','true');
		$this->db->where('request_status','1');
		$query = $this->db->get();
		$frndid = $query->result();
		$numRows = $query->num_rows();
		$frndids = [];
		foreach($frndid as $user_arr)
		{	
			$this->load->model('socialapi_model');
			$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$user_arr->userId);
			$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$user_arr->userId);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{	
				$frndids[] = $user_arr->userId;				
			}
		}
		$friend_id =  array_values($frndids);

		//echo count($friend_id);die;
		if(count($friend_id) >= 1)
		{
			$result = array();
			$id="";
			$start = $end = '';
			$totalpage = $this->getpagecount($data['userID']);
			if($totalpage == $data['page_no'])
			{
				$return_res = array();
				$return_res['responsecode'] = "500";
				$return_res['status'] = "Something went wrong. Please try again later.";
				return $return_res;
			}
			else
			{
				if(isset($data['page_no']) && $data['page_no'] == ''){
					$pageno = 1;
					$start = 0;
					$end = 9;
				}else{
					$start = $data['page_no'] * 10;
					$end = $start + 9;
					//$pageno = $data['page_no'];
					$pageno = ($data['page_no'] == 0) ? "1" : $data['page_no'];
				}
				
				$myID = $data['userID'];
				
				$this->db->select('hiprofile_friends.userId');
				$this->db->from('hiprofile_friends');
				$this->db->join('hiprofile_earned_user_points as earnedsocial','earnedsocial.friend_id = '.$myID.' AND earnedsocial.userId = hiprofile_friends.userId','inner');
				$this->db->where('friend_user_id',$data['userID']);
				$this->db->where('add_friend','true');
				$this->db->where('request_status','1');
				$this->db->order_by('hiprofile_friends.updatedDtm', 'desc');
				$this->db->limit($end, $start);
				$query = $this->db->get();
				$frndid = $query->result();
				//pre($frndid);
				foreach($frndid as $key=>$value)
				{
					$id .= $value->userId . ",";

				}
				$userid =  rtrim($id,',');
				$userid = explode(",",$userid);

				

				// to fetch the records of rank, points, name and updated Dtm
				foreach($userid as $userID)
				{
					$this->load->model('socialapi_model');
					$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$userID);
					$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$userID);
					
					if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
					{	
						$this->db->select('social.profileimage as imgUrl,users.username as description,social.social_rank,social.rank_f2,social.rank,social.points,social.updatedDtm as timesold,earnedsocial.updated_date as times,users.userId as friend_user_id');
						$this->db->from('hiprofile_users as users');
						$this->db->join('hiprofile_user_socialsync as social','social.userId = users.userId','inner');
						$this->db->join('hiprofile_earned_user_points as earnedsocial','earnedsocial.friend_id = '.$myID.' AND earnedsocial.userId = '.$userID.'','inner');
						$this->db->where('users.userId',$userID);
						$this->db->where('users.status', 0);
						$this->db->where('users.isDeleted', 0);
						$this->db->order_by('earnedsocial.updated_date', 'desc');
						$query = $this->db->get();
						$resultantArr = $query->result();
						if($query->num_rows()>0)
						{
							$user[] = $resultantArr;
						}
						
						//echo $query->num_rows();	

						//print_r($user);die;
					}

				}

				if(count($user)>0)
				{
					usort($user, function($a, $b)
					{
						//echo $a->times;die;
						$a = date("d/m/Y",strtotime($a[0]->times));
						$b = date("d/m/Y",strtotime($b[0]->times));
						return strcmp(strtotime($a),strtotime($b));
					});

				}
				
				
				/*if(count($user)>0)
				{

				}*/
				foreach($user as $key=>$value) {
					foreach($value as $finalvalue) {
						if($finalvalue->times!='')
						{
							$tempArr['imgUrl'] = ($finalvalue->imgUrl == "")?"":base_url().'assets'.$finalvalue->imgUrl;
							$tempArr['description'] = $finalvalue->description;
							$tempArr['time'] = $this->time_elapsed_string($finalvalue->times);
							$tempArr['times'] = $finalvalue->times;						
							$tempArr['Mtimes'] = $this->time_elapsed_string_minutes($finalvalue->times);
							$tempArr['friend_user_id'] = $finalvalue->friend_user_id;
							$tempArr['rank'] = round($finalvalue->social_rank + $finalvalue->rank+$finalvalue->rank_f2);
							for($r=1;$r<=2;$r++)
							{
								if($r == 1) 
								{ 
									$tempArr['description'] = '<b>'.$finalvalue->description.'</b>'.' reached rank '.'<b>'.$tempArr['rank'].'</b>';
								}
								if($r == 2) 
								{ 
									$tempArr['description'] = '<b>'.$finalvalue->description.'</b>'.' earned '.'<b>'.$this->get_user_earned_points($finalvalue->friend_user_id,$data['userID']).' points</b> from meeting <b>you</b>';
								}

								unset($tempArr['rank']);							
								$fullArr[] = $tempArr;
							}
						}	
					}
					
					$friendarray[] = $this->get_other_user_activities($data['userID'],$value[0]->friend_user_id,$end,$start);
					
				}
				
				$final_value_friendmerge=array();
				
				if(!empty($friendarray) && $friendarray[0]!= "" && $friendarray[0]!= null)
				{
					$values_friend = array_values(array_filter($friendarray));
					
					$final_value_frient = "";
					foreach($values_friend as $kay=>$value) {
						$final_value_frient = $value;
						$final_value_friendmerge = array_merge((array)$final_value_friendmerge,(array)$final_value_frient);
					}
					$final_value_friendmerge = array_merge((array)$fullArr,(array)$final_value_friendmerge);
				}
				else
				{
					
					$final_value_friendmerge = $fullArr;
				}
				
				//pre($final_value_friendmerge);
				if(!empty($final_value_friendmerge))
				{
					usort($final_value_friendmerge, function($a, $b)
					{
						return $a['Mtimes'] - $b['Mtimes'];
					});

					foreach($final_value_friendmerge as $key=>$data)
					{
						unset($final_value_friendmerge[$key]['Mtimes']);
					}
				}
				
				/*foreach($userid as $userID){
					$this->db->select('social.profileimage as imgUrl,users.username as description,social.social_rank,social.rank_f2,social.rank,social.points,social.updatedDtm as timesold,six_photos.updatedDtm as times,users.userId as friend_user_id');
					$this->db->from('hiprofile_users as users');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = users.userId','inner');
					$this->db->join('hiprofile_user_six_photos as six_photos','users.userId = six_photos.userId','left');				
					$this->db->where('six_photos.userId',$userID);
					$this->db->where('users.status', 0);
					$this->db->where('users.isDeleted', 0);
					$this->db->order_by('six_photos.updatedDtm', 'desc');
					$query2 = $this->db->get();
					$user2[] = $query2->result();
				}*/
				foreach($userid as $userID){
					$this->db->select('social.profileimage as imgUrl,users.name as description,social.social_rank,social.rank_f2,social.rank,social.points,social.updatedDtm as timesold,six_photos_notification.updatedDtm as times,users.userId as friend_user_id');
					$this->db->from('hiprofile_users as users');
					$this->db->join('hiprofile_user_socialsync as social','social.userId = users.userId','inner');
					$this->db->join('hiprofile_user_six_photos_notification as six_photos_notification','users.userId = six_photos_notification.userId','left');				
					$this->db->where('six_photos_notification.userId',$userID);
					$this->db->where('users.status', 0);
					$this->db->where('users.isDeleted', 0);
					$this->db->order_by('six_photos_notification.updatedDtm', 'desc');
					$query2 = $this->db->get();
					$user2[] = $query2->result();
				}
				if(!empty($user2)){
					foreach($user2 as $key=>$value1) {
						foreach($value1 as $finalvalue1) {
							if($finalvalue1->times!='')
							{
								$tempArr1['imgUrl'] = ($finalvalue1->imgUrl == "")?"":base_url().'assets'.$finalvalue1->imgUrl;
								$tempArr1['description'] = '<b>'.$finalvalue1->description.'</b>'.' has just updated '.'<b> SixPhotos </b>';
								$tempArr1['time'] = $this->time_elapsed_string($finalvalue1->times);
								$tempArr1['times'] = $finalvalue1->times;
								$tempArr1['Mtimes'] = $this->time_elapsed_string_minutes($finalvalue1->times);
								$tempArr1['friend_user_id'] = $finalvalue1->friend_user_id;
								$tempArr1['rank'] = round($finalvalue1->social_rank + $finalvalue1->rank+$finalvalue1->rank_f2);

								unset($tempArr1['rank']);							
								$fullArr1[] = $tempArr1;
							}
						}	
					}

				}
				if(!empty($fullArr1)){
					$final_value_friendmerge_sort = array_merge((array)$final_value_friendmerge,(array)$fullArr1);
				}else{
					$final_value_friendmerge_sort = $final_value_friendmerge;
				}	
				
				usort($final_value_friendmerge_sort,function($a,$b){
					$a = new DateTime($a['times']);
					$b = new DateTime($b['times']);
					if ($a == $b) {
						return 0;
					}
					return $a > $b ? -1 : 1;
				});
				
				if(!empty($final_value_friendmerge_sort))
				{
					$return_res = array();
					$return_res['responsecode'] = "200";
					$return_res['responsedetails'] = "success";
					$return_res['data']['page']['total_pages'] = $totalpage;
					$return_res['data']['page']['page_no'] = $pageno;
					$return_res['data']['page']["page_size"] = '10';
					$return_res['data']["recent_activities"] = $final_value_friendmerge_sort;
					
					return $return_res;
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}
			}
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "201";
			$return_res['status'] = "User not added any friends";
			return $return_res;
		}
		exit;
	}
	
	//To get the friend users friend added point details for recent activity
	function get_other_user_activities($useridSS,$friendid,$end,$start)
	{

		$this->db->select('userId');		
		$this->db->from('hiprofile_friends');	
		$this->db->where('friend_user_id',$friendid);
		$this->db->where('userId!=',$useridSS);
		$this->db->where('friend_user_id!=',$useridSS);		
		$this->db->where('add_friend','true');
		$this->db->where('request_status','1');
		$query = $this->db->get();
		$frndid = $query->result();
		$numRows = $query->num_rows();
		$frndids = [];
		foreach($frndid as $user_arr)
		{	
			$this->load->model('socialapi_model');
			$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($friendid,$user_arr->userId);
			$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($friendid,$user_arr->userId);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{	
				$frndids[] = $user_arr->userId;				
			}
		}
		$friend_id =  array_values($frndids);

		//print_r($friend_id);die;
		if(count($friend_id) >= 1)
		{
			$result = array();
			$id="";
			$this->db->select('userId,friend_user_id');
			$this->db->from('hiprofile_friends');
			$this->db->where('friend_user_id',$friendid);
			$this->db->where('add_friend','true');
			$this->db->where('request_status','1');
			//$this->db->limit($end, $start);
			$query = $this->db->get();
			$frndid = $query->result();
			
			$frdid ="";
			foreach($frndid as $key=>$value)
			{
				$id .= $value->userId . ",";
				$frdid .= $value->friend_user_id . ",";

			}
			$userid =  rtrim($id,',');
			$userid = explode(",",$userid);
			
			$frdid1 =  rtrim($frdid,',');
			$frdid2 = explode(",",$frdid1);
			
			$ii=0;
			// to fetch the records of rank, points, name and updated Dtm
			$user = [];
			foreach($userid as $userID)
			{
				if($userID != $useridSS)
				{
					/* pre($useridSS);
					pre($userID);
					pre($friendid);
					
					echo "</br>****************";
					*/
					$this->load->model('socialapi_model');
					$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($friendid,$userID);
					$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($friendid,$userID);
					if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
					{	
						/*$this->db->select('social.profileimage as imgUrl,users.name as description,social.updatedDtm as timesold,social.updatedDtm as times');
						$this->db->from('hiprofile_users as users');
						$this->db->join('hiprofile_user_socialsync as social','social.userId = users.userId','inner');
						$this->db->join('hiprofile_earned_user_points as earnedsocial','earnedsocial.friend_id = '.$friendid.' AND earnedsocial.userId = '.$userID.'','inner');*/
						
						$this->db->select('social.profileimage as imgUrl,users.username as description,social.updatedDtm as timesold,earnedsocial.updated_date as times');
						$this->db->from('hiprofile_users as users');
						$this->db->join('hiprofile_user_socialsync as social','social.userId = users.userId','inner');
						$this->db->join('hiprofile_earned_user_points as earnedsocial','earnedsocial.friend_id = '.$friendid.' AND earnedsocial.userId = '.$userID.'','inner');
						
						
						$this->db->where('users.userId',$userID);
						$this->db->where('users.status', 0);
						$this->db->where('users.isDeleted', 0);
						$this->db->order_by('earnedsocial.updated_date', 'desc');
						$query = $this->db->get();
						$result = $query->result();	
						
						$this->db->select('social.profileimage as imgUrl,users.name as description');
						$this->db->from('hiprofile_users as users');
						$this->db->join('hiprofile_user_socialsync as social','social.userId = users.userId','inner');
						
						$this->db->where('users.userId',$frdid2[$ii]);
						$this->db->where('users.status', 0);
						$this->db->where('users.isDeleted', 0);
						$query = $this->db->get();
						$frduser3 = $query->result();
						$imgurl = $frduser3[0]->imgUrl;

						if(!empty($result))
						{
							$result[0]->imgUrl =$imgurl; 
							$user[]=$result; 
						}
						

						
					}
				}
				$ii++;
			}

			//pre($user);
			//die;
			if(count($user)>0)
			{
				usort($user, function($a, $b)
				{
					$a = date("d/m/Y",strtotime($a[0]->times));
					$b = date("d/m/Y",strtotime($b[0]->times));
					return strcmp(strtotime($a),strtotime($b));
				});

			}

			$fullArr = [];
			//echo count($user);
			//print_r($user);

			if(count($user)>0)
			{
				foreach($user as $key=>$value) {
					foreach($value as $finalvalue) {
						if($finalvalue->times!='')
						{
							$tempArr['imgUrl'] = ($finalvalue->imgUrl == "")?"":base_url().'assets'.$finalvalue->imgUrl;
							$tempArr['description'] 	= $finalvalue->description;
							$tempArr['time'] 			= $this->time_elapsed_string($finalvalue->times);
							$tempArr['times'] 			= $finalvalue->times;
							$tempArr['Mtimes'] 			= $this->time_elapsed_string_minutes($finalvalue->times);
							$tempArr['friend_user_id'] 	= $friendid;

							//print_r($finalvalue);die;
							$tempArr['rank'] = round($finalvalue->social_rank + $finalvalue->rank+$finalvalue->rank_f2);
							for($r=1;$r<=1;$r++)
							{
								if($r == 1) 
								{ 
									$tempArr['description'] = '<b>'.$this->get_user_name($friendid).'</b>'.' earned '.'<b>'.$this->get_user_earned_points($friendid,$userID).' points</b> from meeting <b>'.$finalvalue->description.'</b>';
								}	
								unset($tempArr['rank']);	

								$fullArr[] = $tempArr;
								
							}
						}
					}
				}

			}
			
			
			return $fullArr;
		}
		
		/* $this->db->select('friend_user_id');
		$this->db->from('hiprofile_friends');
		$this->db->where('userId',$friendid);
		$this->db->where('userId!=',$userid);
		$this->db->where('friend_user_id!=',$userid);
		$this->db->where('add_friend','true');
		$this->db->where('request_status','1');
		$this->db->limit($end, $start);
		$query = $this->db->get();
		$frndid = $query->result();

		$point_values = array();
		$tempArr = array();
		foreach($frndid as $key=>$value)
		{
			$this->load->model('socialapi_model');
			$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($friendid,$value->friend_user_id);
			$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($friendid,$value->friend_user_id);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{	
				foreach($value as $finalvalue) {
					$points = $this->get_user_earned_points($friendid,$finalvalue);
					if($points !=0 || $points != ''){
					$tempArr[]['description'] = '<b>'.$this->get_user_name($friendid).'</b>'.' earned '.'<b>'.$points.' points</b> from meeting <b>'.$this->get_user_name($finalvalue).'</b>';
					}
				}
				
			}
		}
		return $point_values[] = $tempArr; */
	}
	
	public function getpagecount($userid)
	{
		$this->db->select('userId');
		$this->db->from('hiprofile_friends');
		$this->db->where('friend_user_id',$userid);
		$this->db->where('add_friend','true');
		$this->db->where('request_status','1');
		$query = $this->db->get();
		$frndid = $query->result();
		foreach($frndid as $user_arr)
		{	
			$this->load->model('socialapi_model');
			$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($userid,$user_arr->userId);
			$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($userid,$user_arr->userId);
			if($checkMyStatus == 0 && $checkMyFriendStatus == 0)
			{	
				$frndids[] = $user_arr->userId;				
			}
		}
		$friend_id =  array_values($frndids);
		if(count($friend_id) > 10 ){
			return ceil(count($friend_id)/10);
		}else{
			return 1;
		}
	}
	
	//To get the username
	function get_user_name($id)
	{
		$this->db->select('users.username as name');
		$this->db->from('hiprofile_users as users');
		$this->db->where('users.userId',$id);
		$this->db->where('users.status', 0);
		$this->db->where('users.isDeleted', 0);
		$query = $this->db->get();
		$user = $query->result();	
		return $user[0]->name;
	}
	/*function time_elapsed_string($datetime, $full = false) 
	{		

		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d = $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}*/
	
	/*Get the time calculation */
	function time_elapsed_string_minutes($time_ago) 
	{		
		$now = $this->get_current_datime();
		$time_ago = strtotime($time_ago);
		$cur_time   = strtotime($now);
		$time_elapsed   = $cur_time - $time_ago;
		$seconds    = $time_elapsed ;
		$minutes    = round($time_elapsed / 60 );
		return $minutes;
	}
	
	function time_elapsed_string($time_ago) 
	{		
		$now = $this->get_current_datime();
		$time_ago = strtotime($time_ago);
		$cur_time   = strtotime($now);
		$time_elapsed   = $cur_time - $time_ago;
		$seconds    = $time_elapsed ;
		$minutes    = round($time_elapsed / 60 );
		$hours      = round($time_elapsed / 3600);
		$days       = round($time_elapsed / 86400 );
		$weeks      = round($time_elapsed / 604800);
		$months     = round($time_elapsed / 2600640 );
		$years      = round($time_elapsed / 31207680 );
		// Seconds
		if($seconds <= 60){
			return "just now";
		}
		//Minutes
		else if($minutes <=60){
			if($minutes==1){
				return "1 minute ago";
			}
			else{
				return "$minutes minutes ago";
			}
		}
		//Hours
		else if($hours <=24){
			if($hours==1){
				return "1 hour ago";
			}else{
				return "$hours hours ago";
			}
		}
		//Days
		else if($days <= 7){
			if($days==1){
				return "1 day ago";
			}else{
				return "$days days ago";
			}
		}
		//Weeks
		else if($weeks <= 4.3){
			if($weeks==1){
				return "1 week ago";
			}else{
				return "$weeks weeks ago";
			}
		}
		//Months
		else if($months <=12){
			if($months==1){
				return "1 month ago";
			}else{
				return "$months months ago";
			}
		}
		//Years
		else{
			if($years==1){
				return "1 year ago";
			}else{
				return "$years years ago";
			}
		}
	}
	
	/*To fetch the curren time from the database*/
	function get_current_datime()
	{
		$this->db->select('Now() as ctime');
		$query = $this->db->get();
		$time = $query->result();
		return $time[0]->ctime;
	}
	

	/*Function to send email verification*/
	function sendVerificatinEmail($email,$userid, $code)
	{

		$update_data = array(
			'email' => $email,
			'email_verification_code' => $code,
			'email_status' => 0
		);

		$this->db->where('userId', $userid);
		$this->db->update('hiprofile_users', $update_data);
		$this->db->where('isDeleted', 0);
		if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	/*Function to send email verification*/
	function sendVerificatinEmailbyid($userid, $code)
	{

		$update_data = array(
			'email_verification_code' => $code,
			'email_status' => 0
		);

		$this->db->where('userId', $userid);
		$this->db->update('hiprofile_users', $update_data);
		$this->db->where('isDeleted', 0);
		if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	//API function is to get customer information by id for email
	function getCustomerInfoById($userId)
	{
		$this->db->select('userId, email, name');
		$this->db->from('hiprofile_users');
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('userId', $userId);
		$query = $this->db->get();

		return $query->result();
	}
	
	//API function to get the users earned points by adding friend
	function get_user_earned_points($userId,$frinedId)
	{
		$this->db->select('earned_points');
		$this->db->from('hiprofile_earned_user_points');
		$this->db->where('userId', $userId);
		$this->db->where('friend_id', $frinedId);
		$this->db->order_by("earned_points", "desc");
		$query = $this->db->get();
		$earnedPoints = $query->result();
		if(!empty($earnedPoints))
		{
			return $earnedPoints[0]->earned_points;
		}
		else
		{
			$earnedPoints = 0;
			return $earnedPoints;
		}
	}
	
	//Function to caluclate worth of two userID
	function getCalculateWorth($userid,$friendid)
	{
		$result = array_merge(array($userid),array($friendid));
		for($i=0;$i<count($result);$i++)
		{

			$this->db->select('social.rank_f2,social.points,social.social_rank,social.rank,social.social_points');
			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
			$this->db->where('BaseTbl.userId', $result[$i]);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.status', 0);
			$query = $this->db->get();
			$user[] = $query->result();
		}

		//FRIEND USER WHO ACCEPTS THE MAIN FRIEND USER WHO SENT AN REQUEST
		//$RankA = $user[0][0]->rank_f2 + $user[0][0]->social_rank + $user[0][0]->rank; 
		
		$RankA = $user[0][0]->social_rank + $user[0][0]->rank; 
		
		
		//$RankA 			= $user[0][0]->rank; 	
		$RankAPoints 	= $user[0][0]->points;
		//USER MAIN WHEN HE SENDS A NEW FRIEND REQUEST SENT
		//$RankB = $user[1][0]->rank_f2 + $user[1][0]->social_rank + $user[1][0]->rank;
		
		$RankB = $user[1][0]->social_rank + $user[1][0]->rank;
		
		//$RankB 			= $user[1][0]->rank;
		$RankBPoints 	= $user[1][0]->points;
		// CHECK THE RANK RANGE BETWEEN A AND B
		//$RankRangeA 	=  round(($RankA -1)/10)+1 ;
		//$RankRangeB 	=  round(($RankB -1)/10)+1 ;
		if(round($RankA) == 0):	
			$RankRangeA 	=  round(($RankA -1)/10)+1 ;
		else:
			$RankRangeA 	=  round($RankA);
		endif;
		
		if(round($RankB) == 0):
			$RankRangeB 	=  round(($RankB -1)/10)+1 ;
		else:
			$RankRangeB 	=  round($RankB);
		endif;
		$EarnedRank = 0;
		$EarnedRankPoints = 0;
		
		// For user Range Range 1-10 calculation
		if (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 4/10;
			$EarnedRankPoints = 400;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 5/10;
			$EarnedRankPoints = 500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 9/10+4;
			$EarnedRankPoints = 900+4000;
		}
		elseif (in_array($RankRangeA, range(1,10)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 10/10+4.5;
			$EarnedRankPoints = 1000+4500;
		}
		
		// For user Range Range 11-20 calculation
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 4/10;
			$EarnedRankPoints = 400;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		elseif (in_array($RankRangeA, range(11,20)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 9/10+4;
			$EarnedRankPoints = 900+4000;
		}
		
		// For user Range Range 21-30 calculation
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 3/10;
			$EarnedRankPoints = 300;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		elseif (in_array($RankRangeA, range(21,30)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 8/10+3.5;
			$EarnedRankPoints = 800+3500;
		}
		
		// For user Range Range 31-40 calculation
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 2/10;
			$EarnedRankPoints = 200;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		elseif (in_array($RankRangeA, range(31,40)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 7/10+3;
			$EarnedRankPoints = 700+3000;
		}
		
		// For user Range Range 41-50 calculation
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		elseif (in_array($RankRangeA, range(41,50)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 6/10+2.5;
			$EarnedRankPoints = 600+2500;
		}
		
		// For user Range Range 51-60 calculation
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		elseif (in_array($RankRangeA, range(51,60)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 5/10+2;
			$EarnedRankPoints = 500+2000;
		}
		
		// For user Range Range 61-70 calculation
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		elseif (in_array($RankRangeA, range(61,70)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 4/10+1.5;
			$EarnedRankPoints = 400+1500;
		}
		
		// For user Range Range 71-80 calculation
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		elseif (in_array($RankRangeA, range(71,80)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 3/10+1;
			$EarnedRankPoints = 300+1000;
		}
		
		// For user Range Range 81-90 calculation
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.2/10;
			$EarnedRankPoints = 20;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		elseif (in_array($RankRangeA, range(81,90)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 2/10+0.5;
			$EarnedRankPoints = 200+500;
		}
		
			// For user Range Range 91-100 calculation
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(1,10))) {
			$EarnedRank = 0.1/10;
			$EarnedRankPoints = 10;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(11,20))) {
			$EarnedRank = 0.2/10;
			$EarnedRankPoints = 20;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(21,30))) {
			$EarnedRank = 0.3/10;
			$EarnedRankPoints = 30;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(31,40))) {
			$EarnedRank = 0.4/10;
			$EarnedRankPoints = 40;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(41,50))) {
			$EarnedRank = 0.5/10;
			$EarnedRankPoints = 50;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(51,60))) {
			$EarnedRank = 0.6/10;
			$EarnedRankPoints = 60;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(61,70))) {
			$EarnedRank = 0.7/10;
			$EarnedRankPoints = 70;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(71,80))) {
			$EarnedRank = 0.8/10;
			$EarnedRankPoints = 80;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(81,90))) {
			$EarnedRank = 0.9/10;
			$EarnedRankPoints = 90;
		}
		elseif (in_array($RankRangeA, range(91,100)) && in_array($RankRangeB, range(91,100))) {
			$EarnedRank = 1/10;
			$EarnedRankPoints = 100;
		}
		return strval($EarnedRankPoints);
	}
	
	/* Function to insert user last seen*/
	function insertLastSeenRecords($methodName,$userId)
	{
		if($methodName!='' && $userId!='')
		{
			$id="";
			$this->db->select('last_seen');
			$this->db->from('hiprofile_user_lastseen');
			$this->db->where('userId',$userId);
			$query = $this->db->get();
			$numRows = $query->num_rows();
			$getResultQuery = $query->result();
			//Check user active or not
			
			if($numRows>0) 
			{ 
				$this->db->set('last_seen', 'NOW()', FALSE);
				$this->db->where('userId', $userId);
				$this->db->update('hiprofile_user_lastseen');

			}
			else
			{
				$this->db->set('userId',$userId);
				$this->db->insert('hiprofile_user_lastseen');
			}
		}

	}
	
	/*To update the users current lat and long*/
	function inserLatLong($lat,$long,$userId)
	{
		if($lat!='' && $long!='' && $userId!='')
		{
			$this->db->set('latitude', $lat);
			$this->db->set('longitude', $long);
			$this->db->where('userId', $userId);
			$this->db->update('hiprofile_users');
		}
	}
	
	//API call to check whether social id exist or not
	public function checksocialid($socialid,$authprovider)
	{
		$this->db->select('BaseTbl.userId as userID');
		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		
		if($authprovider == 'facebook')
		{
			$this->db->where('fbuserid',$socialid);
			$this->db->where('auth_provider_fb',$authprovider);
		}		
		elseif($authprovider == 'instagram')
		{
			$this->db->where('instagramuserid',$socialid);
			$this->db->where('auth_provider_inst',$authprovider);
		}
		elseif($authprovider == 'twitter')
		{
			$this->db->where('twitteruserid',$socialid);
			$this->db->where('auth_provider_twt',$authprovider);	
		}
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$query = $this->db->get();
		$user = $query->result();
		if ($query->num_rows() > 0){
			return $user[0]->userID;
		} 
		else{
			return 0;
		} 	
	}	
	
	//API call to check whether username already exist or not
	public function socialcheckuseremailexists($email)
	{
		$this->db->select('userId');
		$this->db->from('hiprofile_users');
		$this->db->where('email',$email);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$query = $this->db->get();
		$result = $query->result();
		$userid = $result[0]->userId;
		if ($query->num_rows() > 0){
			return $userid;
		} 
		else{
			return 0;
		} 
	}
	
	//API call to check whether mobilenumber already exist or not
	public function socialcheckusermobileexists($mobile)
	{
		$this->db->select('userId');
		$this->db->from('hiprofile_users');
		$this->db->where('mobile',$mobile);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$query = $this->db->get();
		$result = $query->result();
		$userid = $result[0]->userId;
		if ($query->num_rows() > 0){
			return $userid;
		} 
		else{
			return 0;
		} 
	}

    //API TO update six photo favourite
	public function sixPhotosFavourite($data)
	{
		unset($data['api_key']);

		$userID = $data['userID'];
		$image_id = $data['image_id'];		
		$friend_user_id = $data['friend_user_id'];
		$isFavourite = $data['isFavourite'];

		/*if($isFavourite !=1){
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}*/

		$this->db->select('fav_count');
		$this->db->from('hiprofile_user_six_photos');
		$this->db->where('id',$image_id);
		$query1 = $this->db->get();
		$result1 = $query1->result_array();
		if ($query1->num_rows() > 0){
			$fav_count = (int)$result1[0]['fav_count'];
		}else{
			$fav_count = (int)0;
		}
		

		$this->db->select('id');
		$this->db->from('hiprofile_user_six_photos');
		$this->db->where('userID',$friend_user_id);
		$query2 = $this->db->get();
		$result2 = $query2->result_array();

		if ($query2->num_rows() > 0){

			foreach ($result2 as $key => $value) {
				$img_ids[]= $value['id'];
			}		    

			$this->db->select('*');
			$this->db->from('hiprofile_user_six_photo_favourite_lists');
			//$this->db->where_in('image_id',$img_ids);
			$this->db->where('image_id',$image_id);
			$this->db->where('friend_user_id', $userID);
			$this->db->where('userId', $friend_user_id);
			$query = $this->db->get();
			$result = $query->result();
			if ($query->num_rows() == 0){
				
				$this->db->select('*');
				$this->db->from('hiprofile_user_six_photo_favourite_lists');
				$this->db->where_in('image_id',$img_ids);				
				$this->db->where('friend_user_id', $userID);
				$this->db->where('userId', $friend_user_id);
				$query3 = $this->db->get();
				$result3 = $query3->result_array();

				if ($query3->num_rows() > 0){
					$id = $result3[0]['id'];
					$del_img_id = $result3[0]['image_id'];
					$res = $this->db->delete('hiprofile_user_six_photo_favourite_lists', array('id' => $id));
					if($res){

						$this->db->select('fav_count');
						$this->db->from('hiprofile_user_six_photos');
						$this->db->where('id',$del_img_id);
						$query4 = $this->db->get();
						$result4 = $query4->result_array();
						if ($query4->num_rows() > 0){
							$fav_count1 = (int)$result4[0]['fav_count'];
						}else{
							$fav_count1 = (int)0;
						}

						$this->db->set('fav_count', $fav_count1-1);
						$this->db->where('id', $del_img_id);
						$this->db->update('hiprofile_user_six_photos');

						$this->db->set('userId',$friend_user_id);
						$this->db->set('image_id',$image_id);			
						$this->db->set('friend_user_id',$userID);			
						$this->db->insert('hiprofile_user_six_photo_favourite_lists');

						$this->db->set('fav_count', $fav_count+1);
						$this->db->where('id', $image_id);
						$this->db->update('hiprofile_user_six_photos');
					}

				}else{
					$this->db->set('userId',$friend_user_id);
					$this->db->set('image_id',$image_id);			
					$this->db->set('friend_user_id',$userID);			
					$this->db->insert('hiprofile_user_six_photo_favourite_lists');

					$this->db->set('fav_count', $fav_count+1);
					$this->db->where('id', $image_id);
					$this->db->update('hiprofile_user_six_photos');
				}
				if($this->db->affected_rows() >= 0){
					$return_res['responsecode'] = "200";
					$return_res['responsedetails'] = "Success";
					return $return_res;
				}else{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					return $return_res;
				}		            
			}else{
				$res = $this->db->delete('hiprofile_user_six_photo_favourite_lists', array('id' => $result[0]->id));
				if($res){
					$this->db->select('fav_count');
					$this->db->from('hiprofile_user_six_photos');
					$this->db->where('id',$result[0]->image_id);
					$query4 = $this->db->get();
					$result4 = $query4->result_array();
					if ($query4->num_rows() > 0){
						$fav_count1 = (int)$result4[0]['fav_count'];
					}else{
						$fav_count1 = (int)0;
					}

					$this->db->set('fav_count', $fav_count1-1);
					$this->db->where('id', $result[0]->image_id);
					$this->db->update('hiprofile_user_six_photos');
				}

				$return_res['responsecode'] = "200";
				$return_res['responsedetails'] = "Success";
				$return_res['message'] = "Unfavourite successfull";
				return $return_res;
			} 			
		}else{

			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		} 
		
	}


	// API to to update rank/double points
	public function updateWatchAdds($data)
	{
		unset($data['api_key']);
		$this->db->select('Id,rank,points,social_rank');
		$this->db->from('hiprofile_user_socialsync');
		$this->db->where('userId', $data['userID']);
		$query = $this->db->get();
		$result = $query->result_array();
		
		if(count($result) > 0){
			$id = $result[0]['Id'];
			$rank = $result[0]['rank'];
			$points = $result[0]['points'];
			//echo $points;
			if($data['is_package'] == '1'){
				
				if($data['ad_type'] == 0){
					
					$dt['userId'] = $data['userID'];
					
					$dt['ad_type'] = '0';					
					$dt['category'] = 'dp';					
					$dt['is_activated'] = '0';					
					$dt['orderId'] = '100000'.time();					
					$this->db->insert('hiprofile_rank_points_plan_details', $dt);
					$insert_id = $this->db->insert_id();

					if($insert_id >= 0)
					{
						$return_res['responsecode'] = "200";
						$return_res['responsedetails'] = "Success";
						$return_res['message'] = "Double points added to options";
						return $return_res;
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}

				}
				if($data['ad_type'] == 1){
					$package_name = 'hf_'.time();
					$dt['userId'] = $data['userID'];					
					$dt['ad_type'] = '1';					
					$dt['category'] = 'hf';					
					$dt['is_activated'] = '0';
					$dt['orderId'] = '100000'.time();				
					$dt['product_id'] = $package_name;				
					$dt['product_title'] = "5 Fame Rank Boost for 1 hour";				
					$this->db->insert('hiprofile_rank_points_plan_details', $dt);
					$insert_id = $this->db->insert_id();

					$dtt['ranks_points_package_Google_playId'] = $package_name;					
					$dtt['ranks_points_package_rank_up'] = '5';					
					$dtt['ranks_points_package_hours'] = '1h';					
					$dtt['ranks_points_package_price'] = '0.00';
					$dtt['ranks_points_package_category_code'] = 'hf';				
					$this->db->insert('hiprofile_rank_point_caetogry_packages_list', $dtt);

					if($insert_id >= 0)
					{
						$return_res['responsecode'] = "200";
						$return_res['responsedetails'] = "Success";
						$return_res['message'] = "Higher fame added to options";
						return $return_res;
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}

				}
			}else{

				if($data['ad_type'] == 0){
					//$pt = (int) $points + 50 ;
                    $pt = (int) $points + 30 ;
                    //echo $id;
					if($pt >= 1000){
						$this->db->set('rank',$rank + 1);
						$this->db->set('points',$pt - 1000);
						$msg = "Rank has been increased by 1";	
					}else{
						$this->db->set('points',$pt);
						$msg = "You have earned 30 points";	
					}
					//$this->db->set('points',$pt);
					$this->db->set('old_earned_points',$points);				
					$this->db->set('old_rank',$rank);				
					$this->db->where('id', $id);			
					$this->db->update('hiprofile_user_socialsync');
					$this->db->set('watch_add_time',date('Y-m-d H:i:s'));
					$this->db->set('canWatchDoublePointAd','0');
								
					$this->db->where('userId', $data['userID']);			
					$this->db->update('hiprofile_users');

					if($this->db->affected_rows() >= 0)
					{
						$return_res['responsecode'] = "200";
						$return_res['responsedetails'] = "Success";
						$return_res['message'] = $msg;
						return $return_res;
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}
				}
				/*if($data['ad_type'] == 1){
					$rk = (int) $rank + 3 ;
					$this->db->set('rank',$rk);
					$this->db->set('old_earned_points',$points);				
					$this->db->set('old_rank',$rank);				
					$this->db->where('id', $id);			
					$this->db->update('hiprofile_user_socialsync');	

					$this->db->set('watch_add_time',date('Y-m-d H:i:s'));
				
					$this->db->set('canWatchHigherFameAd','0');				
					$this->db->where('userId', $data['userID']);			
					$this->db->update('hiprofile_users');

					$msg = "Rank has been increased by 3";
					if($this->db->affected_rows() >= 0)
					{
						$return_res['responsecode'] = "200";
						$return_res['responsedetails'] = "Success";
						$return_res['message'] = $msg;
						return $return_res;
					}
					else
					{
						$return_res = array();
						$return_res['responsecode'] = "500";
						$return_res['status'] = "Something went wrong. Please try again later.";
						return $return_res;
					}			
				}*/
			}
		}else{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}		
		
	}
        
        function getTopHundredRankUsers()
	{
                //echo 1; die;
		$this->db->select('BaseTbl.userId as userID,BaseTbl.name,BaseTbl.username, social.points, social.social_rank,social.rank,(social.social_rank + social.rank) as total_rank ');
		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');		
		$this->db->where('BaseTbl.isDeleted', 0);
		//$this->db->where('BaseTbl.status', 0);
		//$this->db->order_by('social.social_rank', 'desc');
                $this->db->order_by('total_rank', 'desc');
                $this->db->order_by('social.points', 'asc');
                $this->db->limit(100);
                
		$query = $this->db->get();
//                echo $this->db->last_query();
//                die;
		$res = $query->result();
                
                $data = array();
                if(!empty($res)){
                    foreach ($res as $key => $value) {
                        $data[$key]['name'] = $value->name;
                        $data[$key]['userID'] = $value->userID;
                        $data[$key]['userName'] = !empty($value->username) ? $value->username : "";
                        $data[$key]['rank'] = $value->rank + $value->social_rank;
                    }
                    
                    usort($data, function($a, $b) {
                        return $b['rank'] - $a['rank'];
                    });
                    
                    $return_res = array();
                    $return_res['responsecode'] 	= "200";
                    $return_res['responsedetails'] 	= "success";			
                    $return_res['data']['ranking'] 	= $data;			
                    return $return_res;                    
                    
                }              
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
		
	}
        
        
        //API call to update updateLogoutStatus
	public function updateLogoutStatus($data)
	{
            unset($data['api_key']);             
            $this->db->set('is_active', '1');            
            $this->db->where('userId', $data['userID']);             
            if ($this->db->update('hiprofile_users')){
                return TRUE;
            } 
            else{
                return FALSE;
            } 
	}

	//get the friend request type
	function get_friend_requested_type($myId,$friend_id){
		$this->db->select('id,friend_req_type');
		$this->db->from('hiprofile_friends');
		$this->db->where('userId', $myId);
		$this->db->where('friend_user_id', $friend_id);
		//$this->db->where('request_status', "0");
		$this->db->where('add_friend', "true");
		$query = $this->db->get();
		$ret = $query->row();
		$numRows = $query->num_rows();
		if($numRows>0)
		{
			return $ret->friend_req_type;
		}
		else
		{
			return '';
		}
		
		//$numRows = $query->num_rows();
	}

}