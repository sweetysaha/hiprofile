<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mapsearchapi_model extends CI_Model
{
	
	public function loadMap($data)
	{
		unset($data['api_key']);
		/*To find the users*/
		$update_data = array(
			
			'device_token' => $data['device_token'],
			'device_id' => $data['device_id'],
			'device_type' => $data['device_type'],
			'latitude' => $data['latitude'],
			'longitude' => $data['longitude'],
			'desired_visibility' => $data['desired_visibility']
		);
		$this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
		$this->db->update('hiprofile_users', $update_data);
		if ($this->db->affected_rows() >= 0)
		{
			$this->db->select('social_rank,rank,rank_f2'); 
			$this->db->from('hiprofile_user_socialsync');
			$this->db->where('userId', $data['userID']);
			$query = $this->db->get();
			$userrank = $query->result();
			//$rank = round($userrank[0]->social_rank+$userrank[0]->rank+$userrank[0]->rank_f2);
			$rank = $userrank[0]->social_rank+$userrank[0]->rank;
				
			/*To find the users*/
			$this->db->select('BaseTbl.userId as userID,BaseTbl.name, BaseTbl.dob, BaseTbl.gender, BaseTbl.gender_disclose,	BaseTbl.mobile as mobileNumber, BaseTbl.email,BaseTbl.email_status, BaseTbl.username as userName,BaseTbl.rating,BaseTbl.feedback,BaseTbl.latitude,BaseTbl.longitude,BaseTbl.own_visibility,BaseTbl.friendsonlyvisible,social.social_rank,social.rank,social.points,social.worth,social.bio,social.points_required,social.fbuserid as fbUserId,social.twitteruserid as twitterUserId, social.instagramuserid as instagramUserId, social.facebook_profile_url as fbURL,social.twitter_profile_url as twitterURL, social.instagram_profile_url as instagramURL,social.profileimage as image_url,
			(round(social.rank + social.rank_f2 + social.social_rank)) as total_rank');

			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
			$this->db->where('BaseTbl.ghost_mode', 'false');
			$this->db->where('BaseTbl.approachability >=', 50);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.status', 0);
			$this->db->where('BaseTbl.roleId', 0);
			$this->db->where('BaseTbl.userId !=', $data['userID']);
			$this->db->having('total_rank >=', $data['desired_visibility']);
			$this->db->order_by('BaseTbl.userId', 'DESC');
			$query = $this->db->get();
			$users = $query->result();
			$finalmaparray = array();

			for($i=0;$i<count($users);$i++)
			{ 
				if($rank >= $users[$i]->own_visibility)
				{
					$finalmaparray[] = $users[$i];
				}
			}
			$loop_count = count($finalmaparray);
			foreach($finalmaparray as $user_arr)
			{	
				$this->load->model('socialapi_model');
				$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$user_arr->userID);
				$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$user_arr->userID);
				$userlastseendata = $this->check_userlastseen($user_arr->userID);
				if($checkMyStatus == 0 && $checkMyFriendStatus == 0 && $userlastseendata == 1)
				{
					
					if($user_arr->friendsonlyvisible == 'true')
					{
						$friendslist = $this->check_userfriends($data['userID'],$user_arr->userID);
						$userlistfriend = $this->check_friendsuser($data['userID'],$user_arr->userID);
						if($friendslist == 1 && $userlistfriend == 1)
						{
							$getusermappedfriend = $this->get_user_status_friend_social($data['userID'],$user_arr->userID);
							
							$getearnedpoints = $this->get_user_earned_points($data['userID'],$user_arr->userID);
							
							//Function call to get the user details.					
							$mapfriendsList[] = $this->get_friend_requested_user_details($user_arr->userID,$getusermappedfriend,$getearnedpoints,$data['userID']);				
						}
					}	
					else
					{
						$getusermappedfriend = $this->get_user_status_friend_social($data['userID'],$user_arr->userID);
						
						$getearnedpoints = $this->get_user_earned_points($data['userID'],$user_arr->userID);
						
						//Function call to get the user details.					
						$mapfriendsList[] = $this->get_friend_requested_user_details($user_arr->userID,$getusermappedfriend,$getearnedpoints,$data['userID']);		
					}	
					
				}
			}
				
			/*To get the business*/
			$this->db->select('business.business_id,business.business_title,business.address,business.country,		business.state,business.city,business.pin,business.image_url,business.contact,business.email,business.categories,business.primary_category,business.day_open_from,business.day_open_to,business.hours_open_from,business.hours_open_to,business.latitude,business.longitude,business.business_rating as rating,business.categories as category_ids,business.primary_category as primary_category_id,business.approved_status,business.is_paid');
			$this->db->from('hiprofile_business as business');
			$this->db->join('hiprofile_business_categories_lists as cat', 'business.primary_category = cat.business_categoryId','left');
			$this->db->where('business.isDeleted',0);
			$this->db->where('business.approved_status', 1);
			$this->db->where('cat.cat_status', 0);
			$this->db->where('cat.isDeleted', 0);
			$this->db->order_by('business.business_id', 'DESC');
			$query = $this->db->get();
			$business = $query->result();
			$loop_count = count($business);
			for($i=0;$i<$loop_count;$i++) 
			{
				$business[$i]->image_url = ($business[$i]->business_image_url == "") ? "" : base_url().'assets'.$business[$i]->image_url;		

				//fucntion to get the name of category from id's
				$catlist = $this->getCategoryNameFromId($business[$i]->category_ids);
				$business[$i]->categories = $catlist;				
				$catname = $this->getPrimaryCategoryNameFromId($business[$i]->primary_category_id);
				$business[$i]->primary_category = $catname;		

				// Set status
				if($business[$i]->approved_status == 0){
					$business[$i]->status = 'payment_pending';
				}
				elseif($business[$i]->approved_status == 1){
					$business[$i]->status = 'Approved';
				}else{
					$business[$i]->status = 'pending';
				}
				unset($business[$i]->category_ids);
				unset($business[$i]->primary_category_id);
				unset($business[$i]->approved_status);
				unset($business[$i]->is_paid);
			}
			/*To get the results of the array*/
			if(empty($finalmaparray) && empty($business))
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "No Match Found.";
				return $return_res;
			}
			else
			{
				$this->db->select("desired_visibility,own_visibility,approachability,ghost_mode,friendsonlyvisible,");
				$this->db->from('hiprofile_users');
				$this->db->where('isDeleted', 0);
				$this->db->where('status', 0);
				$this->db->where('roleId', 0);
				$this->db->where('userId', $data['userID']);
				$query = $this->db->get();
				$settings = $query->result();
			
				$return_res = array();
				$return_res['responsecode'] = "200";
				$return_res['responsedetails'] = "Success";
				$return_res['data']['people'] = $mapfriendsList;
				$return_res['data']['business'] = $business;
				$return_res['data']['settings'] = $settings[0];
				return $return_res;
			}
			exit;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		
	}
	
	//API call - To Update the map settings and show the details
    public function updateMappSettings($data)
	{

		unset($data['api_key']);
		$ghostmode = strtolower($data['ghost_mode']);
		$friendsonlyvisible = strtolower($data['friendsonlyvisible']);
		$update_data = array(
			
			'device_token' => $data['device_token'],
			'device_id' => $data['device_id'],
			'device_type' => $data['device_type'],
			'latitude' => $data['latitude'],
			'longitude' => $data['longitude'],
			'desired_visibility' => $data['desired_visibility'],
			'own_visibility' => $data['own_visibility'],
			'approachability' => $data['approachability'],
			'ghost_mode' => $ghostmode,
			'friendsonlyvisible' => $friendsonlyvisible
		);
		$this->db->where('userId', $data['userID']);
		$this->db->where('isDeleted', 0);
		$this->db->where('status', 0);
		$this->db->where('roleId', 0);
		$this->db->update('hiprofile_users', $update_data);
		if ($this->db->affected_rows() >= 0)
		{
			$this->db->select('social_rank,rank,rank_f2'); 
			$this->db->from('hiprofile_user_socialsync');
			$this->db->where('userId', $data['userID']);
			$query = $this->db->get();
			$userrank = $query->result();
			//$rank = round($userrank[0]->social_rank+$userrank[0]->rank+$userrank[0]->rank_f2);
			$rank = $userrank[0]->social_rank+$userrank[0]->rank;
			/*To find the users*/
			$this->db->select('BaseTbl.userId as userID,BaseTbl.name, BaseTbl.dob, BaseTbl.gender, BaseTbl.gender_disclose,	BaseTbl.mobile as mobileNumber, BaseTbl.email,BaseTbl.email_status, BaseTbl.username as userName,BaseTbl.rating,BaseTbl.feedback,BaseTbl.latitude,BaseTbl.longitude,BaseTbl.own_visibility,BaseTbl.friendsonlyvisible,social.social_rank,social.rank,social.points,social.worth,social.bio,social.points_required,social.fbuserid as fbUserId,social.twitteruserid as twitterUserId, social.instagramuserid as instagramUserId, social.facebook_profile_url as fbURL,social.twitter_profile_url as twitterURL, social.instagram_profile_url as instagramURL,social.profileimage as image_url,
			(round(social.rank + social.rank_f2 + social.social_rank)) as total_rank');

			$this->db->from('hiprofile_users as BaseTbl');
			$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
			$this->db->where('BaseTbl.ghost_mode', 'false');
			$this->db->where('BaseTbl.approachability >=', 50);
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.status', 0);
			$this->db->where('BaseTbl.roleId', 0);
			$this->db->where('BaseTbl.userId !=', $data['userID']);
			$this->db->having('total_rank >=', $data['desired_visibility']);
			$this->db->order_by('BaseTbl.userId', 'DESC');
			$query = $this->db->get();
			$users = $query->result();
			$finalmaparray = array();
			for($i=0;$i<count($users);$i++)
			{ 
				if($rank >= $users[$i]->own_visibility)
				{
					$finalmaparray[] = $users[$i];
				}
			}
			$loop_count = count($finalmaparray);
			foreach($finalmaparray as $user_arr)
			{	
				$this->load->model('socialapi_model');
				$checkMyStatus = $this->socialapi_model->checkMyBlockStatus($data['userID'],$user_arr->userID);
				$checkMyFriendStatus = $this->socialapi_model->checkMyFriendBlockStatus($data['userID'],$user_arr->userID);
				$userlastseendata = $this->check_userlastseen($user_arr->userID);
				if($checkMyStatus == 0 && $checkMyFriendStatus == 0 && $userlastseendata == 1)
				{
					
					if($user_arr->friendsonlyvisible == 'true')
					{
						$friendslist = $this->check_userfriends($data['userID'],$user_arr->userID);
						$userlistfriend = $this->check_friendsuser($data['userID'],$user_arr->userID);
						if($friendslist == 1 && $userlistfriend == 1)
						{
							$getusermappedfriend = $this->get_user_status_friend_social($data['userID'],$user_arr->userID);
							
							$getearnedpoints = $this->get_user_earned_points($data['userID'],$user_arr->userID);
							
							//Function call to get the user details.					
							$mapfriendsList[] = $this->get_friend_requested_user_details($user_arr->userID,$getusermappedfriend,$getearnedpoints,$data['userID']);				
						}
					}	
					else
					{
						$getusermappedfriend = $this->get_user_status_friend_social($data['userID'],$user_arr->userID);
						
						$getearnedpoints = $this->get_user_earned_points($data['userID'],$user_arr->userID);
						
						//Function call to get the user details.					
						$mapfriendsList[] = $this->get_friend_requested_user_details($user_arr->userID,$getusermappedfriend,$getearnedpoints,$data['userID']);		
					}		
				}
			}
			/*To get the business*/
			$this->db->select('business.business_id,business.business_title,business.address,business.country,		business.state,business.city,business.pin,business.image_url,business.contact,business.email,business.categories,business.primary_category,business.day_open_from,business.day_open_to,business.hours_open_from,business.hours_open_to,business.latitude,business.longitude,business.business_rating as rating,business.categories as category_ids,business.primary_category as primary_category_id,business.approved_status,business.is_paid');
			$this->db->from('hiprofile_business as business');
			$this->db->join('hiprofile_business_categories_lists as cat', 'business.primary_category = cat.business_categoryId','left');
			$this->db->where('business.isDeleted',0);
			$this->db->where('business.approved_status', 1);
			$this->db->where('cat.cat_status', 0);
			$this->db->where('cat.isDeleted', 0);
			$this->db->order_by('business.business_id', 'DESC');
			$query = $this->db->get();
			$business = $query->result();
			$loop_count = count($business);
			for($i=0;$i<$loop_count;$i++) 
			{
				$business[$i]->image_url = ($business[$i]->business_image_url == "") ? "" : base_url().'assets'.$business[$i]->image_url;		

				//fucntion to get the name of category from id's
				$catlist = $this->getCategoryNameFromId($business[$i]->category_ids);
				$business[$i]->categories = $catlist;				
				$catname = $this->getPrimaryCategoryNameFromId($business[$i]->primary_category_id);
				$business[$i]->primary_category = $catname;		

				// Set status
				if($business[$i]->approved_status == 0){
					$business[$i]->status = 'payment_pending';
				}
				elseif($business[$i]->approved_status == 1){
					$business[$i]->status = 'Approved';
				}else{
					$business[$i]->status = 'pending';
				}
				unset($business[$i]->category_ids);
				unset($business[$i]->primary_category_id);
				unset($business[$i]->approved_status);
				unset($business[$i]->is_paid);
			}
			/*To get the results of the array*/
			if(empty($finalmaparray) && empty($business))
			{
				$return_res = array();
				$return_res['responsecode'] = "201";
				$return_res['responsedetails'] = "No Match Found.";
				return $return_res;
			}
			else
			{
				$this->db->select("desired_visibility,own_visibility,approachability,ghost_mode,friendsonlyvisible,");
				$this->db->from('hiprofile_users');
				$this->db->where('isDeleted', 0);
				$this->db->where('status', 0);
				$this->db->where('roleId', 0);
				$this->db->where('userId', $data['userID']);
				$query = $this->db->get();
				$settings = $query->result();
			
				$return_res = array();
				$return_res['responsecode'] = "200";
				$return_res['responsedetails'] = "Success";
				$return_res['data']['people'] = $mapfriendsList;
				$return_res['data']['business'] = $business;
				$return_res['data']['settings'] = $settings[0];
				return $return_res;
			}			
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
	}
	
	//To fetch all the categories using the id's
	public function getCategoryNameFromId($catIds)
	{
		$id = explode(',',$catIds);
		$this->db->select('business_categoryId as category_id, business_category_icon as icon_name, business_category_names as category');
		$this->db->from('hiprofile_business_categories_lists');
		$this->db->where_in('business_categoryId',$id);
		$this->db->where('cat_status',0);
		$query = $this->db->get();
		$category = $query->result();
		return $category;
	}
	
	//To fetch the primary category using the id
	public function getPrimaryCategoryNameFromId($catIds)
	{
		$this->db->select('business_categoryId as category_id, business_category_icon as icon_name, business_category_names as category');
		$this->db->from('hiprofile_business_categories_lists');
		$this->db->where('business_categoryId',$catIds);
		$this->db->where('cat_status',0);
		$query = $this->db->get();
		$primarycategory = $query->result();
		return $primarycategory[0];
		
	}
	
	//Fucntion to load the friends data of the user id
	function get_user_status_friend_social($userId,$friend_user_id){
		
		$user[] = new stdClass();
		//$this->db->select('friend.request_status as friend_req_status');
		$this->db->select('friend.request_status as friend_req_status,friend.friend_user_id,friend.userId');
		$this->db->from('hiprofile_friends as friend');
		//$this->db->where('friend.userId', $userId);
		//$this->db->where('friend.friend_user_id', $friend_user_id);
		
		$andwhere = '(friend.friend_user_id='.$friend_user_id.' and friend.userId = '.$userId.')';
		$orwhere = '(friend.friend_user_id='.$userId.' and friend.userId = '.$friend_user_id.')';
		$this->db->where($andwhere);
		$this->db->or_where($orwhere);
		
		$this->db->order_by('friend.friend_user_id', 'desc');
		$query = $this->db->get();
		$user1 = $query->result();
		$friendsacceptcount = count($user1);
		if($friendsacceptcount>0){
			for($i=0;$i<$friendsacceptcount;$i++)
			{
				if($user1[$i]->friend_user_id == $friend_user_id && $user1[$i]->userId==$userId)
				{	
					if($user1[$i]->friend_req_status == '1'):
						$user[$i]->friend_req_status = 'approved';
						$user[$i]->is_friend = 'true';
					elseif($user1[$i]->friend_req_status == '2'):
						$user[$i]->friend_req_status = 'rejected';
						$user[$i]->is_friend = 'false';
					elseif($user1[$i]->friend_req_status == '0'):
						$user[$i]->friend_req_status = 'pending';
						$user[$i]->is_friend = 'false';
					endif;	
				}else if($user1[$i]->friend_user_id == $userId && $user1[$i]->userId==$friend_user_id)
				{	
				
					if($user1[$i]->friend_req_status == '1'):
						$user[$i]->friend_req_status = 'approved';
						$user[$i]->is_friend = 'true';
					elseif($user1[$i]->friend_req_status == '2'):
						$user[$i]->friend_req_status = 'rejected';
						$user[$i]->is_friend = 'false';
					elseif($user1[$i]->friend_req_status == '0'):
						$user[$i]->friend_req_status = 'received';
						$user[$i]->is_friend = 'false';
					endif;
			
				}
				
			}
		}else
		{
			
					$user[0]->friend_req_status = 'none';
					$user[0]->is_friend = 'false';	
		}
		$this->db->select('
		social.facebook_status as req_fb_status,
		social.twitter_status as req_twitter_status,
		social.instagram_status as req_insta_status,
		social.contact_status as req_contact_status,
		social.request_status
		');
		$this->db->from('hiprofile_social_friends as social');
		$this->db->where('social.userId', $userId);
		$this->db->where('social.friend_user_id', $friend_user_id);
		$this->db->order_by('social.friend_user_id', 'desc');
		$query = $this->db->get();
		$user2 = $query->result();
		$friendsacceptcount = count($user2);
		// echo $friendsacceptcount;
		//pre($user2);
		$constval = 0;
		$status_val = array("0"=>"none","1"=>"approved","2"=>"rejected","3"=>"cancelled","4"=>"pending");
		$status_Arr = array("req_fb_status","req_twitter_status","req_insta_status","req_contact_status");
		
		if($friendsacceptcount!= 0){
			$j = 0;
			for($i=0;$i<$friendsacceptcount;$i++){
				$temp_arr[] = new stdClass();
				
				if($user2[$i]->req_fb_status == 1) {
					$temp_arr[$j]->req_fb_status = $status_val[$user2[$i]->request_status];
				} 
				
				if($user2[$i]->req_twitter_status == 1) {
					$temp_arr[$j]->req_twitter_status = $status_val[$user2[$i]->request_status];
				} 
				
				if($user2[$i]->req_insta_status == 1) {
					$temp_arr[$j]->req_insta_status = $status_val[$user2[$i]->request_status];
				} 
				if($user2[$i]->req_contact_status == 1) {
					$temp_arr[$j]->req_contact_status = $status_val[$user2[$i]->request_status];
				} 
				$temp_arr[$j]->i = $i;
				$j++;
			}
			$i =0;
			$final_val = array();
			
			foreach($temp_arr as $key=>$value) {
				
				if($value->req_insta_status) {
					$final_val['req_insta_status'] = $value->req_insta_status;
				}
				if($value->req_contact_status) {
					$final_val['req_contact_status'] = $value->req_contact_status;
				}
				if($value->req_fb_status) {
					$final_val['req_fb_status'] = $value->req_fb_status;
				}
				if($value->req_twitter_status) {
					$final_val['req_twitter_status'] = $value->req_twitter_status;
				}
				if($value->i) {
					$final_val['i'] = $value->i;
				}
				
			}
			$final_keys = array_keys($final_val);
			
			$final_com = array_diff($status_Arr, $final_keys);
		
			if(count($final_com) >= 1) {
				foreach($final_com as $value){
					//pre($value);exit;
					if($value == "req_fb_status")      { $user[$i]->req_fb_status = "none"; }
					if($value == "req_twitter_status") { $user[$i]->req_twitter_status = "none"; }
					if($value == "req_insta_status")   { $user[$i]->req_insta_status = "none"; }
					if($value == "req_contact_status") { $user[$i]->req_contact_status = "none"; }
					
				}
			} 
			// pre($final_val);exit;
			if(count($final_val) >= 1) {
				foreach($final_val as $key=>$value){
					// echo $value;exit;
					if($key == "req_fb_status")      { $user[$i]->req_fb_status = $value; }
					if($key == "req_twitter_status") { $user[$i]->req_twitter_status = $value; }
					if($key == "req_insta_status")   { $user[$i]->req_insta_status = $value; }
					if($key == "req_contact_status") { $user[$i]->req_contact_status = $value; }
					
				}
			} 
		
		}else{
			$user[0]->req_fb_status	='none';
			$user[0]->req_contact_status='none';	
			$user[0]->req_twitter_status='none';
			$user[0]->req_insta_status='none';
		}
		return $user;
	}
	
	//Fucntion To get user details when sending friend request
	function get_friend_requested_user_details($frnduserid,$getuserfriend,$getearnedpoints,$userid){
		$this->load->model('userapi_model');
		$this->db->select('BaseTbl.userId as userID,BaseTbl.name, BaseTbl.location_disclose, BaseTbl.dob, BaseTbl.gender, BaseTbl.gender_disclose,	BaseTbl.mobile as mobileNumber, BaseTbl.email,BaseTbl.email_status, BaseTbl.username as userName,BaseTbl.rating,BaseTbl.feedback,BaseTbl.approachability,BaseTbl.share_mobile,BaseTbl.latitude,BaseTbl.longitude,social.social_rank,social.rank,social.rank_f2,social.points,social.worth,social.bio,social.points_required,social.fbuserid as fbUserId,twitteruserid as twitterUserId, instagramuserid as instagramUserId, social.facebook_profile_url as fbURL,social.twitter_profile_url as twitterURL, instagram_profile_url as instagramURL,social.profileimage as image_url');

		$this->db->from('hiprofile_users as BaseTbl');
		$this->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
		$this->db->where_in('BaseTbl.userId', $frnduserid);
		$this->db->where('BaseTbl.isDeleted', 0);
		$this->db->where('BaseTbl.status', 0);
		$this->db->order_by('BaseTbl.userId', 'desc');
		$query = $this->db->get();
		$user = $query->result();
		$usercount = count($user);
		$users[] = new stdClass();
		$userdetails = array();
		if($usercount != 0)
		{
			for($i=0;$i<$usercount;$i++){

				$users[$i]->userID = $user[$i]->userID;
				$users[$i]->name = $user[$i]->name;
				$users[$i]->dob = $user[$i]->dob;
				$users[$i]->gender = strtolower($user[$i]->gender);
				$users[$i]->gender_disclose = $user[$i]->gender_disclose;
				$users[$i]->mobileNumber = $user[$i]->mobileNumber;
				$users[$i]->email = $user[$i]->email;
				$users[$i]->email_status = $user[$i]->email_status;
				$users[$i]->userName = $user[$i]->userName;
				$users[$i]->points = $user[$i]->points;
				$users[$i]->rating = $user[$i]->rating;
				$users[$i]->feedback = $user[$i]->feedback;
				$users[$i]->approachability = $user[$i]->approachability;
				//$users[$i]->worth = $user[$i]->worth;
				$users[$i]->worth = $this->userapi_model->getCalculateWorth($userid,$frnduserid);
				$users[$i]->bio = $user[$i]->bio;
				if($user[$i]->location_disclose == 'true'):
					$lat = $user[$i]->latitude;
					$long = $user[$i]->longitude;
					$userNewLocation = $this->getUserRandomLocation($lat,$long);
					$users[$i]->latitude = $userNewLocation['lat'];
					$users[$i]->longitude = $userNewLocation['long'];
				else:
						$users[$i]->latitude = $user[$i]->latitude;
						$users[$i]->longitude = $user[$i]->longitude;
				endif;
				$users[$i]->registration_timestamp = json_decode(json_encode(time()), FALSE);
				$users[$i]->fbUserId = ($user[$i]->fbUserId == 0)?"":$user[$i]->fbUserId;
				$users[$i]->twitterUserId = ($user[$i]->twitterUserId == 0)?"":$user[$i]->twitterUserId;
				$users[$i]->instagramUserId = ($user[$i]->instagramUserId == 0)?"":$user[$i]->instagramUserId;
				$users[$i]->fbURL = ($user[$i]->fbURL == "")?"":$user[$i]->fbURL;
				$users[$i]->twitterURL = ($user[$i]->twitterURL == "")?"":$user[$i]->twitterURL;
				$users[$i]->instagramURL	 = ($user[$i]->instagramURL == "")?"":$user[$i]->instagramURL;
				$users[$i]->image_url	 = ($user[$i]->image_url == "")?"":base_url().'assets'.$user[$i]->image_url;
				$users[$i]->points_required	 = ($user[$i]->points_required == "")?"":$user[$i]->points_required;
				$users[$i]->share_mobile	 = ($user[$i]->share_mobile == "0")?"false":"true";
				$users[$i]->location_disclose = $user[$i]->location_disclose;
				$users[$i]->rating = ($user[$i]->rating == "")?"0":$user[$i]->rating;
				$users[$i]->feedback = ($user[$i]->feedback == "")?"":$user[$i]->feedback;
				//$users[$i]->rank = strval(round($user[$i]->social_rank + $user[$i]->rank + $user[$i]->rank_f2));
				$users[$i]->rank = strval($user[$i]->social_rank + $user[$i]->rank);
				$users[$i]->earned_points = (string)$getearnedpoints;
				$users[$i]->last_seen = (string)getLastSeenDataUser($frnduserid);
				unset($user[$i]->social_rank);
				unset($user[$i]->rank_f2);	
			
				$userdetails =(object) array_merge((array)$users[$i],(array)$getuserfriend[$i]);	
				
			}
			return $userdetails;
		}
		else
		{
			$return_res = array();
			$return_res['responsecode'] = "500";
			$return_res['status'] = "Something went wrong. Please try again later.";
			return $return_res;
		}
		exit;
		
	}
	
	//To get the random user Location within 5km radius
	function getUserRandomLocation($lat,$long)
	{
		//genereate number for km up to 5 not greater than 6 with meters of two decimals
		$rand = $this->frand(1,4,2); 
		//converters km to meters
		$mymeters = $rand*1000; 
		//original user latitude
		$fromLatitude = $lat; 
		//original user longitutde
		$fromLongitude = $long; 
		//random 1 to 5 km converted as meters
		$distanceInMetres = $mymeters; 
		$bearing = 0;
		//metres
		$earthMeanRadius = 6371009.0; 
		$destinationLatitude = rad2deg(
		asin(
			sin(deg2rad($fromLatitude)) *
				cos($distanceInMetres / $earthMeanRadius) +
			cos(deg2rad($fromLatitude)) *
				sin($distanceInMetres / $earthMeanRadius) *
				cos(deg2rad($bearing))
		)
		);
		$destinationLongitude = rad2deg(
		deg2rad($fromLongitude) +
		atan2(
			sin(deg2rad($bearing)) *
				sin($distanceInMetres / $earthMeanRadius) *
				cos(deg2rad($fromLatitude)),
			cos($distanceInMetres / $earthMeanRadius) -
				sin(deg2rad($fromLatitude)) * sin(deg2rad($destinationLatitude))
		)
		);
		//echo $destinationLatitude.','.$destinationLongitude, PHP_EOL;
		$lat_long = array();
		$lat_long['lat'] = $destinationLatitude;
		$lat_long['long']= $destinationLongitude;
		return $lat_long;
		
	}
	
	//function to generate the randnumber with decimals
	function frand($min, $max, $decimals = 0) {
	  $scale = pow(10, $decimals);
	  return mt_rand($min * $scale, $max * $scale) / $scale;
	}
	
	//API function to get the users earned points by adding friend
	function get_user_earned_points($userId,$frinedId)
    {
        $this->db->select('earned_points');
        $this->db->from('hiprofile_earned_user_points');
        $this->db->where('userId', $userId);
		$this->db->where('friend_id', $frinedId);
		$this->db->order_by("earned_points", "desc");
        $query = $this->db->get();
        $earnedPoints = $query->result();
		if(!empty($earnedPoints))
		{
			return $earnedPoints[0]->earned_points;
		}
		else
		{
			$earnedPoints = 0;
			return $earnedPoints;
		}
    }
	
	// function to get the friend visible status
	function check_friendvisibleStatus($userID)
	{
		$this->db->select('friendsonlyvisible');
        $this->db->from('hiprofile_users');
        $this->db->where('userId', $userID);
        $this->db->where('roleId', 0);
		$this->db->where('isDeleted', 0);
		$query = $this->db->get();
        $result = $query->result();
		return $result[0]->friendsonlyvisible;
	}
	
	// function to check if the user has with friend
	function check_userfriends($userID,$frinedId)
	{
		$this->db->select('id');
        $this->db->from('hiprofile_friends');
        $this->db->where('userId', $userID);
		$this->db->where('friend_user_id', $frinedId);
        $this->db->where('add_friend', 'true');
		$this->db->where('request_status', '1');
		$query = $this->db->get();
        $result = $query->result();
		return count($result[0]->id);
	}
	
	// function to check if the friend with user
	function check_friendsuser($userID,$frinedId)
	{
		$this->db->select('id');
        $this->db->from('hiprofile_friends');
        $this->db->where('userId', $frinedId);
		$this->db->where('friend_user_id', $userID);
        $this->db->where('add_friend', 'true');
		$this->db->where('request_status', '1');
		$query = $this->db->get();
        $result = $query->result();
		return count($result[0]->id);
	}
	
	// function to check last seen of the user
	function check_userlastseen($userID)
	{
		$this->db->select('userId');
        $this->db->from('hiprofile_user_lastseen');
        $this->db->where('userId', $userID);
		$this->db->order_by("last_seen", "desc");
		$query = $this->db->get();
        $lastseen = $query->result();
		$numRows = $query->num_rows();
		if($numRows >=1)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}