<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cron_model extends CI_Model
{
    /**
     * This function is used to get the notification listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function cronJob()
    {
		$userid = "";
		$friendid = "";		$crtime = $this->get_current_datime();		$current_date_time =  date("Y-m-d H:i", strtotime($crtime)).':00';
		//$current_date_time2  = date("Y-m-d H:i").':00';
		$this->db->select('*');
		$this->db->from('hiprofile_blocked_timeframe_friends');
		$this->db->where('date_format(end_block_current_time,"%Y-%m-%d %h:%i:%s") <', $current_date_time);
		$this->db->where('status',0);
		$query = $this->db->get();
		$blocked_user = $query->result_array();
		$numRows = $query->num_rows();
		if($numRows >= 1)
		{
			foreach($blocked_user as $value)
			{
				if($value['is_friend'] == 0)
				{
					/*update the friend timeframe*/
					$this->db->set('status',1);
					$this->db->where_in('userId',$value['userId']);
					$this->db->where_in('friend_id',$value['friend_id']);
					$this->db->update('hiprofile_blocked_timeframe_friends');
				}
				else
				{
					/*delete the friend timeframe*/
					$this->db->where_in('userId',$value['userId']);
					$this->db->where_in('friend_id',$value['friend_id']);
					$this->db->delete('hiprofile_blocked_timeframe_friends');
				}									
				/*delete the record from the blocked users table*/
				$this->db->where_in('userId',$value['userId']);
				$this->db->where_in('friend_user_id',$value['friend_id']);
				$this->db->delete('hiprofile_blocked_users');
			}
		}
       /*
	   // the message
		$msg = "First line of text\nSecond line of text";

		// use wordwrap() if lines are longer than 70 characters
		$msg = wordwrap($msg,70);

		// send email
		mail("praveen.kumar@indusnet.co.in","My subject",$msg);
		*/
		
    }		/*To fetch the curren time from the database*/	function get_current_datime()	{		$this->db->select('Now() as ctime');		$query = $this->db->get();		$time = $query->result();		return $time[0]->ctime;	}
}
?>