<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Boost_model extends CI_Model
{

    function checkbooststatus()
    {
	   	$crtime = $this->get_current_datime();
		$current_date_time =  date("Y-m-d H:i", strtotime($crtime)).':00';
		$this->db->select('userId,product_id,orderId,category,is_activated,activation_time,expiration_time,ad_type');
		$this->db->from('hiprofile_rank_points_plan_details');
		$this->db->where('expiration_time <', $current_date_time);
		$this->db->where('is_activated',1);
		$query = $this->db->get();
		$activeusers = $query->result_array();
		$numRows = $query->num_rows();
		if($numRows >= 1)
		{
			foreach($activeusers as $value)
			{
				if($value['is_activated'] == 1)
				{
					/*update the friend timeframe*/
					$this->db->set('is_activated',2);
					$this->db->where('userId',$value['userId']);
					$this->db->where('product_id',$value['product_id']);
					$this->db->where('orderId',$value['orderId']);
					$this->db->where('category',$value['category']);
					$this->db->update('hiprofile_rank_points_plan_details');
					if($value['category'] == 'dp' &&  $value['ad_type'] === NULL)
					{
						$currentpoints = $this->getuserpoint($value['userId']);
						$totalpoints = $currentpoints/2;
						$update_data = array('points' => $totalpoints);
						$this->db->where('userId', $value['userId']);
						$this->db->update('hiprofile_user_socialsync', $update_data);
						
					}
					elseif($value['category'] == 'hf')
					{
						$packagedetail = $this->toGetPackageDetails($value['product_id'],$value['category']);
						$rank  = $packagedetail->rank;
						$currentrank = $this->getuserrank($value['userId']);
						$totalrank = $currentrank-$rank;
						$update_data = array('rank' => $totalrank);
						$this->db->where('userId', $value['userId']);
						$this->db->update('hiprofile_user_socialsync', $update_data);
					}
				}
			}
		}
    }
	
	/*To fetch the curren time from the database*/
	function get_current_datime()
	{
		$this->db->select('Now() as ctime');
		$query = $this->db->get();
		$time = $query->result();
		return $time[0]->ctime;
	}
	
	//Function get the rank,hours or points using the category
	function toGetPackageDetails($product_id,$category)
	{
		$this->db->select('
		ranks_points_package_rank_up as rank,
		ranks_points_package_hours as hours
		');
		$this->db->from('hiprofile_rank_point_caetogry_packages_list');
		$this->db->where('ranks_points_package_Google_playId',$product_id);
		$this->db->where('ranks_points_package_category_code',$category);
		$this->db->where('IsDeleted', 0);
		$query = $this->db->get();
		$rankpackageplan = $query->result();
		return $rankpackageplan[0];
	}
	
	//function to get the user current rank
	function getuserrank($userID)
	{
		$this->db->select('rank');
        $this->db->from('hiprofile_user_socialsync');
        $this->db->where('userId', $userID);
        $query = $this->db->get();
		$currentrank = $query->result();
		return $currentrank[0]->rank;
	}
	//function to get the user current points
	function getuserpoint($userID)
	{
		$this->db->select('points');
        $this->db->from('hiprofile_user_socialsync');
        $this->db->where('userId', $userID);
        $query = $this->db->get();
		$currentpoint = $query->result();
		return $currentpoint[0]->points;
	}
}
?>