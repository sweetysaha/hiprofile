<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contentapi_model extends CI_Model
{
	function getContenttrends($contentId)
    {
        $this->db->select('contentTitle as title, contentDesc as data, imageurl as image_url');
        $this->db->from('hiprofile_sub_contents');
        $this->db->where('isDeleted', 0);
        $this->db->where('p_contentid', $contentId);
        $query = $this->db->get();

        return $query->result();
    }
	
	function getContenttips($contentId)
    {
        $this->db->select('contentTitle as title, contentDesc as data, imageurl as image_url ');
        $this->db->from('hiprofile_sub_contents');
        $this->db->where('isDeleted', 0);
        $this->db->where('p_contentid', $contentId);
        $query = $this->db->get();

        return $query->result();
    }
}