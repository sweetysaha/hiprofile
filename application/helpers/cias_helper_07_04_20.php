<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * This function is used to print the content of any data
 */
function pre($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

/**
 * This function used to get the CI instance
 */
if(!function_exists('get_instance'))
{
    function get_instance()
    {
        $CI = &get_instance();
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 */
if(!function_exists('getHashedPassword'))
{
    function getHashedPassword($plainPassword)
    {
        return password_hash($plainPassword, PASSWORD_DEFAULT);
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 * @param {string} $hashedPassword : This is hashed password
 */
if(!function_exists('verifyHashedPassword'))
{
    function verifyHashedPassword($plainPassword, $hashedPassword)
    {
        return password_verify($plainPassword, $hashedPassword) ? true : false;
    }
}

/**
 * This method used to get current browser agent
 */
if(!function_exists('getBrowserAgent'))
{
    function getBrowserAgent()
    {
        $CI = get_instance();
        $CI->load->library('user_agent');

        $agent = '';

        if ($CI->agent->is_browser())
        {
            $agent = $CI->agent->browser().' '.$CI->agent->version();
        }
        else if ($CI->agent->is_robot())
        {
            $agent = $CI->agent->robot();
        }
        else if ($CI->agent->is_mobile())
        {
            $agent = $CI->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }

        return $agent;
    }
}

/*if(!function_exists('setProtocol'))
{
    function setProtocol()
    {
        $CI = &get_instance();
                    
        $CI->load->library('email');
        
        $config['protocol'] = PROTOCOL;
        $config['mailpath'] = MAIL_PATH;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $CI->email->initialize($config);
        
        return $CI;
    }
}*/

/*
if(!function_exists('emailConfig'))
{
    function emailConfig()
    {
        $CI->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['mailpath'] = MAIL_PATH;
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
    }
}
*/

if(!function_exists('resetPasswordEmail'))
{
    function resetPasswordEmail($detail)
    {
        $data["data"] = $detail;
		/* 
		pre($detail);
        die; 
		*/
		$config['useragent'] = "CodeIgniter";
		$config['protocol'] = 'smtp';
		$config['_smtp_auth']   = TRUE;
		$config['smtp_host'] = 'email-smtp.eu-west-1.amazonaws.com';
		$config['smtp_port'] = '587';
		$config['smtp_user'] = 'AKIAIVOX4CAJJASO2FMA';
		$config['smtp_pass'] = 'AkZIiaGSk85ypMCsI8vGETe7ChDLPQ1ikOrZUlpSRRU7';
		$config['newline'] = "\r\n"; 
		$config['smtp_crypto'] = 'tls'; 
		$config['priority'] = 5;
		$config['charset'] = 'UTF-8';
		$config['mailtype'] = "html";
		
        $CI = get_instance(); 
		$CI->load->library('email');
		$CI->email->initialize($config);
        $CI->email->from(EMAIL_FROM, FROM_NAME);
		$CI->email->reply_to(REPLY_TO, REPLY_NAME); 
        $CI->email->subject("Reset Password");
        $CI->email->message($CI->load->view('email/resetPassword', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
         
        return $status;
    }
}

if(!function_exists('verifyEmail'))
{
    function verifyEmail($detail)
    {
        $data["data"] = $detail;
		
		/*  pre($data);
        die;  
		 */
				
		$config['useragent'] = "CodeIgniter";
		$config['protocol'] = 'smtp';
		$config['_smtp_auth']   = TRUE;
		$config['smtp_host'] = 'email-smtp.eu-west-1.amazonaws.com';
		$config['smtp_port'] = '587';
		$config['smtp_user'] = 'AKIAIVOX4CAJJASO2FMA';
		$config['smtp_pass'] = 'AkZIiaGSk85ypMCsI8vGETe7ChDLPQ1ikOrZUlpSRRU7';
		$config['newline'] = "\r\n"; 
		$config['smtp_crypto'] = 'tls'; 
		$config['priority'] = 5;
		$config['charset'] = 'UTF-8';
		$config['mailtype'] = "html";
		
        $CI = get_instance(); 
		$CI->load->library('email');
		$CI->email->initialize($config);
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        if(trim(REPLY_TO) != "" && trim(REPLY_NAME) != "")
        {
        	$CI->email->reply_to(REPLY_TO, REPLY_NAME); 
        }
		$CI->email->subject("Email Verification");
		//$CI->email->message("Dear User,\nPlease click on below URL or paste into your browser to verify your Email Address\n\n".$detail['reset_link']."\n"."\n\nThanks\nHiProfile Admin Team");
		$CI->email->message($CI->load->view('email/verificationemail', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
        //echo $CI->email->print_debugger();
       return $status;
    }
}

if(!function_exists('setFlashData'))
{
    function setFlashData($status, $flashMsg)
    {
        $CI = get_instance();
        $CI->session->set_flashdata($status, $flashMsg);
    }
}

/**
 * function used for friend request received push notification
 */
if (!function_exists('friendRequestReceived')) {
	
	function friendRequestReceived($data) 
	{
	
		//defined a new constant for firebase api key
		define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');
				
		$ci = & get_instance();
        $ci->db->select('userId as friend_user_id,notification_type,friend_pic_url,friend_name,friend_gender,notification_type,FCMtoken');
        $ci->db->where('userId', $data['userID']);
		$ci->db->where('friend_user_id', $data['friend_user_id']);
		$ci->db->where('notification_type', strtolower('friend_request_received'));
        $ci->db->from('hiprofile_notification_list');
        $query = $ci->db->get();
        $result = $query->row_array();
		if(!empty($result))
		{
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			$url = 'https://fcm.googleapis.com/fcm/send';
			$devicetoken = $result['FCMtoken'];		
			unset($result['FCMtoken']);	
			unset($result['status']);			
			$fields = array();
			$fields['to'] = $devicetoken;
			$fields['data']= $result;
			//header with content_type api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization: key=' . FIREBASE_API_KEY,
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			$result = curl_exec($ch);
			curl_close ($ch);
		}
	}
}

/**
 * function used for friend request accept push notification
 */
if (!function_exists('friendRequestAccept')) {
	
	function friendRequestAccept($data) {

		//defined a new constant for firebase api key
		define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');

		$ci = & get_instance();
        $ci->db->select('userId as friend_user_id,notification_type,friend_pic_url,friend_name,friend_gender,status,FCMtoken');
        $ci->db->where('userId', $data['userID']);
		$ci->db->where('friend_user_id', $data['friend_user_id']);
		$ci->db->where('notification_type', strtolower('friend_request_approved'));
		$ci->db->where('status', '1');
        $ci->db->from('hiprofile_notification_list');
        $query = $ci->db->get();
        $result = $query->row_array();
		if(!empty($result))
		{
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			$result['notification_type'] = ($result['status'] == "1") ? "friend_request_approved" : "";
			$url = 'https://fcm.googleapis.com/fcm/send';
			$devicetoken = $result['FCMtoken'];	
			$result['status'] = ($result['status'] == "1") ? "accepted" : "";		
			unset($result['FCMtoken']);	
			//unset($result['status']);	
			$fields = array();
			$fields['to'] = $devicetoken;
			$fields['data']= $result;
			//header with content_type api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization: key=' . FIREBASE_API_KEY,
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			$result = curl_exec($ch);
			curl_close ($ch);
		}
	}
}


/**
 * function used for friend request cancel push notification
 */
if (!function_exists('friendRequestCancel')) {
	
	function friendRequestCancel($data) {
		//defined a new constant for firebase api key
		define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');

		$ci = & get_instance();
        $ci->db->select('userId as friend_user_id,notification_type,friend_pic_url,friend_name,friend_gender,status,FCMtoken');
        $ci->db->where('userId', $data['userID']);
		$ci->db->where('friend_user_id', $data['friend_user_id']);
		$ci->db->where('notification_type', strtolower('friend_request_rejected'));
		$ci->db->where('status', '2');
        $ci->db->from('hiprofile_notification_list');
        $query = $ci->db->get();
        $result = $query->row_array();
		if(!empty($result))
		{
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			$result['notification_type'] = ($result['status'] == "2") ? "friend_request_rejected" : "";
			$url = 'https://fcm.googleapis.com/fcm/send';
			$devicetoken = $result['FCMtoken'];		
			$result['status'] = ($result['status'] == "2") ? "rejected" : "";
			unset($result['FCMtoken']);	
			//unset($result['status']);	
			$fields = array();
			$fields['to'] = $devicetoken;
			$fields['data']= $result;
			//header with content_type api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization: key=' . FIREBASE_API_KEY,
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			$result = curl_exec($ch);
			curl_close ($ch);
		}
	}
}


/**
 * function used for send social media friend request push notification
 */
if (!function_exists('sendSocialFriendRequest')) {
	
	function sendSocialFriendRequest($data) {

		//defined a new constant for firebase api key
		define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');

		$ci = & get_instance();
        $ci->db->select('userId as friend_user_id,notification_type,friend_pic_url,friend_name,friend_gender,social_media,FCMtoken');
        $ci->db->where('userId', $data['userID']);
		$ci->db->where('friend_user_id', $data['friend_user_id']);
		$ci->db->where('social_media', strtolower($data['social_media']));
        $ci->db->from('hiprofile_notification_list');
        $query = $ci->db->get();
        $result = $query->row_array();
		if(!empty($result))
		{
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			$url = 'https://fcm.googleapis.com/fcm/send';
			$devicetoken = $result['FCMtoken'];		
			unset($result['FCMtoken']);			
			$fields = array();
			$fields['to'] = $devicetoken;
			$fields['data']= $result;
			//header with content_type api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization: key=' . FIREBASE_API_KEY,
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			$result = curl_exec($ch);
			curl_close ($ch);
		}
	}
}


/**
 * function used for send social media friend request push notification
 */
if (!function_exists('sendSocialFriendRequestAccept')) {
	
	function sendSocialFriendRequestAccept($data) {

		//defined a new constant for firebase api key
		define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');

		$ci = & get_instance();
        $ci->db->select('userId as friend_user_id,notification_type,friend_pic_url,friend_name,friend_gender,social_media,notification_type,status,FCMtoken');
        $ci->db->where('userId', $data['userID']);
		$ci->db->where('friend_user_id', $data['friend_user_id']);
		$ci->db->where('notification_type', strtolower('social_media_request_approved'));
		$ci->db->where('social_media', strtolower($data['social_media']));
		$ci->db->where('status', '1');
        $ci->db->from('hiprofile_notification_list');
        $query = $ci->db->get();
        $result = $query->row_array();
		if(!empty($result))
		{
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			$result['status'] = ($result['status'] == "1") ? "accepted" : "";
			$url = 'https://fcm.googleapis.com/fcm/send';
			$devicetoken = $result['FCMtoken'];		
			unset($result['FCMtoken']);	
			$fields = array();
			$fields['to'] = $devicetoken;
			$fields['data']= $result;
			//header with content_type api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization: key=' . FIREBASE_API_KEY,
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			$result = curl_exec($ch);
			curl_close ($ch);
		}
	}
}


/**
 * function used for send social media friend request push notification
 */
if (!function_exists('sendSocialFriendRequestCancel')) {
	
	function sendSocialFriendRequestCancel($data) {
		
		//defined a new constant for firebase api key
		define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');

		$ci = & get_instance();
        $ci->db->select('userId as friend_user_id,notification_type,friend_pic_url,friend_name,friend_gender,social_media,notification_type,status,FCMtoken');
        $ci->db->where('userId', $data['userID']);
		$ci->db->where('friend_user_id', $data['friend_user_id']);
		$ci->db->where('notification_type', strtolower('social_media_request_rejected'));
		$ci->db->where('social_media', strtolower($data['social_media']));
		$ci->db->where('status', '2');
        $ci->db->from('hiprofile_notification_list');
        $query = $ci->db->get();
        $result = $query->row_array();
		if(!empty($result))
		{
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			$result['status'] = ($result['status'] == "2") ? "rejected" : "";
			$url = 'https://fcm.googleapis.com/fcm/send';
			$devicetoken = $result['FCMtoken'];		
			unset($result['FCMtoken']);	
			$fields = array();
			$fields['to'] = $devicetoken;
			$fields['data']= $result;
			//header with content_type api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization: key=' . FIREBASE_API_KEY,
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			$result = curl_exec($ch);
			curl_close ($ch);
		}
	}
}


/**
 * function used for send rank upgrade push notification
 */
if (!function_exists('sendrankpush')) {
	
	function sendrankpush($data) 
	{		
		
		//defined a new constant for firebase api key
		define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');
				
		$ci = & get_instance();
        $ci->db->select('BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.device_token as FCMtoken,social.profileimage as friend_pic_url,social.social_rank,social.rank,social.rank_f2');
		$ci->db->from('hiprofile_users as BaseTbl');
		$ci->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
        $ci->db->where('BaseTbl.userId', $data['userID']);
        $query = $ci->db->get();
        $result = $query->row_array();		
		if(!empty($result))
		{
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			$url = 'https://fcm.googleapis.com/fcm/send';
			$devicetoken = $result['FCMtoken'];		
			$result['new_rank'] = strval(round($result['social_rank'] + $result['rank'] + $result['rank_f2']));
			$result['notification_type'] = "rank_up";
			$result['status'] = "";
			unset($result['FCMtoken']);	
			unset($result['social_rank']);	
			unset($result['rank']);	
			unset($result['rank_f2']);		
			$fields = array();
			$fields['to'] = $devicetoken;
			$fields['data']= $result;
			//header with content_type api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization: key=' . FIREBASE_API_KEY,
			);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			$result = curl_exec($ch);
			curl_close ($ch);
		}
	}
}

/*IOS PUSH NOTIFICATION START*/

if (!function_exists('iossendrankpush')) {
	
	function iossendrankpush($data) 
	{		
		$ci = & get_instance();
        $ci->db->select('BaseTbl.userId as friend_user_id,BaseTbl.name as friend_name,BaseTbl.gender as friend_gender,BaseTbl.device_token as FCMtoken,social.profileimage as friend_pic_url,social.social_rank,social.rank,social.rank_f2');
		$ci->db->from('hiprofile_users as BaseTbl');
		$ci->db->join('hiprofile_user_socialsync as social','social.userId = BaseTbl.userId');
        $ci->db->where('BaseTbl.userId', $data['userID']);
        $query = $ci->db->get();
        $result = $query->row_array();		
		if(!empty($result))
		{	
			//defined a new constant for IOS private key
			$passphrase = 'admin';
			
			// Create the payload body
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			
			$devicetoken = $result['FCMtoken'];		
			$result['notification_type'] = "rank_up";
			$result['new_rank'] = strval(round($result['social_rank'] + $result['rank'] + $result['rank_f2']));
			
			//$result['status'] = "";
			$rank = strval(round($result['social_rank'] + $result['rank'] + $result['rank_f2']));	
				
			unset($result['FCMtoken']);	
			unset($result['social_rank']);	
			unset($result['rank']);	
			unset($result['rank_f2']);	
			unset($result['friend_pic_url']);
			unset($result['friend_name']);
			unset($result['friend_gender']);			

			$message = 'Your ranked up. Your new rank is '.$rank;
			$badge = 1;
			$sound = 'default';
			
			$payload = array();
			$payload['aps'] = array('alert' => $message, 'badge' => intval($badge), 'sound' => $sound,'category'=> 'rankUp','mutable-content'=> intval(1));
			$payload['payload'] = $result;
			
			// Encode the payload as JSON
			$payload = json_encode($payload);
			
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', trim($devicetoken)) . pack('n', strlen($payload)) . $payload;
			
			$apnsHost = 'gateway.push.apple.com';
			//$apnsHost = 'gateway.sandbox.push.apple.com';
			$apnsPort = 2195;
			
			// .pem is your certificate file
			$apns_cert = APPPATH.'../assets/IOS/HiProfile_Dis_Push.pem'; 
			//$apns_cert = APPPATH.'../assets/IOS/HiProfile_Dev_Push.pem'; 
			
			$streamContext  = stream_context_create();
			stream_context_set_option($streamContext , 'ssl', 'local_cert', $apns_cert);
			stream_context_set_option($streamContext , 'ssl', 'passphrase', $passphrase);
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
			/*if (!$fp){
				exit("Failed to connect: $error $errorString" . PHP_EOL);
			}*/
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			return $result;
			// Close the connection to the server
			socket_close($fp);
			fclose($fp);
		}
	}
}

if (!function_exists('iosfriendRequestReceived')) {
	
	function iosfriendRequestReceived($data) 
	{
		
		$ci = & get_instance();
        $ci->db->select('userId as friend_user_id,notification_type,friend_pic_url,friend_name,friend_gender,notification_type,FCMtoken');
        $ci->db->where('userId', $data['userID']);
		$ci->db->where('friend_user_id', $data['friend_user_id']);
		$ci->db->where('notification_type', strtolower('friend_request_received'));
        $ci->db->from('hiprofile_notification_list');
        $query = $ci->db->get();
        $result = $query->row_array();
		if(!empty($result))
		{
			//defined a new constant for IOS private key
			$passphrase = 'admin';
			
			// Create the payload body
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			$devicetoken = $result['FCMtoken'];		
			unset($result['FCMtoken']);	
			unset($result['status']);
			unset($result['friend_pic_url']);
		

			$message = $result['friend_name'].' has sent you Friend Request';
			$badge = 1;
			$sound = 'default';
			
			$payload = array();
			$payload['aps'] = array('alert' => $message, 'badge' => intval($badge), 'sound' => $sound,'category'=> 'friendRequestReceived','mutable-content'=> intval(1));
			$payload['payload'] = $result;

			// Encode the payload as JSON
			$payload = json_encode($payload);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', trim($devicetoken)) . pack('n', strlen($payload)) . $payload;
			
			$apnsHost = 'gateway.push.apple.com';
			//$apnsHost = 'gateway.sandbox.push.apple.com';
			$apnsPort = 2195;
			
			// .pem is your certificate file
			$apns_cert = APPPATH.'../assets/IOS/HiProfile_Dis_Push.pem'; 
			//$apns_cert = APPPATH.'../assets/IOS/HiProfile_Dev_Push.pem'; 
			
			$streamContext  = stream_context_create();
			stream_context_set_option($streamContext , 'ssl', 'local_cert', $apns_cert);
			stream_context_set_option($streamContext , 'ssl', 'passphrase', $passphrase);
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
			/*if (!$fp){
				exit("Failed to connect: $error $errorString" . PHP_EOL);
			}*/
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			return $result;
			// Close the connection to the server
			socket_close($fp);
			fclose($fp);
		}
	}
}

/**
 * function used for friend request accept push notification
 */
if (!function_exists('iosfriendRequestAccept')) {
	
	function iosfriendRequestAccept($data) {

		//defined a new constant for firebase api key
		define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');

		$ci = & get_instance();
        $ci->db->select('userId as friend_user_id,notification_type,friend_pic_url,friend_name,friend_gender,status,FCMtoken');
        $ci->db->where('userId', $data['userID']);
		$ci->db->where('friend_user_id', $data['friend_user_id']);
		$ci->db->where('notification_type', strtolower('friend_request_approved'));
		$ci->db->where('status', '1');
        $ci->db->from('hiprofile_notification_list');
        $query = $ci->db->get();
        $result = $query->row_array();
		if(!empty($result))
		{
			//defined a new constant for IOS private key
			$passphrase = 'admin';
			
			// Create the payload body
			$result['friend_pic_url'] = ($result['friend_pic_url'] == "") ? "" : base_url().'assets'.$result['friend_pic_url'];
			$result['notification_type'] = ($result['status'] == "1") ? "friend_request_approved" : "";
			$devicetoken = $result['FCMtoken'];	
			$result['status'] = ($result['status'] == "1") ? "accepted" : "";		
			unset($result['FCMtoken']);	
			unset($result['status']);	
			unset($result['friend_pic_url']);
			unset($result['friend_gender']);
			
			$message = $result['friend_name'].' accepted your Friend Request';
			$badge = 1;
			$sound = 'default';
			
			$payload = array();
			$payload['aps'] = array('alert' => $message, 'badge' => intval($badge), 'sound' => $sound,'category'=> 'friendRequestApproved','mutable-content'=> intval(1));
			$payload['payload'] = $result;

			// Encode the payload as JSON
			$payload = json_encode($payload);
			
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', trim($devicetoken)) . pack('n', strlen($payload)) . $payload;
			
			$apnsHost = 'gateway.push.apple.com';
			//$apnsHost = 'gateway.sandbox.push.apple.com';
			$apnsPort = 2195;
			
			// .pem is your certificate file
			$apns_cert = APPPATH.'../assets/IOS/HiProfile_Dis_Push.pem';
			//$apns_cert = APPPATH.'../assets/IOS/HiProfile_Dev_Push.pem'; 			
			
			$streamContext  = stream_context_create();
			stream_context_set_option($streamContext , 'ssl', 'local_cert', $apns_cert);
			stream_context_set_option($streamContext , 'ssl', 'passphrase', $passphrase);
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
			/*if (!$fp){
				exit("Failed to connect: $error $errorString" . PHP_EOL);
			}*/
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			return $result;
			// Close the connection to the server
			socket_close($fp);
			fclose($fp);
			
		}
	}
}
/*IOS PUSH NOTIFICATION END*/


/**
 * function used for get users last activity data
 */
if (!function_exists('getLastSeenDataUser')) {
	
	function getLastSeenDataUser($userId)
	{
		$ci = & get_instance();
        $ci->db->select('last_seen');
		$ci->db->from('hiprofile_user_lastseen');
        $ci->db->where('userId',$userId);
		$query = $ci->db->get();
		$numRows = $query->num_rows();
		$getResultQuery = $query->result();
		//Check user active or not
		
		if($numRows>0) 
		{ 
			return $getResultQuery[0]->last_seen;
		}
		else
		{
			return '';
		}
	}
}

/**
 * function to the get range
 */
if (!function_exists('in_range')) {	
	/**
	 * Determines if $number is between $min and $max
	 *
	 * @param  integer  $number     The number to test
	 * @param  integer  $min        The minimum value in the range
	 * @param  integer  $max        The maximum value in the range
	 * @param  boolean  $inclusive  Whether the range should be inclusive or not
	 * @return boolean              Whether the number was in the range
	 */
	function in_range($number, $min, $max, $inclusive = FALSE)
	{
		if (is_int($number) && is_int($min) && is_int($max))
		{
			return $inclusive
				? ($number >= $min && $number <= $max)
				: ($number > $min && $number < $max) ;
		}

		return FALSE;
	}
}

if (!function_exists('encryptCookie')) {	

	function encryptCookie($data, $key) 
	{
		// Remove the base64 encoding from our key
		$encryption_key = base64_decode($key);
		// Generate an initialization vector
		$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
		// Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
		$encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
		// The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
		return base64_encode($encrypted . '::' . $iv);
	}
 
}
if (!function_exists('decryptCookie')) {

	function decryptCookie($data, $key) 
	{
		// Remove the base64 encoding from our key
		$encryption_key = base64_decode($key);
		// To decrypt, split the encrypted data from our IV - our unique separator used was "::"
		list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
		return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
	}
}


if (!function_exists('number_format_short')) {

        function number_format_short($n, $precision = 1) {
            if ($n < 1000) {
                // 0 - 900
                $n_format = number_format($n, $precision);
                $suffix = '';
            } else if ($n < 1000000) {
                // 0.9k-850k
                $n_format = number_format($n / 1000, $precision);
                $suffix = 'K';
            } else if ($n < 1000000000) {
                // 0.9m-850m
                $n_format = number_format($n / 1000000, $precision);
                $suffix = 'M';
            } else if ($n < 1000000000000) {
                // 0.9b-850b
                $n_format = number_format($n / 1000000000, $precision);
                $suffix = 'B';
            } else {
                // 0.9t+
                $n_format = number_format($n / 1000000000000, $precision);
                $suffix = 'T';
            }
            // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
            // Intentionally does not affect partials, eg "1.50" -> "1.50"
            if ($precision > 0) {
                $dotzero = '.' . str_repeat('0', $precision);
                $n_format = str_replace($dotzero, '', $n_format);
            }
            return $n_format . $suffix;
        }

    }

?>