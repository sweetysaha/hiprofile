<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class : Login (LoginController)
 * Notification class to control to all the contents.
 */
require APPPATH . '/libraries/BaseController.php';

class Cron extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('cron_model');
    }
    /**
     * This function used to load the first screen of the content
     */
    public function index()
    {
		
      $this->cron_model->cronJob();
	  //$this->loadViews("category", $this->global, $data, NULL);
    }

}

?>