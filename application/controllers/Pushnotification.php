<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class : Login (LoginController)
 * Notification class to control to all the contents.
 */
require APPPATH . '/libraries/BaseController.php';

class pushnotification extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
		$this->load->helper('upload','date', 'url','email','path'); 
    }
    /**
     * This function used to load the first screen of the content
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Hiprofile : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the Notification list
     */
    function pushNotification()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$this->global['pageTitle'] = 'HiProfile : Push Notification';
            $this->loadViews("pushnotification",$this->global,NULL, NULL);
        }
    }
	
	function sendfcmnotification()
	{
		define('FIREBASE_API_KEY', 'AAAArRD7_eo:APA91bFKiE7vrZ9mv_edcTSJ5gIZlRmPF86bqkSe1AC-uf1Ja8ZPqSD8JZ5A-3H_F696-ytym-Az0qSirSrTJzfkpumR-wjbVEJlN0agpqYMT1n8fO8aB9hks_aVFUprWPRezrz3nypz');
		$url = 'https://fcm.googleapis.com/fcm/send';
		$devicetype = $this->input->post('devicetype');
		$devicetoken = $this->input->post('devicetoken');
		$fcmjson = $this->input->post('fcmjson');
		
		$fields = array();
		$fields['to'] = $devicetoken;
		$fields['data']= json_decode($fcmjson, TRUE);
		//header with content_type api key
		$headers = array(
			'Content-Type:application/json',
			'Authorization: key=' . FIREBASE_API_KEY,
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		
		$result = curl_exec($ch);
		if ($result === FALSE) {
			//die('FCM Send Error: ' . curl_error($ch));
			$this->session->set_flashdata('error', 'Notification sent failed curl, Error messgae from curl:'.curl_error($ch));
		}
		else
		{
			$this->session->set_flashdata('success', 'Notification sent successfully');
		}
		curl_close ($ch);
		redirect('pushNotification');
	} 
	
	/*function sendfcmnotification()
	{
		define('FIREBASE_API_KEY', 'AAAAlfyajRo:APA91bHyFN8YUofxoGrlv7Gcsp-6Q0liB4Rmc_wNlNEkKlBomBRnJRMvVUqYKM_24KlAvEusAqGKbHYkcX12_MwJd3uiB3dZacep_KmM4qw1gYqRYb37GGcVilggJDE_mDzrdCbXcybU');
		$devicetype = $this->input->post('devicetype');
		$devicetoken = $this->input->post('devicetoken');
		$fcmjson = $this->input->post('fcmjson');

		 $url = 'https://fcm.googleapis.com/fcm/send';
			 $msg = array
			  (
			'body' 	=> $fcmjson,
			'title' => $devicetoken
			);
			$fields = array
			(
				'to'		=> $devicetoken,
				'notification'	=> $msg
			);
			//$fields = json_encode ( $fields );

			$headers = array (
					'Authorization: key=' . FIREBASE_API_KEY,
					'Content-Type: application/json'
			);

			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );

			$result = curl_exec ( $ch );
			if($result == true)
			{
				$this->session->set_flashdata('success', 'Notification sent successfully');
			}
			else
			{
				$this->session->set_flashdata('error', 'Notification sent failed');
			}
			curl_close ( $ch );
	
			redirect('pushNotification');
	}*/
	

}

?>