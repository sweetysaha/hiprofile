<?php if(!defined('BASEPATH')) exit('No direct script access allowed');



require APPPATH . '/libraries/BaseController.php';



/**

 * Class : Rank (RankController)

 * Rank Class to control all Rank related operations.

 */

class Rank extends BaseController

{

    /**

     * This is default constructor of the class

     */

    public function __construct()

    {

        parent::__construct();

        $this->load->model('rank_model');

        $this->isLoggedIn();   

    }

    

    /**

     * This function used to load the first screen of the user

     */

    public function index()

    {

        $this->global['pageTitle'] = 'HiProfile : Dashboard';

        

        $this->loadViews("dashboard", $this->global, NULL , NULL);

    }

    

    /**

     * This function is used to load the Rank list

     */

	function rankListing()

    {



        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

			

            $this->global['pageTitle'] = 'HiProfile : Rank Upgrade Plan Management';



            $this->loadViews("ranks", $this->global, NULL, NULL);

        }

    }

	

	function rankplanrecords()

    {

            $this->load->model('rank_model');

			// Datatables Variables

			$draw = intval($this->input->get("draw"));

			$start = intval($this->input->get("start"));

			$length = intval($this->input->get("length"));





			$users = $this->rank_model->get_rankListing();



			$data = array();



			foreach($users->result() as $r) {

			$status ='';

			if($r->is_activated == 0){ $status = "<span style='color: #2315ff;'>Purchased</span>";} 

			elseif($r->is_activated == 1){ $status = "<span style='color: #27a000;'>Activated</span>";} 

			elseif($r->is_activated == 2){ $status =  "<span style='color: #ff0b0b;'>Expired</span>";}

			$data[] = array(

			$r->slno = count($r->name),

			$r->name,

			$r->userId,

			$r->product_title,

			$r->product_description,

			$r->category = ($r->category == 'hf')?'Higher Fame':'Double Points',

			$r->is_activated = $status,

			$r->Id = '<a class="btn btn-sm btn-success" title="VIEW" href="'.$r->Id.'" id="viewlink" onclick="doview('.$r->Id.'); return false;"><i class="fa fa-eye"></i></a>'

			);

			}

			$output = array(

			"draw" => $draw,

			"recordsTotal" => $users->num_rows(),

			"recordsFiltered" => $users->num_rows(),

			"data" => $data

			);

			echo json_encode($output);

			exit();

    }

	

	/*To view the plan purchased*/

	function planView()

	{

		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')

		{

			$id = $_POST['id'];

			$data['planInfo'] = $this->rank_model->getPlanInfo($id);

			$html = $this->load->view("viewPlan",$data,true);	

			echo $html;

		}

		else

		{

			redirect('https://www.google.com');

		}

	}

	

	/*To view all the fame ranks plans*/

	function fameRank()

	{

		if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

			

            $this->global['pageTitle'] = 'HiProfile : Manage Rank Plans';



            $this->loadViews("famerankplans", $this->global, NULL, NULL);

        }

	}

	

	/*To fetch all the records rank plans*/

	function hprecords()

    {			

			// Datatables Variables

			$draw = intval($this->input->get("draw"));

			$start = intval($this->input->get("start"));

			$length = intval($this->input->get("length"));





			$rankplan = $this->rank_model->get_rankplansListing();

			$data = array();



			foreach($rankplan->result() as $r) {

			

			$data[] = array(

			$r->slno = count($r->id),

			$r->googleid,

			$r->ranks,

			$r->hours,

			$r->price = '$'.$r->price,

			$r->category = ($r->category == 'hf')?'Higher Fame':'',

			$r->id = '<a class="btn btn-sm btn-info" title="EDIT" href="'.base_url()."editrankfameplan/".$r->id.'"><i class="fa fa-pencil"></i></a> <a class="btn btn-sm btn-danger deleterankplan" title="DELETE" href="javascript:void(0);" data-id="'.$r->id.'"><i class="fa fa-trash"></i></a>'

			);

			}

			$output = array(

			"draw" => $draw,

			"recordsTotal" => $rankplan->num_rows(),

			"recordsFiltered" => $rankplan->num_rows(),

			"data" => $data

			);

			echo json_encode($output);

			exit();

    }


    


    /*To view all the fame ranks lists*/

    function fameRankList()

    {

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            

            $this->global['pageTitle'] = 'HiProfile : Manage Fame Rank';



            $this->loadViews("famerank", $this->global, NULL, NULL);

        }

    }


    /*To fetch all the records fame rank list*/

    function fmrecords()

    {           

            // Datatables Variables

            $draw = intval($this->input->get("draw"));

            $start = intval($this->input->get("start"));

            $length = intval($this->input->get("length"));





            $rankplan = $this->rank_model->get_fameRankListing();

            $data = array();



            foreach($rankplan->result() as $r) {

            

            $data[] = array(

            $r->slno = count($r->Id),

            $r->name,

            $r->product_title,

            $r->orderId,

            $r->localised_price = $r->localised_price,

            $r->store,

            $r->created_date

            );

            }

            $output = array(

            "draw" => $draw,

            "recordsTotal" => $rankplan->num_rows(),

            "recordsFiltered" => $rankplan->num_rows(),

            "data" => $data

            );

            echo json_encode($output);

            exit();

    }

	

	/*To view all the Double Points plans*/

	function doublePoints()

	{

		if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

			

            $this->global['pageTitle'] = 'HiProfile : Manage Double Point Plans';



            $this->loadViews("pointsplans", $this->global, NULL, NULL);

        }

	}

	

	/*To fetch all the records of the double points*/

	function dprecords()

    {

			// Datatables Variables

			$draw = intval($this->input->get("draw"));

			$start = intval($this->input->get("start"));

			$length = intval($this->input->get("length"));





			$pointplans = $this->rank_model->get_pointsplanListing();

			$data = array();



			foreach($pointplans->result() as $r) {

			

			$data[] = array(

			$r->slno = count($r->id),

			$r->googleid,

			$r->hours,

			$r->price = '$'.$r->price,

			$r->category = ($r->category == 'dp')?'Double Points':'',

			$r->id = '<a class="btn btn-sm btn-info" title="EDIT" href="'.base_url()."editdoublepointplan/".$r->id.'"><i class="fa fa-pencil"></i></a> <a class="btn btn-sm btn-danger deletedoublepointplan" title="DELETE" href="javascript:void(0);" data-id="'.$r->id.'"><i class="fa fa-trash"></i></a>'

			);

			}

			$output = array(

			"draw" => $draw,

			"recordsTotal" => $pointplans->num_rows(),

			"recordsFiltered" => $pointplans->num_rows(),

			"data" => $data

			);

			echo json_encode($output);

			exit();

    }

	

	/**

     * This function is used to delete the rank plan

     * @return boolean $result : TRUE / FALSE

     */

    function deleteFameRankPlan()

    {

        if($this->isAdmin() == TRUE)

        {

            echo(json_encode(array('status'=>'access')));

        }

        else

        {

            $id = $this->input->post('id');

            $rankInfo = array('isDeleted'=>1);

            

            $result = $this->rank_model->deleteFameRankPlan($id, $rankInfo);

            

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }

            else { echo(json_encode(array('status'=>FALSE))); }

        }

    }

	

	/**

     * This function is used to delete the points plan

     * @return boolean $result : TRUE / FALSE

     */

    function deletedoublepointsPlan()

    {

        if($this->isAdmin() == TRUE)

        {

            echo(json_encode(array('status'=>'access')));

        }

        else

        {

            $id = $this->input->post('id');

            $pointInfo = array('IsDeleted'=>1);

            

            $result = $this->rank_model->deleteDoublePointsPlan($id, $pointInfo);

            

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }

            else { echo(json_encode(array('status'=>FALSE))); }

        }

    }

    /*To display an new form to add the rank plan*/

	function addNewRankPlan()

    {

	

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->global['pageTitle'] = 'HiProfile : Manage Double Point Plans';



            $this->loadViews("newrankplan", $this->global, NULL, NULL);

        }

    }

	

	/*To display an new form to add the point plan*/

	function addNewPointPlan()

    {

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->global['pageTitle'] = 'HiProfile : Manage Double Point Plans';



            $this->loadViews("newpointplan", $this->global, NULL, NULL);

        }

    }

	

	/*Function to save the rank plan*/

	function saveRankPlan()

	{

		if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->load->library('form_validation');

            

            $this->form_validation->set_rules('googleid','Google Id','trim|required|max_length[128]|xss_clean');

			$this->form_validation->set_rules('rank','Rank','trim|required|max_length[128]|xss_clean');

            $this->form_validation->set_rules('price','Price','required|numeric|xss_clean');

			$this->form_validation->set_rules('category','Category','required|xss_clean');

			$this->form_validation->set_rules('times','Time','required|xss_clean');

			

            if($this->form_validation->run() == FALSE)

            {

                $this->addNewRankPlan();

            }

            else

            {

				$googleid = $this->input->post('googleid');

                $rank = $this->input->post('rank');

				$boosttime = $this->input->post('boosttime');

				$times = $this->input->post('times');

				$price = $this->input->post('price');

			    $category = 'hf';

				$totaltime = $boosttime.$times;

				

                $rankinfo = array('ranks_points_package_Google_playId'=>$googleid,'ranks_points_package_rank_up'=>$rank, 'ranks_points_package_hours'=>$totaltime, 'ranks_points_package_price'=>$price,'ranks_points_package_category_code'=>$category);



                $result = $this->rank_model->addnewrankplan($rankinfo);

                

                if($result > 0)

                {

                    $this->session->set_flashdata('success', 'New Rank Plan created successfully');

                }

                else

                {

                    $this->session->set_flashdata('error', 'Rank Plan creation failed');

                }

                

                redirect('famerank');

            }

        }

	}

	

	/*Function to save point plan*/

	function savePointPlan()

	{

		if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->load->library('form_validation');

            

            $this->form_validation->set_rules('googleid','Google Id','trim|required|max_length[128]|xss_clean');

            $this->form_validation->set_rules('price','Price','required|numeric|xss_clean');

			$this->form_validation->set_rules('category','Category','required|xss_clean');

			$this->form_validation->set_rules('times','Time','required|xss_clean');

			

            if($this->form_validation->run() == FALSE)

            {

                $this->addNewPointPlan();

            }

            else

            {

				$googleid = $this->input->post('googleid');

				$boosttime = $this->input->post('boosttime');

				$times = $this->input->post('times');

				$price = $this->input->post('price');

			    $category = 'dp';

				$totaltime = $boosttime.$times;

				

                $pointinfo = array('ranks_points_package_Google_playId'=>$googleid,'ranks_points_package_rank_up'=>0, 'ranks_points_package_hours'=>$totaltime, 'ranks_points_package_price'=>$price,'ranks_points_package_category_code'=>$category);



                $result = $this->rank_model->addnewpointplan($pointinfo);

                

                if($result > 0)

                {

                    $this->session->set_flashdata('success', 'New DOuble Point Plan created successfully');

                }

                else

                {

                    $this->session->set_flashdata('error', 'Double Point Plan creation failed');

                }

                

                redirect('doublepoint');

            }

        }

	}

	

	/*To edit and save the edited Rank plan details*/

	function editRankPlan($id = NULL)

    {



        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            if($id == null)

            {

                redirect('famerank');

            }

			

            $category = 'hf';

            $data['rankinfo'] = $this->rank_model->getplansInfo($id,$category);

            

            $this->global['pageTitle'] = 'HiProfile : Edit High Fame Rank';

            

            $this->loadViews("editrankplans", $this->global, $data, NULL);

        }

	}

	

	/*To edit and save the edited Double point plan details*/

	function editPointPlan($id = NULL)

    {

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            if($id == null)

            {

                redirect('doublepoint');

            }

			

            $category = 'dp';

            $data['pointInfo'] = $this->rank_model->getplansInfo($id,$category);

            

            $this->global['pageTitle'] = 'HiProfile : Edit Double Point';

            

            $this->loadViews("editpointplans", $this->global, $data, NULL);

        }

	}

	

	/*To update the rank plan details*/

	function updateRankPlan()

    {

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->load->library('form_validation');

            $id = $this->input->post('id');

            $this->form_validation->set_rules('googleid','Google Id','trim|required|max_length[128]|xss_clean');

			$this->form_validation->set_rules('rank','Rank','trim|required|max_length[128]|xss_clean');

            $this->form_validation->set_rules('price','Price','required|numeric|xss_clean');

			$this->form_validation->set_rules('category','Category','required|xss_clean');

			$this->form_validation->set_rules('times','Time','required|xss_clean');

            if($this->form_validation->run() == FALSE)

            {

                $this->editRankPlan($id);

            }

            else

            {

				$googleid = $this->input->post('googleid');

                $rank = $this->input->post('rank');

				$boosttime = $this->input->post('boosttime');

				$times = $this->input->post('times');

				$price = $this->input->post('price');

			    $category = 'hf';

				$totaltime = $boosttime.$times;

				

                $rankinfo = array('ranks_points_package_Google_playId'=>$googleid,'ranks_points_package_rank_up'=>$rank, 'ranks_points_package_hours'=>$totaltime, 'ranks_points_package_price'=>$price,'ranks_points_package_category_code'=>$category);



                $result = $this->rank_model->updateRankPlan($rankinfo,$id);

                

                if($result > 0)

                {

                    $this->session->set_flashdata('success', 'Rank Plan Updated successfully');

                }

                else

                {

                    $this->session->set_flashdata('error', 'Rank Plan update failed');

                }

                

                redirect('famerank');

            }

        }

	}

	

	/*To update the points plan details*/

	function updateDoublePointPlan()

    {

		

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->load->library('form_validation');

            $id = $this->input->post('id');

            $this->form_validation->set_rules('googleid','Google Id','trim|required|max_length[128]|xss_clean');

            $this->form_validation->set_rules('price','Price','required|numeric|xss_clean');

			$this->form_validation->set_rules('category','Category','required|xss_clean');

			$this->form_validation->set_rules('times','Time','required|xss_clean');

			

            if($this->form_validation->run() == FALSE)

            {

                $this->editPointPlan($id);

            }

            else

            {

				$googleid = $this->input->post('googleid');

				$boosttime = $this->input->post('boosttime');

				$times = $this->input->post('times');

				$price = $this->input->post('price');

			    $category = 'dp';

				$totaltime = $boosttime.$times;

				

                $pointinfo = array('ranks_points_package_Google_playId'=>$googleid,'ranks_points_package_rank_up'=>0, 'ranks_points_package_hours'=>$totaltime, 'ranks_points_package_price'=>$price,'ranks_points_package_category_code'=>$category);



                $result = $this->rank_model->updatePointPlan($pointinfo,$id);

                

                if($result > 0)

                {

                    $this->session->set_flashdata('success', 'Double Point Plan Updated successfully');

                }

                else

                {

                    $this->session->set_flashdata('error', 'Double Point Plan update failed');

                }

                

                redirect('doublepoint');

            }

        }

	}

}

?>