<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Review extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
		$this->load->helper('upload','date', 'url','email','path'); 
		$this->load->model('business_model');
    }
	
    public function index()
    {
        $this->global['pageTitle'] = 'Hiprofile : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    function reviewListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('review_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
			$count = $this->review_model->reviewListingCount($searchText);

			$returns = $this->paginationCompress ( "reviewListing/", $count, 10 );
            
            $data['reviewRecords'] = $this->review_model->reviewListing($searchText, $returns["page"], $returns["segment"]);
			
            $this->global['pageTitle'] = 'HiProfile : Review Management';

            $this->loadViews("review", $this->global, $data, NULL);
        }
    }
	
	function businessReviewListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('review_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
			$count = $this->review_model->getBusinessReviewListingCount($searchText);

			$returns = $this->paginationCompress ( "businessReviewListing/", $count, 10 );
            
            $data['reviewRecords'] = $this->review_model->getBusinessReviewListing($searchText, $returns["page"], $returns["segment"]);
			
            $this->global['pageTitle'] = 'HiProfile : Business Review Management';

            $this->loadViews("business_reviewlist", $this->global, $data, NULL);
        }
    }
	
	function businessView()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$businessid = $_POST['businessid'];
			$data['businessInfo'] = $this->business_model->getBusinessInfo($businessid);
			$html = $this->load->view("viewbusiness",$data,true);	
			echo $html;
		}
		else
		{
			redirect('https://www.google.com');
		}
	}
}