<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class : Login (LoginController)
 * Content class to control to all the contents.
 */
require APPPATH . '/libraries/BaseController.php';

class Content extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('content_model');
        $this->isLoggedIn();   
		$this->load->helper('upload'); 
    }
    
    /**
     * This function used to load the first screen of the content
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Hiprofile : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the content list
     */
    function contentListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('content_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->content_model->contentListingCount($searchText);

			$returns = $this->paginationCompress ( "contentListing/", $count, 5 );
            
            $data['contentRecords'] = $this->content_model->contentListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'HiProfile : Content Management';
            
            $this->loadViews("contents", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNewContentForm()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('content_model');
            
            $this->global['pageTitle'] = 'HiProfile : Add New Content';

            $this->loadViews("addNewContentForm", $this->global, NULL, NULL);
        }
    }

    /**
     * This function is used to add new content to the system
     */
    function addNewContent()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('contentTitle','Content Title','trim|required|xss_clean');
            
            $this->form_validation->set_rules('contentDesc','Content Description','required|xss_clean');

            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewContentForm();
            }
            else
            {
				if(isset($_FILES['c_img']['name']) && $_FILES['c_img']['name'] != "")
				{
        
					$config['upload_path']   = './uploads/'; 
					$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
					$config['file_name'] = time().$_FILES['c_img']['name'];
					$this->load->library('upload', $config); 
					if ( ! $this->upload->do_upload('c_img')) 
					{
						//$error = array('error' => $this->upload->display_errors()); 
						$this->session->set_flashdata('error', 'Error while uploading the image. Please try again.');
						redirect('addNewContentForm');
					}
					else 
					{ 
						$data = array('upload_data' => $this->upload->data()); 
						$imagename = $data['upload_data']['file_name'];
						$contenttitle = ucwords(strtolower($this->input->post('contentTitle')));
						$contentdesc = $this->input->post('contentDesc');
						
						$contentInfo = array('contentTitle'=>$contenttitle,'contentDesc'=>$contentdesc, 'imageurl'=>$imagename,'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
						
						$this->load->model('content_model');
						$result = $this->content_model->addNewContent($contentInfo);
						
						if($result > 0)
						{
							$this->session->set_flashdata('success', 'New Content Added successfully');
						}
						else
						{
							$this->session->set_flashdata('error', 'Content creation failed');
						}
					} 
				}
				else
				{
				
					$contenttitle = ucwords(strtolower($this->input->post('contentTitle')));
					$contentdesc = $this->input->post('contentDesc');
					
					$contentInfo = array('contentTitle'=>$contenttitle,'contentDesc'=>$contentdesc, 'imageurl'=>"",'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
					
					$this->load->model('content_model');
					$result = $this->content_model->addNewContent($contentInfo);
					
					if($result > 0)
					{
						$this->session->set_flashdata('success', 'New Content Added successfully');
					}
					else
					{
						$this->session->set_flashdata('error', 'Content creation failed');
					}
				}
                redirect('contentListing');

            }
        }
    }

    
    /**
     * This function is used load content edit information
     * @param number $contentId : Optional : This is content id
     */
    function editOldContent($contentId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($contentId == null)
            {
                redirect('contentListing');
            }
            
            $data['contentInfo'] = $this->content_model->getContentInfo($contentId);
            
            $this->global['pageTitle'] = 'HiProfile : Edit Content';
            
            $this->loadViews("editOldContent", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the content information
     */
    function editContent()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $contentId = $this->input->post('contentId');
            
 $this->form_validation->set_rules('contentTitle','Content Title','trim|required|xss_clean');
            $this->form_validation->set_rules('contentDesc','Content Description','required|xss_clean');

            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldContent($contentId);
            }
            else
            {
					if(isset($_FILES['c_img']['name']) && $_FILES['c_img']['name'] != "")
				{
        
					$config['upload_path']   = './uploads/'; 
					$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
					$config['file_name'] = time().$_FILES['c_img']['name'];
					$this->load->library('upload', $config); 
					if ( ! $this->upload->do_upload('c_img')) 
					{
						//$error = array('error' => $this->upload->display_errors()); 
						$this->session->set_flashdata('error', 'Error while uploading the image. Please try again.');
						redirect('contentListing');
					}
					else
					{
						$data = array('upload_data' => $this->upload->data()); 
						$imagename = trim($data['upload_data']['file_name']);
						$contenttitle = $this->input->post('contentTitle');
						$contentdesc = $this->input->post('contentDesc');
						$contentInfo = array();
						$contentInfo = array('contentTitle'=>$contenttitle, 'contentDesc'=>$contentdesc, 'imageurl'=>$imagename,'updatedBy'=>$this->vendorId, 
								'updatedDtm'=>date('Y-m-d H:i:s'));
						
						$result = $this->content_model->editContent($contentInfo, $contentId);
						
						if($result == true)
						{
							$this->session->set_flashdata('success', 'Content updated successfully');
						}
						else
						{
							$this->session->set_flashdata('error', 'Content updation failed');
						}
	}
				}
				else
				{
					$imgval = $this->content_model->checkImageExists($contentId);
					$img = ($imgval != 0) ? $imgval : "";
					$contenttitle = $this->input->post('contentTitle');
					$contentdesc = $this->input->post('contentDesc');
					$contentInfo = array();
					$contentInfo = array('contentTitle'=>$contenttitle, 'contentDesc'=>$contentdesc, 'imageurl'=>$img,'updatedBy'=>$this->vendorId, 
							'updatedDtm'=>date('Y-m-d H:i:s'));
					
					$result = $this->content_model->editContent($contentInfo, $contentId);
					
					if($result == true)
					{
						$this->session->set_flashdata('success', 'Content updated successfully');
					}
					else
					{
						$this->session->set_flashdata('error', 'Content updation failed');
					}
				}
                redirect('contentListing');
            }
        }
    }


    /**
     * This function is used to delete the content using contentId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteContent()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $contentId = $this->input->post('contentId');
            $contentInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->content_model->deleteContent($contentId, $contentInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	function mainDeleteContentImage()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $contentId = $this->input->post('contentId');
            $contentInfo = array('imageurl'=>"",'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->content_model->deleteContentImageMain($contentId, $contentInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'HiProfile : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>