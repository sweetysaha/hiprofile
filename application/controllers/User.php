<?php if(!defined('BASEPATH')) exit('No direct script access allowed');



require APPPATH . '/libraries/BaseController.php';



/**

 * Class : User (UserController)

 * User Class to control all user related operations.

 */

class User extends BaseController

{

    /**

     * This is default constructor of the class

     */

    public function __construct()

    {

        parent::__construct();

        $this->load->model('user_model');

        $this->isLoggedIn();   

    }

    

    /**

     * This function used to load the first screen of the user

     */

    public function index()

    {

        $this->global['pageTitle'] = 'HiProfile : Dashboard';

        

        $this->loadViews("dashboard", $this->global, NULL , NULL);

    }

    

    /**

     * This function is used to load the user list

     */

    /*function userListing()

    {

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->load->model('user_model');

        

            $searchText = $this->input->post('searchText');

            $data['searchText'] = $searchText;

            

            $this->load->library('pagination');

            

            $count = $this->user_model->userListingCount($searchText);



			$returns = $this->paginationCompress ( "userListing/", $count, 10 );

            

            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);

            

            $this->global['pageTitle'] = 'HiProfile : User Management';



            $this->loadViews("users", $this->global, $data, NULL);

        }

    }*/

	function userListing()

    {

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

			

            $this->global['pageTitle'] = 'HiProfile : User Management';



            $this->loadViews("users", $this->global, NULL, NULL);

        }

    }

	

	function usersrecords()

    {

            $this->load->model('user_model');

			// Datatables Variables

			$draw = intval($this->input->get("draw"));

			$start = intval($this->input->get("start"));

			$length = intval($this->input->get("length"));





			$users = $this->user_model->get_userListing();



			$data = array();



			foreach($users->result() as $r) {

			

			$data[] = array(

			$r->slno = count($r->name),

			$r->name,

            $r->username,

			$r->email,

			$r->mobile,

			$r->status = ($r->status == 0)?"<span style='color:#08d208;'>Active</span>":"<span style='color:#dd4b39;'>Deactive</span>",

			$r->points,

			$r->rank+$r->social_rank,

			$r->userId = '<a class="btn btn-sm btn-success" title="VIEW" href="'.$r->userId.'" id="viewlink" onclick="doview('.$r->userId.'); return false;"><i class="fa fa-eye"></i></a> <a class="btn btn-sm btn-info" title="EDIT" href="'.base_url()."editOld/".$r->userId.'"><i class="fa fa-pencil"></i></a> <a class="btn btn-sm btn-danger deleteUser" title="DELETE" href="#" data-userid="'.$r->userId.'"><i class="fa fa-trash"></i></a>'

			);

			}

			$output = array(

			"draw" => $draw,

			"recordsTotal" => $users->num_rows(),

			"recordsFiltered" => $users->num_rows(),

			"data" => $data

			);

			echo json_encode($output);

			exit();

    }



    /**

     * This function is used to load the add new form

     */

    function addNew()

    {

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->load->model('user_model');

            $data['roles'] = $this->user_model->getUserRoles();

            

            $this->global['pageTitle'] = 'HiProfile : Add New User';



            $this->loadViews("addNew", $this->global, $data, NULL);

        }

    }



    /**

     * This function is used to check whether email already exist or not

     */

    function checkEmailExists()

    {

        $userId = $this->input->post("userId");

        $email = $this->input->post("email");



        if(empty($userId)){

            $result = $this->user_model->checkEmailExists($email);

        } else {

            $result = $this->user_model->checkEmailExists($email, $userId);

        }



        if(empty($result)){ echo("true"); }

        else { echo("false"); }

    }

    

	  /**

     * This function is used to check whether username already exist or not

     */

    function checkUsernameExist()

    {

		$username = $this->input->post("username");

		$error = 0;

		if (!empty($username)) {

			$result = $this->user_model->checkUsernameExist($username);

			//print_r($result);

			if (!empty($result)) {

				$error = 1;

			} else {

				$error = 0;

			}

		}

		echo $error;

		exit();

    }

	

	 /**

     * This function is used to check whether mobile already exist or not

     */

    function checkMobileExist()

    {

		$phone = $this->input->post("phone");

		$error = 0;

		if (!empty($phone)) {

			

			$result = $this->user_model->checkMobileExist($phone);

			//print_r($result);

			if (!empty($result)) {

				$error = 1;

			} else {

				$error = 0;

			}

		}

		echo $error;

		exit();

    }

    /**

     * This function is used to add new user to the system

     */

    function addNewUser()

    {

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->load->library('form_validation');

            

            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');

			$this->form_validation->set_rules('username','User Name','trim|required|max_length[128]|xss_clean');

            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');

            $this->form_validation->set_rules('password','Password','required|max_length[20]');

            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');

            //$this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean|numeric');

			$this->form_validation->set_rules('phone','Mobile Number','required|xss_clean');

			$this->form_validation->set_rules('gender','Gender','required|xss_clean');

            $this->form_validation->set_rules('dob','Date Of Birth','required|xss_clean');

            

            

            if($this->form_validation->run() == FALSE)

            {

                $this->addNew();

            }

            else

            {

				

                $name = ucwords(strtolower($this->input->post('fname')));

                $email = $this->input->post('email');

                $password = $this->input->post('password');

                //$roleId = $this->input->post('role'); 

				$roleId = 0;

                //$mobile = $this->input->post('mobile');

				$mobile = str_replace(" ","",$this->input->post('phone'));

				$username = $this->input->post('username');

				$gender = $this->input->post('gender');

				$dob = $this->input->post('dob');

				$status = $this->input->post('status');

				$fburl = $this->input->post('fburl');

				$twitterurl = $this->input->post('twitterurl');

				$instaurl = $this->input->post('instaurl');

                

                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'name'=> $name,'mobile'=>$mobile, 'mobile'=>$mobile, 'username'=>$username,'gender'=>$gender,'dob'=>$dob,'status'=>$status,'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));

                

				$socialInfo = array('facebook_profile_url'=>$fburl,'twitter_profile_url'=>$twitterurl,'instagram_profile_url'=>$instaurl);

				

                $this->load->model('user_model');

                $result = $this->user_model->addNewUser($userInfo, $socialInfo);

                

                if($result > 0)

                {

                    $this->session->set_flashdata('success', 'New User created successfully');

                }

                else

                {

                    $this->session->set_flashdata('error', 'User creation failed');

                }

                

                redirect('userListing');

            }

        }

    }



    

    /**

     * This function is used load user edit information

     * @param number $userId : Optional : This is user id

     */

    function editOld($userId = NULL)

    {

        if($this->isAdmin() == TRUE || $userId == 1)

        {

            $this->loadThis();

        }

        else

        {

            if($userId == null)

            {

                redirect('userListing');

            }

            

            $data['roles'] = $this->user_model->getUserRoles();

            $data['userInfo'] = $this->user_model->getUserInfo($userId);

			$data['friendcount'] =$this->user_model->getUserFrinedInfo($userId); 

            

            $this->global['pageTitle'] = 'HiProfile : Edit User';

            

            $this->loadViews("editOld", $this->global, $data, NULL);

        }

    }

    

    

    /**

     * This function is used to edit the user information

     */

    function editUser()

    {

        if($this->isAdmin() == TRUE)

        {

            $this->loadThis();

        }

        else

        {

            $this->load->library('form_validation');

            

            $userId = $this->input->post('userId');

            

            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');

            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');

            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');

            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');

            $this->form_validation->set_rules('gender','Gender','trim|required|xss_clean');

            //$this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');

			$this->form_validation->set_rules('phone','Mobile Number','required|xss_clean');



            

            if($this->form_validation->run() == FALSE)

            {

                $this->editOld($userId);

            }

            else

            {

                $name = ucwords(strtolower($this->input->post('fname')));

                $email = $this->input->post('email');

                $password = $this->input->post('password');

                $roleId = $this->input->post('role');

                //$mobile = $this->input->post('mobile');

				$mobile = str_replace(" ","",$this->input->post('phone'));

				$username = $this->input->post('username');

				$gender = $this->input->post('gender');

				$dob = $this->input->post('dob');

				$status = $this->input->post('status');

                

				$fburl = $this->input->post('fburl');

				$twiturl = $this->input->post('twitterurl');

				$insturl = $this->input->post('instaurl');

				

                $userInfo = array();

				$socialInfo = array();

                if(empty($password))

                {

                    $userInfo = array('email'=>$email, 'roleId'=>$roleId, 'name'=>$name,

                                    'mobile'=>$mobile,'username'=>$username,'gender'=>$gender,

									'dob'=>$dob,'status'=>$status,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

									

					$socialInfo = array('facebook_profile_url'=>$fburl, 'twitter_profile_url'=>$twiturl, 'instagram_profile_url'=>$insturl);

									

                }

                else

                {

                    $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId,'name'=>ucwords($name), 'mobile'=>$mobile, 'username'=>$username,'gender'=>$gender,'dob'=>$dob,'status'=>$status,'updatedBy'=>$this->vendorId,'updatedDtm'=>date('Y-m-d H:i:s'));

					

					$socialInfo = array('facebook_profile_url'=>$fburl, 'twitter_profile_url'=>$twiturl, 'instagram_profile_url'=>$insturl);

                }



                $result = $this->user_model->editUser($userInfo, $socialInfo, $userId);

                

                if($result == true)

                {

                    $this->session->set_flashdata('success', 'User updated successfully');

                }

                else

                {

                    $this->session->set_flashdata('error', 'User updation failed');

                }

                

                redirect('userListing');

            }

        }

    }





    /**

     * This function is used to delete the user using userId

     * @return boolean $result : TRUE / FALSE

     */

    function deleteUser()

    {

        if($this->isAdmin() == TRUE)

        {

            echo(json_encode(array('status'=>'access')));

        }

        else

        {

            $userId = $this->input->post('userId');

            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

            

            $result = $this->user_model->deleteUser($userId, $userInfo);

            

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }

            else { echo(json_encode(array('status'=>FALSE))); }

        }

    }

    

    /**

     * This function is used to load the change password screen

     */

    function loadChangePass()

    {

        $this->global['pageTitle'] = 'HiProfile : Change Password';

        

        $this->loadViews("changePassword", $this->global, NULL, NULL);

    }

    

    

    /**

     * This function is used to change the password of the user

     */

    function changePassword()

    {

        $this->load->library('form_validation');

        

        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');

        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');

        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');

        

        if($this->form_validation->run() == FALSE)

        {

            $this->loadChangePass();

        }

        else

        {

            $oldPassword = $this->input->post('oldPassword');

            $newPassword = $this->input->post('newPassword');

            

            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);

            

            if(empty($resultPas))

            {

                $this->session->set_flashdata('nomatch', 'Your old password not correct');

                redirect('loadChangePass');

            }

            else

            {

                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,

                                'updatedDtm'=>date('Y-m-d H:i:s'));

                

                $result = $this->user_model->changePassword($this->vendorId, $usersData);

                

                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }

                else { $this->session->set_flashdata('error', 'Password updation failed'); }

                

                redirect('loadChangePass');

            }

        }

    }



    function pageNotFound()

    {

        $this->global['pageTitle'] = 'HiProfile : 404 - Page Not Found';

        

        $this->loadViews("404", $this->global, NULL, NULL);

    }

	

	function userView()

	{

		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')

		{

			$userid = $_POST['userid'];

			$data['userInfo'] = $this->user_model->getUserInfo($userid);

			$data['friendcount'] =$this->user_model->getUserFrinedInfo($userid); 

			$html = $this->load->view("viewuser",$data,true);	

			echo $html;

		}

		else

		{

			redirect('https://www.google.com');

		}

	}

}



?>

