<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class : Login (LoginController)
 * Business class to control to all the business.
 */
require APPPATH . '/libraries/BaseController.php';

class Business extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('business_model');
        $this->isLoggedIn();   
		$this->load->helper('upload'); 
    }
    
    /**
     * This function used to load the first screen of the business
     */
    public function index()
    {
		
        $this->global['pageTitle'] = 'Hiprofile : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the business list
     */
    function businessListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('business_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->business_model->businessListingCount($searchText);

			$returns = $this->paginationCompress ( "businessListing/", $count, 10 );
            
            $data['businessRecords'] = $this->business_model->businessListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'HiProfile : Business Management';
            
            $this->loadViews("business", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNewBusinessForm()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('business_model');
            
            $this->global['pageTitle'] = 'HiProfile : Add New Business';

            $this->loadViews("addNewBusinessForm", $this->global, NULL, NULL);
        }
    }

    /**
     * This function is used to add new business to the system
     */
    function addNewBusiness()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {

            $this->load->library('form_validation');
                      			
            $this->form_validation->set_rules('businessTitle','Business Title','required|xss_clean');
			$this->form_validation->set_rules('businessAddress','Business Address','required|xss_clean');
			$this->form_validation->set_rules('businessCountry','Business Country','required|xss_clean');		
            $this->form_validation->set_rules('businessState','Business State','required|xss_clean');		
			$this->form_validation->set_rules('businessCity','Business City','required|xss_clean');
			$this->form_validation->set_rules('businessPin','Business Pincode / Zip','required|xss_clean');
			$this->form_validation->set_rules('businessContact','Business Contact','required|xss_clean');
			$this->form_validation->set_rules('businessEmail','Business Email','required|valid_email|xss_clean');
			$this->form_validation->set_rules('businessdof','Business Day Open From','required|xss_clean');
			$this->form_validation->set_rules('businessdot','Business Date Open To','required|xss_clean');
			$this->form_validation->set_rules('businesshof','Business Hours Open From','required|xss_clean');
			$this->form_validation->set_rules('businesshot','Business Hours Open To','required|xss_clean');
			$this->form_validation->set_rules('businesslatitude','Business Latitude','required|xss_clean');
			$this->form_validation->set_rules('businesslongitude','Business Longitude','required|xss_clean');
			$this->form_validation->set_rules('userid','user Identity','required|xss_clean');
			$this->form_validation->set_rules('businesscategoryname','Business Primary Category Name','required|xss_clean');
			$this->form_validation->set_rules('businesscategoryicon[]','Business Categories','required|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewBusinessForm();
            }
            else
            {			
				if(isset($_FILES['c_img']['name']) && $_FILES['c_img']['name'] != "")
				{
        
					$config['upload_path']   = './assets/business/images/'; 
					$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
					$config['file_name'] = time().$_FILES['c_img']['name'];
					$this->load->library('upload', $config); 
					if ( ! $this->upload->do_upload('c_img')) 
					{
						//$error = array('error' => $this->upload->display_errors()); 
						$this->session->set_flashdata('error', 'Error while uploading the image. Please try again.');
						redirect('addNewBusinessForm');
					}
					else 
					{ 
						$data = array('upload_data' => $this->upload->data()); 
						$imagename = $data['upload_data']['file_name'];
						$businesstitle = $this->input->post('businessTitle');
						$businessaddress = $this->input->post('businessAddress');
						$businesscountry = $this->input->post('businessCountry');
						$businessstate = $this->input->post('businessState');
						$businesscity = $this->input->post('businessCity');
						$businesspin = $this->input->post('businessPin');
						$businesscontact = $this->input->post('businessContact');
						$businessemail = $this->input->post('businessEmail');
						$businessdof = $this->input->post('businessdof');
						$businessdot = $this->input->post('businessdot');
						$businesshof = $this->input->post('businesshof');
						$businesshot = $this->input->post('businesshot');
						$businesslatitude = $this->input->post('businesslatitude');
						$businesslongitude = $this->input->post('businesslongitude');
						$businesscategoryname = $this->input->post('businesscategoryname');
						$businesscaticon = $this->input->post('businesscategoryicon');
						$businesscategoryicon = rtrim(implode(',', $businesscaticon), ',');
						$userID = $this->input->post('userid');
						$businessInfo = array();
						$businessInfo = array(
						'business_title'	=>$businesstitle, 
						'address'			=>$businessaddress, 
						'country'			=>$businesscountry,
						'state'				=>$businessstate, 
						'city'				=>$businesscity, 
						'pin'				=>$businesspin,
						'contact'			=>$businesscontact, 
						'email'				=>$businessemail, 
						'day_open_from'		=>$businessdof,
						'day_open_to'		=>$businessdot, 
						'hours_open_from'	=>$businesshof, 
						'hours_open_to'		=>$businesshot,
						'latitude'			=>$businesslatitude, 
						'longitude'			=>$businesslongitude, 
						'image_url'			=>'/business/images/'.$imagename,
						'userId'			=>$userID,
						'primary_category' 	=>$businesscategoryname,
						'categories'		=>$businesscategoryicon
						);
											
						$this->load->model('business_model');
						$result = $this->business_model->addNewBusiness($businessInfo);
						
						if($result > 0)
						{
							$this->session->set_flashdata('success', 'New Business Added successfully');
						}
						else
						{
							$this->session->set_flashdata('error', 'Business creation failed');
						}
					} 
				}
				else
				{
				
					$businesstitle = $this->input->post('businessTitle');
					$businessaddress = $this->input->post('businessAddress');
					$businesscountry = $this->input->post('businessCountry');
					$businessstate = $this->input->post('businessState');
					$businesscity = $this->input->post('businessCity');
					$businesspin = $this->input->post('businessPin');
					$businesscontact = $this->input->post('businessContact');
					$businessemail = $this->input->post('businessEmail');
					$businessdof = $this->input->post('businessdof');
					$businessdot = $this->input->post('businessdot');
					$businesshof = $this->input->post('businesshof');
					$businesshot = $this->input->post('businesshot');
					$businesslatitude = $this->input->post('businesslatitude');
					$businesslongitude = $this->input->post('businesslongitude');
					$businesscategoryname = $this->input->post('businesscategoryname');
					$businesscaticon = $this->input->post('businesscategoryicon');
					$businesscategoryicon = rtrim(implode(',', $businesscaticon), ',');
					$userID = $this->input->post('userid');
					$businessInfo = array();
					$businessInfo = array(
					'business_title'	=>$businesstitle, 
					'address'			=>$businessaddress, 
					'country'			=>$businesscountry,
					'state'				=>$businessstate, 
					'city'				=>$businesscity, 
					'pin'				=>$businesspin,
					'contact'			=>$businesscontact, 
					'email'				=>$businessemail, 
					'day_open_from'		=>$businessdof,
					'day_open_to'		=>$businessdot, 
					'hours_open_from'	=>$businesshof, 
					'hours_open_to'		=>$businesshot,
					'latitude'			=>$businesslatitude, 
					'longitude'			=>$businesslongitude, 
					'image_url'			=>'',
					'userId'			=>$userID,
					'primary_category' 	=>$businesscategoryname,
					'categories'		=>$businesscategoryicon
					);
									
					$this->load->model('business_model');
					$result = $this->business_model->addNewBusiness($businessInfo);
					
					if($result > 0)
					{
						$this->session->set_flashdata('success', 'New Business Added successfully');
					}
					else
					{
						$this->session->set_flashdata('error', 'Business creation failed');
					}
				}
                redirect('businessListing');

            }
        }
    }

    
    /**
     * This function is used load business edit information
     * @param number $businessId : Optional : This is business id
     */
    function editOldBusiness($businessId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($businessId == null)
            {
                redirect('businessListing');
            }
            
            $data['businessInfo'] = $this->business_model->getBusinessInfo($businessId);
            
            $this->global['pageTitle'] = 'HiProfile : Edit Business';
            
            $this->loadViews("editOldBusiness", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the business information
     */
    function editBusiness()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			
            $this->load->library('form_validation');
            
            $businessId = $this->input->post('businessId');
			
            $this->form_validation->set_rules('businessTitle','Business Title','required|xss_clean');
			$this->form_validation->set_rules('businessAddress','Business Address','required|xss_clean');
			$this->form_validation->set_rules('businessCountry','Business Country','required|xss_clean');		
            $this->form_validation->set_rules('businessState','Business State','required|xss_clean');		
			$this->form_validation->set_rules('businessCity','Business City','required|xss_clean');
			$this->form_validation->set_rules('businessPin','Business Pincode / Zip','required|xss_clean');
			$this->form_validation->set_rules('businessContact','Business Contact','required|xss_clean');
			$this->form_validation->set_rules('businessEmail','Business Email','required|valid_email|xss_clean');
			$this->form_validation->set_rules('businessdof','Business Day Open From','required|xss_clean');
			$this->form_validation->set_rules('businessdot','Business Date Open To','required|xss_clean');
			$this->form_validation->set_rules('businesshof','Business Hours Open From','required|xss_clean');
			$this->form_validation->set_rules('businesshot','Business Hours Open To','required|xss_clean');
			$this->form_validation->set_rules('businesslatitude','Business Latitude','required|xss_clean');
			$this->form_validation->set_rules('businesslongitude','Business Longitude','required|xss_clean');
			$this->form_validation->set_rules('businesscategoryname','Business Primary Category Name','required|xss_clean');
			$this->form_validation->set_rules('businesscategoryicon[]','Business Categories','required|xss_clean');
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldBusiness($businessId);
            }
            else
            {
				if(isset($_FILES['c_img']['name']) && $_FILES['c_img']['name'] != "")
				{

					$config['upload_path']   = './assets/business/images/'; 
					$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
					$config['file_name'] = time().$_FILES['c_img']['name'];
					$this->load->library('upload', $config); 
					if (!$this->upload->do_upload('c_img')) 
					{
						$this->session->set_flashdata('error', 'Error while uploading the image. Please try again.');
						redirect('businessListing');
					}
					else
					{
						//status 1-PaymntPending, 2-Pending, 3-Approved
						$status = "";
						if($this->input->post('statuss')):
							$status = $this->input->post('statuss');
						endif;
						$data = array('upload_data' => $this->upload->data()); 
						$imagename = trim($data['upload_data']['file_name']);
						$businesstitle = $this->input->post('businessTitle');
						$businessaddress = $this->input->post('businessAddress');
						$businesscountry = $this->input->post('businessCountry');
						$businessstate = $this->input->post('businessState');
						$businesscity = $this->input->post('businessCity');
						$businesspin = $this->input->post('businessPin');
						$businesscontact = $this->input->post('businessContact');
						$businessemail = $this->input->post('businessEmail');
						$businessdof = $this->input->post('businessdof');
						$businessdot = $this->input->post('businessdot');
						$businesshof = $this->input->post('businesshof');
						$businesshot = $this->input->post('businesshot');
						$businesslatitude = $this->input->post('businesslatitude');
						$businesslongitude = $this->input->post('businesslongitude');
						$businesscategoryname = $this->input->post('businesscategoryname');
						$businesscaticon = $this->input->post('businesscategoryicon');
						$businesscategoryicon = rtrim(implode(',', $businesscaticon), ',');
						$businessInfo = array();
						$businessInfo = array(
						'business_title'	=>$businesstitle, 
						'address'			=>$businessaddress, 
						'country'			=>$businesscountry,
						'state'				=>$businessstate, 
						'city'				=>$businesscity, 
						'pin'				=>$businesspin,
						'contact'			=>$businesscontact, 
						'email'				=>$businessemail, 
						'day_open_from'		=>$businessdof,
						'day_open_to'		=>$businessdot, 
						'hours_open_from'	=>$businesshof, 
						'hours_open_to'		=>$businesshot,
						'latitude'			=>$businesslatitude, 
						'longitude'			=>$businesslongitude, 
						'image_url'			=>'/business/images/'.$imagename,
						'primary_category' 	=>$businesscategoryname,
						'categories'		=>$businesscategoryicon
						);
						
						$result = $this->business_model->editBusiness($businessInfo, $businessId,$status);
						
						if($result == true)
						{
							$this->session->set_flashdata('success', 'Business updated successfully');
						}
						else
						{
							$this->session->set_flashdata('error', 'Business updation failed');
						}
					}
				}
				else
				{
					//status 1-PaymntPending, 2-Pending, 3-Approved
					$status = "";
					if($this->input->post('statuss')):
						$status = $this->input->post('statuss');
					endif;
					$imgval = $this->business_model->checkImageExists($businessId);
					$imgvals = ($imgval != "") ? $imgval : "";
					$businesstitle = $this->input->post('businessTitle');
						$businessaddress = $this->input->post('businessAddress');
						$businesscountry = $this->input->post('businessCountry');
						$businessstate = $this->input->post('businessState');
						$businesscity = $this->input->post('businessCity');
						$businesspin = $this->input->post('businessPin');
						$businesscontact = $this->input->post('businessContact');
						$businessemail = $this->input->post('businessEmail');
						$businessdof = $this->input->post('businessdof');
						$businessdot = $this->input->post('businessdot');
						$businesshof = $this->input->post('businesshof');
						$businesshot = $this->input->post('businesshot');
						$businesslatitude = $this->input->post('businesslatitude');
						$businesslongitude = $this->input->post('businesslongitude');
						$businesscategoryname = $this->input->post('businesscategoryname');
						$businesscaticon = $this->input->post('businesscategoryicon');
						$businesscategoryicon = rtrim(implode(',', $businesscaticon), ',');
						$businessInfo = array();
						$businessInfo = array(
						'business_title'	=>$businesstitle, 
						'address'			=>$businessaddress, 
						'country'			=>$businesscountry,
						'state'				=>$businessstate, 
						'city'				=>$businesscity, 
						'pin'				=>$businesspin,
						'contact'			=>$businesscontact, 
						'email'				=>$businessemail, 
						'day_open_from'		=>$businessdof,
						'day_open_to'		=>$businessdot, 
						'hours_open_from'	=>$businesshof, 
						'hours_open_to'		=>$businesshot,
						'latitude'			=>$businesslatitude, 
						'longitude'			=>$businesslongitude, 
						'image_url'			=>$imgvals,
						'primary_category' 	=>$businesscategoryname,
						'categories'		=>$businesscategoryicon
						);
					
					$result = $this->business_model->editBusiness($businessInfo, $businessId, $status);
					
					if($result == true)
					{
						$this->session->set_flashdata('success', 'Business updated successfully');
					}
					else
					{
						$this->session->set_flashdata('error', 'Business updation failed');
					}
				}
                redirect('businessListing');
            }
        }
    }


    /**
     * This function is used to delete the business using businessId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteBusiness()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $businessId = $this->input->post('businessId');
            $businessInfo = array('isDeleted'=>1,'updated_date'=>date('Y-m-d H:i:s'));
            
            $result = $this->business_model->deleteBusiness($businessId, $businessInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	function mainDeleteBusinessImage()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $businessId = $this->input->post('businessId');
            $businessInfo = array('image_url'=>"",'updated_date'=>date('Y-m-d H:i:s'));
            
            $result = $this->business_model->deleteBusinessImageMain($businessId, $businessInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	function getAlluserId()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $result = $this->business_model->getAllUserIds();
			return $result;
        }
    }
	
	function getAllcategories()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $result = $this->business_model->getAllcategories();
			return $result;
        }
    }
	
	function getselectedcategories()
    {
		$businessId = $this->input->post('businessid');
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $result = $this->business_model->getselectedcategories($businessId);
			return $result;
        }
    }
	
	function getprimarycategories()
    {
		$businessId = $this->input->post('businessid');
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $result = $this->business_model->getprimarycategories($businessId);
			return $result;
        }
    }
	
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'HiProfile : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>