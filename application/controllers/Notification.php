<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class : Login (LoginController)
 * Notification class to control to all the contents.
 */
require APPPATH . '/libraries/BaseController.php';

class Notification extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('notification_model');
        $this->isLoggedIn();   
		$this->load->helper('upload','date', 'url','email','path'); 
		//test
    }
    /**
     * This function used to load the first screen of the content
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Hiprofile : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the Notification list
     */
    function notificationListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('notification_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->notification_model->notificationListingCount($searchText);

			$returns = $this->paginationCompress ("notificationListing/", $count, 10 );
            
            $data['notificationRecords'] = $this->notification_model->notificationListing($searchText, $returns["page"], $returns["segment"]);
            $data['notify_email'] = $this->notification_model->sendBulkEmail();
            $this->global['pageTitle'] = 'HiProfile : Notification Management';
            
            $this->loadViews("notification", $this->global, $data, NULL);
        }
    }
	
	function notifyemail()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$message = $_POST['message'];
			$email = $_POST['email'];
			$userId = $_POST['userId'];
			$notify_email = $this->notification_model->sendemail($message,$email,$userId);
			if($notify_email == "success"){
				echo "success";
			}
			else
			{
				echo "fail";
			}
		}
		else
		{
			redirect('https://www.google.com');
		}
	}
	
	function notifysms()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$message = $_POST['message'];
			$mobile = $_POST['mobile'];
			$notify_mobile = $this->notification_model->sendsms($message,$mobile);
			if($notify_mobile == "success"){
				echo "success";
			}
			else
			{
				echo "fail";
			}
		}
		else
		{
			redirect('https://www.google.com');
		}
	}
	
	function sendBulkEmail()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if($_POST['val'] == 'getemail')
			{
				$data['notify_email'] = $this->notification_model->sendBulkEmail();
				$html = $this->load->view("sendbulkemail",$data,true);
				echo $html;
			}
			if($_POST['val'] == 'sendmail')
			{				/*Configuration of mail*/				$config['useragent'] = "CodeIgniter";				$config['protocol'] = 'smtp';				$config['_smtp_auth']   = TRUE;				$config['smtp_host'] = 'email-smtp.eu-west-1.amazonaws.com';				$config['smtp_port'] = '587';				$config['smtp_user'] = 'AKIAIVOX4CAJJASO2FMA';				$config['smtp_pass'] = 'AkZIiaGSk85ypMCsI8vGETe7ChDLPQ1ikOrZUlpSRRU7';				$config['newline'] = "\r\n"; 				$config['smtp_crypto'] = 'tls'; 				$config['priority'] = 5;				
				$message = $_POST['notfymsg'];
				$emailids = $_POST['emailids'];
				$data["data"] = $message;
				$finalemails = explode(',', $emailids);
				$list = array_filter($finalemails);
				$string_email = implode(',', $list);
				$this->load->library('email');				$this->email->initialize($config);
				$this->email->set_mailtype('html');
				$this->email->from(EMAIL_FROM, FROM_NAME);				$this->email->reply_to(REPLY_TO, REPLY_NAME); 
				$this->email->subject("HiProfile Notification");
				//$this->email->message($message);
				$this->email->message($this->load->view('email/notification_email', $data, TRUE));
				
				$this->email->to($string_email);
				$status = $this->email->send();
				if($status)
				{
					echo "Notification Email Sent Successfully";
					exit;
				}
				else
				{
					echo "Notification Email Not Sent. Please Try Again";
					exit;
				}
				
			}
		}
		else
		{
			redirect('https://www.google.com');
		}
	}
}

?>