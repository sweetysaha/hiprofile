<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class : Category (CategoryController)
 * Category class to control to all the categorys.
 */
require APPPATH . '/libraries/BaseController.php';

class Category extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model');
        $this->isLoggedIn();   
		$this->load->helper('upload'); 
    }
    
    /**
     * This function used to load the first screen of the category
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Hiprofile : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the category list
     */
    function categoryListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('category_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->category_model->categoryListingCount($searchText);

			$returns = $this->paginationCompress ( "categoryListing/", $count, 10 );
            
            $data['categoryRecords'] = $this->category_model->categoryListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'HiProfile : Category Management';
            
            $this->loadViews("category", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNewCategoryForm()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('category_model');
            
            $this->global['pageTitle'] = 'HiProfile : Add New Category';

            $this->loadViews("addNewCategoryForm", $this->global, NULL, NULL);
        }
    }

    /**
     * This function is used to add new category to the system
     */
    function addNewCategory()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('categoryName','Category Name','trim|required|xss_clean');
            $this->form_validation->set_rules('categoryIcon','Category Icon Name','required|xss_clean');

            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewCategoryForm();
            }
            else
            {
				$categoryname = $this->input->post('categoryName');
				$categoryicon = $this->input->post('categoryIcon');
				$statuss = $this->input->post('statuss');

				$categoryInfo = array('business_category_names'=>$categoryname, 'business_category_icon'=>$categoryicon, 'cat_status'=>$statuss);

				$this->load->model('category_model');
				$result = $this->category_model->addNewCategory($categoryInfo);

				if($result > 0)
				{
					$this->session->set_flashdata('success', 'New Category Added successfully');
				}
				else
				{
					$this->session->set_flashdata('error', 'Category creation failed');
				}

				redirect('categoryListing');

            }
        }
    }

    
    /**
     * This function is used load Category edit information
     * @param number $categoryId : Optional : This is Category id
     */
    function editOldCategory($categoryId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($categoryId == null)
            {
                redirect('categoryListing');
            }
            
            $data['categoryInfo'] = $this->category_model->getCategoryInfo($categoryId);
            
            $this->global['pageTitle'] = 'HiProfile : Edit Category';
            
            $this->loadViews("editOldCategory", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the Category information
     */
    function editCategory()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $categoryId = $this->input->post('categoryId');
            
			$this->form_validation->set_rules('categoryName','Category Name','trim|required|xss_clean');
            $this->form_validation->set_rules('categoryIcon','Category Icon Name','required|xss_clean');

            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldCategory($categoryId);
            }
            else
            {
				$categoryname = $this->input->post('categoryName');
				$categoryicon = $this->input->post('categoryIcon');
				$statuss = $this->input->post('statuss');
				$categoryInfo = array();
				$categoryInfo = array('business_category_names'=>$categoryname, 'business_category_icon'=>$categoryicon, 'cat_status'=>$statuss);
				
				$result = $this->category_model->editCategory($categoryInfo, $categoryId);
				
				if($result == true)
				{
					$this->session->set_flashdata('success', 'Category updated successfully');
				}
				else
				{
					$this->session->set_flashdata('error', 'Category updation failed');
				}
                redirect('categoryListing');
            }
        }
    }


    /**
     * This function is used to delete the Category using categoryId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteCategory()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $categoryId = $this->input->post('categoryId');
            $categoryInfo = array('isDeleted'=>1);
            
            $result = $this->category_model->deleteCategory($categoryId, $categoryInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'HiProfile : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>