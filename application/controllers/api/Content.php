<?php

require(APPPATH.'/libraries/REST_Controller.php');
 
class Content extends REST_Controller{
    
    public function __construct()
    {
        parent::__construct();

        $this->load->model('contentapi_model');
		$this->load->helper('date');
		$this->load->helper('email');
		$this->load->helper('url');
    }

    //API -  To fetch the latest trends content
	function latesttrends_post()
	{
		$latesttrendid = 4;
		$data = $this->post();
		$validate = array(
			"userID",
			"device_token",
			"device_id",
			"device_type"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->contentapi_model->getContenttrends($latesttrendid);
				if(!empty($result))
				{
					$loop_count = count($result);
					for($i=0;$i<$loop_count;$i++) {
						$result[$i]->image_url = ($result[$i]->image_url == "") ? "" : base_url().'uploads/'.$result[$i]->image_url;
					}
					$return_res = array();
					$return_res['responsecode'] 	= "200";
					$return_res['responsedetails'] 	= "success";
					$return_res['data']['trends']   = $result;
					$this->response($return_res);
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					$this->response($return_res);
				}
				
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  To fetch the health tips content
	function healthtips_post()
	{
		$healthtipid = 5;
		$data = $this->post();
		$validate = array(
			"userID",
			"device_token",
			"device_id",
			"device_type"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->contentapi_model->getContenttips($healthtipid);
				
				if(!empty($result))
				{
					$loop_count = count($result);
					for($i=0;$i<$loop_count;$i++) {
						$result[$i]->image_url = ($result[$i]->image_url == "") ? "" : base_url().'uploads/'.$result[$i]->image_url;
					}
					$return_res = array();
					$return_res['responsecode'] 	= "200";
					$return_res['responsedetails'] 	= "success";
					$return_res['data']['health_tip'] 	= $result;
					$this->response($return_res);
				}
				else
				{
					$return_res = array();
					$return_res['responsecode'] = "500";
					$return_res['status'] = "Something went wrong. Please try again later.";
					$this->response($return_res);
				}
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($return_res);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($return_res);
		}
		exit;
	}
}