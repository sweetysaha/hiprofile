<?php

require(APPPATH.'/libraries/REST_Controller.php');
 
class Search extends REST_Controller{
	
	 public function __construct()
	{
		parent::__construct();

		$this->load->model('searchapi_model');
		$this->load->model('userapi_model');
		$this->load->helper('date');
		$this->load->helper('email');
	}
	
	//API -  Search user and business in the locality 
	function searchbusiness_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID", 
				"device_token",
				"device_id",
				"device_type",
				"latitude",
				"longitude",
				"search_string"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				/*To update lat and long*/
				$this->userapi_model->inserLatLong($data['latitude'], $data['longitude'], $data['userID']);
				$searchtext = trim($data['search_string']);
				if($searchtext == "")
				{
					$result = array();
					$result['responsecode'] = "501";
					$result['status'] = "Required field missing. Please try again.";
					$this->response($result);
				}
				else
				{
					$result = $this->searchapi_model->getSearchbusiness($data);
					$this->response($result);
				}
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;		
	}	
}