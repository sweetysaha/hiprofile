<?php

require(APPPATH.'/libraries/REST_Controller.php');
 
class Mapsearch extends REST_Controller{
	
	 public function __construct()
	{
		parent::__construct();

		$this->load->model('mapsearchapi_model');
		$this->load->model('userapi_model');
		$this->load->helper('date');
		$this->load->helper('email');
	}
	
	//API -  Send social media friend request
	function map_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;		
		$validates = array(
		"userID",
		"device_token",
		"device_id",
		"device_type",
		"latitude",
		"longitude",
		"desired_visibility",
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				/*To update lat and long*/
				$this->userapi_model->inserLatLong($data['latitude'], $data['longitude'], $data['userID']);
				$result = $this->mapsearchapi_model->loadMap($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	function mapSettings_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"device_token",
		"device_id",
		"device_type",
		"latitude",
		"longitude",
		"desired_visibility",
		"own_visibility",
		"approachability",
		"ghost_mode",				
		"friendsonlyvisible"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				/*To update lat and long*/
				$this->userapi_model->inserLatLong($data['latitude'], $data['longitude'], $data['userID']);
				$result = $this->mapsearchapi_model->updateMappSettings($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}	
	

}