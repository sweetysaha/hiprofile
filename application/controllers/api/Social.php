<?php


require(APPPATH.'/libraries/REST_Controller.php');

class Social extends REST_Controller{
    
    public function __construct()
    {
        parent::__construct();

        $this->load->model('socialapi_model');
		$this->load->model('userapi_model');
		$this->load->helper(array('date', 'url','email','path'));
    }

    //API -  Send social media friend request
	function socialFriendRequest_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"friend_user_id",
		"social_media",
		"device_token",
		"device_id",
		"device_type"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->sendFriendRequest($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  Send social media friend request
	function socialFriendRequestResponse_post()
	{
		
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"friend_user_id",
		"social_media",
		"device_token",
		"device_id",
		"device_type",
		"status"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->socialResponse($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  Send social media friend request cancel
	function socialFriendRequestCancel_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"friend_user_id",
		"social_media",
		"device_token",
		"device_id",
		"device_type"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->socialRequestCancel($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  To send all notification for the user
	function notificationList_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"device_token",
		"device_id",
		"device_type"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->notificationList($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  To send user friend request accept or not
	function friendRequestResponse_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"device_token",
		"device_id",
		"device_type"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->friendRequestResponse($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}

	/*10 may 2018*/
	
	//API -  To unfriend an user
	function unfriendUser_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"friend_user_id",
		"device_token",
		"device_id",
		"device_type"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->to_Unfirend_User($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	///API -  To Block and Ublock an user
	function blockUnblock_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"friend_user_id",
		"device_token",
		"device_id",
		"device_type"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->to_Block_Unblock_User($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  To Block The User for particulat time-frame
	function blockTimeFrame_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"friend_user_id",
		"device_token",
		"device_id",
		"device_type"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->to_blocKUserTimeFrame($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  To Block The User for particulat time-frame
	function unBlockTimeFrame_post()
	{
		$data = $this->post();
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->socialapi_model->to_unBlocKUserTimeFrame($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  To get the user firend list
	function getfirendlist_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"device_token",
		"device_id",
		"device_type"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->get_Friend_List($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  To get the user firend list
	function getblocklist_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validates = array(
		"userID",
		"device_token",
		"device_id",
		"device_type"
		);
		$this->validateparam($validates,$data);
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->get_Block_List($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}	
	/*function to sync and add the user facebook friend list*/
	function addfriendlist_post()	
	{	
		$data = $this->post();		
		$getCurrentMethod = $this->router->method;	
		$validates = array(	
		"userID",	
		"device_token",	
		"device_id",	
		"device_type",	
		"facebook_friends"	
		);		
		$this->validateparam($validates,$data);	
		if(!empty($data['api_key'])){	
			if($data['api_key'] == api_key){	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);	
				$result = $this->socialapi_model->addFriendList($data);	
				$this->response($result);	
			}		
			else{	
				$result = array();		
				$result['responsecode'] = "406";	
				$result['status'] = "Invalid API key.";	
				$this->response($result);	
			}		
		}		
		else{
			$result = array();		
			$result['responsecode'] = "405";	
			$result['status'] = "API key is missing.";		
			$this->response($result);	
		}		
		exit;	
	}	
	/*Function to send the user friend list*/	
	function getfriendlist_post()
	{	
		$data = $this->post();	
		$getCurrentMethod = $this->router->method;	
		$validates = array(	
		"userID",	
		"device_token",	
		"device_id",	
		"device_type"	
		);	
		$this->validateparam($validates,$data);	
		if(!empty($data['api_key'])){		
			if($data['api_key'] == api_key)
			{	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);				$result = $this->socialapi_model->getFriendList($data);	
				$this->response($result);		
			}		
			else
			{	
				$result = array();	
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";	
				$this->response($result);	
			}	
		}	
		else
		{
			$result = array();	
			$result['responsecode'] = "405";	
			$result['status'] = "API key is missing.";		
			$this->response($result);	
		}		
		exit;
	}
	
}