<?php

require(APPPATH.'/libraries/REST_Controller.php');
require(APPPATH.'../Twilio/Jwt/JWT.php'); 
require(APPPATH.'../Twilio/Rest/Api.php'); 

class User extends REST_Controller{
    
    public function __construct()
    {
        parent::__construct();

        $this->load->model('userapi_model');
        $this->load->model('socialapi_model');
		$this->load->helper(array('date', 'url','email','path'));
    }



    //API -  Check user login
	function userlogin_post()
	{
		//echo "hi";die;
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		if(isset($data['email'])):
		$validate = array(
		"email",
		"password",
		"device_token",
		"device_type",
		"device_id"
		);
			$this->validateparam($validate,$data);
			$this->validateparamemail($data['email']);
		else:
		$validates = array(
		"username",
		"password",
		"device_token",
		"device_type",
		"device_id"
		);
			$this->validateparam($validates,$data);
		endif;
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->userapi_model->checklogin($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  check user register
	function userregister_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
		"username", 
		"email",
		"name",
		"gender",
		"mobile",
		"password",
		"latitude",
		"longitude",
		"device_token",
		"device_type",
		"device_id"
		);
		$this->validateparam($validate,$data);
		$this->validateparamemail($data['email']);
		
		if(!empty($data['api_key']))
		{
			
			if($data['api_key'] == api_key)
			{
				$email_result = $this->userapi_model->checkemailexists($data['email']);
				$username_result = $this->userapi_model->checkuserexists($data['username']);
				$mobile_result = $this->userapi_model->checkusermobileexists($data['mobile']);
				
				if($email_result == 0 && $username_result == 0 && $mobile_result == 0)
				{
					$result = array();
					$result = $this->userapi_model->doregister($data);
					$this->response($result);
				}
				else if($email_result != 0 || $username_result != 0 || $mobile_result != 0)
				{
					
					$return_res = array();
					if($email_result != 0 && $username_result != 0 && $mobile_result != 0)
					{
						
						$return_res['responsecode'] = "201";
						$return_res['status'] = "Email ID / Username / Mobile already registered.";
						$this->response($return_res);
					}
					else if($email_result != 0)
					{
						
						$return_res['responsecode'] = "201";
						$return_res['status'] = "Email ID already registered.";
						$this->response($return_res);
					}
					else if($username_result != 0)
					{
						
						$return_res['responsecode'] = "202";
						$return_res['status'] = "Username already registered.";
						$this->response($return_res);
					}
					else if($mobile_result != 0)
					{
						
						$return_res['responsecode'] = "201";
						$return_res['status'] = "Mobile already registered.";
						$this->response($return_res);
					}
					
				}						
			}
			else
			{
				
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API -  check user register
	function forgotpassword_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$this->validateparam(array("email"),$data);
		$this->validateparamemail($data['email']);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$restrictEmail = $this->userapi_model->restrictEmail($data['email']);
				if(empty($restrictEmail))
				{
					$result = array();
					$result['responsecode'] = "500";
					$result['status'] = "Access Denied For The User";
					$this->response($result);
				}
				else
				{
					$email_result = $this->userapi_model->checkemailexists($data['email']);
					if($email_result != 0)
					{
						$encoded_email = urlencode($data['email']);
						$this->load->helper('string');
						$data['email'] = $data['email'];
						$data['activation_id'] = random_string('alnum',15);
						$data['createdDtm'] = date('Y-m-d H:i:s');
						$data['agent'] = getBrowserAgent();
						$data['client_ip'] = $this->get_client_ip_server();
						$save = $this->userapi_model->resetUserPassword($data); 
						if($save)
						{					
							$data1['reset_link'] = base_url() . "frgtpassword/" . $data['activation_id'] . "/" . $encoded_email;
							$userInfo = $this->userapi_model->getCustomerInfoByEmail($data['email']);

							if(!empty($userInfo))
							{
								$data1["name"] = $userInfo[0]->name;
								$data1["email"] = $userInfo[0]->email;
								$data1["message"] = "Reset Your Password";
								$sendStatus = resetPasswordEmail($data1);
								$return_res = array();
								if($sendStatus)
								{
									$return_res['responsecode'] = "200";
									$return_res['responsedetails'] = "Success";
									$this->response($return_res);
								} 
								else 
								{
									$return_res['responsecode'] = "500";
									$return_res['status'] = "Something went wrong. Please try again later.";
									$this->response($return_res);
								}
							}
							else 
							{
								$return_res['responsecode'] = "500";
								$return_res['status'] = "Something went wrong. Please try again later.";
								$this->response($return_res);
							}
						}
						else
						{
							$return_res['responsecode'] = "500";
							$return_res['status'] = "Something went wrong. Please try again later.";
							$this->response($return_res);
						}
					}
					else
					{
						$return_res['responsecode'] = "201";
						$return_res['status'] = "Your email is not yet registered with HiProfile";
						$this->response($return_res);
					}
				}
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	// API to update device token using its user id
	function updevicetkn_get()
	{
		$data = $this->get();
		$getCurrentMethod = $this->router->method;
		$validate = array(
		"userID",
		"device_token",
		"device_type",
		"device_id"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$result = $this->userapi_model->getupdateDeviceToken($data);
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	// API to update users device details to track in which devices the user is currently logged in
	function loadprofile2_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
		"userID",
		"device_token",
		"device_type",
		"device_id",
		"friend_user_id"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->userapi_model->getupdateDeviceDetails($data);
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}

	// API to update users device details to track in which devices the user is currently logged in
	function loadprofile_post()
	{
		//echo "hello";die;
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
		"userID",
		"device_token",
		"device_type",
		"device_id",
		"friend_user_id",
		//"friend_req_type",
		);
		$this->validateparam($validate,$data);
		//$result['friend_req_type']=$data['friend_req_type'];

		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->userapi_model->getupdateDeviceDetails($data);
				//print_r($result);die;
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	// API to Register and Authenticate in WD app using social 
	// This API will auto register the user in WD app and will login. 
	function socialsync_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
		"auth_provider",
		"userID",
		"device_token",
		"device_id",
		"device_type",
		"social_id",
		"name",
		"friendcount"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$result = $this->userapi_model->getsocialSyncinsert($data);
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}

	// API to update feedback to the user
	function feedback_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID",
				"device_token",
				"device_type",
				"device_id"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->userapi_model->getfeedback($data);
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	// API to changepassword to the user
	function changepassword_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array
		(
				"userID",
				"device_token",
				"device_type",
				"device_id"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$restrictId = $this->userapi_model->restrictID($data['userID']);
				if(empty($restrictId))
				{
					$result = array();
					$result['responsecode'] = "500";
					$result['status'] = "Access Denied For The User";
					$this->response($result);
				}
				else
				{
					$resultPas = $this->userapi_model->matchOldPassword($data['userID'],$data['current_password']);
					if(empty($resultPas))
					{
						$result = array();
						$result['responsecode'] = "201";
						$result['responsedetails'] = "Current password didn't match";
						$this->response($result);
					}
					else
					{
						$usersData = array('password'=>getHashedPassword($data['new_password']));
						$data = $this->userapi_model->getchangepassword($data['userID'], $usersData);
						if($data >= 0) 
						{
							$result = array();
							$result['responsecode'] = "200";
							$result['responsedetails'] = "Success";
							$this->response($result);
						}
						else 
						{ 
							$result = array();
							$result['responsecode'] = "500";
							$result['status'] = "Something went wrong. Please try again later.";
							$this->response($result);
						}
							
					}
				}
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}	
	
	//API TO update the user profile details
	function editprofile_post()
	{
            
		$data = $this->post();
                
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID",
				"device_token",
				"device_type",
				"device_id"
		);
		$this->validateparam($validate,$data);
		if($data['email'] != ""):
			$this->validateparamemail($data['email']);
		endif;
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$email_result = $this->userapi_model->checkemailexists($data['email']);
				if($email_result != 0)
				{
					$users_email_result = $this->userapi_model->checkuseremailexists($data['email'],$data['userID']);
					if($users_email_result != 0)
					{
						$result = array();
						$result = $this->userapi_model->geteditedprofile($data);
						$this->response($result); 
					}
					else
					{
						$result = array();
						$result['responsecode'] = "5001";
						$result['status'] = "Something went wrong. Please try again later.";
						$this->response($result);
					}
				}	
				else
				{
					/*Email */
					$encoded_email = urlencode(str_rot13($data['email']));
					$this->load->helper('string');
					$data['email'] = $data['email'];
					$data['activation_id'] = uniqid('email_', true);
					$data['createdDtm'] = date('Y-m-d H:i:s');
					$save = $this->userapi_model->sendVerificatinEmail($data['email'],$data['userID'],$data['activation_id']);
					if($save)
					{					
						$data1['reset_link'] = base_url() . "verificationemail/" . $data['activation_id'] . "/" . $encoded_email;
						$userInfo = $this->userapi_model->getCustomerInfoByEmail($data['email']);
						if(!empty($userInfo))
						{
							$data1["name"] = $userInfo[0]->name;
							$data1["email"] = $userInfo[0]->email;
							$data1["message"] = "Verify Your Email";
							$sendStatus = verifyEmail($data1);
							
							if($sendStatus)
							{
								$result = array();
								$result = $this->userapi_model->geteditedprofile($data);
								$this->response($result); 
							} 
							else 
							{
								$result = array();
								$result['responsecode'] = "5002";
								$result['status'] = "Something went wrong. Please try again later.";
								$this->response($result);
							}
						}
						else 
						{
							$result = array();
							$result['responsecode'] = "5003";
							$result['status'] = "Something went wrong. Please try again later.";
							$this->response($result);
						}
					} 
					else 
					{	
						$result = array();
						$result['responsecode'] = "5004";
						$result['status'] = "Something went wrong. Please try again later.";
						$this->response($result);
					}
				}
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API TO update the user profile details
	function editprofileimage_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID",
				"device_token",
				"device_type",
				"device_id"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$img = $this->getimagedecode($data['profile_pic_base64']);
				//$img = $this->getimagedecode(base64image);
				$old = umask(0);
				$path = set_realpath('assets/userprofile/images');
				if(!file_put_contents($path. $img['filename'], $img['image']))
				{
					umask($old);
					$result = array();
					$result['responsecode'] = '501';
					$result['status'] = 'Image not Uploaded. Please try again.';
					$this->response($result);
					exit;
				}
				else
				{
					chmod($img['filename'], 0777);
					umask($old);
					$data['profile_pic_base64'] = $img['filename'];
					$result = $this->userapi_model->editprofileimg($data);
					$this->response($result);
				}
				
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	// API to show points of user friends
	function points_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID",
				"device_token",
				"device_type",
				"device_id"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->userapi_model->getShowpoints($data);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//To generate JWT token for OTP
	public function requestOTP_post()
	{
		$data = $this->post();
		$validate = array(
				"username",
				"email",
				"mobilenumber",
				"device_type",
				"device_id"
		);
		$this->validateparam($validate,$data);
		if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) 
		{
		  $response['responsecode'] = '201';
		  $response['error'] = 'Email is Invalid';
          $this->response($response);
		  exit;
		}
		else if(!is_numeric(trim($data['mobilenumber']))) 
		{
			$response['responsecode'] = '201';
			$response['error'] = 'Mobile Number is Invalid';
			$this->response($response);
			exit;
		}	 
		else 
		{
			if(!empty($data['api_key']))
			{
				if($data['api_key'] == api_key)
				{
					$mobile_result = $this->userapi_model->checkmobileexists($data['mobilenumber']);
					if($mobile_result != 0)
					{
						$return_res = array();
						$return_res['responsecode'] = "202";
						$return_res['error'] = "mobile number already exists with another user";
						$this->response($return_res);
					}
					else
					{
						//$result = array();
						// Use the REST API Client to make requests to the Twilio REST API
						$secretkey = 'WjFcI1CWdxg6Gl7odjCz0rKCOZDWdU4B';
						$app_id = "99985";
						$phone_number = $data['mobilenumber'];
						$iat = time();
						$payload = array(
						"app_id"=>$app_id,
						"phone_number"=>$phone_number,
						"iat"=>$iat
						);
						$jwtlib = new Twilio\Jwt\Jwt();
						$jwt_token = $jwtlib->encode($payload, $secretkey, "HS256");
						$return_res = array();
						$return_res['responsecode'] = "200";
						$return_res['status'] = "success";
						$return_res['jwt_token'] =  $jwt_token;
						$this->response($return_res);
						//$result = $this->userapi_model->getJwtToken($data);
						//echo json_encode($result);
					}
				}
				else
				{
					$result = array();
					$result['responsecode'] = "406";
					$result['error'] = "Invalid API key.";
					$this->response($result);
				}
			}
			else
			{
				$result = array();
				$result['responsecode'] = "405";
				$result['error'] = "API key is missing.";
				$this->response($result);
			}
		}
		exit;
	}
	
	// API to get recent activity using its user id
	function recentActivity_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
		"userID",
		"device_token",
		"device_type",
		"device_id"
		);

		$this->validateparam($validate,$data);

		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				//echo "hi";die;
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);

				$result = $this->userapi_model->getactivity($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['error'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['error'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	//API TO update the user profile details
	function emailverification_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID",
				"device_token",
				"device_type",
				"device_id"
		);
		$this->validateparam($validate,$data);
		if($data['email'] != ""):
			$this->validateparamemail($data['email']);
		endif;
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				/*Email */
				$encoded_id = urlencode(str_rot13($data['userID']));
				$this->load->helper('string');
				$data['email'] = $data['email'];
				$data['activation_id'] = uniqid('email_', true);
				$data['createdDtm'] = date('Y-m-d H:i:s');
				$save = $this->userapi_model->sendVerificatinEmailbyid($data['userID'],$data['activation_id']);
				if($save)
				{					
					$data1['reset_link'] = base_url() . "verificationemailbyid/" . $data['activation_id'] . "/" . $encoded_id;
					$userInfo = $this->userapi_model->getCustomerInfoById($data['userID']);
					if(!empty($userInfo))
					{
						$data1["name"] = $userInfo[0]->name;
						$data1["email"] = $userInfo[0]->email;
						$data1["message"] = "Verify Your Email";
						$sendStatus = verifyEmail($data1);
						
						if($sendStatus)
						{
							$result = array();
							$result['responsecode'] = "200";
							$result['status'] = "Success";
							$this->response($result);
						} 
						else 
						{
							$result = array();
							$result['responsecode'] = "500";
							$result['status'] = "Something went wrong. Please try again later.";
							$this->response($result);
						}
					}
					else 
					{
						$result = array();
						$result['responsecode'] = "500";
						$result['status'] = "Something went wrong. Please try again later.";
						$this->response($result);
					}
				}
				else
				{
					$result = array();
					$result['responsecode'] = "500";
					$result['status'] = "Something went wrong. Please try again later.";
					$this->response($result);
				}
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	// API to update the approachability using the user id for the user
	function updateapproach_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
		"userID",
		"device_token",
		"device_type",
		"device_id"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->userapi_model->toUpdateApproachability($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	/*******************************************************************/	
	//API -  To check user social Login
	function socialLogin_post()
	{
		$data = $this->post();
		$id = "";
		$validate = array(
		"auth_provider",
		"device_token",
		"device_id",
		"device_type",
		"social_id",
		"name",
		"friendcount",
		"name"
		);
		$this->validateparam($validates,$data);
		if($data['email'] != ""):
			$this->validateparamemail($data['email']);
		endif;
		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){

				if($data['email']!="")
				{
					$id = $this->userapi_model->socialcheckuseremailexists($data['email']);
				}
				elseif($data['mobile']!="")
				{
					$id = $this->userapi_model->socialcheckusermobileexists($data['mobile']);
				}
				else
				{
					$id	= $this->userapi_model->checksocialid($data['social_id'],$data['auth_provider']);
				}
				if($id == 0)
				{
					$result = array();
					$result['responsecode'] = "200";
					$result['responsedetails'] = "Success";
					$result['already_registered'] = "false";
					$result['user'] = null;
					$this->response($result);
				}
				else
				{
					$result = array();
					$result = $this->userapi_model->getsocialLogin($data,$id);
					$this->response($result);
				}
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
	
	
	//API -  check user social register
	function socialregister_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
		"auth_provider", 
		"device_token",
		"device_id",
		"device_type",
		"social_id",
		"name",
		"friendcount",
		"email",
		"username",
		"name",
		"gender",
		"mobile",
		"password",
		"latitude",
		"longitude"
		);
		$this->validateparam($validate,$data);
		$this->validateparamemail($data['email']);
		
		if(!empty($data['api_key']))
		{
			
			if($data['api_key'] == api_key)
			{
				$email_result = $this->userapi_model->checkemailexists($data['email']);
				$username_result = $this->userapi_model->checkuserexists($data['username']);
				$mobile_result = $this->userapi_model->checkusermobileexists($data['mobile']);
				
				if($email_result == 0 && $username_result == 0 && $mobile_result == 0)
				{
					$result = array();
					$result = $this->userapi_model->getsocialRegister($data);
					$this->response($result);
				}
				else if($email_result != 0 || $username_result != 0 || $mobile_result != 0)
				{
					
					$return_res = array();
					if($email_result != 0 && $username_result != 0 && $mobile_result != 0)
					{
						
						$return_res['responsecode'] = "201";
						$return_res['status'] = "Email ID / Username / Mobile already registered.";
						$this->response($return_res);
					}
					else if($email_result != 0)
					{
						
						$return_res['responsecode'] = "201";
						$return_res['status'] = "Email ID already registered.";
						$this->response($return_res);
					}
					else if($username_result != 0)
					{
						
						$return_res['responsecode'] = "202";
						$return_res['status'] = "Username already registered.";
						$this->response($return_res);
					}
					else if($mobile_result != 0)
					{
						
						$return_res['responsecode'] = "201";
						$return_res['status'] = "Mobile already registered.";
						$this->response($return_res);
					}
					
				}
			}
			else
			{
				
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit; 
	}

	//API TO upload user's six photos  pic
	function uploadsixphotos_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID",
				"device_token",
				"device_type",
				"device_id",
				"image_position"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$img = $this->getimagedecode($data['image_base64']);				
				//$img = $this->getimagedecode(base64image);
				$old = umask(0);
				$path = set_realpath('assets/userprofile/six_photos');
				if(!file_put_contents($path. $img['filename'], $img['image']))
				{
					umask($old);
					$result = array();
					$result['responsecode'] = '501';
					$result['status'] = 'Image not Uploaded. Please try again.';
					$this->response($result);
					exit;
				}
				else
				{
					chmod($img['filename'], 0777);
					umask($old);
					$data['image_base64'] = $img['filename'];
					$result = $this->userapi_model->uploadsiximg($data);
					$this->response($result);
				}
				
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}



	//API TO set favorite six photo
	function favsixphoto_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID",
				"device_token",
				"device_type",
				"device_id",
				"image_id",
				"isFavourite",
				"friend_user_id"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->userapi_model->sixPhotosFavourite($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}


	//API TO set favorite list of six photo
	function favlistsixphoto_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID",
				"device_token",
				"device_type",
				"device_id",
				"image_id"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->socialapi_model->favListSixPhotos($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}

	// API to update the six photos is seen or not 
	function isseensixphotos_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(		
		"device_token",
		"device_type",
		"device_id",		
		"image_id",
		"isSeen"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->userapi_model->updateSixPhotosIsSeen($data);
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}

	// API to to update rank/double points 
	function watchadds_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
		"userID",
		"device_token",
		"device_type",
		"device_id",
		"is_package"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->userapi_model->updateWatchAdds($data);
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
        
        // API to get top 100 rank users
	function topranking_post()
	{
		$data = $this->post();
//                echo "<pre>";
//                print_r($data);die;
		$getCurrentMethod = $this->router->method;
		$validate = array(		
                    "device_token",
                    "device_type",
                    "device_id"
		);
		$this->validateparam($validate,$data);

		if(!empty($data['api_key'])){
			if($data['api_key'] == api_key){
				$result = $this->userapi_model->getTopHundredRankUsers();				
				$this->response($result);
			}
			else{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;
	}
}