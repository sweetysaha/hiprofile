<?php

require(APPPATH.'/libraries/REST_Controller.php');
 
class Boost extends REST_Controller{
	
	 public function __construct()
	{
		parent::__construct();

		$this->load->model('boostapi_model');
		$this->load->model('userapi_model');
		$this->load->helper('date');
		$this->load->helper('email');
	}
	
	//API -  To get all the rank upgrade Plans
	function getRankPlans_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID", 
				"device_token",
				"device_id",
				"device_type"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->boostapi_model->fetchRankPlans($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;		
	}	
	
	//API -  To get all the double points upgrade Plans
	function getDoublePointsPlans_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID", 
				"device_token",
				"device_id",
				"device_type"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->boostapi_model->fetchDoublePointsPlans($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;		
	}
	
	//API -  To get all the purchases points and rank upgrade Plans
	function getPurchasedPlans_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID", 
				"device_token",
				"device_id",
				"device_type",
				"product_id",
				"product_description",
				"buying_timestamp",
				"orderId",
				"category",
				"store",
				"localised_price"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->boostapi_model->saveHfDf($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;		
	}
        
        //API -  To get all the purchases points and rank upgrade Plans
	function getPurchasedFrameRank_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID", 
				"device_token",
				"device_id",
				"device_type",
				"product_id",
				"product_description",
				"buying_timestamp",
				"orderId",
				"category",
				"store",
				"localised_price"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->boostapi_model->saveFrameRank($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;		
	}
	
	//API -  To get all the user rank upgrade Plans
	function getUserRankPlans_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID", 
				"device_token",
				"device_id",
				"device_type"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->boostapi_model->userRankPlans($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;		
	}
	
	//API -  To get all the user points upgrade Plans
	function getUserPointsPlans_post()
	{
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID", 
				"device_token",
				"device_id",
				"device_type"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->boostapi_model->userPointsPlans($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;		
	}
	
	//API -  To get all the active upgrade Plans
	function getActivatePlans_post()
	{
		
		$data = $this->post();
		$getCurrentMethod = $this->router->method;
		$validate = array(
				"userID", 
				"device_token",
				"device_id",
				"device_type",
				//"product_id",
				"category",
				"is_free_pcakage",
				"orderId"
		);
		$this->validateparam($validate,$data);
		if(!empty($data['api_key']))
		{
			if($data['api_key'] == api_key)
			{	
				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);
				$result = $this->boostapi_model->toActivatePlans($data);
				$this->response($result);
			}
			else
			{
				$result = array();
				$result['responsecode'] = "406";
				$result['status'] = "Invalid API key.";
				$this->response($result);
			}
		}
		else
		{
			$result = array();
			$result['responsecode'] = "405";
			$result['status'] = "API key is missing.";
			$this->response($result);
		}
		exit;		
	}
}