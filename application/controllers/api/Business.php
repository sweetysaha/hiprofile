<?php



require(APPPATH.'/libraries/REST_Controller.php');

 

class Business extends REST_Controller{

	

	 public function __construct()

	{

		parent::__construct();



		$this->load->model('businessapi_model');

		$this->load->model('userapi_model');

		$this->load->helper('date');

		$this->load->helper(array('date', 'url','email','path'));

	}

	

	//API -  Check add business

	function addbusiness_post()

	{

		$data = $this->post();

		$getCurrentMethod = $this->router->method;

		$validate = array(

				"userID", 

				"device_token",

				"device_id",

				"device_type"

		);

		$this->validateparam($validate,$data);

		$this->validateparamemail($data['email']);

		$address = $data['country'].','.$data['state'].','.$data['pin'];

		$res = $this->getCoordinates($address);
		
		
		$data['latitude'] = $res['lat'];

		$data['longitude'] = $res['lng'];

		if(!empty($data['api_key']))

		{

			if($data['api_key'] == api_key)

			{

				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);

				$img = $this->getimagedecode($data['image_base64']);

				//$img = $this->getimagedecode(base64image);

				$path = set_realpath('assets/business/images');

				if(!file_put_contents($path. $img['filename'], $img['image']))

				{

					$result = array();

					$result['responsecode'] = '501';

					$result['status'] = 'Image not Uploaded. Please try again.';

					$this->response($result);

					exit;

				}

				else

				{

					$data['image_base64'] = $img['filename'];

					$result = $this->businessapi_model->addbusiness($data);

					$this->response($result);

				}

			}

			else

			{

				$result = array();

				$result['responsecode'] = "406";

				$result['status'] = "Invalid API key.";

				$this->response($result);

			}

		}

		else

		{

			$result = array();

			$result['responsecode'] = "405";

			$result['status'] = "API key is missing.";

			$this->response($result);

		}

		exit;		

	}

	

	//API -  Check user business details

	function mybusiness_post()

	{

		$data = $this->post();

		$getCurrentMethod = $this->router->method;

		$validate = array(

				"userID", 

				"device_token",

				"device_id",

				"device_type"

		);

		$this->validateparam($validate,$data);

		if(!empty($data['api_key']))

		{

			if($data['api_key'] == api_key)

			{	

				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);

				$result = $this->businessapi_model->getmybusiness($data);

				$this->response($result);

			}

			else

			{

				$result = array();

				$result['responsecode'] = "406";

				$result['status'] = "Invalid API key.";

				$this->response($result);

			}

		}

		else

		{

			$result = array();

			$result['responsecode'] = "405";

			$result['status'] = "API key is missing.";

			$this->response($result);

		}

		exit;		

	}

	

	

	//API -  Check user has and edit business details

	function editbusiness_post()

	{

		$data = $this->post();

		$getCurrentMethod = $this->router->method;

		$validate = array(

				"userID", 

				"device_token",

				"device_id",

				"device_type",

				"business_id"

		);

		$this->validateparam($validate,$data);

		$this->validateparamemail($data['email']);

		$address = $data['country'].','.$data['state'].','.$data['pin'];

		$res = $this->getCoordinates($address);

		echo $data['latitude'] = $res['lat'];

		echo $data['longitude'] = $res['lng'];
		die;

		if(!empty($data['api_key']))

		{

			if($data['api_key'] == api_key)

			{	

				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);

				if($data['image_base64'] != "")

				{

					$img = $this->getimagedecode($data['image_base64']);

					//$img = $this->getimagedecode(base64image);

					$path = set_realpath('assets/business/images');

					if(!file_put_contents($path. $img['filename'], $img['image']))

					{

						$result = array();

						$result['responsecode'] = '501';

						$result['status'] = 'Image not Uploaded. Please try again.';

						$this->response($result);

						exit;

					}

					else

					{

						$data['image_base64'] = $img['filename'];

						$result = $this->businessapi_model->geteditbusiness($data);

						$this->response($result);

					}

					

				}

				else

				{

					$result = $this->businessapi_model->geteditbusiness($data);

					$this->response($result);

				}

				

			}

			else

			{

				$result = array();

				$result['responsecode'] = "406";

				$result['status'] = "Invalid API key.";

				$this->response($result);

			}

		}

		else

		{

			$result = array();

			$result['responsecode'] = "405";

			$result['status'] = "API key is missing.";

			$this->response($result);

		}

		exit;		

	}

	

	//API -  Display all the categories for the Business

	function showcategories_post()

	{

		$data = $this->post();

		$getCurrentMethod = $this->router->method;

		$validate = array(

				"userID", 

				"device_token",

				"device_id",

				"device_type"

		);

		$this->validateparam($validate,$data);

		if(!empty($data['api_key']))

		{

			if($data['api_key'] == api_key)

			{	

				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);

				$result = $this->businessapi_model->getBusinessCategoryList($data);

				$this->response($result);

			}

			else

			{

				$result = array();

				$result['responsecode'] = "406";

				$result['status'] = "Invalid API key.";

				$this->response($result);

			}

		}

		else

		{

			$result = array();

			$result['responsecode'] = "405";

			$result['status'] = "API key is missing.";

			$this->response($result);

		}

		exit;		

	}

	

	//API -  Business List for the userID with pagination support 

	function businesslist_post()

	{

		$data = $this->post();

		$getCurrentMethod = $this->router->method;

		$validate = array(

				"userID", 

				"device_token",

				"device_id",

				"device_type",

				"latitude",

				"longitude",

		);

		$this->validateparam($validate,$data);

		if(!empty($data['api_key']))

		{

			if($data['api_key'] == api_key)

			{	

				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);

				/*To update lat and long*/

				$this->userapi_model->inserLatLong($data['latitude'], $data['longitude'], $data['userID']);

				$result = $this->businessapi_model->getBusinessList($data);

				$this->response($result);

			}

			else

			{

				$result = array();

				$result['responsecode'] = "406";

				$result['status'] = "Invalid API key.";

				$this->response($result);

			}

		}

		else

		{

			$result = array();

			$result['responsecode'] = "405";

			$result['status'] = "API key is missing.";

			$this->response($result);

		}

		exit;		

	}



	//Add rating for business from userid and business Id

	function rating_post()

	{

		$data = $this->post();

		$getCurrentMethod = $this->router->method;

		$validate = array(

				"userID", 

				"device_token",

				"device_id",

				"device_type",

				"rating",

				"business_id"

		);

		$this->validateparam($validate,$data);

		if(!empty($data['api_key']))

		{

			if($data['api_key'] == api_key)

			{	

				$last_seen_date =   $this->userapi_model->insertLastSeenRecords($getCurrentMethod,$data['userID']);

				$result = $this->businessapi_model->addrating($data);

				$this->response($result);

			}

			else

			{

				$result = array();

				$result['responsecode'] = "406";

				$result['status'] = "Invalid API key.";

				$this->response($result);

			}

		}

		else

		{

			$result = array();

			$result['responsecode'] = "405";

			$result['status'] = "API key is missing.";

			$this->response($result);

		}

		exit;		

	}	

}