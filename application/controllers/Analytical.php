<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class : Login (LoginController)
 * Notification class to control to all the contents.
 */
require APPPATH . '/libraries/BaseController.php';

class Analytical extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('analytical_model');
        $this->isLoggedIn();   
		$this->load->helper('upload','date', 'url','email','path'); 
    }
    /**
     * This function used to load the first screen of the content
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Hiprofile : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the Notification list
     */
    function analyticalReport()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$this->global['pageTitle'] = 'HiProfile : View Analytical Reports';
            $this->loadViews("analyticalreport",$this->global,NULL, NULL);
        }
    }
	function generateReport()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			 $this->load->library('form_validation');
                      			
            $this->form_validation->set_rules('fromdate','From Date','required|xss_clean');
			$this->form_validation->set_rules('todate','To Date','required|xss_clean');
			$this->form_validation->set_rules('selectSection','Report Selection','required|xss_clean');		
            
            if($this->form_validation->run() == FALSE)
            {
                $this->analyticalReport();
            }
            else
            {
				$fdate = $this->input->post('fromdate');
				$tdate = $this->input->post('todate');
				$fromdate = date($fdate).' '.'00:00:00';
				$todate = date($tdate).' '.'23:59:59';
				$selectSection = $this->input->post('selectSection');
				$gchart = $this->input->post('generatechart');
				$data['results'] = $this->analytical_model->generatePdf($fromdate,$todate,$selectSection);
				$data['selectSection'] = $selectSection;
				if(isset($gchart) && $gchart == 'on' && $gchart!=""){
					$data['img'] = $this->input->post('chartimgurl');
				}
				$this->load->helper('url'); 
				$html = $this->load->view("pdfgeneration",$data,true);	
				$this->load->library('Pdf');
				$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
				if($selectSection == 1):
					$pdf->SetTitle('User Analytical Report');
				elseif($selectSection == 2):
					$pdf->SetTitle('Businessess Analytical Report');
				elseif($selectSection == 3):
					$pdf->SetTitle('Boosted User Analytical Report');
				elseif($selectSection == 4):
					$pdf->SetTitle('User Upgraded Analytical Report');
				elseif($selectSection == 5):
					$pdf->SetTitle('Friends Analytical Report');
				endif;
				
				// set default header data
				//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

				// set header and footer fonts
				$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
				$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

				// set default monospaced font
				$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
				$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

				// set auto page breaks
				$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

				// set image scale factor
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

				// set font
				$pdf->SetFont('helvetica', 'B', 20);
				$pdf->SetAuthor('HiProfile');
				$pdf->SetDisplayMode('real', 'default');
				$pdf->AddPage();
				$pdf->writeHTML($html, true, 0, true, 0);
				$pdf->lastPage();
				ob_end_clean();
				if($selectSection == 1):
					$pdf->Output('user_analytical_report.pdf', 'D');
				elseif($selectSection == 2):
					$pdf->Output('businessess_analytical_report.pdf', 'D');
				elseif($selectSection == 3):
					$pdf->Output('boosted_user_analytica_report.pdf', 'D');
				elseif($selectSection == 4):
					$pdf->Output('user_upgraded_analytical_report.pdf', 'D');
				elseif($selectSection == 5):
					$pdf->Output('friends_analytical_report.pdf', 'D');
				endif;
			}
        }
    }
	
	function selectedChart()
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$fromdate = $_POST['fromdate'].' '.'00:00:00';
			$todate = $_POST['todate'].' '.'23:59:59';
			$optionvalue = $_POST['optionvalue'];
			$data = $this->analytical_model->generateChart($fromdate,$todate,$optionvalue);
			print_r(json_encode($data, true));
		}
		else
		{
			redirect('https://www.google.com');
		}
    }
	
	function viewReport()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$fromdate = $_POST['fromdate'].' '.'00:00:00';
			$todate = $_POST['todate'].' '.'23:59:59';
			$optionvalue = $_POST['optionvalue'];
			$data['selectSection'] = $optionvalue;
			$data['results'] = $this->analytical_model->viewReport($fromdate,$todate,$optionvalue);
			$html = $this->load->view("viewReport",$data,true);	
			echo $html;

		}
		else
		{
			redirect('https://www.google.com');
		}
	}
	
	function chartimagegenerate()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$imagesrc = $_POST['srctext'];
			
			if(!empty($imagesrc))
			{
				$imgData = $_POST['srctext'] ;
				$image_parts = explode(";base64,", $imgData);
				$image_type_aux = explode("image/", $image_parts[0]);
				$image_type = $image_type_aux[1];
				$image_base64 = base64_decode($image_parts[1]);	
				
				$file =  $base_url.'charimg/'.uniqid() . '.png';
				$old = umask(0);
				$success = file_put_contents($file, $image_base64);
				chmod($file, 0777);
				umask($old);
				//print $success ? $file : 'Unable to save the file.';
				echo $file;
			}
			else
			{
				echo "error";
			}
		}
		else
		{
			redirect('https://www.google.com');
		}
	}
}