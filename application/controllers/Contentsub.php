<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class : Login (LoginController)
 * Content class to control to all the contents.
 */
require APPPATH . '/libraries/BaseController.php';

class Contentsub extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contentsub_model');
        $this->isLoggedIn();   
		$this->load->helper('url');
    }
    
    /**
     * This function used to load the first screen of the content
     */
    public function index()
    {
        $this->global['pageTitle'] = 'HiProfile : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the content list
     */
    function subContentListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$this->load->library('pagination');
			$pid = $this->uri->segment('3');
		$newpage = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;
			$data['pid'] = $this->uri->segment('3');
            $this->load->model('contentsub_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            
            
            $count = $this->contentsub_model->contentListingCount($searchText,$pid);

		$data['contentRecords'] = $this->contentsub_model->get_current_page_records($searchText,$pid, 5, $newpage*5);
			
			/* Start pagination config */
			$config['base_url'] = base_url() . "Contentsub/subContentListing/$pid/";
            $config['total_rows'] = $count;
            $config['per_page'] = 5;
			$config["uri_segment"] = 4;
			$config['num_links'] = 2;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";
            $this->pagination->initialize($config);
            //$data["links"] = $this->pagination->create_links();
			
			/* End pagination config */
			
            //echo "<pre>";print_r($data);exit;
            
            $this->global['pageTitle'] = 'HiProfile : Content Management';
            
            $this->loadViews("subContents", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function subAddNewContentForm()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['pid'] = $this->input->get('id', TRUE);
            $this->load->model('contentsub_model');
            
            $this->global['pageTitle'] = 'HiProfile : Add New Content';

            $this->loadViews("subAddNewContentForm", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to add new content to the system
     */
    function subAddNewContent()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {

            $this->load->library('form_validation');
            
           $this->form_validation->set_rules('contentTitle','Content Title','trim|required|xss_clean');
            
            $this->form_validation->set_rules('contentDesc','Content Description','required|xss_clean');
            if($this->form_validation->run() == FALSE)
            {
                $this->subAddNewContentForm();
            }
            else
            {
				$pid = $this->input->get('id', TRUE);
                $contenttitle = ucwords(strtolower($this->input->post('contentTitle')));
                $contentdesc = $this->input->post('contentDesc');
				$pid = $this->input->post('pid');
                 if(isset($_FILES['c_img']['name']) && $_FILES['c_img']['name'] != "")
				{
        
					$config['upload_path']   = './uploads/'; 
					$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
					$config['file_name'] = time().$_FILES['c_img']['name'];
					$this->load->library('upload', $config); 
					if ( ! $this->upload->do_upload('c_img')) 
					{
						//$error = array('error' => $this->upload->display_errors()); 
						$this->session->set_flashdata('error', 'Error while uploading the image. Please try again.');
						redirect('Contentsub/subAddNewContentForm?id='.$pid);
					}
					else
					{
						$data = array('upload_data' => $this->upload->data()); 
						$imagename = trim($data['upload_data']['file_name']);
						$contentInfo = array();
						$contentInfo = array('contentTitle'=>$contenttitle,'contentDesc'=>$contentdesc, 'createdDtm'=>date('Y-m-d H:i:s'),'imageurl'=>$imagename,'p_contentid'=>$pid);
                
						$this->load->model('contentsub_model');
						$result = $this->contentsub_model->addNewContent($contentInfo);
						
						if($result > 0)
						{
							if($pid == 4 ){
							$this->session->set_flashdata('success', 'New latest trends added successfully');
							}else{
								$this->session->set_flashdata('success', 'New health tips added successfully');
							}
						}
						else
						{
							if($pid == 4 ){
							$this->session->set_flashdata('error', 'Latest trends content creation failed');
							}else{
								$this->session->set_flashdata('error', 'Health tips content creation failed');
							}
							
						}
					}
				}
				else
				{
					
					$contentInfo = array();
					$contentInfo = array('contentTitle'=>$contenttitle,'contentDesc'=>$contentdesc, 'createdDtm'=>date('Y-m-d H:i:s'),'imageurl'=>"",'p_contentid'=>$pid);
			
					$this->load->model('contentsub_model');
					$result = $this->contentsub_model->addNewContent($contentInfo);
					
					if($result > 0)
					{
						if($pid == 4 ){
							$this->session->set_flashdata('success', 'New latest trends added successfully');
							}else{
								$this->session->set_flashdata('success', 'New health tips added successfully');
							}
					}
					else
					{
						if($pid == 4 ){
							$this->session->set_flashdata('error', 'Latest trends content creation failed');
							}else{
								$this->session->set_flashdata('error', 'Health tips content creation failed');
							}
					}
				}
                redirect('Contentsub/subContentListing/'.$pid);
            }
        }
    }

    
    /**
     * This function is used load content edit information
     * @param number $contentId : Optional : This is content id
     */
    function subEditOldContent($contentId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($contentId == null)
            {
                redirect('Contentsub/contentListing');
            }
            
            $data['contentInfo'] = $this->contentsub_model->getContentInfo($contentId);
            
            $this->global['pageTitle'] = 'HiProfile : Edit Content';
            
            $this->loadViews("subEditOldContent", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the content information
     */
    function subEditContent()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $contentId = $this->input->post('contentId');
            
             $this->form_validation->set_rules('contentTitle','Content Title','trim|required|xss_clean');
            $this->form_validation->set_rules('contentDesc','Content Description','required|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldContent($contentId);
            }
            else
            {
				if(isset($_FILES['c_img']['name']) && $_FILES['c_img']['name'] != "")
				{
        
					$config['upload_path']   = './uploads/'; 
					$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
					$config['file_name'] = time().$_FILES['c_img']['name'];
					$this->load->library('upload', $config); 
					if ( ! $this->upload->do_upload('c_img')) 
					{
						//$error = array('error' => $this->upload->display_errors()); 
						$this->session->set_flashdata('error', 'Error while uploading the image. Please try again.');
						redirect('Contentsub/subContentListing/'.$result['result'][0]->pid);
					}
					else 
					{ 
						$data = array('upload_data' => $this->upload->data()); 
						$imagename = $data['upload_data']['file_name'];
						$contenttitle = $this->input->post('contentTitle');
						$contentdesc = $this->input->post('contentDesc');
						$contentInfo = array();
						$contentInfo = array('contentTitle'=>$contenttitle, 'contentDesc'=>$contentdesc,'imageurl'=>$imagename,'updatedDtm'=>date('Y-m-d H:i:s'));
						
						$result = $this->contentsub_model->editContent($contentInfo, $contentId);
						
						if($result['results'] == true)
						{
							if($result['result'][0]->pid == 4 ){
							$this->session->set_flashdata('success', 'New latest trends updated successfully');
							}else{
								$this->session->set_flashdata('success', 'New health tips updated successfully');
							}
						}
						else
						{
							if($result['result'][0]->pid == 4 ){
							$this->session->set_flashdata('error', 'Latest trends content creation failed');
							}else{
								$this->session->set_flashdata('error', 'Health tips content creation failed');
							}
						}
					} 
				}
				else
				{	
					$imgval = $this->contentsub_model->checkImageExists($contentId);
					$img = ($imgval != 0) ? $imgval : "";
					$contenttitle = $this->input->post('contentTitle');
					$contentdesc = $this->input->post('contentDesc');
					$contentInfo = array();
					$contentInfo = array('contentTitle'=>$contenttitle, 'contentDesc'=>$contentdesc,'imageurl'=>$img,'updatedDtm'=>date('Y-m-d H:i:s'));
					
					$result = $this->contentsub_model->editContent($contentInfo, $contentId);
					
					if($result['results'] == true)
					{
						if($result['result'][0]->pid == 4 ){
							$this->session->set_flashdata('success', 'New latest trends added successfully');
							}else{
								$this->session->set_flashdata('success', 'New health tips added successfully');
							};
					}
					else
					{
						if($result['result'][0]->pid == 4 ){
							$this->session->set_flashdata('error', 'Latest trends content creation failed');
							}else{
								$this->session->set_flashdata('error', 'Health tips content creation failed');
							}
					
					} 
				}
                redirect('Contentsub/subContentListing/'.$result['result'][0]->pid);
            }
        }
    }
	
	/**
     * This function is used load content edit information
     * @param number $contentId : Optional : This is content id
     */
    function editOldContent($contentId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($contentId == null)
            {
                redirect('contentListing');
            }
            
            $data['contentInfo'] = $this->content_model->getContentInfo($contentId);
            
            $this->global['pageTitle'] = 'Hiprofile : Edit Content';
            
            $this->loadViews("editOldContent", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to delete the content using contentId
     * @return boolean $result : TRUE / FALSE
     */
    function subDeleteContent()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $contentId = $this->input->post('contentId');
            $contentInfo = array('isDeleted'=>1,'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->contentsub_model->deleteContent($contentId, $contentInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	function subDeleteContentImage()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $contentId = $this->input->post('contentId');
            $contentInfo = array('imageurl'=>"",'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->contentsub_model->deleteContent($contentId, $contentInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'HiProfile : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
	
	
}

?>