<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class : Login (LoginController)
 * Notification class to control to all the contents.
 */
require APPPATH . '/libraries/BaseController.php';

class senderios extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('upload','date', 'url','email','path'); 
    }
    /**
     * This function used to load the first screen of the content
     */
    public function index()
    {
			//defined a new constant for IOS private key
			$passphrase = 'admin';
			$devicetoken = '8cc6295ebb3a150b49de841b0f8c9eae2608b6be6ce512be497eb84350c66e87';
			// Create the payload body			
			$message = 'praveen test ios push from live 28 august';
			$badge = 1;
			$sound = 'default';
			
			$payload = array();
			$payload['aps'] = array('alert' => $message, 'badge' => intval($badge), 'sound' => $sound,'category'=> 'friendRequestApproved','mutable-content'=> intval(1));
			$payload['payload'] = $result;

			// Encode the payload as JSON
			$payload = json_encode($payload);
			
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', trim($devicetoken)) . pack('n', strlen($payload)) . $payload;
			
			$apnsHost = 'gateway.sandbox.push.apple.com';
			$apnsPort = 2195;
			
			// .pem is your certificate file
			$apns_cert = APPPATH.'../assets/IOS/HiProfile_Dev_Push.pem'; 
			
			$streamContext  = stream_context_create();
			stream_context_set_option($streamContext , 'ssl', 'local_cert', $apns_cert);
			stream_context_set_option($streamContext , 'ssl', 'passphrase', $passphrase);
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
			if (!$fp){
				exit("Failed to connect: $error $errorString" . PHP_EOL);
			}
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			pre($result);
			// Close the connection to the server
			socket_close($fp);
			fclose($fp);
    }
}

?>