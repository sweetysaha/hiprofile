
<?php if(!defined('BASEPATH')) exit('No direct script access allowed');



/**

 * Class : Login (LoginController)

 * Login class to control to authenticate user credentials and starts user's session.

 */

class Login extends CI_Controller

{

    /**

     * This is default constructor of the class

     */

    public function __construct()

    {

        parent::__construct();
        //echo "hello"; die;

        $this->load->model('login_model');

		$this->load->helper('url');

    }



    /**

     * Index Page for this controller.

     */

    public function index()

    {

        $this->isLoggedIn();

    }

    

    /**

     * This function used to check the user is logged in or not

     */

    function isLoggedIn()

    {

        $isLoggedIn = $this->session->userdata('isLoggedIn');

        

        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)

        {

            $this->load->view('login');

        }

        else

        {

            redirect('/dashboard');

        }

    }

    

    

    /**

     * This function used to logged in user

     */

    public function loginMe()

    {

        $this->load->library('form_validation');

        

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');

        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');

        

        if($this->form_validation->run() == FALSE)

        {

            $this->index();

        }

        else

        {

            $email = $this->input->post('email');

            $password = $this->input->post('password');

            
            //echo "hi";die;
            $result = $this->login_model->loginMe($email, $password);



            if(count($result) > 0)

            {

				if(!empty($_POST["remember_me"])) 

				{

					setcookie ("ci_un",encryptCookie($email,encryption_decryption_key),time()+ 3600);

					setcookie ("ci_p",encryptCookie($password,encryption_decryption_key),time()+ 3600);

					setcookie ("ci_rm",encryptCookie(1,encryption_decryption_key),time()+ 3600);

				} 

				else

				{

					setcookie("ci_un","");

					setcookie("ci_p","");

					setcookie ("ci_rm","");

				}

                foreach ($result as $res)

                {

                    $sessionArray = array('userId'=>$res->userId,                    

                                            'role'=>$res->roleId,

											'superadmin'=>$res->super_admin_roleId,

                                            'roleText'=>$res->role,

                                            'name'=>$res->name,

                                            'isLoggedIn' => TRUE

                                    );

                                    

                    $this->session->set_userdata($sessionArray);

                    

                    redirect('/dashboard');

                }

            }

            else

            {

                $this->session->set_flashdata('error', 'Email or password mismatch');

                

                redirect('/login');

            }

        }

    }



    /**

     * This function used to load forgot password view

     */

    public function forgotPassword()

    {

        $this->load->view('forgotPassword');

    }

    

    /**

     * This function used to generate reset password request link

     */

    function resetPasswordUser()

    {

        $status = '';

        

        $this->load->library('form_validation');

        

        $this->form_validation->set_rules('login_email','Email','trim|required|valid_email|xss_clean');

                

        if($this->form_validation->run() == FALSE)

        {

            $this->forgotPassword();

        }

        else 

        {

            $email = $this->input->post('login_email');

            

            if($this->login_model->checkEmailExistforadmin($email))

            {

                $encoded_email = urlencode($email);

                

                $this->load->helper('string');

                $data['email'] = $email;

                $data['activation_id'] = random_string('alnum',15);

                $data['createdDtm'] = date('Y-m-d H:i:s');

                $data['agent'] = getBrowserAgent();

                $data['client_ip'] = $this->input->ip_address();

                

                $save = $this->login_model->resetPasswordUser($data);                

                

                if($save)

                {

                    $data1['reset_link'] = base_url() . "resetPasswordConfirmUser/" . $data['activation_id'] . "/" . $encoded_email;

                    $userInfo = $this->login_model->getCustomerInfoByEmail($email);



                    if(!empty($userInfo)){

                        $data1["name"] = $userInfo[0]->name;

                        $data1["email"] = $userInfo[0]->email;

                        $data1["message"] = "Reset Your Password";

                    }



                    $sendStatus = resetPasswordEmail($data1);



                    if($sendStatus){

                        $status = "send";

                        setFlashData($status, "Reset password link sent successfully, please check mails.");

                    } else {

                        $status = "notsend";

                        setFlashData($status, "Email has been failed, try again.");

                    }

                }

                else

                {

                    $status = 'unable';

                    setFlashData($status, "It seems an error while sending your details, try again.");

                }

            }

            else

            {

                $status = 'invalid';

                setFlashData($status, "This email is not registered with us.");

            }

            redirect('/forgotPassword');

        }

    }



    // This function used to reset the password 

    function resetPasswordConfirmUser($activation_id, $email)

    {

		

        // Get email and activation code from URL values at index 3-4

        $email = urldecode($email);



        // Check activation id in database

        $is_correct = $this->login_model->checkActivationDetails($email, $activation_id);

        

        $data['email'] = $email;

        $data['activation_code'] = $activation_id;

        

        //if ($is_correct == 1)

		if ($is_correct>=1)

        {

            $this->load->view('newPassword', $data);

        }

        else

        {

            redirect('/resetLinkExpired');

        }

    }

	

	//This function used to redirect to link expired page

    function linkexpired()

    {

        $this->load->view('resetLinkExpired');

    }

	

    //This function used to redirect to forgot password mobile user link expired page

    function forgotlinkexpired()

    {

        $this->load->view('passwordLinkExpired');

    }

	

    // This function used to create new password

    function createPasswordUser()

    {

        $status = '';

        $message = '';

        $email = $this->input->post("email");

        $activation_id = $this->input->post("activation_code");

        

        $this->load->library('form_validation');

        

        $this->form_validation->set_rules('password','Password','required|max_length[20]|min_length[6]');

        $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]|min_length[6]');

        

        if($this->form_validation->run() == FALSE)

        {

            $this->resetPasswordConfirmUser($activation_id, urlencode($email));

        }

        else

        {

            $password = $this->input->post('password');

            $cpassword = $this->input->post('cpassword');

            

            // Check activation id in database

            $is_correct = $this->login_model->checkActivationDetails($email, $activation_id);

            

            if($is_correct >= 1)

            {                

                $this->login_model->createPasswordUser($email, $password);

                

                $status = 'success';

                $message = 'Password changed successfully';

            }

            else

            {

                $status = 'error';

                $message = 'Password changed failed';

            }

            

            setFlashData($status, $message);



            redirect("/login");

        }

    }



	// This function used to reset the password for mobile

    function resetPasswordConfirmUserMob($activation_id, $email)

    {

		

        // Get email and activation code from URL values at index 3-4

        $email = urldecode($email);



        // Check activation id in database

        $is_correct = $this->login_model->checkActivationDetails($email, $activation_id);

        

        $data['email'] = $email;

        $data['activation_code'] = $activation_id;

        

        //if ($is_correct == 1)

		if ($is_correct>=1)

        {

            $this->load->view('mobilenewPassword', $data);

			

        }

        else

        {

            redirect('/forgotPasswordLinkexpired');

        }

    }

	

	// This function user to reset password for Mobileusers

	function createPasswordUserMobile()

    {

        $status = '';

        $message = '';

        $email = $this->input->post("email");

        $activation_id = $this->input->post("activation_code");

        

        $this->load->library('form_validation');

        

        $this->form_validation->set_rules('password','Password','required|max_length[20]|min_length[6]');

        $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');

        

        if($this->form_validation->run() == FALSE)

        {

            $this->resetPasswordConfirmUserMob($activation_id, urlencode($email));

        }

        else

        {

            $password = $this->input->post('password');

            $cpassword = $this->input->post('cpassword');

            

            // Check activation id in database

            $is_correct = $this->login_model->checkActivationDetails($email, $activation_id);

            

            if($is_correct >= 1)

            {                

                $this->login_model->createPasswordUser($email, $password);

                

                $status = 'success';

                $message = 'Password changed successfully';

            }

            else

            {

                $status = 'error';

                $message = 'Password changed failed';

            }

            

            setFlashData($status, $message);



            redirect("/message");

        }

    }



	//This function used to redirect to message page

    function message()

    {

        $this->load->view('message');

    }

	

	// This function user  Verify the Email 

    function verificationemail($activation_id="", $email="")

    {

        // Get email and activation code from URL values at index 3-4

		$email = urldecode(str_rot13($email));



        // Check activation id in database

        $is_correct = $this->login_model->updateEmailStatus($email, $activation_id);



        $data['email'] = $email;

        $data['activation_code'] = $activation_id;

        

        if ($is_correct == 1)

        {

			$data['success'] = 'success';

            $data['succmessage'] = 'Email successfully verified';

        }

        else

        {

			$data['error'] = 'error';

            $data['errmessage'] = 'Already verified this mail';

        }

		//setFlashData($status, $message);



        $this->load->view('emailsuccess',$data);

		//redirect("/emailsuccess");

    }

	

	// This function user  Verify the Email 

    function verificationemailbyid($activation_id="", $encoded_id="")

    {

        // Get email and activation code from URL values at index 3-4

		$userid = urldecode(str_rot13($encoded_id));



        // Check activation id in database

        $is_correct = $this->login_model->updateEmailStatusById($userid, $activation_id);



        $data['userid'] = $userid;

        $data['activation_code'] = $activation_id;

        

        if ($is_correct == 1)

        {

			$data['success'] = 'success';

            $data['succmessage'] = 'Email successfully verified';

        }

        else

        {

			$data['error'] = 'error';

            $data['errmessage'] = 'Already verified this mail';

        }

		//setFlashData($status, $message);



        $this->load->view('emailsuccess',$data);

		//redirect("/emailsuccess");

    }





}



?>