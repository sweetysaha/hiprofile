<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>HiProfile | Email Verification</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">

      </div><!-- /.login-logo -->
      <div class="login-box-body">
	  <center><img src="<?php echo base_url(); ?>assets/hiplogo.png" alt="HiProfileLogo" style=" width: 100px; height: 100px; "></center>
	  </br>
        <div class="row">
		
            <div class="col-md-12">
                <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
            </div>
        </div>
        <?php
        $this->load->helper('form');
        //$error = $this->session->flashdata('error');
        if(isset($error))
        {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> -->
                <?php echo $errmessage; ?>                    
            </div>
        <?php }
        //$success = $this->session->flashdata('success');
        if(isset($success))
        {
            ?>
            <div class="alert alert-success alert-dismissable">
                <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
                <?php echo $succmessage; ?>                    
            </div>
        <?php } ?>
        
      
        
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  </body>
</html>