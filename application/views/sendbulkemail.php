<div class="content-wrapper" >    

    <section class="content">

    

        <div class="row">

            <!-- left column -->

            <div class="col-md-12">

              <!-- general form elements -->

                <div class="box box-primary">

                    <div class="box-header" style="text-align:center;" >

                        <h3 class="box-title" >Notification Email</h3>

                    </div><!-- /.box-header -->

                    <!-- form start -->

                    

                    <form role="form" action="javascript:void(0);" method="post" id="editUser" role="form" name="userform">

                        <div class="box-body">

                            <div class="row">

                                <div class="col-md-12">                                

                                    <div class="form-group">

                                        <label for="notify_msg">Enter The Notification Message:</label>

                                        <textarea class="form-control required" name="notify_msg" id="notify_msg" rows="10" style="margin: 0px; width: 550px; height: auto;" required></textarea> 

                                    </div>

       

                                </div>

								<div class="selectedemail"></div>

								<div class="col-md-8">  

									                             

									<div class="form-group">

                                        <label for="useremail">Select Multiple Email:</label>

                                        <select class="form-control required" multiple="multiple" name="useremail" id="useremail" 

										onchange="getselectedval();" required 

										style="width: 400px;">

										<?php foreach($notify_email as $result): ?>



											<option value="<?php echo $result['email']; ?>"><?php echo $result['email']; ?></option>

										<?php endforeach;?>

										</select>

                                        <input type="hidden" id="selectedemail" class="selectedemail">

                                    </div>

                                </div>

                            </div> 

						</div>

                        <div class="box-footer">

                            <input type="submit" onclick="sendEmail()" class="btn btn-success" value="Send Email" />

                        </div>

                    </form>

                </div>

            </div>

        </div>    

    </section>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.min.css" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js" charset="utf-8"></script>

<script type="text/javascript">



function sendEmail() {

	

	var notfymsg = document.getElementById("notify_msg").value;

    var emailids = document.getElementById("selectedemail").value;

	if (!notfymsg.match(/\S/)) 

	{

		alert("Message field cannot be empty");

		return false;

	} 

	else

	{

		$.ajax({

			type: 'POST',

			url: baseURL+"sendBulkEmail",

			dataType: "html",

			data:{val:"sendmail",notfymsg:notfymsg,emailids:emailids},

			success: function (data) {

				$.fancybox.open(data);

			}

		});

	}

	

}

function getselectedval(){

	var str='';

	document.getElementById("selectedemail").value = "";

	var x=document.getElementById("useremail");

	for (i=0;i<x.length;i++) { 

	if(x[i].selected){

			str +=x[i].value + ","; 

		}

	} 

	document.getElementById("selectedemail").value = str;

	return true;

}



</script>