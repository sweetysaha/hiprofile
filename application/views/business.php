<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-suitcase"></i> Business Management
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>addNewBusinessForm"><i class="fa fa-plus"></i> Add New Business</a>
                </div>
            </div>
        </div>
		<div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Business List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>businessListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Sl.No</th>
                      <th>Business Title</th>
					  <th>Address</th>
					  <th>Contact No.</th>
					  <th>Opening Day</th>
					  <th>Opening Time</th>
					  <th>Category</th>
					  <th>Email</th>
					  <th>Business Icon</th>
					  <th>Uploaded By</th>
					  <th>Approval Status</th>

                      <th class="text-center">Actions</th>
                    </tr>
                   <?php
                    if(!empty($businessRecords))
                    {
						
						if($this->uri->segment(2))
						{
							$i = $this->uri->segment(2)+1;
						}
						else
						{
							$i = 1;
						}
						
                        foreach($businessRecords as $record)
                        {
                    ?>
                    <tr>
                      <td><?php echo $i; ?></td>

					   <td><?php echo $record->business_title ?></td>
                       <td><?php echo $record->address.', '.$record->city.', '.$record->state.', PIN-'.$record->pin.', '.$record->country; ?></td>
					   <td><?php echo $record->contact; ?></td>
					   <td><?php echo mb_substr($record->day_open_from, 0, 3).' - '.mb_substr($record->day_open_to, 0, 3); ?></td>
					   <td><?php echo $record->hours_open_from.' - '.$record->hours_open_to; ?></td>
					   <td><?php echo $record->primary_category; ?></td>
					   <td><?php echo $record->email; ?></td>
					   <?php if($record->image_url){?>
					   <td><img src="<?php echo base_url().'assets'.$record->image_url ?>" alt="<?php echo $record->business_title; ?>" style=" width: 100px; height: 100px; "></td>
					   <?php }else{?>
					   <td></td>
					   <?php } ?>
					   <td><?php echo $record->name; ?></td>
					   <?php
						$status = "";
						if($record->approved_status == 1){
							echo $status = '<td style="color:#08d208;">Approved</td>';
						}elseif($record->is_paid == 0){
							echo $status ='<td style="color:#dd4b39;">Payment Pending</td>';
						}elseif($record->approved_status == 0){
							echo $status = '<td style="color:#dd4b39;">Pending</td>';
						}
						?>
                      <td class="text-center">
                          <a class="btn btn-sm btn-info" title="EDIT" href="<?php echo base_url().'editOldBusiness/'.$record->business_id; ?>"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-sm btn-danger deleteBusiness" title="DELETE" href="#" data-businessid="<?php echo $record->business_id; ?>"><i class="fa fa-trash"></i></a>
					
                      </td>
                    </tr>
					<?php
						++$i;
                        }
                    }
                    ?>
                  </table>
            
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "businessListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
