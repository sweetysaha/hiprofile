<?php
$business_id = '';
$business_title = '';
$address = '';
$country = '';
$state = '';
$city = '';
$pin = '';
$contact = '';
$email = '';



$business_rating = '';
$primary_category = '';
$categories = '';
$day_open_from = '';
$day_open_to = '';
$hours_open_from = '';
$hours_open_to = '';
$image_url = '';
$userId = '';
$latitude = '';
$longitude = '';
$isDeleted = '';
$is_paid = '';
$approved_status = '';
$pmnt_amount = '';
$payment_ref = '';
if(!empty($businessInfo))
{
    foreach ($businessInfo as $uf)
    {
        $business_id = $uf->business_id;
        $business_title = $uf->business_title;
        $address = $uf->address.', '.$uf->city.', '.$uf->pin.', '.$uf->state.', '.$uf->country;
		$contact = $uf->contact;
		$email = $uf->email;
		$hours_open_from = $uf->hours_open_from.' - '.$uf->hours_open_to;
		$day_open_from = $uf->day_open_from.' - '.$uf->day_open_to;
		$image_url = $uf->image_url;
		$latitude = $uf->latitude;
		$longitude = $uf->longitude;
		$isDeleted = $uf->isDeleted;
		$is_paid = $uf->is_paid;
		$approved_status = $uf->approved_status;
		$pmnt_amount = $uf->pmnt_amount;
		$payment_ref = $uf->payment_ref;
    }
?>
<div class="container table-responsive">
  <center><h2>Business Details</h2></center>    
	<table class="table table-striped">
		<tbody>
			<tr>
				<td>Business Title</td>
				<td><?php echo $business_title; ?></td>
			</tr>
			<tr>
				<td>Address</td>
				<td><?php echo $address; ?></td>
			</tr>
			<tr>
				<td>Contact</td>
				<td><?php echo $contact; ?></td>
			</tr>
			<tr>
				<td>Business Email</td>
				<td><?php echo $email; ?></td>
			</tr>
			<tr>
				<td>Opening Hours</td>
				<td><?php echo $hours_open_from; ?></td>
			</tr>
			<tr>
				<td>Opening Days</td>
				<td><?php echo $day_open_from; ?></td>
			</tr>
			<tr>
				<td>Image</td>
				<td><img src="<?php echo base_url().'assets'.$image_url; ?>" alt="<?php echo $business_title; ?>" style=" width: 100px; height: 100px; "></td>
			</tr>
			<tr>
				<td>latitude</td>
				<td><?php echo $latitude; ?></td>
			</tr>
			<tr>
				<td>longitude</td>
				<td><?php echo $longitude; ?></td>
			</tr>
			<tr>
				<td>Approval Status</td>
				<?php if($approved_status == 1){
					echo '<td style="color:#08d208;">Approved</td>';
					}elseif($is_paid == 0){
					echo '<td style="color:#dd4b39;">Payment Pending</td>';
					}elseif($approved_status == 0){
					echo '<td style="color:#dd4b39;">Pending</td>';
					}
					?>
			</tr>
		</tbody>
	</table>
</div>
<?php
}
else
{
	echo "No Data Found..";
}
?>
<style>
.table-striped>tbody>tr:nth-child(odd)>td,
.table-striped>tbody>tr:nth-child(odd)>th {
	background-color: #dbe2e4;
}
</style>