<?php

$name = '';
$userId = '';
$product_title = '';
$product_description = '';
$category = '';
$is_activated = '';
$product_id = '';
$purchase_json = '';
$orderId = '';
$purchase_token = '';
$activation_time = '';
$expiration_time = '';
$purchased_date = '';
$status = '';

if(!empty($planInfo))
{
    foreach ($planInfo as $uf)
    {
		if($uf->is_activated == 0){
			$status = "<span style='color: #2315ff;'>Purchased</span>";
		} 
		elseif($uf->is_activated == 1){ 
			$status = "<span style='color: #27a000;'>Activated</span>";
		} 
		elseif($uf->is_activated == 2){ 
			$status =  "<span style='color: #ff0b0b;'>Expired</span>";
		}
        $userId = $uf->userId;
        $name = $uf->name;
        $product_title = $uf->product_title;
        $product_description = $uf->product_description;
		$category = $uf->category;
		$product_id = $uf->product_id;
		$purchase_json = $uf->purchase_json;
		$orderId = $uf->orderId;
		$st = $status;
		$purchase_token = $uf->purchase_token;
		$activation_time = $uf->activation_time;
		$expiration_time = $uf->expiration_time;
		$purchased_date = $uf->purchased_date;
    }
?>
<div class="container table-responsive">
  <center><h2>Purchased User Plan Details</h2></center>    
	<table class="table table-striped">
		<tbody>
			<tr>
				<td>Full Name</td>
				<td><?php echo $name; ?></td>
			</tr>
			<tr>
				<td>User Id</td>
				<td><?php echo $userId; ?></td>
			</tr>
			<tr>
				<td>Order Id</td>
				<td><?php echo $orderId; ?></td>
			</tr>
			<tr>
				<td>Category</td>
				<td><?php echo ($category ==  'hf') ? 'Higher Fame' : 'Double Points'; ?></td>
			</tr>
			<tr>
				<td>Product Id</td>
				<td><?php echo $product_id; ?></td>
			</tr>
			<tr>
				<td>Product Title</td>
				<td><?php echo $product_title; ?></td>
			</tr>
			<tr>
				<td>Product Description</td>
				<td><?php echo $product_description; ?></td>
			</tr>
			<tr>
				<td>Purchase Json</td>
				<td><?php echo $purchase_json; ?></td>
			</tr>
			<tr>
				<td>Status</td>
				<td><?php echo $st; ?></td>
			</tr>
			<tr>
				<td>Purchased Date and Time</td>
				<td><?php if(isset($purchased_date)){ echo date('d-M-Y h:i:s a', strtotime($purchased_date));} ?></td>
			</tr>
			<tr>
				<td>Activated Date and Time</td>
				<td><?php if(isset($activation_time)){ echo date('d-M-Y h:i:s a', strtotime($activation_time));} ?></td>
			</tr>
			<tr>
				<td>Expired Date and Time</td>
				<td><?php if(isset($expiration_time)){ echo date('d-M-Y h:i:s a', strtotime($expiration_time));} ?></td>
			</tr>
			
		</tbody>
	</table>
</div>
<?php
}
else
{
	echo "No Data Found..";
}
?>
<style>
.table-striped>tbody>tr:nth-child(odd)>td,
.table-striped>tbody>tr:nth-child(odd)>th {
	background-color: #dbe2e4;
}
</style>