<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-suitcase"></i> Business Management
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Business Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addBusiness" action="<?php echo base_url() ?>addNewBusiness" method="post" role="form" enctype="multipart/form-data" accept-charset="utf-8">
						<div class="box-body">
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="businessTitle">Business Title</label>
                                        <input type="text" class="form-control required" id="businessTitle" name="businessTitle" value="">
									</div> 
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessAddress">Business Address</label>
										<input type="text" class="form-control required" id="businessAddress"  name="businessAddress" value="">
									</div>
								</div>
                            </div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessCountry">Business Country</label>
										<input type="text" class="form-control required" id="businessCountry"  name="businessCountry" value="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesState">Business State</label>
										<input type="text" class="form-control required" id="businessState"  name="businessState" value="" >
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessCity">Business City </label>
										<input type="text" class="form-control required" id="businessCity"  name="businessCity" value="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="Content">Business PIN / ZIP Code</label>
										<input type="text" class="form-control required" id="businessPin"  name="businessPin" value="" >
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessContact ">Business Contact </label>
										<input type="text" class="form-control required" id="businessContact"  name="businessContact" value="" onkeypress="return isNumber(event);">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessEmail">Business Email </label>
										<input type="email" class="form-control required" id="businessEmail"  name="businessEmail" value="" >
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessdof">Business Day Open From </label>
										<select class="form-control required" name="businessdof" id="businessdof" >
										<option value="">Select Day</option>
										  <option value="Monday">Monday</option>
										  <option value="Tuesday">Tuesday</option>
										  <option value="Wednesday">Wednesday</option>
										  <option value="Thursday">Thursday</option>
										  <option value="Friday">Friday</option>
										  <option value="Saturday">Saturday</option>
										  <option value="Sunday">Sunday</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessdot">Business Day Open To </label>
										<select class="form-control required" name="businessdot" id="businessdot" >
										  <option value="">Select Day</option>
										  <option value="Monday">Monday</option>
										  <option value="Tuesday">Tuesday</option>
										  <option value="Wednesday">Wednesday</option>
										  <option value="Thursday">Thursday</option>
										  <option value="Friday">Friday</option>
										  <option value="Saturday">Saturday</option>
										  <option value="Sunday">Sunday</option>
										  </select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesshof">Business Hours Open From</label>
										<input type="time" class="form-control required" id="businesshof"  name="businesshof" value="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesshot">Business Hours Open To</label>
										<input type="time" class="form-control required" id="businesshot"  name="businesshot" value="" >
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesslatitude ">Business Latitude</label>
										<input type="text" class="form-control required" id="businesslatitude"  name="businesslatitude" value="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesslongitude">Business Longitude </label>
										<input type="text" class="form-control required" id="businesslongitude"  name="businesslongitude" value="" >
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesscategoryicon">Business Categories </label>
										<!-- <input type="text" class="form-control required" id="businesscategoryicon"  name="businesscategoryicon" > -->
										<div id="showcat">
										  <!-- CATEGORIES TO BE DISPLAYED HERE -->
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesscategoryname">Business Primary Category Name</label>
										<!--<input type="text" class="form-control required" id="businesscategoryname"  name="businesscategoryname">-->
										
										<select name="businesscategoryname" id="businesscategoryname" class="form-control required">
										</select>
									</div>
								</div>								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="UserId">User Identity</label>
										<div id="show">
										  <!-- ITEMS TO BE DISPLAYED HERE -->
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessimage">Business Image</label>
										<input type="file" id="c_img" name="c_img" size="20"  class="form-control" value=""/> 
									</div>
								</div>
							</div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						<input type="button" class="btn btn-default" value="Go Back" onclick="goBack()" style=" background-color: #dd4b39; color: white; "/>
                            <input type="submit" class="btn btn-primary" value="Submit" />
							<script>
							function goBack() {
								window.history.go(-1);
							}
							</script>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/addBusiness.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
		$.ajax({
			type: "GET",
			url: baseURL + "getAlluserId", 
			data: 'data',
			success: function(data) {
			$("#show").html(data); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
			 }
		});
		
		$.ajax({
			type: "GET",
			url: baseURL + "getAllcategories", 
			data: 'data',
			success: function(data) {
			$("#showcat").html(data); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
			 }
		});
});

function getMultipleSelectedValue()
{
  document.getElementById("businesscategoryname").options.length = 0;
  var x=document.getElementById("businesscategoryicon");
  for (var i = 0; i < x.options.length; i++) {
	 if(x.options[i].selected ==true){
		  var option = document.createElement("option");
			option.text = x.options[i].text;
			option.value = x.options[i].value;
			var select = document.getElementById("businesscategoryname");
			select.add(option,null);
	  }
  }
}

function isNumber(evt) {
evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	return false;
}
return true;
}
</script>