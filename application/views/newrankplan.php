<?php $hours = 24; $minutes = 60; ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer"></i> Rank Plan Management
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Plan Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="saverankplan" action="<?php echo base_url() ?>saverankplan" method="post" role="form"  name="rankform">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="googleid">Google Play Id</label>
                                        <input type="text" class="form-control required" id="googleid" name="googleid" maxlength="128" placeholder="Google Play Id" required="true">
                                    </div>
                                    
                                </div>
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="rank">Rank Upgrade</label>
                                        <input type="text" class="form-control" id="rank" placeholder="Rank" name="rank" value="" maxlength="128" required="true">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="boosttime">Boost time</label>
                                         <input type="text" class="form-control" id="boosttime" placeholder="Boost time" name="boosttime" value="" maxlength="128" required="true">
                                    </div>
                                </div>
								<div class="col-md-6">                                
                                   <div class="form-group">
                                        <label for="times">Hours / Minutes</label>
                                        <select name="times" id="times" class="form-control required">
											<option value="">Select Hours / Minutes</option>
											<option value="h">Hour</option>
											<option value="m">Minute</option>
										</select>
                                    </div>
                                </div>
                            </div>	
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="price">Price</label>
                                        <input type="text" class="form-control" id="price" placeholder="price" name="price" value="" maxlength="128" required="true">
                                    </div>
                                </div>
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="Category">Category</label>
                                        <input type="text" class="form-control" id="category" placeholder="category" name="category" value="hf" maxlength="128" readonly>
                                    </div>
                                </div>
                            </div>										
                         </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
							<input type="button" class="btn btn-default" value="Go Back" onclick="goBack()" style=" background-color: #dd4b39; color: white; "/>
                            <input type="submit" id="submit" class="btn btn-primary" value="Submit"/>
                            <input type="reset" class="btn btn-default" value="Reset" />
							<script>
							function goBack() {
								window.history.go(-1);
							}
							</script>
                        </div>
                    </form>
                </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addrankplan.js" type="text/javascript"></script>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js" type="text/javascript"></script>

<!-- Javascript -->

</script>