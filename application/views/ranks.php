<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer"></i> Manage Plans Purchased
      </h1>
	  
    </section>
    <section class="content">
        <!--<div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>addNew"><i class="fa fa-plus"></i> Add New User Purchased Plan</a>
                </div>
            </div>
        </div> -->
		<div class="col-md-4">
			<?php
				$this->load->helper('form');
				$error = $this->session->flashdata('error');
				if($error)
				{
			?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('error'); ?>                    
			</div>
			<?php } ?>
			<?php  
				$success = $this->session->flashdata('success');
				if($success)
				{
			?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php } ?>
			
			<div class="row">
				<div class="col-md-12">
					<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Purchased Plan List</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table id="book-table" class="table table-hover table table-bordered table-striped display">
                   <thead>
					   <tr>
						  <th>Sl.No</th>
						  <th>Name</th>
						  <th>UserId</th>
						  <th>Product Title</th>
						  <th>Product Description</th>
						  <th>Category</th>
						  <th>Status</th>
						  <th>Action</th>
						</tr>
					</thead>
                    
                  </table>
                  
                </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
		var t = jQuery('#book-table').DataTable({
			"ajax": {
				url : "<?php echo base_url().'planrecords' ?>",
				type : 'GET'
			},
			"order": [[ 1, 'asc' ]]
		});
		
		t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    });
	
	function doview(val) {
		jQuery.ajax({
			type: 'POST',
			url: baseURL+"planView",
			dataType: "html",
			data:{id:val},
			success: function (data) {
			    jQuery.fancybox.open(data);
			}
		});
        return false;
    }
</script>
