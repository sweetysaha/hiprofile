<?php

$userId = '';
$name = '';
$email = '';
$mobile = '';
$roleId = '';
$status = '';
$gender = '';
$dob = '';
$facebook_profile_url = '';
$twitter_profile_url = '';
$instagram_profile_url = '';
$fb_friendcount = '';
$inst_friendcount = '';
$twt_friendcount = '';
$friend_user_id = '';

if(!empty($userInfo))
{
    foreach ($userInfo as $uf)
    {
        $userId = $uf->userId;
        $name = $uf->name;
        $email = $uf->email;
        $mobile = $uf->mobile;
        $roleId = $uf->roleId;
		$status = $uf->status;
		$gender = $uf->gender;
		$username = $uf->username;
		$dob = $uf->dob;
		$fbprofileurl = $uf->facebook_profile_url;
		$twitterprofileurl = $uf->twitter_profile_url;
		$instaprofileurl = $uf->instagram_profile_url;
		$socailmediacount = $uf->fb_friendcount+$uf->twt_friendcount+$uf->inst_friendcount;
    }
?>
<div class="container table-responsive">
  <center><h2>User Details</h2></center>    
	<table class="table table-striped">
		<tbody>
			<tr>
				<td>Full Name</td>
				<td><?php echo $name; ?></td>
			</tr>
			<tr>
				<td>User Name</td>
				<td><?php echo $username; ?></td>
			</tr>
			<tr>
				<td>Email Address</td>
				<td><?php echo $email; ?></td>
			</tr>
			<tr>
				<td>Mobile Number</td>
				<td><?php echo $mobile; ?></td>
			</tr>
			<tr>
				<td>Status</td>
				<td><?php echo ($status ==  '0') ? 'Active' : 'Deactive'; ?></td>
			</tr>
			<tr>
				<td>Gender</td>
				<td><?php echo $gender; ?></td>
			</tr>
			<tr>
				<td>Date Of Birth</td>
				<td><?php echo $dob; ?></td>
			</tr>
			<tr>
				<td>Facebook Profile URL</td>
				<td><?php echo $fbprofileurl; ?></td>
			</tr>
			<tr>
				<td>Twitter Profile URL</td>
				<td><?php echo $twitterprofileurl; ?></td>
			</tr>
			<tr>
				<td>Instagram Profile URL</td>
				<td><?php echo $instaprofileurl; ?></td>
			</tr>
			<tr>
				<td>User's Friends Count</td>
				<td><?php echo $friendcount; ?></td>
			</tr>
			<tr>
				<td>Total Social Media Friends Count</td>
				<td><?php echo $socailmediacount; ?></td>
			</tr>
			
		</tbody>
	</table>
</div>
<?php
}
else
{
	echo "No Data Found..";
}
?>
<style>
.table-striped>tbody>tr:nth-child(odd)>td,
.table-striped>tbody>tr:nth-child(odd)>th {
	background-color: #dbe2e4;
}
</style>