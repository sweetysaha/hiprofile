<?php
$business_id = ''; 
	$business_title = ''; 
		$address = ''; 
			$country = ''; 
				$state ='';
					$city =''; 
						$pin = ''; 
							$contact = ''; 
								$email =''; 
									$business_rating ='';
										$day_open_from = ''; 
											$business_primary_category ='';
											$business_categories = '';
										$day_open_to = '';
									$hours_open_from =''; 
								$hours_open_to =''; 
							$image_url = ''; 
						$userId = ''; 
					$latitude =''; 	
				$longitude ='';
			$is_paid = ''; 
		$approved_status =''; 
	$pmnt_amount =''; 
$payment_ref =''; 
if(!empty($businessInfo))
{
    foreach ($businessInfo as $cf)
    {
		$businessid = $cf->business_id;
        $businesstitle = $cf->business_title;
		$businessaddress = $cf->address;
		$businesscountry = $cf->country;
		$businesstate = $cf->state;
		$businesscity = $cf->city;
		$businesspin = $cf->pin;
		$businesscontact = $cf->contact;
		$businessemail = $cf->email;
		$businessdof = $cf->day_open_from;
		$businessdot = $cf->day_open_to;
		$businesshof = $cf->hours_open_from;
		$businesshot = $cf->hours_open_to;
		$businessimage = $cf->image_url;
		$businesslatitude = $cf->latitude;
		$businesslongitude = $cf->longitude;
		$business_is_paid = $cf->is_paid;
		$business_approved_status = $cf->approved_status;
		$business_pmt_amount = $cf->pmnt_amount;
		$business_primary_category = $cf->primary_category;
		$business_categories = $cf->categories;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-suitcase"></i> Business Management
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Business Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url(); ?>editBusiness" method="post" id="editBusiness" role="form" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="box-body">
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="businessTitle">Business Title</label>
                                        <input type="text" class="form-control required" id="businessTitle" name="businessTitle" value="<?php echo $businesstitle; ?>">
									    <input type="hidden" value="<?php echo htmlspecialchars($businessid); ?>" name="businessId" id="businessId" />    

									</div> 
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessAddress">Business Address</label>
										<input type="text" class="form-control required" id="businessAddress"  name="businessAddress" value="<?php echo htmlspecialchars($businessaddress); ?>">
									</div>
								</div>
                            </div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessCountry">Business Country</label>
										<input type="text" class="form-control required" id="businessCountry"  name="businessCountry" value="<?php echo htmlspecialchars($businesscountry); ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesState">Business State</label>
										<input type="text" class="form-control required" id="businessState"  name="businessState" value="<?php echo htmlspecialchars($businesstate); ?>" >
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessCity">Business City </label>
										<input type="text" class="form-control required" id="businessCity"  name="businessCity" value="<?php echo htmlspecialchars($businesscity ); ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="Content">Business PIN / ZIP Code</label>
										<input type="text" class="form-control required" id="businessPin"  name="businessPin" value="<?php echo htmlspecialchars($businesspin); ?>" >
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessContact ">Business Contact </label>
										<input type="text" class="form-control required" id="businessContact"  name="businessContact" value="<?php echo htmlspecialchars($businesscontact); ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessEmail">Business Email </label>
										<input type="text" class="form-control required" id="businessEmail"  name="businessEmail" value="<?php echo htmlspecialchars($businessemail); ?>" >
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessdof">Business Day Open From </label>
										<input type="text" class="form-control required" id="businessdof"  name="businessdof" value="<?php echo htmlspecialchars($businessdof); ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessdot">Business Day Open To </label>
										<input type="text" class="form-control required" id="businessdot"  name="businessdot" value="<?php echo htmlspecialchars($businessdot ); ?>" >
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesshof">Business Hours Open From</label>
										<input type="text" class="form-control required" id="businesshof"  name="businesshof" value="<?php echo htmlspecialchars($businesshof); ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesshot">Business Hours Open To</label>
										<input type="text" class="form-control required" id="businesshot"  name="businesshot" value="<?php echo htmlspecialchars($businesshot); ?>" >
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesslatitude">Business Latitude</label>
										<input type="text" class="form-control required" id="businesslatitude"  name="businesslatitude" value="<?php echo htmlspecialchars($businesslatitude); ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesslongitude">Business Longitude </label>
										<input type="text" class="form-control required" id="businesslongitude"  name="businesslongitude" value="<?php echo htmlspecialchars($businesslongitude); ?>" >
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<!--<label for="businesscategoryicon">Business Categories </label>
										<input type="text" class="form-control required" id="businesscategoryicon"  name="businesscategoryicon" value="<?php //echo htmlspecialchars($business_categories); ?>" >
										-->
										<label for="businesscategoryicon">Business Categories </label>
										<div id="showcat">
										  <!-- CATEGORIES TO BE DISPLAYED HERE -->
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businesscategoryname">Business Primary Category Name</label>
										<!--<input type="text" class="form-control required" id="businesscategoryname"  name="businesscategoryname" value="<?php //echo htmlspecialchars($business_primary_category); ?>"> -->
										<div id="showprimarycat">
										  <!-- PRIMARY CATEGORIES TO BE DISPLAYED HERE -->
										</div>
									</div>
								</div>							
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
									<?php if($this->session->superadmin == 1): ?>
									
									<label for="status">Status</label>
									<select class="form-control required" name="statuss">
									  <option value="1" selected="selected">Payment Pending</option>
									  <option value="2" <?php if($business_is_paid == 1){ echo 'selected="selected"'; }?> >Penidng</option>
									  <option value="3" <?php if($business_approved_status == 1){ echo 'selected="selected"'; }?> >Approved</option>
									</select>
									<?php 
										else:
											$status = "";
										if($business_approved_status == 1){
											$status = "Approved";
										}
										elseif($business_is_paid == 0){
											$status = "Payment Pending";
										}elseif($business_approved_status == 0){
											$status = "Penidng";
										}
									?>
										<label for="status">Status</label>
											<input type="text" class="form-control required" id="status"  name="status" value="<?php echo htmlspecialchars($status); ?>" readonly disable>
									<?php endif; ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="businessimage">Business Image</label>
										<input type="file" id="c_img" name="c_img" size="20"  class="form-control" value="<?php echo $businessimage ; ?>"/> 
										<?php if($businessimage  != ""): ?>
										<img src="<?php echo base_url().'assets'.$businessimage  ?>" class="img-responsive" style="width: 100px;">
										<input type="checkbox" name="checkimagebusiness" id="checkimagebusiness"/>
										<label>Delete Image</label>
										<?php endif; ?>
									</div>
								</div>
							</div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						<input type="button" class="btn btn-default" value="Go Back" onclick="goBack()" style=" background-color: #dd4b39; color: white; "/>
                            <input type="submit" class="btn btn-primary" value="Submit" />
							<script>
							function goBack() {
								window.history.go(-1);
							}
							</script>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url();?>assets/js/common.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/editBusiness.js" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function() {	
		$.ajax({
			type: "GET",
			url: baseURL + "getAllcategories", 
			data: 'data',
			success: function(data) {
			$("#showcat").html(data); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
			 }
		});
		
		$.ajax({
			type: "POST",
			url: baseURL + "getselectedcategories", 
			data: { businessid : <?php echo $businessid; ?> } ,
			success: function(data) {
			//Make an array
			var dataarray=data.split(",");
			// Set the value
			$("#businesscategoryicon").val(dataarray);
			// Then refresh
			$("select#businesscategoryicon").multiselect("refresh");
			}
		});
		
		$.ajax({
			type: "POST",
			url: baseURL + "getprimarycategories", 
			data: { businessid : <?php echo $businessid; ?> } ,
			success: function(data) {
			$("#showprimarycat").html(data); /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
			 }
		});
		
});

function getMultipleSelectedValue()
{
  document.getElementById("businesscategoryname").options.length = 0;
  var x=document.getElementById("businesscategoryicon");
  for (var i = 0; i < x.options.length; i++) {
	 if(x.options[i].selected ==true){
		  var option = document.createElement("option");
			option.text = x.options[i].text;
			option.value = x.options[i].value;
			var select = document.getElementById("businesscategoryname");
			select.add(option,null);
	  }
  }
}

</script>