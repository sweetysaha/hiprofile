<?php

$userId = '';
$name = '';
$email = '';
$mobile = '';
$roleId = '';
$status = '';
$gender = '';
$dob = '';
$facebook_profile_url = '';
$twitter_profile_url = '';
$instagram_profile_url = '';
$fb_friendcount = '';
$inst_friendcount = '';
$twt_friendcount = '';
$friend_user_id = '';

if(!empty($userInfo))
{
    foreach ($userInfo as $uf)
    {
        $userId = $uf->userId;
        $name = $uf->name;
        $email = $uf->email;
        $mobile = $uf->mobile;
        $roleId = $uf->roleId;
		$status = $uf->status;
		$gender = $uf->gender;
		$username = $uf->username;
		$dob = $uf->dob;
		$fbprofileurl = $uf->facebook_profile_url;
		$twitterprofileurl = $uf->twitter_profile_url;
		$instaprofileurl = $uf->instagram_profile_url;
		$socailmediacount = $uf->fb_friendcount+$uf->twt_friendcount+$uf->inst_friendcount;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editUser" method="post" id="editUser" role="form" name="userform">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control" id="fname" placeholder="Full Name" name="fname" value="<?php echo $name; ?>" maxlength="128">
                                        <input type="hidden" value="<?php echo $userId; ?>" name="userId" id="userId" />    
                                    </div>
                                    
                                </div>
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="username">User Name</label>
                                        <input type="text" class="form-control" id="username" placeholder="User Name" name="username" value="<?php echo $username; ?>" maxlength="128">
										<div style="color:red;" id="UsernameExitprintarea"></div>
                                        
                                    </div>
                                </div>
                            </div>
							<div class="row">
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo $email; ?>" maxlength="128">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control" id="phone" placeholder="Mobile Number" name="phone" value="<?php echo $mobile; ?>" onkeypress="return isNumber(event);">
										<div style="color:red;" id="UserPhoneExitprintarea"></div>
                                    </div>
                                </div>
							</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
									<label for="role">Gender</label>
										<select class="form-control" id="gender" name="gender">
											<option value="">Select Gender</option>
											<option value="male"<?php echo ( str_replace(' ', '', strtolower($gender)) ==  'male') ? ' selected="selected"' : '';?>>Male</option>
											<option value="female"<?php echo ( str_replace(' ', '', strtolower($gender)) ==  'female') ? ' selected="selected"' : '';?>>Female</option>
										</select>
									</div>
								</div>    
                                <div class="col-md-6">								
									<div class="form-group">
                                        <label for="dob">Date Of Birth</label>
                                        <input type="text" class="form-control" id="dob" placeholder="dd/mm/yyyy" name="dob" value="<?php echo str_replace(" ","",$dob); ?>"  required>
										<div style="color:red;" id="dateprintarea"></div>
                                    </div>
                                </div>
                                
                            </div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
									<label for="role">Status</label>
										<select class="form-control" id="status" name="status">
											<option value="0"<?php echo ($status==  '0') ? ' selected="selected"' : '';?> style="color:#08d208;">Active</option>
											<option value="1"<?php echo ($status==  '1') ? 'selected="selected"' : '';?> style="color:#dd4b39;">Deactive</option>
										</select>
									</div>
								</div>        
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="facebookprofileurl">Facebook Profile URL</label>
                                        <input type="text" class="form-control" id="fburl" placeholder="Facebook Profile URL" name="fburl" value="<?php echo $fbprofileurl; ?>">
                                    </div>
                                </div>								
                            </div>
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="twitterprofileurl">Twitter Profile URL</label>
                                        <input type="text" class="form-control" id="twitterurl" placeholder="Twitter Profile URL" name="twitterurl" value="<?php echo $twitterprofileurl; ?>">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="instagramprofileurl">Instagram Profile URL</label>
                                        <input type="text" class="form-control" id="instaurl" placeholder="Instagram Profile URL" name="instaurl" value="<?php echo $instaprofileurl; ?>">
                                    </div>
                                </div>
                            </div>
					        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="friendcount">User's Friends Count</label>
                                        <input type="text" class="form-control" id="userfrndcount" placeholder="User's Friends Count" name="userfrndcount" value="<?php echo $friendcount; ?>" readonly="true" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="socialfriendcount">Total Social Media Friends Count</label>
                                        <input type="text" class="form-control" id="socialfrndcount" placeholder="Total Social Media Friends Count" name="socialfrndcount" value="<?php echo $socailmediacount; ?>" readonly="true">
                                    </div>
                                </div>
                            </div>						
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						<input type="button" class="btn btn-default" value="Go Back" onclick="goBack()" style=" background-color: #dd4b39; color: white; "/>
                            <input type="submit" onclick="return validateForm()" class="btn btn-primary" value="Submit" />
							 <script>
							function goBack() {
								window.history.go(-1);
							}
							</script>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>
<link href = "<?php echo base_url(); ?>assets/css/intlTelInput.css" rel = "stylesheet">
<script src="<?php echo base_url(); ?>assets/js/intlTelInput.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js" type="text/javascript"></script>

<!-- Javascript -->
<script>
 $(function() {
	 var date = new Date();
	$( "#dob" ).datepicker({
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/mm/yy",
		yearRange: "1918:+nn",
		//minDate:0,
		maxDate:0
		
	});
	
	//$( "#dob" ).datepicker("show");
 });
 /*$("#phone").blur(function (e) {
		e.preventDefault();
		var no = $("#phone").val();
		
		if (!no.match(/^(\+\d{1,3}[- ]?)?\d{10}$/))
		{
			alert("Please Enter a Valid Mobile Number");
			 $("#phone").val("+");
			 
			 return false;
		}

	});*/

function isNumber(evt) {
evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	return false;
}
return true;
}

function validateForm() {
	
	$("#dateprintarea").html('');
	var dateofbirth = document.getElementById("dob").value;  
	var dob = dateofbirth.split('/');  
	var dd = dob[0];  
	var mm = dob[1];  
	var yy = dob[2];  

	if (dd == '' || mm == '' || yy == '' || isNaN(dd) || isNaN(mm) || isNaN(yy) || dd == 0 || mm == 0 || yy == 0) 
	{  
		$("#dateprintarea").html('Invalid Date of Birth');		
		return false;  
	}   
	else if (dob.length != 3) 
	{  
		$("#dateprintarea").html('Please enter Date Of Birth in (dd/mm/yyyy) format');	
		return false;  
	}  
	else if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && dd > 30) 
	{  
		$("#dateprintarea").html('Please enter Day less than equal to 30');	
		return false;  
	}  
	else if ((mm == 1 || mm == 3 || mm == 5 || mm == 7 || mm == 8 || mm == 10 || mm == 12) && dd > 31) {  
		$("#dateprintarea").html('Please enter Day less than equal to 31');	
		return false;  
	}  
	else if (mm == 2) 
	{  
		var lyear = false;  
		if ((!(yy % 4) && yy % 100) || !(yy % 400)) {  
			lyear = true;  
		}  
		if ((lyear == false) && (dd >= 29)) {  
			$("#dateprintarea").html('Invalid date format in February!');
			return false;  
		}  
		if ((lyear == true) && (dd > 29)) {  
			$("#dateprintarea").html('Invalid date format in February!');
			return false;  
		}  
	}  
	else if (parseFloat(dob[1]) > 12) 
	{  
		$("#dateprintarea").html('Please enter Month less than equal to 12');					
		return false;  
	}  
	else if (dob[2].length != 4) 
	{  
		$("#dateprintarea").html('Please enter Year in Four Digit (yyyy)');
		return false;  
	} 
	else
	{
		$("#submit").prop('disabled', false);
		return true;
	}	
}

var newInput = document.getElementById("dob");
newInput.addEventListener('keydown', function( e )
{
    if(e.which !== 8) {
        var numChars = e.target.value.length;
        if(numChars === 2 || numChars === 5){
            var thisVal = e.target.value;
            thisVal += '/';
            e.target.value = thisVal;
        }
    }
});
$("#phone").intlTelInput();

$("#username").blur(function(){
	var userName = $("#username").val();
	if (userName != '') {
		$.ajax({
			type: "POST",
			url: baseURL + "checkUsernameExist", 
			data: 'username=' + encodeURIComponent(userName),
			success: function(data) {
				if (data == 1) {
					$("#UsernameExitprintarea").html('Username already taken.');
					$("#submit").prop('disabled', true);
				} else {
					$("#UsernameExitprintarea").html('');
					$("#submit").prop('disabled', false);
				}
			
			}
		});
	}
});

$("#phone").blur(function(){
	var userPhone = $("#phone").val();
	if (userPhone != '') {
		$.ajax({
			type: "POST",
			url: baseURL + "checkMobileExist", 
			data: 'phone=' + encodeURIComponent(userPhone),
			success: function(data) {
				if (data == 1) {
					$("#UserPhoneExitprintarea").html('Mobile number already taken.');
					$("#submit").prop('disabled', true);

				} else {
					$("#UserPhoneExitprintarea").html('');
					$("#submit").prop('disabled', false);

				}
			
			}
		});
	}
});
</script>