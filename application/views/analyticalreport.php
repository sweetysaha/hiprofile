<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-files-o"></i> View Analytical Reports
      </h1>
	  
    </section>
    <section class="content">
		<div class="col-md-4">
			<?php
				$this->load->helper('form');
				$error = $this->session->flashdata('error');
				if($error)
				{
			?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('error'); ?>                    
			</div>
			<?php } ?>
			<?php  
				$success = $this->session->flashdata('success');
				if($success)
				{
			?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php } ?>
			
			<div class="row">
				<div class="col-md-12">
					<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Analytical Report Details</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                 <form role="form" id="analyticalreport" action="<?php echo base_url() ?>generateReport" method="post" role="form" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="box-body">
                            <div class="row">
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fromdate">Select From Date For Report</label>
										<input type="text" class="form-control required" id="fromdate" name="fromdate" required readonly>
                                    </div> 									
								</div>
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="todate">Select To Date For Report</label>
										<input type="text" class="form-control required" id="todate" name="todate" required readonly>
                                    </div> 
								</div>
                                <div class="col-md-6">
									<div class="form-group">
										<label for="selectSection">Report Selection </label>
										<select class="form-control required" name="selectSection" id="selectSection" onchange="pdfoptionchange();" required>
										<option value="">Please Select</option>
										<option value="1">No of users signed</option>
										<option value="2">No of businesses added</option>
										<option value="3">No of users boosted there ranks</option>
										<option value="4">No of users upgraded their account</option>
										<option value="5">No of friends added</option>
										</select>
									</div>
                                </div>
								<div class="col-md-6">
									<div class="form-group">
										
										<input type="checkbox" class="checkbox-inline" id="generatechart" name="generatechart" required> <label for="generatechart">Generate Chart</label>
										<input type="hidden" id="chartimgurl" class="chartimgurl" name="chartimgurl" value="">

									</div> 
                                </div>
                            </div>  
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<div name="chart" id="chart" style="width: 100%;height: auto;"></div>
										<div name="chart_src" id="chart_src" style="display:none";></div>
									</div> 
                                </div>	
							</div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
							<input type="button" class="btn btn-default" value="Go Back" onclick="goBack()" style=" background-color: #dd4b39; color: #ffffff; "/>
							<input type="button" class="btn btn-primary" onclick="viewfunction();" value="View Report" style=" background-color: #36a959; color: #ffffff; "/>
                            <input type="button" class="btn btn-primary" onclick="myFunction();" value="Export Report" />
							<script>
							function goBack() {
								window.history.go(-1);
							}
							</script>
                        </div>
						<div id="ajax-content-container" class="ajax-content-container"></div>
                    </form>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js" charset="utf-8"></script>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/loader.js"></script> 

<!-- Javascript -->
<script>
function myFunction() {
	if($('#generatechart').prop('checked')){
		var charsource = $('#chart_src').html();
		$.ajax({
			type: 'POST',
			url: baseURL+"chartimagegenerate",
			dataType: "html",
			data:{srctext:charsource},
			success: function (dataview) {
				document.getElementById('chartimgurl').value = dataview;
				submit();
			}
		});
	}
	else
	{
		submit();
	}
	
}
function pdfoptionchange() {
 document.getElementById("generatechart").checked = false;
 $("#chart").hide();
}
function submit(){
	
	var fd = document.getElementById("fromdate").value;
	var td = document.getElementById("todate").value;
	var opv = document.getElementById("selectSection").value;
	if(fd.length == 0){
		alert("Please select an valid from date");
		document.getElementById("fromdate").focus();
	}
	else if(td.length == 0){
		alert("Please select an valid to date");
		document.getElementById("todate").focus();
	}
	else if(opv.length == 0){
		alert("Please select the Report Selection");
		document.getElementById("selectSection").focus();
	}
	else{
		document.getElementById("analyticalreport").submit();
	}
}
function viewfunction() {
		
	var fd = document.getElementById("fromdate").value;
	var td = document.getElementById("todate").value;
	var opv = document.getElementById("selectSection").value;
	if(fd.length == 0){
		alert("Please select an valid from date");
		document.getElementById("fromdate").focus();
	}
	else if(td.length == 0){
		alert("Please select an valid to date");
		document.getElementById("todate").focus();
	}
	else if(opv.length == 0){
		alert("Please select the Report Selection");
		document.getElementById("selectSection").focus();
	}
	else{
		$.ajax({
			type: 'POST',
			url: baseURL+"viewReport",
			dataType: "html",
			data:{fromdate:fd,todate:td,optionvalue:opv},
			success: function (dataview) {
			   //$('#ajax-content-container').html(dataview);
			    $.fancybox.open(dataview);
			}
		});
	}
}
$(function() {
	 var date = new Date();
	$( "#fromdate,#todate" ).datepicker({
		changeYear: true,
		changeMonth: true,
		dateFormat: "yy-mm-dd",
		yearRange: "1918:+nn",
		//minDate:0,
		maxDate:0
		
	});
	$('#generatechart').show();
 });
 
 $('#generatechart').on('change', function() { 
	var fromdate = $("#fromdate").val();
	var todate = $("#todate").val();
	var optionvalue = $("#selectSection option:selected").val();
    if (this.checked) {
		if(jQuery.trim(fromdate).length == 0 || jQuery.trim(fromdate).length == "")
		{
			alert("Please select an valid from date");
			$('#generatechart').attr('checked', false);
			$('#fromdate').focus();
			return false;
		}
		else if(jQuery.trim(todate).length == 0 || jQuery.trim(todate).length == "")
		{
			alert("Please select an valid to date");
			$('#generatechart').attr('checked', false);
			$('#todate').focus();
			return false;
		}
		else if(jQuery.trim(optionvalue).length == 0 || jQuery.trim(optionvalue).length == "")
		{
			alert("Please select the Report Selection");
			$('#generatechart').attr('checked', false);
			$('#selectSection').focus();
			return false;
		}
		else
		{
			// Load the Visualization API and the line package.
			google.charts.load('current', {'packages':['corechart']});
			// Set a callback to run when the Google Visualization API is loaded.
			google.charts.setOnLoadCallback(drawVisualization);
			$('#chart').show();
		}
    }
	
	if (!this.checked) {
		$('#chart').hide();
		$('#generatechart').hide();
		location.reload();
    }
});

function drawVisualization() 
{
	var fromdate = $("#fromdate").val();
	var todate = $("#todate").val();
	var optionvalue = $("#selectSection option:selected").val(); 
	if(optionvalue == 1)
	{
		var PieChartData = $.ajax({
			type: 'POST',
			url: baseURL+"selectedChart",
			data:{fromdate:fromdate,todate:todate,optionvalue:optionvalue},
			success: function (data1) {
			var data = new google.visualization.DataTable();
				// Add legends with data type
				data.addColumn('string', 'Name');
				data.addColumn('number', 'CreatedDate');
				//Parse data into Json
				var jsonData = $.parseJSON(data1);
				for (var i = 0; i < jsonData.length; i++) {
					data.addRow([jsonData[i].name, parseInt(jsonData[i].userId)]);
				} 
				var options = {
					chartArea: {
						width: '80%'
					  },
					legend: {
						position: 'right', 
					textStyle: {
						color: 'blue', 
					fontSize: 16} 
					},
					pieSliceText: 'none',
					title: 'User Added Analytical Report',
					width: 900, 
					height: 600,
					is3D:'false',
				};
				var chart = new google.visualization.PieChart(document.getElementById('chart'));
				// Wait for the chart to finish drawing before calling the getIm geURI() method.
				google.visualization.events.addListener(chart, 'ready', function ()      {
				 document.getElementById('chart_src').innerHTML = chart.getImageURI();
				});
				chart.draw(data, options);
			}
		});
	}
	if(optionvalue == 2)
	{
		var PieChartData = $.ajax({
			type: 'POST',
			url: baseURL+"selectedChart",
			data:{fromdate:fromdate,todate:todate,optionvalue:optionvalue},
			success: function (data1) {
			var data = new google.visualization.DataTable();
				// Add legends with data type
				data.addColumn('string', 'Business Title');
				data.addColumn('number', 'created Date');
				//Parse data into Json
				var jsonData = $.parseJSON(data1);
				for (var i = 0; i < jsonData.length; i++) {
					data.addRow([jsonData[i].business_title, parseInt(jsonData[i].business_id)]);
				} 
				var options = {
					chartArea: {
						width: '80%'
					  },
					legend: {
						position: 'right', 
					textStyle: {
						color: 'blue', 
					fontSize: 16} 
					},
					pieSliceText: 'none',
					title: 'Business Added Analytical Report',
					width: 900, 
					height: 600,
					is3D:'false',
				};
				var chart = new google.visualization.PieChart(document.getElementById('chart'));
				// Wait for the chart to finish drawing before calling the getIm geURI() method.
				google.visualization.events.addListener(chart, 'ready', function ()      {
				 document.getElementById('chart_src').innerHTML = chart.getImageURI();
				});
				chart.draw(data, options);
			}
		});
	}
	if(optionvalue == 3)
	{
		var PieChartData = $.ajax({
			type: 'POST',
			url: baseURL+"selectedChart",
			data:{fromdate:fromdate,todate:todate,optionvalue:optionvalue},
			success: function (data1) {
			var data = new google.visualization.DataTable();
				// Add legends with data type
				data.addColumn('string', 'product_title');
				data.addColumn('number', 'userId');
				//Parse data into Json
				var jsonData = $.parseJSON(data1);
				for (var i = 0; i < jsonData.length; i++) {
					data.addRow([jsonData[i].product_id, parseInt(jsonData[i].userId)]);
				} 
				var options = {
					chartArea: {
						width: '80%'
					  },
					legend: {
						position: 'right', 
					textStyle: {
						color: 'blue', 
					fontSize: 16} 
					},
					pieSliceText: 'none',
					title: 'Analytical Report Using Graphical Representation Of PIe Chart',
					width: 900, 
					height: 600,
					is3D:'false',
				};
				var chart = new google.visualization.PieChart(document.getElementById('chart'));
				// Wait for the chart to finish drawing before calling the getIm geURI() method.
				google.visualization.events.addListener(chart, 'ready', function ()      {
				 document.getElementById('chart_src').innerHTML = chart.getImageURI();
				});
				chart.draw(data, options);
			}
		});
	}
	if(optionvalue == 4)
	{
		var PieChartData = $.ajax({
			type: 'POST',
			url: baseURL+"selectedChart",
			data:{fromdate:fromdate,todate:todate,optionvalue:optionvalue},
			success: function (data1) {
			var data = new google.visualization.DataTable();
				// Add legends with data type
				data.addColumn('string', 'product_title');
				data.addColumn('number', 'userId');
				//Parse data into Json
				var jsonData = $.parseJSON(data1);
				for (var i = 0; i < jsonData.length; i++) {
					data.addRow([jsonData[i].product_id, parseInt(jsonData[i].userId)]);
				} 
				var options = {
					chartArea: {
						width: '80%'
					  },
					legend: {
						position: 'right', 
					textStyle: {
						color: 'blue', 
					fontSize: 16} 
					},
					pieSliceText: 'none',
					title: 'Analytical Report Using Graphical Representation Of PIe Chart',
					width: 900, 
					height: 600,
					is3D:'false',
				};
				var chart = new google.visualization.PieChart(document.getElementById('chart'));
				// Wait for the chart to finish drawing before calling the getIm geURI() method.
				google.visualization.events.addListener(chart, 'ready', function ()      {
				 document.getElementById('chart_src').innerHTML = chart.getImageURI();
				});
				chart.draw(data, options);
			}
		});
	}
	if(optionvalue == 5)
	{
		var PieChartData = $.ajax({
			type: 'POST',
			url: baseURL+"selectedChart",
			data:{fromdate:fromdate,todate:todate,optionvalue:optionvalue},
			success: function (data1) {
				
			var data = new google.visualization.DataTable();
				// Add legends with data type
				data.addColumn('string', 'Name');
				data.addColumn('number', 'UserId');
				data.addColumn('number', 'FriendId');
				data.addColumn('string', 'FriendName');
				//Parse data into Json
				var jsonData = $.parseJSON(data1);
			
				for (var i = 0; i < jsonData['users'].length; i++) {
					data.addRow([jsonData['users'][i].username,parseInt(jsonData['users'][i].userId),parseInt(jsonData['friends'][i].friend_user_id),jsonData['friends'][i].friendusername]);
				} 
				var options = {
					chartArea: {
						width: '80%'
					  },
					legend: {
						position: 'right', 
					textStyle: {
						color: 'blue', 
					fontSize: 16} 
					},
					pieSliceText: 'none',
					title: 'Friends Analytical Report',
					width: 900, 
					height: 600,
					is3D:'false',
				};
				var chart = new google.visualization.PieChart(document.getElementById('chart'));
				// Wait for the chart to finish drawing before calling the getIm geURI() method.
				google.visualization.events.addListener(chart, 'ready', function ()      {
				 document.getElementById('chart_src').innerHTML = chart.getImageURI();
				});
				chart.draw(data, options);
			}
		});
	}
	
}
</script>
