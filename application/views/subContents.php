<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
		<?php 
		if($pid == 4): 
		echo "<i class='fa fa-bullseye fa-1x'></i>";
		echo " Latest Trends Content Management"; 
		elseif($pid == 5): 
		echo "<i class='fa fa-medkit fa-1x'></i> ";
		echo "Health Tips Content Management"; 
		endif;?> 
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url().'Contentsub/subAddNewContentForm?id='.$pid?>" >
					<?php if($pid == 4): ?>
					<i class="fa fa-plus"></i> Add New Latest Trends</a>
					<?php elseif($pid == 5): ?>
					<i class="fa fa-plus"></i> Add New Health Tips</a>
					<?php endif;?> 
                </div>
            </div>
        </div>
		<div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
				<?php if($pid == 4): ?>
		                    <h3 class="box-title">Latest Trends List</h3>
					<?php elseif($pid == 5): ?>
							<h3 class="box-title">Health Tips List</h3>
					<?php endif;?> 
                    <div class="box-tools">
                    <form action="<?php echo base_url().'Contentsub/subContentListing/'.$pid ?>" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="book-table" class="table table-hover table table-bordered table-striped display">
					<thead>
						<tr>
						  <th>Sl.No</th>
						  <th>Title</th>
						  <th>Image</th>
						  <th>Description</th>
						  <th>Publish Date</th>
						  <th class="text-center">Actions</th>
						</tr>
					</thead>
                    <?php 
					//$k = $this->uri->segment(3)-1;
						$k = 4;
						if(!empty($contentRecords)) : 
							 if($this->uri->segment(3) && $this->uri->segment(4)): 
								$i = 5 * $this->uri->segment(4);
							 else: 
								$i = 1;
							 endif;
                         foreach($contentRecords as $record): ?>
						<tr>
						<?php if($this->uri->segment(3) && $this->uri->segment(4)): ?>
							<td><?php echo $i-$k; $k--;?></td>
						<?php else: ?>
							<td><?php echo $i++; ?></td>
						<?php endif;?>
                      
					   <td><?php echo $record->contentTitle ?></td>
					   <?php if($record->imageurl){?>
					   <td><img src="<?php echo base_url().'uploads/'.$record->imageurl ?>" alt="<?php echo $record->contentTitle; ?>" style=" width: 100px; height: 100px; "></td>
					   <?php }else{?>
					   <td></td>
					   <?php }?>
					   <?php
					   $oDate = new DateTime($record->createdDtm);
					   $sDate = $oDate->format("d-m-Y");
					   ?>
					  <td><?php echo $record->contentDesc; ?></td>
					  <td data-order="<?php echo $record->createdDtm ?>"><?php echo $sDate; ?></td>
                      <td class="text-center" >
					  
                          <a class="btn btn-sm btn-info" title="EDIT" href="<?php echo base_url().'Contentsub/subEditOldContent/'.$record->contentId; ?>"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-sm btn-danger deleteContentsub" href="#" data-contentid="<?php echo $record->contentId; ?>" title="DELETE"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
						<?php endforeach; ?>
                    <?php endif; ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common.min.css">	
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
		    jQuery('#book-table').DataTable();

        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "Contentsub/subContentListing/"+<?php echo $pid ?> +"/"+ value);
            jQuery("#searchList").submit();
        });
    });
</script>