
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="ion ion-ios-list-outline"></i> Business Review Management
       </h1>
	  
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url();?>reviewListing/"><i class="fa fa-eye"></i> Show App Reviews</a>
                </div>
            </div>
        </div>
		<div class="col-md-4">
			<?php
				$this->load->helper('form');
				$error = $this->session->flashdata('error');
				if($error)
				{
			?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('error'); ?>                    
			</div>
			<?php } ?>
			<?php  
				$success = $this->session->flashdata('success');
				if($success)
				{
			?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php } ?>
			
			<div class="row">
				<div class="col-md-12">
					<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Business Review List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>businessReviewListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Sl.No</th>
                      <th>Business Title</th>
                      <th>Business Email</th>
					  <th>Business Contact</th>
                      <th>Business Rating</th>
					  <th>Action</th>
                    </tr>
                    <?php
                    if(!empty($reviewRecords))
                    {
						
						if($this->uri->segment(2))
						{
							$i = $this->uri->segment(2)+1;
						}
						else
						{
							$i = 1;
						}
						
                        foreach($reviewRecords as $record)
                        {
						 $businessrating = ($record->business_rating == "0")?"":$record->business_rating
                    ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $record->business_title; ?></td>
                      <td><?php echo $record->email; ?></td>
					  <td><?php echo $record->contact; ?></td>
                      <td><?php echo $businessrating; ?></td>
					  <td><button class="btn btn-sm btn-success" title="View Business" id="viewlink" onclick="doview(<?php echo $record->business_id; ?>); return false;"><i class="fa fa-eye"></i></td>
                    </tr>
                    <?php
						++$i;
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "businessReviewListing/" + value);
            jQuery("#searchList").submit();
        });
    });
	
	function doview(val) {

		jQuery.ajax({
			type: 'POST',
			url: baseURL+"businessView",
			dataType: "text",
			data:{businessid:val},
			success: function (data) {
			    jQuery.fancybox.open(data);
			}
		});
        return false;
    }
</script>
