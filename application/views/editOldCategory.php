<?php

$categoryId = '';
$categoryname = '';
$categoryicon = '';
$status = '';

if(!empty($categoryInfo))
{
    foreach ($categoryInfo as $cf)
    {
        $categoryId = $cf->categoryId;
        $categoryname = $cf->business_category_names;
        $categoryicon = $cf->business_category_icon;
		$status = $cf->cat_status;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-list-alt"></i> Category Management
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Category Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editCategory" method="post" id="editCategory" role="form" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="box-body">
							 <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="categoryName">Category Name</label>
                                        <input type="text" class="form-control required" id="categoryname" name="categoryName" value="<?php echo $categoryname; ?>">
									    <input type="hidden" value="<?php echo htmlspecialchars($categoryId); ?>" name="categoryId" id="categoryId" />    
									</div> 
                                </div>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Content">Category Icon Name</label>
										 <select class="form-control required selectpicker" id="categoryIcon" name="categoryIcon">
										  <option value="glass" <?php if($categoryicon == 'glass'){ echo "selected"; } ?>>glass</option>
										  <option value="disco" <?php if($categoryicon == 'disco'){ echo "selected"; } ?>>disco</option>
										  <option value="fork_knife" <?php if($categoryicon == 'fork_knife'){ echo "selected"; } ?>>fork_knife</option>
										  <option value="chips" <?php if($categoryicon == 'chips'){ echo "selected"; } ?>>chips</option>
										  <option value="hamburger" <?php if($categoryicon == 'hamburger'){ echo "selected"; } ?>>hamburger</option>
										  <option value="bed" <?php if($categoryicon == 'bed'){ echo "selected"; } ?>>bed</option>
										  <option value="movie" <?php if($categoryicon == 'movie'){ echo "selected"; } ?>>movie</option>
										  <option value="bag" <?php if($categoryicon == 'bag'){ echo "selected"; } ?>>bag</option>
										  <option value="trolley" <?php if($categoryicon == 'trolley'){ echo "selected"; } ?>>trolley</option>
										  <option value="stage" <?php if($categoryicon == 'stage'){ echo "selected"; } ?>>stage</option>
										  <option value="mall" <?php if($categoryicon == 'mall'){ echo "selected"; } ?>>mall</option>
										</select>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
										<label for="status">Status</label>
										<select class="form-control required" name="statuss">
										  <option value="0" selected="selected">Active</option>
										  <option value="1" <?php if($status == 1){ echo 'selected="selected"'; }?> >Deactive</option>
										  
										</select>
									</div>
                                </div>
                            </div> 
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						<input type="button" class="btn btn-default" value="Go Back" onclick="goBack()" style=" background-color: #dd4b39; color: white; "/>
                            <input type="submit" class="btn btn-primary" value="Submit" />
							<script>
							function goBack() {
								window.history.go(-1);
							}
							</script>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editCategory.js" type="text/javascript"></script>