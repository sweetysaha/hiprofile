<?php
$count = $reviewcount = 0;
$this->db = $this->load->database('default', true);
$this->db->select('userId');
$this->db->from('hiprofile_users');
$this->db->where('isDeleted', 0);
$this->db->where('roleId !=', 1);
$query = $this->db->get();
$count = count($query->result());

$this->db->select('rating');
$this->db->from('hiprofile_users');
$this->db->where('rating !=', "");
$this->db->where('isDeleted', 0);
$this->db->where('roleId !=', 1);
$query = $this->db->get();
$reviewcount = count($query->result());

$this->db->select('business_rating');
$this->db->from('hiprofile_business');
$this->db->where('business_rating !=', 0);
$this->db->where('isDeleted', 0);
$query = $this->db->get();
$businessreviewcount = count($query->result());

$this->db->select('approved_status');
$this->db->from('hiprofile_business');
$this->db->where('isDeleted', 0);
$this->db->where('approved_status', 1);
$query = $this->db->get();
$businesscount = count($query->result());

$this->db->select('userId');
$this->db->from('hiprofile_rank_points_plan_details');
$query = $this->db->get();
$rankcount = count($query->result());
?>

<div class="content-wrapper" style="min-height: 901px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>Control panel</small>
      </h1>
    </section>
    
    <section class="content">
        <div class="row">
			<div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo  $count; ?></h3>
                  <p>Latest End User Registration</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>userListing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
               <!-- <a href="<?php //echo base_url(); ?>userListing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->

			  </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $rankcount?></h3>
                  <p>Latest Payments For Rank Upgrade</p>
                </div>
                <div class="icon">
                  <i class="ion ion-card"></i>
                </div>
                <a href="<?php echo base_url(); ?>plans" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
				  <h3><?php echo  $businesscount; ?></h3>
                  <p>Latest Payments For Business Listing</p>
                </div>
                <div class="icon">
                  <i class="ion ion-cash"></i>
                </div>
                <a href="<?php echo base_url(); ?>businessListing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo  $reviewcount+$businessreviewcount; ?></h3>
                  <p>Latest Reviews</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-list-outline"></i>
                </div>
                <a href="<?php echo base_url(); ?>reviewListing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>
    </section>
</div>