<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>

        <i class="fa fa-tachometer"></i> Manage Fame Rank

      </h1>

	  

    </section>

    <section class="content">

        <div class="row">

            <div class="col-xs-12">

              <div class="box">

                <div class="box-header">

                    <h3 class="box-title">Fame Rank Lists</h3>

                </div><!-- /.box-header -->

                <div class="box-body table-responsive">

                  <table id="book-table" class="table table-hover table table-bordered table-striped display">

                   <thead>

					   <tr>

						  <th>Sl.No</th>

						  <th>User</th>

						  <th>Product Title</th>

						  <th>Order Id</th>

						  <th>Price</th>

						  <th>Store</th>

						  <th>Created Date</th>

						</tr>

					</thead>

                    

                  </table>

                  

                </div><!-- /.box-body -->



              </div><!-- /.box -->

            </div>

        </div>

    </section>

</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.min.css" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js" charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>

<script type="text/javascript">

    jQuery(document).ready(function(){

		var t = jQuery('#book-table').DataTable({

			"ajax": {

				url : "<?php echo base_url().'fmrecords' ?>",

				type : 'GET'

			},

			"order": [[ 1, 'asc' ]]

		});

		

		t.on( 'order.dt search.dt', function () {

        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {

            cell.innerHTML = i+1;

        } );

    } ).draw();

    });

</script>

