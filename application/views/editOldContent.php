<?php

$contentId = '';
$contenttitle = '';
$contentdesc = '';
$imageurl = '';

if(!empty($contentInfo))
{
    foreach ($contentInfo as $cf)
    {
        $contentId = $cf->contentId;
        $contenttitle = $cf->contentTitle;
        $contentdesc = $cf->contentDesc;
		$imageurl = $cf->imageurl;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-archive"></i> Content Management
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Content Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editContent" method="post" id="editUser" role="form" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="box-body">
							 <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="contentTitle">Content Title</label>
                                        <input type="text" class="form-control required" id="contentTitle" name="contentTitle" value="<?php echo $contenttitle; ?>">
									    <input type="hidden" value="<?php echo htmlspecialchars($contentId); ?>" name="contentId" id="contentId" />    

									</div> 
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="Content">Content Description</label>
                                        <textarea class="form-control required" id="contentDesc"  name="contentDesc" ><?php echo htmlspecialchars($contentdesc); ?></textarea>
                                    </div>
                                </div>
								<div class="col-md-12">
                                    <div class="form-group">
										<label for="health">Image Upload</label>
										<input type="file" id="c_img" name="c_img" size="20"  class="form-control" value="<?php echo $imageurl; ?>"/> 
										<?php if($imageurl != ""): ?>
										<img src="<?php echo base_url().'uploads/'.$imageurl ?>" class="img-responsive" style="width: 100px;">
										<input type="checkbox" name="checkimagemain" id="checkimagemain"/>
										<label>Delete Image</label>
										<?php endif; ?>
									</div>
                                </div>
                            </div> 
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
						<input type="button" class="btn btn-default" value="Go Back" onclick="goBack()" style=" background-color: #dd4b39; color: white; "/>
                            <input type="submit" class="btn btn-primary" value="Submit" />
							<script>
							function goBack() {
								window.history.go(-1);
							}
							</script>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/addContent.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/common.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script type="text/javascript">
tinymce.init({
selector: "textarea",
plugins: "link image"
});
</script>