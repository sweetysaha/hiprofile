<!DOCTYPE html>

<html style="height: auto; min-height: 100%;">

  <head>

    <meta charset="UTF-8">

	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/dist/img/favicon.png"/>

    <title><?php echo $pageTitle; ?></title>

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.4 -->

    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    

    <!-- FontAwesome 4.3.0 -->

    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Ionicons 2.0.0 -->

    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->

    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

    <!-- AdminLTE Skins. Choose a skin from the css/skins 

         folder instead of downloading all of them to reduce the load. -->

    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

    <style>

    	.error{

    		color:red;

    		font-weight: normal;

    	}

    </style>

    <!-- jQuery 2.1.4 -->

    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>

    <script type="text/javascript">

        var baseURL = "<?php echo base_url(); ?>";

    </script>

    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

  </head>

  <!-- <body class="sidebar-mini skin-black-light"> -->

<?php

$datetime = $this->session->userdata ( 'lastlogintime' );

$full = false;

$now = new DateTime;

$ago = new DateTime($datetime);

$diff = $now->diff($ago);



$diff->w = floor($diff->d / 7);

$diff->d -= $diff->w * 7;



$string = array(

	'y' => 'year',

	'm' => 'month',

	'w' => 'week',

	'd' => 'day',

	'h' => 'hour',

	'i' => 'minute',

	's' => 'second',

);

foreach ($string as $k => &$v) {

	if ($diff->$k) {

		$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');

	} else {

		unset($string[$k]);

	}

}



if (!$full) $string = array_slice($string, 0, 1);

$lastlogin =  $string ? implode(', ', $string) . ' ago' : 'just now';

?>

  <body class="skin-blue sidebar-mini" style="height: auto; min-height: 100%;">

    <div class="wrapper" style="height: auto; min-height: 100%;">

      

      <header class="main-header">

        <!-- Logo -->

        <a href="<?php echo base_url(); ?>" class="logo">

          <!-- mini logo for sidebar mini 50x50 pixels -->

          <span class="logo-mini"><b>Hi</b>Profile</span>

          <!-- logo for regular state and mobile devices -->

          <span class="logo-lg"><b>Hi</b>Profile</span>

        </a>

        <!-- Header Navbar: style can be found in header.less -->

        <nav class="navbar navbar-static-top" role="navigation">

          <!-- Sidebar toggle button-->

          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">

            <span class="sr-only">Toggle navigation</span>

          </a>

          <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

              <!-- User Account: style can be found in dropdown.less -->

              <li class="dropdown user user-menu">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                  <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/>

                  <span class="hidden-xs"><?php echo 'Welcome, '.$name; ?></span>

                </a>

                <ul class="dropdown-menu">

                  <!-- User image -->

                  <li class="user-header">

                    <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="img-circle" alt="User Image" />

                    <p>

                      <?php echo $name; ?>

                      <small><?php echo "Last Login Time: ".$lastlogin; ?></small>

                    </p>

                  </li>

                  <!-- Menu Footer-->

                  <li class="user-footer">

                    <div class="pull-left">

                      <a href="<?php echo base_url(); ?>loadChangePass" class="btn btn-default btn-flat"><i class="fa fa-key"></i> Change Password</a>

                    </div>

                    <div class="pull-right">

                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign Out</a>

                    </div>

                  </li>

                </ul>

              </li>

            </ul>

          </div>

        </nav>

      </header>

      <!-- Left side column. contains the logo and sidebar -->

      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <section class="sidebar" style="height: auto;">

          <div class="user-panel">

        <div class="pull-left image">

          <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="img-circle" alt="User Image">

        </div>

        <div class="pull-left info">

          <p><?php echo $name; ?></p>

          <a href="<?php echo base_url(); ?>"><i class="fa fa-circle text-success"></i> Online</a>

        </div>

      </div> 

          <!-- sidebar menu: : style can be found in sidebar.less -->

          <ul class="sidebar-menu">

            <li class="header">MAIN NAVIGATION</li>

            <li class="treeview">

              <a href="<?php echo base_url(); ?>dashboard">

                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>

              </a>

            </li>

            <?php if($role == ROLE_ADMIN): ?>

				<li class="treeview">

				  <a href="<?php echo base_url(); ?>userListing">

					<i class="fa fa-users"></i>

					<span>Manage Users</span>

				  </a>

				</li>

				<li class="treeview">

				  <a href="<?php echo base_url().'Contentsub/subContentListing/4'?>" >

					<i class="fa fa-bullseye"></i>

					<span>Manage Latest Trends</span>

				  </a>

				</li>

				<li class="treeview">

				  <a href="<?php echo base_url().'Contentsub/subContentListing/5'?>" >

					<i class="fa fa-medkit"></i>

					<span>Manage Health Tips</span>

				  </a>

				</li>

				<li class="treeview">

				  <a href="<?php echo base_url(); ?>categoryListing" >

					<i class="fa fa-list-alt"></i>

					<span>Manage Business Categories</span>

				  </a>

				</li>

				<li class="treeview">

				  <a href="<?php echo base_url(); ?>businessListing" >

					<i class="fa fa-suitcase"></i>

					<span>Manage Businesses</span>

				  </a>

				</li>		

				<li class="treeview">

				  <a href="<?php echo base_url(); ?>notificationListing">

					<i class="fa fa fa-bullhorn"></i>

					<span>Manage Notifications</span>

				  </a>

				</li>

				<li class="treeview">

				  <a href="<?php echo base_url(); ?>contentListing" >

					<i class="fa fa-archive"></i>

					<span>Manage Contents</span>

				  </a>

				</li>

			

				<li class="treeview">

				<a href="#">

				<i class="fa fa-tachometer"></i> <span>Manage Rank Upgrade Plans</span>

				<span class="pull-right-container">

				<i class="fa fa-angle-left pull-right"></i>

				</span>

				</a>

				<ul class="treeview-menu" style="display: none;">

				<li><a href="<?php echo base_url(); ?>plans"><i class="fa fa-circle-o"></i>Manage Plans Purchased</a></li>

				<li><a href="<?php echo base_url(); ?>famerank"><i class="fa fa-circle-o"></i> Manage Rank Fame plans</a></li>

				<li><a href="<?php echo base_url(); ?>doublepoint"><i class="fa fa-circle-o"></i> Manage Double Points Plans</a></li>

				</ul>

				</li>

          <li class="treeview">

          <a href="<?php echo base_url(); ?>fameranklist">

            <i class="fa fa-tachometer"></i> 

            <span>Manage Fame Rank</span>

          </a>

        </li> 

				<li class="treeview">

				  <a href="<?php echo base_url(); ?>analyticalReport" >

					<i class="fa fa-files-o"></i>

					<span>View Analytical Reports</span>

				  </a>

				</li>

			<?php endif; ?>

          </ul>

        </section>

        <!-- /.sidebar -->

      </aside>