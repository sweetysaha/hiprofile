<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>

        <i class="fa fa fa-bullhorn"></i> Manage Notifications

      </h1>

	  

    </section>

    <section class="content">

		<div class="row">

            <div class="col-xs-12 text-right">

                <div class="form-group">
                	

                	<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" href="javascript:void(0);"> Copy Email</a>

                    <a class="btn btn-primary" id="bulk_email" onClick="sendbulkmail();" href="javascript:void(0);"><i class="fa fa-send"></i> Send Bulk Email Notifications</a>

                </div>

            </div>

        </div>

		<div class="col-md-4">

			<?php

				$this->load->helper('form');

				$error = $this->session->flashdata('error');

				if($error)

				{

			?>

			<div class="alert alert-danger alert-dismissable">

				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

				<?php echo $this->session->flashdata('error'); ?>                    

			</div>

			<?php } ?>

			<?php  

				$success = $this->session->flashdata('success');

				if($success)

				{

			?>

			<div class="alert alert-success alert-dismissable">

				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

				<?php echo $this->session->flashdata('success'); ?>

			</div>

			<?php } ?>

			

			<div class="row">

				<div class="col-md-12">

					<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>

				</div>

            </div>

        </div>

        <div class="row">

            <div class="col-xs-12">

              <div class="box">

                <div class="box-header">

                    <h3 class="box-title">Notifications List</h3>

                    <div class="box-tools">

                        <form action="<?php echo base_url() ?>notificationListing" method="POST" id="searchList">

                            <div class="input-group">

                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>

                              <div class="input-group-btn">

                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>

                              </div>

                            </div>

                        </form>

                    </div>

                </div><!-- /.box-header -->

                <div class="box-body table-responsive no-padding">

                  <table class="table table-hover">

                    <tr>

                      <th>Sl.No</th>

                      <th>Name</th>

                      <th>Email</th>

                      <th>Mobile</th>

                      <th>Message Box</th>

					  <th>Send Email</th>

					  <?php //<th>Send SMS</th> ?>

                    </tr>

                    <?php

                    if(!empty($notificationRecords))

                    {

						

						if($this->uri->segment(2))

						{

							$i = $this->uri->segment(2)+1;

							$j = $this->uri->segment(2)+1;

							$k = $this->uri->segment(2)+1;

						}

						else

						{

							$i = 1;

							$j = 1;

							$k = 1;

						}

						

                        foreach($notificationRecords as $record)

                        {

                    ?>

					

                    <tr>

                      <td><?php echo $i; ?></td>

                      <td><?php echo $record->name ?></td>

                      <td><?php echo $record->email ?></td>

                      <td><?php echo $record->mobile ?></td>

					  <td><input type="text" placeholder="Enter Message" id="notification_message<?php echo $j; ?>" name="notification_message" required></td>

					  <td><input type="button" name="sendemail" id="sendemail" value="Send Email" style=" background-color: #b7c5ce; "  onClick="sendmail(<?php echo $j; ?>);"></td>

					  <?php /*<td><input type="button" name="sendsms" id="sendsms" value="Send SMS" style=" background-color: #f3d3a9; " onClick="sendsms(<?php echo $k; ?>);"></td>*/ ?>

					  <td><input type="hidden" id="emailuserid<?php echo $j; ?>" value="<?php echo $record->userId; ?>"></td>

					  <td><input type="hidden" id="mobileuserid<?php echo $k; ?>" value="<?php echo $record->userId; ?>"></td>

					  <td><input type="hidden" id="email<?php echo $j; ?>" value="<?php echo $record->email; ?>"></td>

					  <td><input type="hidden" id="mobile<?php echo $k; ?>" value="<?php echo $record->mobile; ?>"></td>

                    </tr>

                    <?php

						++$i;

						++$j;

						++$k;

                        }

                    }

                    ?>

                  </table>

                  

                </div><!-- /.box-body -->

                <div class="box-footer clearfix">

                    <?php echo $this->pagination->create_links(); ?>

                </div>

              </div><!-- /.box -->

            </div>

        </div>

    </section>

</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">All Email Address</h4>
      </div>
      <div class="modal-body">
      	<div style="height: 200px; overflow-y: scroll;">
        <?php foreach($notify_email as $result): 
    		   echo $result['email'];
    		   echo "<br/>";
		 endforeach;?>
		 </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.min.css" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.min.js" charset="utf-8"></script>

<script type="text/javascript">

    jQuery(document).ready(function(){

        jQuery('ul.pagination li a').click(function (e) {

            e.preventDefault();            

            var link = jQuery(this).get(0).href;            

            var value = link.substring(link.lastIndexOf('/') + 1);

            jQuery("#searchList").attr("action", baseURL + "notificationListing/" + value);

            jQuery("#searchList").submit();

        });

		

		

    });

	function sendmail(val)

	{

		var messagebox = document.getElementById("notification_message"+val).value;

		if (!messagebox.match(/\S/)) 

		{

			alert("Message field cannot be empty");

			document.getElementById("notification_message"+val).focus();

			return false;

		} 

		else 

		{

			var email = document.getElementById("email"+val).value;

			var userid = document.getElementById("emailuserid"+val).value;

			 $.ajax({

				type: "POST",

				url: baseURL+"notifyemail",

				data: {message:messagebox,email:email,userId:userid},

					success: function(response){

					   if(response == "success")

					   {

						  alert("Email has been sent successfully");

					   }

					   else if(response == "fail")

					   {

						   alert("Email has been not sent.Please try again");

					   }

					   else

					   {

						  alert("Email has been sent successfully");

					   }

					}

				})

		}

	}

	function sendsms(val)

	{

		var messagebox = document.getElementById("notification_message"+val).value;

		if (!messagebox.match(/\S/)) 

		{

			alert("Message field cannot be empty");

			document.getElementById("notification_message"+val).focus();

			return false;

		} 

		else 

		{

			var mobile = document.getElementById("mobile"+val).value;

			mobile = mobile.replace( /-/g, "" );

			mobile = mobile.replace(/\s/g,''); 

			 $.ajax({

				type: "POST",

				url: baseURL+"notifysms",

				data: {message:messagebox,mobile:mobile},

					success: function(response){

					   if(response == "success")

					   {

						  alert("Message sent successfully");

					   }

					   else if(response == "fail")

					   {

						   alert("Message cannot be delivered");

					   }

					   else

					   {

						  alert("Something went wrong.Please try again");

					   }

					}

				})

		}

	}

	function sendbulkmail(){

		$.ajax({

			type: 'POST',

			url: baseURL+"sendBulkEmail",

			dataType: "html",

			data:{val:"getemail"},

			success: function (html) {
				
			    $.fancybox.open(html);
			    	     

			}

		});

	}

</script>
