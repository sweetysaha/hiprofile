<?php 
$id = "";
$googelid = "";
$hours = "";
$price = "";
$catgory = "";

if(!empty($pointInfo))
{
    foreach ($pointInfo as $cf)
    {
		$id = $cf->ranks_points_packageId;
        $googelid = $cf->ranks_points_package_Google_playId;
		$hours = $cf->ranks_points_package_hours;
		$price = $cf->ranks_points_package_price;
		$times = preg_replace('/[0-9]+/', '', $cf->ranks_points_package_hours);
		$catgory = $cf->ranks_points_package_category_code;
    }
}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer"></i> Edit Double Point Plan Details
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Plan Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="updatepointplan" action="<?php echo base_url() ?>updatedoublepointplan" method="post" role="form"  name="pointform">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="googleid">Google Play Id</label>
                                        <input type="text" class="form-control required" id="googleid" name="googleid" maxlength="128" value="<?php echo htmlspecialchars($googelid); ?>" placeholder="Google Play Id">
										<input type="hidden" value="<?php echo htmlspecialchars($id); ?>" name="id" id="id" /> 
                                    </div>
                                    
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="boosttime">Boost time</label>
                                         <input type="text" class="form-control" id="boosttime" placeholder="Boost time" name="boosttime" value="<?php echo htmlspecialchars(substr($hours, 0, -1)); ?>" maxlength="128" required="true">
                                    </div>
                                </div>
								<div class="col-md-6">                                
                                   <div class="form-group">
                                        <label for="times">Hours / Minutes</label>
                                        <select name="times" id="times" class="form-control required">
											<option value="">Select Hours / Minutes</option>
											<option value="h" <?php if($times == 'h'){ echo 'selected="selected"'; }?>>Hour</option>
											<option value="m" <?php if($times == 'm'){ echo 'selected="selected"'; }?>>Minute</option>
										</select>
                                    </div>
                                </div>
                            </div>	
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="price">Price</label>
                                        <input type="text" class="form-control" id="price" placeholder="price" name="price" value="<?php echo htmlspecialchars($price); ?>" maxlength="128" required="true">
                                    </div>
                                </div>
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="Category">Category</label>
                                        <input type="text" class="form-control" id="category" placeholder="category" name="category" value="<?php echo htmlspecialchars($catgory); ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                            </div>										
                         </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
							<input type="button" class="btn btn-default" value="Go Back" onclick="goBack()" style=" background-color: #dd4b39; color: white; "/>
                            <input type="submit" id="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
							<script>
							function goBack() {
								window.history.go(-1);
							}
							</script>
                        </div>
                    </form>
                </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/editpointplan.js" type="text/javascript"></script>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js" type="text/javascript"></script>

<!-- Javascript -->
