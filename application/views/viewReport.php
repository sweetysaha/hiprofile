<style>
.table_width{width:99%;}
.table_width .box{border-top:0;}
.table_inner_div{padding: 15px;}
.table_inner_div .table>thead>tr>th{background: #3c8dbc;border-left: 1px solid #78a1b9;font-weight: 500;  border-right: 1px solid #78a1b9;color: #fff;  text-align: left;    vertical-align: middle;}
.table_inner_div .table-bordered>tbody>tr>td{border: 1px solid #e4e4e4;}
.table_inner_div .table-striped>tbody>tr:nth-of-type(odd) {  background-color: #f2f5f9;}
</style>
<div class="viewContent table_width">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php if($selectSection == 1): ?>
			<title>User Analytical Report</title>
		<?php elseif($selectSection == 2): ?>
			<title>Businessess Analytical Report</title>
		<?php elseif($selectSection == 3): ?>
			<title>Boosted User Analytical Report</title>
		<?php elseif($selectSection == 4): ?>
			<title>User Upgraded Analytical Report</title>
		<?php elseif($selectSection == 5): ?>
			<title>Friends Analytical Report</title>
		<?php endif; ?>

		</head>
	<body>
<div class="row_feuild">
<div class="table_inner_div">
<div class="box">
	<?php if($selectSection == 1): ?>
		<table border="1" cellspacing="0" cellpadding="5" id="book-table" class="table table-striped table-bordered" >
		<?php if(!empty($results)): ?>
			<thead>

		  <tr>
			<th>Email Address</th>
			<th>User Name</th>
			<th>Full Name</th>
			<th>Date Of Birth</th>
			<th>Gender</th>
			<th>Mobile Number</th>
			<th>Latitude</th>	
			<th>Longitude</th>
			<th>App Rating</th>
			<th>App Feedback</th>
		  </tr>
			</thead>

				<?php foreach($results as $result):?>
				  <tr>
					<td><?php echo $result['email']; ?></td>
					<td><?php echo $result['username']; ?></td>
					<td><?php echo $result['name']; ?></td>
					<td><?php echo $result['dob']; ?></td>
					<td><?php echo $result['gender']; ?></td>
					<td><?php echo $result['mobile']; ?></td>
					<td><?php echo $result['latitude']; ?></td>
					<td><?php echo $result['longitude']; ?></td>
					<td><?php echo $result['rating'] ; ?></td>
					<td><?php echo $result['feedback']; ?></td>
				  </tr>
				<?php endforeach; ?>
			<?php else: ?>
			   <tr>
				<th>NO RECORD FOUND...</th>
			</tr>
			<?php endif; ?>
		</table>
		<?php  endif;?>
		<?php if($selectSection == 2): ?>
		<table border="1" cellspacing="0" cellpadding="5" id="book-table" class="table table-striped table-bordered" >
		<?php if(!empty($results)): ?>
		<thead>
		  <tr>
			<th>Business Title</th>
			<th>Address</th>
			<th>Contact</th>
			<th>Business Email</th>
			<th>Opening Days</th>
			<th>Opening Hours</th>
			<th>Category</th>
			<th>Image</th>
			<th>latitude</th>
			<th>longitude</th>
			<th>Uploaded By</th>
			<th>Approval Status</th>
		  </tr>
		 </thead>
				<?php foreach($results as $result):?>
				  <tr>
					<td><?php echo $result['business_title']; ?></td>
					<td><?php echo $result['address'].', '.$result['city'].', '.$result['state'].', PIN-'.$result['city'].', '.$result['country']; ?></td>
					<td><?php echo $result['contact']; ?></td>
					<td><?php echo $result['email']; ?></td>
					<td><?php echo $result['day_open_from'].' - '.$result['day_open_to']; ?></td>
					<td><?php echo $result['hours_open_from'].' - '.$result['hours_open_to']; ?></td>
					<td><?php echo $result['primary_category']; ?></td>
					<td><img src="<?php echo base_url().'assets'.$result['image_url']; ?>" alt="<?php echo $result['business_title']; ?>" style=" width: 100px; height: 100px; "></td>
					<td><?php echo $result['latitude']; ?></td>
					<td><?php echo $result['longitude']; ?></td>
					<td><?php echo $result['name']; ?></td>
					<?php
					if($result['approved_status'] == 1){
					echo '<td style="color:#08d208;">Approved</td>';
					}elseif($result->is_paid == 0){
					echo '<td style="color:#dd4b39;">Payment Pending</td>';
					}elseif($result->approved_status == 0){
					echo '<td style="color:#dd4b39;">Pending</td>';
					}
					?>
				  </tr>
				<?php endforeach; ?>
			<?php else: ?>
			   <tr>
				<th>NO RECORD FOUND...</th>
			</tr>
			<?php endif; ?>
		</table>
		<?php  endif;?>
		<?php if($selectSection == 3): ?>
		<table border="1" cellspacing="0" cellpadding="5" id="book-table" class="table table-striped table-bordered" >
		<?php if(!empty($results)): ?>
			<thead>

		  <tr>	
			<th>UserID</th>
			<th>Full Name</th>
			<th>Category</th>
			<th>ProductID</th>
			<th>Product Title</th>
			<th>product Description</th>
			<th>Status</th>
			<th>Purchased Time</th>
			<th>Activated Time</th>
			<th>Expired Time</th>
		  </tr>
			</thead>

				<?php foreach($results as $result):?>
				  <tr>
					<td><?php echo $result['userId']; ?></td>
					<td><?php echo $result['name']; ?></td>
					<td><?php echo $result['category']; ?></td>
					<td><?php echo $result['product_id']; ?></td>
					<td><?php echo $result['product_title']; ?></td>
					<td><?php echo $result['product_description']; ?></td>
					<?php $status ='';
						if($result['is_activated'] == 0){ 
						$status = "<span style='color: #2315ff;'>Purchased</span>";} 
						elseif($result['is_activated'] == 1){ $status = "<span style='color: #27a000;'>Activated</span>";} 
						elseif($result['is_activated'] == 2){ $status =  "<span style='color: #ff0b0b;'>Expired</span>";}
					?>
					<td><?php echo $status; ?></td>
					<td><?php echo $result['purchasedtime']; ?></td>
					<td><?php echo $result['activatedtime'] ; ?></td>
					<td><?php echo $result['expiredtime']; ?></td>
				  </tr>
				<?php endforeach; ?>
			<?php else: ?>
			   <tr>
				<th>NO RECORD FOUND...</th>
			</tr>
			<?php endif; ?>
		</table>
		<?php  endif;?>
		<?php if($selectSection == 4): ?>
		<table border="1" cellspacing="0" cellpadding="5" id="book-table" class="table table-striped table-bordered" >
			<thead>

		<?php if(!empty($results)): ?>
		  <tr>	
			<th>UserID</th>
			<th>Full Name</th>
			<th>Category</th>
			<th>ProductID</th>
			<th>Product Title</th>
			<th>product Description</th>
			<th>Status</th>
			<th>Purchased Time</th>
			<th>Activated Time</th>
			<th>Expired Time</th>
		  </tr>
			</thead>

				<?php foreach($results as $result):?>
				  <tr>
					<td><?php echo $result['userId']; ?></td>
					<td><?php echo $result['name']; ?></td>
					<td><?php echo $result['category']; ?></td>
					<td><?php echo $result['product_id']; ?></td>
					<td><?php echo $result['product_title']; ?></td>
					<td><?php echo $result['product_description']; ?></td>
					<?php $status ='';
						if($result['is_activated'] == 0){ 
						$status = "<span style='color: #2315ff;'>Purchased</span>";} 
						elseif($result['is_activated'] == 1){ $status = "<span style='color: #27a000;'>Activated</span>";} 
						elseif($result['is_activated'] == 2){ $status =  "<span style='color: #ff0b0b;'>Expired</span>";}
					?>
					<td><?php echo $status; ?></td>
					<td><?php echo $result['purchasedtime']; ?></td>
					<td><?php echo $result['activatedtime'] ; ?></td>
					<td><?php echo $result['expiredtime']; ?></td>
				  </tr>
				<?php endforeach; ?>
			<?php else: ?>
			   <tr>
				<th>NO RECORD FOUND...</th>
			</tr>
			<?php endif; ?>
		</table>
		<?php  endif;?>
		<?php if($selectSection == 5): ?>
		<div class="row">

		<div class="col-md-6" style="padding-left:  0;padding-right: 5px;">
		<table border="1" cellspacing="0" cellpadding="5" id="book-table" class="table table-striped table-bordered"  style="display: inline-block;">
			<thead>

		<?php if(!empty($results)): ?>
		  <tr>
			<th>User ID</th>
			<th>User Name</th>
			<th>User Email</th>
			<th>User DOB</th>
			<th>User Gender</th>
			<th>User Ph.Number</th>
		  </tr>
			</thead>

				<?php foreach($results['users'] as $key=>$result):?>
				  <tr>
					<td><?php echo $result['userId']; ?></td>
					<td><?php echo $result['username']; ?></td>
					<td><?php echo $result['useremail']; ?></td>
					<td><?php echo $result['userdob']; ?></td>
					<td><?php echo $result['usergender']; ?></td>
					<td><?php echo $result['usermobile']; ?></td>
					</tr>
				<?php endforeach; ?>
		</table>
		</div>
		<div class="col-md-6" style="padding-left: 5px; padding-right: 0;">
		<table border="1" cellspacing="0" cellpadding="5" id="book-table" class="table table-striped table-bordered"  style="float: left">
			<thead>	
		  <tr>
			<th>Friends ID</th>
			<th>Friends Name</th>
			<th>Friends Email</th>
			<th>Friends DOB</th>
			<th>Friends Gender</th>
			<th>Friends Ph.Number</th>
		  </tr>
			</thead>
				<?php foreach($results['friends'] as $key=>$result):?>
					<tr>
					<td><?php echo $result['friend_user_id']; ?></td>
					<td><?php echo $result['frienduseremail']; ?></td>
					<td><?php echo $result['friendusername']; ?></td>
					<td><?php echo $result['frienduserdob']; ?></td>
					<td><?php echo $result['friendusergender']; ?></td>
					<td><?php echo $result['friendusermobile']; ?></td>
				  </tr>
				<?php endforeach;?>
		</table>
		</div>	
		</div>
			<?php else: ?>
			   <tr>
				<th>NO RECORD FOUND...</th>
			</tr>
		<?php  endif;?>
		<?php  endif;?>

</div>
</div>
</div>
	</body>
	</html>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/common.min.css">	
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
		//jQuery('#book-table').DataTable();
		/*jQuery('#book-table').DataTable({
        "ajax": {
            url : "/get_items",
            type : 'GET'
        },*/
       
    });
</script>

