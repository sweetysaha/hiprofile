<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-list-alt"></i> Category Management
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>addNewCategoryForm"><i class="fa fa-plus"></i> Add New Category</a>
                </div>
            </div>
        </div>
		<div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Category List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>categoryListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Sl.No</th>
                      <th>Category Name</th>
					  <th>Category Icon Name</th>
					  <th>Category Status</th>
                      <th class="text-center">Actions</th>
                    </tr>
                    <?php if(!empty($categoryRecords)) : 
						if($this->uri->segment(2))
						{
							$i = $this->uri->segment(2)+1;
						}
						else
						{
							$i = 1;
						}
						 
					
					?>
					
                        <?php foreach($categoryRecords as $record): ?>
                    <tr>
                      <td><?php echo $i++; ?></td>

					   <td><?php echo $record->business_category_names ?></td>
                       <td><?php  echo $record->business_category_icon ?></td>
					   <?php
						$status = "";
						if($record->cat_status == 0){
							echo $status = '<td style="color:#08d208;">Active</td>';
						}else{
							echo $status ='<td style="color:#dd4b39;">Deactive</td>';
						}
						?>
                      <td class="text-center">
                          <a class="btn btn-sm btn-info" title="EDIT" href="<?php echo base_url().'editOldCategory/'.$record->categoryId; ?>"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-sm btn-danger deleteCategory" title="DELETE" href="#" data-categoryid="<?php echo $record->categoryId; ?>"><i class="fa fa-trash"></i></a>
					
                      </td>
                    </tr>
						<?php endforeach; ?>
                    <?php endif; ?>
                  </table>
            
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "categoryListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
