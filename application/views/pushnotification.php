<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class=""></i> Push Notifications
      </h1>
	  
    </section>
    <section class="content">
		<div class="col-md-4">
			<?php
				$this->load->helper('form');
				$error = $this->session->flashdata('error');
				if($error)
				{
			?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('error'); ?>                    
			</div>
			<?php } ?>
			<?php  
				$success = $this->session->flashdata('success');
				if($success)
				{
			?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php } ?>
			
			<div class="row">
				<div class="col-md-12">
					<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Enter Push Notifications details</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                 <form role="form" id="addContent" action="<?php echo base_url() ?>sendfcmnotification" method="post" role="form" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="devicetoken">Device Token</label>
                                        <input type="text" class="form-control required" id="devicetoken" name="devicetoken" required>
                                    </div> 
                                </div>
								  <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="devicetype">Device Type</label>
                                        <input type="text" class="form-control required" id="devicetype" name="devicetype">
                                    </div> 
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="fcmjson">FCM Json Data</label>
                                        <textarea rows="7" class="form-control required" id="fcmjson"  name="fcmjson" required></textarea>
                                    </div>
                                </div>
                            </div>                             
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
							<input type="button" class="btn btn-default" value="Go Back" onclick="goBack()" style=" background-color: #dd4b39; color: white; "/>
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
							<script>
							function goBack() {
								window.history.go(-1);
							}
							</script>
                        </div>
                    </form>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
