
$(document).ready(function(){
	
	var addBusinessForm = $("#addBusiness");
	
	var validator = addBusinessForm.validate({
		
		rules:{
			businessTitle 		:{ required : true },
			businessAddress		:{ required : true },
			businessCountry		:{ required : true },
			businesState	 	:{ required : true },
			businessCity	 	:{ required : true },
			businessPin		 	:{ required : true },
			businesscontact		:{ required : true },
			businessdof			:{ required : true },
			businessdot			:{ required : true },
			businesshof			:{ required : true },
			businesshot			:{ required : true },
			businesslatitude 	:{ required : true },
			businesslongitude	:{ required : true }
			//businessEmail 		:{ required : true, email : true, remote : { url : baseURL + "checkBusinessEmailExists", type :"post", data : { businessId : function(){ return $("#businessId").val(); } } } }
			},
		messages:{
			businessTitle		 :{ required : "This field is required" },
			businessAddress 	 :{ required : "This field is required" },
			businessCountry 	 :{ required : "This field is required" },
			businesState 		 :{ required : "This field is required" },
			businessCity 		 :{ required : "This field is required" },
			businessPin 		 :{ required : "This field is required" },
			businesscontact		 :{ required : "This field is required" },
			businessdof			 :{ required : "This field is required" },
			businessdot 		 :{ required : "This field is required" },
			businesshof			 :{ required : "This field is required" },
			businesshot			 :{ required : "This field is required" },
			businesslatitude 	 :{ required : "This field is required" },
			businesslongitude	 :{ required : "This field is required" }	
			//businessEmail				 :{ required : "This field is required", email : "Please enter valid email address", remote : "Email already taken" }		
		}
	});
});