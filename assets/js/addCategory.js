$(document).ready(function(){
	
	var addUserForm = $("#addCategory");
	
	var validator = addUserForm.validate({
		
		rules:{
			categoryName :{ required : true },
			categoryIcon : { required : true }
		},
		messages:{
			categoryName :{ required : "This field is required" },
			categoryIcon : { required : "This field is required" }			
		}
	});
});
