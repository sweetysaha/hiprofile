

jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteUser", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "deleteUser",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	jQuery(document).on("click", ".deleteContent", function(){
		var contentId = $(this).data("contentid"),
			hitURLs = baseURL + "deleteContent",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this content ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURLs,
			data : { contentId : contentId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Content successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("Content deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	jQuery(document).on("click", ".deleteContentsub", function(){
		var contentId = $(this).data("contentid"),
			hitURLs = baseURL + "Contentsub/subDeleteContent",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this content ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURLs,
			data : { contentId : contentId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Content successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("Content deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	jQuery(document).on("click", ".deleteCategory", function(){
		var categoryid = $(this).data("categoryid"),
			hitURLs = baseURL + "deleteCategory",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this category ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURLs,
			data : { categoryId : categoryid } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Category successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("Category deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	// jQuery(document).on("click", ".searchList", function(){
		
	// });
	jQuery(document).on("click", "#checkimagemain", function(){

	if (this.checked)
	{
		var contentId =  $('#contentId').val(),	
		hitURLs = baseURL + "mainDeleteContentImage";
		var confirmation = confirm("Are you sure to delete this image from content?");
		if (confirmation == true) {
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURLs,
			data : { contentId : contentId } 
			}).done(function(data){
				 jQuery("#img-responsive").remove();
				 jQuery("#checkimagemain").remove();
				 jQuery('#c_img').val(null); 
				if(data.status = true) { alert("Image successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("Image deletion failed"); }
				else { alert("Access denied..!"); }
			});
		} else {
			$('#checkimage').prop('checked', false); // Unchecks it
		}
	}

	});
	
	jQuery(document).on("click", "#checkimage", function(){

	if (this.checked)
	{
		var contentId =  $('#contentId').val(),
		hitURLs = baseURL + "Contentsub/subDeleteContentImage";
		var confirmation = confirm("Are you sure to delete this image from content?");
		if (confirmation == true) {
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURLs,
			data : { contentId : contentId } 
			}).done(function(data){
				 jQuery("#img-responsive").remove();
				 jQuery("#checkimage").remove();
				 jQuery('#c_img').val(null); 
				if(data.status = true) { alert("Image successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("Image deletion failed"); }
				else { alert("Access denied..!"); }
			});
		} else {
			$('#checkimage').prop('checked', false); // Unchecks it
		}
	}

	});
	
	jQuery(document).on("click", ".deleteBusiness", function(){
		var businessId = $(this).data("businessid"),
			hitURLs = baseURL + "deleteBusiness",
			currentRow = $(this);
		var confirmation = confirm("Are you sure to delete this business ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURLs,
			data : { businessId : businessId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Business successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("Business deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	jQuery(document).on("click", ".deleterankplan", function(){
		var id = $(this).data("id"),
			hitURLs = baseURL + "deleteFameRankPlan",
			currentRow = $(this);
		var confirmation = confirm("Are you sure to delete this Plan ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURLs,
			data : { id : id } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Rank Plan successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("Rank Plan deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	jQuery(document).on("click", ".deletedoublepointplan", function(){
		var id = $(this).data("id"),
			hitURLs = baseURL + "deletedoublepointsPlan",
			currentRow = $(this);
		var confirmation = confirm("Are you sure to delete this Plan ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURLs,
			data : { id : id } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Double Point Plan successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("Double Point Plan deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
		jQuery(document).on("click", "#checkimagebusiness", function(){

	if (this.checked)
	{
		var businessId =  $('#businessId').val(),	
		hitURLs = baseURL + "mainDeleteBusinessImage";
		var confirmation = confirm("Are you sure to delete this image from business?");
		if (confirmation == true) {
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURLs,
			data : { businessId : businessId } 
			}).done(function(data){
				 jQuery("#img-responsive").remove();
				 jQuery("#checkimagebusiness").remove();
				 jQuery('#c_img').val(null); 
				if(data.status = true) { alert("Image successfully deleted"); window.location.reload(); }
				else if(data.status = false) { alert("Image deletion failed"); }
				else { alert("Access denied..!"); }
			});
		} else {
			$('#checkimagebusiness').prop('checked', false); // Unchecks it
		}
	}

	});
});
