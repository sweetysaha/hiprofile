$(document).ready(function(){
	
	var addUserForm = $("#addContent");
	
	var validator = addUserForm.validate({
		
		rules:{
			contentTitle :{ required : true },
			contentDesc : { required : true }
		},
		messages:{
			contentTitle :{ required : "This field is required" },
			contentDesc : { required : "This field is required" }			
		}
	});
});
