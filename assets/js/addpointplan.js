

$(document).ready(function(){
	
	var savepointplan = $("#savepointplan");
	
	var validator = savepointplan.validate({
		
		rules:{
			googleid :{ required : true },
			times : { required : true, selected : true},
			boosttime : {required : true,digits : true },
			price : {required : true},
			category : { required : true}
		},
		messages:{
			googleid :{ required : "This field is required" },
			category : { required : "This field is required" },
			price : {required : "This field is required"},
			times : { required : "This field is required", selected : "Please select atleast one option" },	
			boosttime : {required : "This field is required"}			
		}
	});
});
